<?php $__env->startSection('page'); ?>


    <style>
        .table th, .table td {
            font-weight: 500;
        }
    </style>
    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5>المستخدمين</h5>
                       
                        <div class="pull-right">
                         
                                <a href="<?php echo e(route('users.create')); ?>"
                                   class="btn btn-primary"><?php echo e(__('messages.action_add')); ?></a>
                  
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        
                        
                        
                        <table id="table-ajax" class="table panel-body"
             data-url="/core/users"
             data-fields='[
                 {"data": "user_name","title":"<?php echo e(__('messages.user_name')); ?>","searchable":"true"},
                 {"data": "user_mail","title":"<?php echo e(__('messages.email')); ?>","searchable":"true"},
                 {"data": "user_idno","title":"<?php echo e(__('messages.user_idno')); ?>","searchable":"true"},
                 {"data": "user_mobile","title":"رقم الجوال","searchable":"true"},
                 {"data": "department.name","title":"الجهة","searchable":"false"},
                 {"data": "action","name":"actions","searchable":"false", "orderable":"false"}
             ]'>
           </table>
                  
                        <a href="<?php echo e(url()->previous()); ?>" class="btn btn-danger text-white rounded-0 backbtn"> رجوع</a>
                        <br/>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>