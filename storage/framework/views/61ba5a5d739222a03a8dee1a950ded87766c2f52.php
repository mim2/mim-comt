<?php $__env->startSection('page'); ?>

            



    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <a class="temp p-2 d-table" href="<?php echo e(route('sessions.index', $session->committee)); ?>" target="" title="">
                                <i class="fa fa-gear"></i>
                                <span>إدارة الإجتماعات</span>
                            </a>
                       

                            <h5>
                                <img alt="اللجان" class=" ml-1" src="/assets/images/attendesfolder.png"> بناء المحضر 
                            </h5>
                            <h5>
                                <img alt="" class="ml-1"
                                     src="/assets/images/<?php echo e($session->committee->committee_or_council == 1 ? 'folder':'business-meeting'); ?>.svg">
                                     <?php echo e($session->committee->committee_or_council == 1 ? 'لجنة' : 'مجلس'); ?>

                                     <?php echo e($session->committee->name); ?>


                                     - الإجتماع  
                                        
                                     <?php echo e($session->sessionTitle->name); ?>

                            </h5>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <?php echo e(Form::open(['method' => 'POST', 'route' => ['recommendations.store', $session]])); ?>



                        <div class="row setup-content" >
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">المناقشة</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" name="recommendation" rows="6" > <?php echo e(Input::old('recommendation')); ?></textarea>
                                </div>

                                
                                 <add-task
                                 v-bind:members = "<?php echo e($member); ?>"
                                 v-bind:time = "<?php echo e($time); ?>"
                                 selected-items="<?php echo e(old('selected')); ?>"
                                 ></add-task>


                             
                                      
                                    </div>
                                    
                            </div>


                        <div class="row mt-3 justify-content-center">
                              

                            <a  href="<?php echo e(route('sessions.attendees.index', [$session->committee, $session])); ?>"
                                    class="btn btn-danger text-white nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button">
                                        <i class="fa fa-arrow-right ml-2"></i> رجوع
                            </a>

                           

                            <input type="submit" class="btn btn-primary border-0 rounded-0" value="حفظ" >
                                <i class="fa fa-save"></i> 
                            

                            

                        </div>
                    </div>
                </div>


                <?php echo e(Form::close()); ?>


            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.main.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>