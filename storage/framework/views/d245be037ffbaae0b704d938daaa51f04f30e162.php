    
<?php $__env->startSection('page'); ?>

    


    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            
                                <a class="temp p-2 d-table" href="<?php echo e(route('sessions.index', $committee)); ?>" target="_self" title=" إدارة الإجتماع">
                                        <i class="fa fa-gear"></i>
                                        <span>إدارة الإجتماعات</span>
                                    </a>
                                
                                    <p class="sissionPage p-2 acolor">
                                        <img alt="اللجان" class=" ml-1" src="/assets/images/subject.png"> الأجندة
                                    </p>
                                   
                                    <h5>
                                        <img alt="اللجان" class=" ml-1" src="/assets/images/<?php echo e($session->committee->committee_or_council == 1 ? 'folder' : 'business-meeting'); ?>.svg">
                                        <?php echo e($session->committee->committee_or_council == 1 ? 'لجنة' : 'مجلس'); ?>

                                        <?php echo e($session->committee->name); ?> 
                                        
                                        - الإجتماع  
                                        
                                        <?php echo e($session->sessionTitle->name); ?>

                                    </h5>
                                 
                        </div><!-- add new sission -->

                       
                    </div>
                </div>
               

                <div class="row 33">
                    <div class="col SissionDetails">
                        <div class="row">
                            
                            <div class="col maindetails p-2 mt-3">
                               

                                
                                <?php echo e(Form::open(['route' => ['subjects.store', $session], 'method' => 'post'])); ?>

                                
                                <add-subject
                                title="<?php echo e(old('title')); ?>"
                                ></add-subject>
                                
                                <br />
                                <?php echo e(Form::close()); ?>

                                <br />
                                <?php $__currentLoopData = $sessionSubjects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subject): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    
                                    <div class="d-flex flex-row mb-3 justify-content-between text-center topDetails" >
                                        <div class="p-1 m-2 sissionplace" style="text-align: right !important" >
                                            <span class="spTitle" ></span> <?php echo e($subject->title); ?>

                                        </div>
                                      
                                         <?php echo e(Form::open(['route' => ['subjects.destroy', $subject], 'method' => 'delete', 'id' => $subject])); ?>

                                            <button type="submit" class="p-1 m-2 sissionremove "
                                                    onclick="return confirm('متأكد من الحذف؟')">
                                                <i class="fa fa-trash ml-2" style="cursor: pointer;"></i>
                                            </button>
                                        <?php echo e(Form::close()); ?>

                                        <a href="<?php echo e(route('subjects.edit', $subject)); ?>" class="p-1 m-2 sissionedit">
                                            <img alt="" class="ml-1 w-50" src="/assets/images/editBtn.png">
                                        </a>
                                        
                                    </div>

                                    
                                    

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                
                                
                                
                                
        
                                    <a style="background:green!important"  href="<?php echo e(route('sessions.files.index', [$session->committee, $session])); ?>"
                                        class="btn nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button">
                                        <i class="fa fa-arrow-left ml-2"></i> التالي
                                    </a>
                             
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        
    </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.main.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>