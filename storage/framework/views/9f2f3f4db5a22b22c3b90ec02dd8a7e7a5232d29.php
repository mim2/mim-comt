<style>

    .reqimg {
        background: #fd3b6e;
        padding: 5px;
        color: #fff;
    }

    .reqimg:hover {
        background: #0c6eb9;
        color: #fff !important;
        text-decoration: none;
    }

    a:hover {

    }

    .enl {
        font: 600 14px sans-serif;
    }

    .colorrin {
        color: #ff680c;
    }

</style>



<?php $__env->startSection('page'); ?>
 

  
    <div class="container mt-5">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            
                
                           
                        <p class="standing p-2"><span class="p-2 text-center details"><?php echo e($committee->type->name); ?></span></p>
                         
						
							<span class="p-2 text-center details">
								
                                <a href="<?php echo e(asset($committee->decision_url)); ?>" target="_blank" class="standing p-2">
                                    <i class="fa fa-image"></i> صورة القرار 
                                </a>
                            </span>
                            

                        </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <h4><i class="fa fa-folder ml-2"></i>الإسم</h4>
                        <p class="d-flex flex-column p-3 form-group"><?php echo e($committee->name); ?></p>
                        <h4><i class="fa fa-list ml-2"></i>التصنيف</h4>
                        <p class="d-flex flex-column p-3 form-group"><?php echo e($committee->category == 1 ? 'خارجي' : 'داخلي'); ?></p>
                        <?php if($committee->type_id == 3): ?>
                        <h4><i class="fa fa-calendar ml-2"></i>تاريخ البداية</h4>
                        <p class="d-flex flex-column p-3 form-group"><?php echo $committee->start_date->format('Y-m-d'); ?></p>
                        <h4><i class="fa fa-calendar ml-2"></i>تاريخ النهاية</h4>
                        <p class="d-flex flex-column p-3 form-group"><?php echo $committee->end_date->format('Y-m-d'); ?></p>
                        <?php endif; ?>
                        <h4><i class="fa fa-briefcase ml-2"></i>المهام</h4>
						<p class="d-flex flex-column p-3 form-group"><?php echo $committee->description; ?></p>
						<h4><i class="fa fa-briefcase ml-2"></i> رقم القرار </h4>
                        <p class="d-flex flex-column p-3 form-group"><?php echo $committee->decision_name; ?></p>
                        <h4><i class="fa fa-user ml-2"></i>الاعضاء</h4>

                        <?php $__currentLoopData = $committee->members; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $member): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="d-flex text-center">
                                <div class="col p-3 userspro">
                                    <i class="fa fa-user ml-2"></i>
                                    <?php echo e($member->member->user_name); ?>

                                </div>
                                <div class="col p-3 userspro">
                                    <?php echo e($member->memberType->name); ?>

								</div>
								
								
								
                                
                            </div>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <h4 class="mt-4 titleIn">الإجتماعات</h4>
                            
                        <?php $__currentLoopData = $committee->sessions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $session): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="d-flex text-center">
                                <div class="col p-3 userspro">
                                    <?php echo e($session->sessionTitle->name); ?>

                                </div>
                                <div class="col p-3 userspro">
                                    <?php echo e($session->status->name); ?>

                                </div>
                                <div class="col p-3 userspro">
                                    <a href="<?php echo e(route('member.sessions.show', [$session->committee, $session])); ?>">
                                        محضر الإجتماع
                                    </a>
                                </div>
                            </div>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						
						
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>



<?php $__env->stopSection(); ?>




<?php echo $__env->make('layouts.main.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>