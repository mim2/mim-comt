<div class="mt-2 mb-5">
    <div class="row">

        <div class="col">
            <div class="btn text-center p-0 mt-4 col"></div>
        </div>
        <div class="col">

            <ul class="mainMenu d-flex justify-content-center align-items-start">
                <?php if( auth()->user()->isMemberOfGroup('committes_creator') || Auth::user()->is_super_admin == 1  ): ?>
                    <li class="text-center p-3">
                        <a href="/committees/committees">
                            <img class="ml-1" src="/assets/images/folder.svg" alt="">
                            <p>اللجان والمجالس</p>
                        </a>
                    </li>
                    <li class="text-center p-3">
                        <a href="/committees/committees/create">
                            <img class="ml-1" src="/assets/images/business-meeting.svg" />
                            <p>اضافة لجنة / مجلس</p>
                        </a>
                    </li>
                <?php endif; ?>
                

                <li class="text-center p-3">
                    <a href="<?php echo e(route('authorize.index')); ?>">
                        <img class="ml-1" src="/assets/images/authorize.svg" />
                        <p>التفويضات</p>
                    </a>
                </li>
                <li class="text-center p-3">
                    <a href="<?php echo e(route('committees.profile.edit', auth()->user()->user_id)); ?>">
                        <i class="fa fa-lock fa-2x"></i>
                        <p>تحديث كلمة المرور</p>
                    </a>
                </li>

            </ul>

        </div>
    </div>
</div>
<br>


    