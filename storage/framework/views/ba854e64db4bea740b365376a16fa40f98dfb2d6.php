<?php $__env->startSection('page'); ?>

     


    <div class="container mt-5">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5>
                                <img alt="" class="ml-1" src="/assets/images/<?php echo e($committee->committee_or_council == 1 ? 'folder' : 'business-meeting'); ?>.svg">
								<?php echo e($committee->committee_or_council == 1 ? 'لجنة' : 'مجلس'); ?> <?php echo e($committee->name); ?>

                            </h5>
                            <span class="p-2 text-center details"></span>

                            
						
							<span class="p-2 text-center details">
								<a class="standing p-2" data-target="#newsision" data-toggle="modal" href="" target="_self" title="اضافة ملف">إضافة ملف</a>
								<a class="standing p-2"  href="<?php echo e(route('member.committees.print', $committee)); ?>"><img alt="اللجان" class=" ml-1" src="/assets/images/folder.svg">طباعة</a>
							</span>

                        </div>
                        <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="newsision" role="dialog" tabindex="-1">
								<div class="modal-dialog modal-lg" role="document">
										<?php echo e(Form::open(['route' => ['member.committees.store', $committee->id], 'class' => 'form-horizontal', 'method' => 'post', 
										'files' => true ])); ?>

									<div class="modal-content border-0 rounded-0">
										<div class="modal-header border-0 rounded-0 p-0">
											<h5 id="exampleModalLabel"><i class="fa fa-plus"></i>إضافة ملف</h5>
										</div>
										<div class="modal-body">
												<?php echo e(csrf_field()); ?>

												<div class="row">
													<div class="form-group col border-0 rounded-0">
														<div class="custom-file">
															<input class="custom-file-input" id="customFile" type="file" name="file_path"> <label class="custom-file-label" for="customFile">ارفع ملف</label>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="form-group col border-0 rounded-0">
														<input class="form-control" name="file_name" type="text"
																placeholder="إسم الملف">   
													</div>
												</div>
											</div>
										<div class="modal-footer border-0 rounded-0">
												<button type="submit" class="btn sendBtn border-0 rounded-0 ml-2 pl-4 pr-4">
													إضافة
												</button>
												<button class="btn nonBtn border-0 rounded-0 ml-2 pl-4 pr-4" data-dismiss="modal" type="button">
													إلغاء
												</button>
											</div>
									</div>
									<?php echo e(Form::close()); ?>

								</div>
						</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <h4><i class="fa fa-briefcase ml-2"></i>المهام</h4>
						<span class="d-flex flex-column p-3 form-group" style="text-align: right !important; background:#fff; font-size: 22px; !important" dir="ltr" ><?php echo $committee->description; ?></span>
						<h4><i class="fa fa-briefcase ml-2"></i> رقم القرار </h4>
                        <p class="d-flex flex-column p-3 form-group"><?php echo $committee->decision_name; ?></p>
                        <h4><i class="fa fa-user ml-2"></i>الاعضاء</h4>

                        <?php $__currentLoopData = $committee->members; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $member): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="d-flex text-center">
                                <div class="col p-3 userspro">
                                    <i class="fa fa-user ml-2"></i>
									<?php echo e($member->member->user_name); ?>

									<?php echo e(@$member->auth_name); ?>

                                </div>
                                <div class="col p-3 userspro">
                                    <?php echo e($member->memberType->name); ?> 
								</div>
								
						 </div>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <h4 class="mt-4 titleIn">الإجتماعات</h4>

                        <?php $__currentLoopData = $committee->sessions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $session): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="d-flex text-center">
                                <div class="col p-3 userspro">
                                    <?php echo e($session->sessionTitle->name); ?>

                                </div>
                                <div class="col p-3 userspro">
                                    <?php echo e($session->status->name); ?>

                                </div>
                                <div class="col p-3 userspro">
                                    <a href="<?php echo e(route('member.sessions.show', [$session->committee, $session])); ?>">
                                        محضر الإجتماع
                                    </a>
                                </div>
                            </div>

						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						
						<!-- popup buttons -->
						
						<div  class="d-flex justify-content-end mainbuttons mt-4 mb-4" style="    justify-content: center !important;">
							
							<button  class="btn btn-primary border-0 rounded-0 ml-3 comfilesbg" data-target=".comfiles" data-toggle="modal" type="button"><img alt="اللجان" class=" ml-1" src="/assets/images/file.svg"> ملفات 
								<?php echo e($committee->committee_or_council == 1 ? 'اللجنة' : 'المجلس'); ?>

							
							</button>
							<div aria-hidden="true" aria-labelledby="myLargeModalLabel" style="background: #5b4e56" class="modal fade comfiles" role="dialog" tabindex="-1">
								<div class="modal-dialog modal-lg border-0 rounded-0">
									<button aria-label="Close" class="close" style="background: #5b4e56; color: red; text-shadow: 0 0px 0 #fff;" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
									<div class="modal-content border-0 rounded-0 p-2">
										<h4 class="p-2 d-inline m-0" style="background: #5b4e56; color:#fff"><img alt="اللجان" class=" ml-1" src="/assets/images/file.svg"> ملفات <?php echo e($committee->committee_or_council == 1 ? 'اللجنة' : 'المجلس'); ?></h4><!-- files row -->
										<?php $__currentLoopData = $cfiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cfile): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<div class="d-flex text-center filess">
											<div class="col p-3 userspro title">
												<i class="fa fa-user ml-2"></i> <?php echo e($cfile->file_name); ?>

											</div><a class="col p-3 userspro down" href="/<?php echo e($cfile->file_path); ?>"><i class="fa fa-download ml-2"></i></a> <a class="col p-3 userspro trash">
													<?php echo e(Form::open(['route' => ['member.committee.cfiles.delete', $committee->id, $cfile->id], 'method' => 'delete', 'id' => $cfile->id])); ?>

													<button type="submit" class="p-1 m-2 sissionremove trashed"
															onclick="return confirm('متأكد من الحذف؟')">
														<i class="fa fa-trash ml-2"></i>
													</button>
													<?php echo e(Form::close()); ?>

												</a>
										</div>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										<!-- end files row -->
									</div>
								</div>
							
							</div>
							
						
							<div class="row"><button class="btn btn-primary border-0 rounded-0 comachbg" data-target=".comach" data-toggle="modal" type="button"><img alt="اللجان" class=" ml-1" src="/assets/images/folder.svg"> أرشيف 
								<?php echo e($committee->committee_or_council == 1 ? 'اللجنة' : 'المجلس'); ?>

							
							</button></div>
							<div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade comach" role="dialog" tabindex="-1">
								<div class="modal-dialog modal-lg border-0 rounded-0">
									<button aria-label="Close" style="background: #5b4e56; color: red; text-shadow: 0 0px 0 #fff;" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
									<div class="modal-content border-0 rounded-0 p-2">
										<h4 class="p-2 d-inline m-0" style="background: #5b4e56; color:#fff"><img alt="اللجان" class=" ml-1" src="/assets/images/file.svg"> إرشيف <?php echo e($committee->committee_or_council == 1 ? 'اللجنة' : 'المجلس'); ?></h4>
										<div class="achcontainer p-2 mt-3">
											<h6 class="p-2 d-inline mt-3 mb-2" style="background: #fff; color:black; font-weight: bold;"><img alt="اللجان" class=" ml-1" src="/assets/images/calendar.svg">2020</h6><!-- files row -->
											<div class="">
												
													<?php $i=0; ?>
													<?php $__currentLoopData = $sessions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $session): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														<ul class="list-group">
															<li class="list-group-item d-flex justify-content-between align-items-center">
																<?php echo e($session->sessionTitle['name']); ?>

																<span class="badge badge-primary badge-pill"><?php echo e(count($sessionFiles[$i])); ?></span>
															</li>
													
														</ul>
														<ul>
															<?php $__currentLoopData = $sessionFiles[$i]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sessionFile): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<li>
																	<a href="<?php echo e(asset( $sessionFile->file_path)); ?>"><?php echo e($sessionFile->file_name); ?></a>
																</li>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</ul>
														<?php $i++; ?>
												    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												

											</div><!-- files row -->
											<hr>
											
											<hr>
										</div><!-- end files row -->
									</div>
								</div>
							</div>
							
						</div><!-- end popup buttons -->
						
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>