<?php $__env->startSection('stylesheets'); ?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<style>
    .edia {
        color: #c73b1e !important;
    }
</style>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('page'); ?>

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5><img class="ml-1" src="/assets/images/folder.svg" alt="">اللجان / المجالس</h5>
                        </div>
                        
                    </div>
                    
                </div>
                <?php echo $__env->make('committees::committees.committees_datatables', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
    </div>
    
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <?php echo $__env->make('committees::committees.index-scripts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>