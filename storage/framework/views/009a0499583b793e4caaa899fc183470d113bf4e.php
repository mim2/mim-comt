<script src="/vendor-assets/lib/jquery/jquery.js"></script>

<script src="/vendor-assets/lib/bootstrap/js/bootstrap.js"></script>
<script src="/vendor-assets/lib/moment/moment.js"></script>
<script src="/vendor-assets/lib/jquery-ui/jquery-ui.js"></script>
<script src="/vendor-assets/lib/select2/js/select2.full.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>

<script src="<?php echo e(asset('select2/dist/js/select2.min.js')); ?>" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="/js/html5shiv.js"></script>
<script src="/js/respond.min.js"></script>
<![endif]-->

<script src="/vendor-assets/js/bootstrap-timepicker.min.js"></script>
<script src="/vendor-assets/js/main.js"></script>
<script src="/assets/js/bootstrap-datetimepicker.js"></script>
<script src="/assets/fullcalendar/main.js"></script>
<script src="/assets/fullcalendar/main.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
   
<script>

    var has_errors = <?php echo e($errors->count() > 0 ? 'true' : 'false'); ?>;

    if ( has_errors) {
        Swal.fire({
            title: 'ERRORS',
            type: 'error',
            html: jQuery("#ERROR_COPY").html(),
 
        })
    }

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>


<?php echo $__env->make('sweet::alert', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo e(Html::script(mix('assets/js/app.js'))); ?>

<?php echo e(Html::script('js/lang.js')); ?>



<script src="/vendor-assets/js/js/datatables.all.min.js"></script>
<script src="/vendor-assets/js/js/datatables.bootstrap.js"></script>
<script src="/vendor-assets/js/js/components-multi-select.min.js"></script>
<script src="/vendor-assets/js/js/components-datatable_ar.js"></script>







