
<a href="<?php echo e(route('group_permissions', ['id' => $id])); ?>" class="btn btn-xs btn-primary">
  <i class="fa fa-key"></i> <?php echo e(__('messages.permissions')); ?>

</a>

<a href="<?php echo e(route('groups.edit', ['id' => $id])); ?>" class="btn btn-xs btn-info">
  <i class="fa fa-pencil"></i> <?php echo e(__('messages.action_edit')); ?>

</a>


<a href="<?php echo e(route('groups.show', ['id' => $id])); ?>" class="btn btn-xs btn-success">
  <i class="fa fa-eye"></i><?php echo e(__('messages.action_show')); ?>

</a>


<?php echo Form::model($id, ['method' => 'delete', 'route' => ['groups.destroy', $id], 'class' =>'form-inline form-delete-modal', 'style' => 'display: inline;']); ?>

    <?php echo Form::hidden('id', $id); ?>

    <button type="submit" class="btn btn-xs btn-danger delete-object-modal-button" >
      <i class="fa fa-trash-o"></i>
      <?php echo e(__('messages.action_delete')); ?>

    </button>
<?php echo Form::close(); ?>

