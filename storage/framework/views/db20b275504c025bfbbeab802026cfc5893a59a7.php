<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='assets/fullCalendar/main.min.js'></script>
<script src='assets/fullCalendar/locales-all.js'></script>
<script src='assets/fullCalendar/locales-all.min.js'></script>
<script src='assets/fullCalendar/main.js'></script>
<script src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script src="https://raw.githubusercontent.com/xsoh/moment-hijri/master/moment-hijri.js"></script>
<script>
  
    document.addEventListener('DOMContentLoaded', function() {
        moment.locale('ar-SA');// 2- Set the global locale to `ar-SA`
      m = moment();
        var calendarEl = document.getElementById('calendar');
        events=<?php echo json_encode($events); ?>;
        var calendar = new FullCalendar.Calendar(calendarEl, {
          locale: 'ar',
          events: events,
          expandRows: true,
          eventRender: function(event, element) {
                element.qtip({
                    content: event.description
                });
            },
           
          
          contentHeight: 600,
          themeSystem: 'bootstrap',
          hiddenDays: [ 5, 6 ],
          initialView: 'dayGridWeek',
          eventDidMount: function(info) {
            if (info.event.extendedProps.status === 'done') {

            // Change background color of row
            info.el.style.backgroundColor = 'red';

            // Change color of dot marker
            var dotEl = info.el.getElementsByClassName('fc-event-dot')[0];
            if (dotEl) {
                dotEl.style.backgroundColor = 'red';
            }
            }
         },        
          
        //   list:     'list',

          listMonth: {
                month: 'long',
                day: '2-digit',
            },
          headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            
           
            right: 'dayGridWeek,dayGridMonth,listMonth', // buttons for switching between views
                locale: 'ar',

            },
            views: {
                timeGridFourDay: {
                type: 'timeGrid',
                
                duration: { days: 2 },
                buttonText: '2 day',
                },
                timeGrid: {
                dayMaxEventRows: 6 // adjust to 6 only for timeGridWeek/timeGridDay
                
                }
            },
            businessHours: {
                daysOfWeek: [ 4, 3, 2, 1, 0 ], // Monday - Thursday
                
                startTime: '08:00', 
                endTime: '16:00', 
            },
            titleFormat: { 
                month: 'long',
                year: 'numeric'
            },
            buttonText: {
                today:    'اليوم',
                month:    'شهر',
                week:     'اسبوع',
                day:      'يوم',
                list:     'اجندة',
            },
            
            
        });
        calendar.setOption('locale', 'ar');
        calendar.render();
      });
</script>