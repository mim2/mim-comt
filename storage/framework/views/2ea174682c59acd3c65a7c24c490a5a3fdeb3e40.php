

<?php if($committee->managingMemberIds()->contains(auth()->user()->user_id)): ?>

<div class="d-flex flex-row mt-5 mb-3 justify-content-center text-center bigIco">

    <?php if(!$session->isSigned()): ?>

    <?php if($session->status_id == 2 || $session->status_id == 1 || $session->status_id == 6 ): ?>

            <a class="m-2" href="<?php echo e(route('subjects.index', [$session->committee, $session])); ?>">
                <img class="ml-1 orangBtn d-block p-4 mb-3 <?php echo e($session->subjects->count() > 0 ? 'bg_orangiconicheck' : ''); ?>"
                    src="/assets/images/subject.png" alt="">
                <span> إضافة أجندة</span>
            </a>
            
            <div class="checkbox icheck-success">
                <input type="checkbox" id="success2" name="success" <?php echo e($session->subjects->count() > 0 ? 'checked' : ''); ?>>
                <label for="exampleRadios1"></label>
            </div>
            <?php else: ?>
            <a class="m-2" >
                    <img class="ml-1 grayBtn d-block p-4 mb-3 <?php echo e($session->subjects->count()  ? 'bg_grayiconicheck' : ''); ?>"
                        src="/assets/images/subject.png" alt="">
                    <span> إضافة أجندة</span>
                </a>
                
                <div class="checkbox icheck-success">
                    <input type="checkbox" id="success2" name="success" <?php echo e($session->subjects->count() > 0 ? 'checked' : ''); ?>>
                    <label for="exampleRadios1"></label>
                </div>
        
     <?php endif; ?>

            <?php if($session->status_id == 2 || $session->status_id == 1 || $session->status_id == 6 ): ?>
                <a class="m-2" href="<?php echo e(route('sessions.files.index',  [$session->committee, $session])); ?>">
                    <img class="ml-1 orangBtn d-block p-4 mb-3 <?php echo e($session->files->count() > 0 ? 'bg_orangiconicheck' : ''); ?>"
                        src="/assets/images/addFile.png" alt="">
                    <span>إضافة ملفات</span>
                </a>
                <div class="checkbox icheck-success">
                    <input type="checkbox" id="success2" name="success" <?php echo e($session->files->count() > 0 ? 'checked' : ''); ?>>
                    <label for="exampleRadios1"></label>
                </div>
            <?php else: ?>
                <a class="m-2">
                        <img class="ml-1 grayBtn d-block p-4 mb-3 <?php echo e($session->files->count() > 0 ? 'bg_grayiconicheck' : ''); ?>"
                             src="/assets/images/addFile.png" alt="">
                        <span>إضافة ملفات</span>
                    </a>
                    <div class="checkbox icheck-success">
                        <input type="checkbox" id="success2" name="success" <?php echo e($session->files->count() > 0 ? 'checked' : ''); ?>>
                        <label for="exampleRadios1"></label>
                    </div>
            <?php endif; ?>


    <?php else: ?>

        <a class="m-2">
            <img class="ml-1 grayBtn d-block p-4 mb-3 <?php echo e($session->subjects->count() > 0 ? 'bg_grayiconicheck' : ''); ?>"
                 src="/assets/images/subject.png" alt="">
            <span>إضافة أجندة</span>
        </a>
        <div class="checkbox icheck-success">
            <input type="checkbox" id="success2" name="success"  }}>
            <label for="exampleRadios1"></label>
        </div>

        

    <?php endif; ?>

    <?php if($session->status_id == 2 || $session->status_id == 1 || $session->status_id == 6): ?>

        <a class="m-2" href="<?php echo e(route('guests.index', [$session->committee, $session])); ?>">
            <img class="ml-1 orangBtn d-block p-4 mb-3 <?php echo e($session->guests->count() ? 'bg_orangiconicheck' : ''); ?>"
                 src="/assets/images/addusers.png" alt="">
            <span>إضافة مدعويين</span>
        </a>

    <?php else: ?>

        <a class="m-2">
            <img class="ml-1 grayBtn d-block p-4 mb-3 <?php echo e($session->guests->count() ? 'bg_grayiconicheck' : ''); ?>"
                 src="/assets/images/addusers.png" alt="">
            <span>إضافة مدعويين</span>
        </a>

    <?php endif; ?>
    <div class="checkbox icheck-success">
        <input type="checkbox" id="success2"
               name="success" <?php echo e($session->guests->count() ? 'checked' : ''); ?>>
        <label for="exampleRadios1"></label>
    </div>

    <?php $flag = $session->attendees->count() == $session->committee->members->count() + $session->guests->count() ?>

    <?php if( $session->status_id == 2 || $session->status_id == 1 || $session->status_id == 6): ?>
    
        <a class="m-2" href="<?php echo e(route('sessions.attendees.index', [$session->committee, $session])); ?>">
            <img alt="" class="ml-1 orangBtn d-block p-4 mb-3 <?php echo e($flag ? 'bg_orangiconicheck' : ''); ?>"
                 src="/assets/images/attendance.png">
            <span>التحضير</span>
        </a>

    <?php else: ?>

        <a class="m-2">
            <img alt="" class="ml-1 grayBtn d-block p-4 mb-3 <?php echo e($flag ? 'bg_grayiconicheck' : ''); ?>"
                 src="/assets/images/attendance.png">
            <span>التحضير</span>
        </a>

    <?php endif; ?>
    <div class="checkbox icheck-success">
        <input type="checkbox" id="success2"
               name="success" <?php echo e($session->attendees->count()  ? 'checked' : ''); ?>>
        <label for="exampleRadios1"></label>
    </div>

    <?php if($session->status_id == 6): ?>

        <?php if($session->recommendation != null): ?>
        <a class="m-2" href="<?php echo e(route('recommendations.change', [$session,$session->recommendation->id])); ?>">
            <img class="ml-1 orangBtn d-block p-4 mb-3 <?php echo e(strlen($session->recommendations) > 0 ? 'bg_orangiconicheck' : ''); ?>"
                 src="/assets/images/attendesfolder.png" alt="">
            <span>بناء المحضر</span>
        </a>
        <?php else: ?> 

        <a class="m-2" href="<?php echo e(route('recommendations.edit',  $session)); ?>">
            <img class="ml-1 orangBtn d-block p-4 mb-3 <?php echo e(strlen($session->recommendations) > 0 ? 'bg_orangiconicheck' : ''); ?>"
                 src="/assets/images/attendesfolder.png" alt="">
            <span>بناء المحضر</span>
        </a>
        <?php endif; ?>


    <?php else: ?>

        <a class="m-2">
            <img class="ml-1 grayBtn d-block p-4 mb-3 <?php echo e(strlen($session->recommendations) > 0 ? 'bg_grayiconicheck' : ''); ?>"
                 src="/assets/images/attendesfolder.png" alt="">
            <span>بناء المحضر</span>
        </a>

    <?php endif; ?>
    <div class="checkbox icheck-success">
        <input type="checkbox" id="success2"
               name="success" <?php echo e(strlen($session->recommendation) > 0 ? 'checked' : ''); ?>>
        <label for="exampleRadios1"></label>
    </div>

     
    <?php if( $session->recommendation != null   && $session->status_id == 6): ?> 

        <a class="m-2" href="<?php echo e(route('sessions.elevate', $session)); ?>">
                <img alt="" class="ml-1 redBtn d-block p-4 mb-3" src="/assets/images/sendf.png">
                <span>إرسال المحضر للتوقيع </span>
            </a>
        

            
          
    <?php else: ?>

        <a class="m-2">
            <img alt="" class="ml-1 badBtn d-block p-4 mb-3" src="/assets/images/sendf.png">
            <span>تم إرسال المحضر للتوقيع
            </span>

            
        </a>

        


    <?php endif; ?>

    

    <?php if($session->status_id == 10): ?> 

        <a class="m-2" target="_blanc" href="<?php echo e(route('member.sessions.print', [$session->committee, $session])); ?>">
                <img alt="" class="ml-1 orangBtn d-block p-4 mb-3" src="/assets/images/SessionSchedule.png">
                <span>طباعة المحضر</span>
            </a>

            
        

          
    <?php endif; ?>

    

    


</div>

<?php endif; ?>


  