<?php $__env->startSection('stylesheets'); ?>
    <link rel="stylesheet" type="text/css" href="/vendor-assets/css/ummalqura.calendars.picker.css">



<?php $__env->stopSection(); ?>

<div class="form-group" style="border-coler: gold">
    <?php echo e(Form::label('name', 'اسم اللجنة / المجلس', ['class' => 'control-label'])); ?>

    <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>
    <?php echo e(Form::text('name', old('name') , ['class' => 'form-control rounded-0'])); ?>

    <span class="text-danger"><?php echo e($errors->first('name')); ?></span>
</div>
<hr>
<div class="form-group">
    <?php echo e(Form::label('committee_or_council', 'النوع', ['class' => 'control-label rounded-0'])); ?>

    <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>

    <div class="radio">
        <label class="radio-inline col-md-2">
            <?php echo Form::radio('committee_or_council', '1', (Input::old('committee_or_council') == '1'), array('id'=>'committee_or_council', 'class'=>'form-check-input', 'checked' => 'checked')); ?>


            <span class="mr-4">لجنة</span></label>
        
            <label class="radio-inline col-md-2">
                <?php echo Form::radio('committee_or_council', '2', (Input::old('committee_or_council') == '2'), array('id'=>'committee_or_council', 'class'=>'form-check-input')); ?>

                <span class="mr-4">مجلس</span></label>
                
            <span class="text-danger"><?php echo e($errors->first('committee_or_council')); ?></span>
    </div>

    <?php echo e(Form::label('category', 'التصنيف', ['class' => 'control-label rounded-0'])); ?>

    <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>

    <div class="radio">

        <label class="radio-inline col-md-2">
            <?php echo Form::radio('category', '0', (Input::old('category') == '0'), array('id'=>'categories', 'class'=>'form-check-input', 'checked' => 'checked')); ?>

            <span class="mr-4">داخلي</span></label>

        <label class="radio-inline col-md-2">
            <?php echo Form::radio('category', '1', (Input::old('category') == '1'), array('id'=>'categories', 'class'=>'form-check-input')); ?>

            <span class="mr-4">خارجي</span></label>
        
        <span class="text-danger"><?php echo e($errors->first('category ')); ?></span>
    </div>
</div>


<div class="form-group">
    <?php echo e(Form::label('type_id', 'النوع', ['class' => 'control-label'])); ?>

    <?php echo e(Form::select('type_id', $types, old('type_id') , ['class' => 'form-control'])); ?>


    <span class="text-danger"><?php echo e($errors->first('type_id')); ?></span>

    
</div>


<hr>
<div class="form-group parent_id">
    <?php echo e(Form::label('parent_id', 'اختر اللجنة الرئيسية', ['class' => 'control-label'])); ?>

    <?php echo e(Form::select('parent_id', $parents, old('parent_id') , ['class' => 'form-control parent_id'])); ?>

    <span class="text-danger"><?php echo e($errors->first('parent_id')); ?></span>
</div>

<div class="form-group session_parent_id">
    <?php echo e(Form::label('parent_id', 'اختر المجلس الرئيسي', ['class' => 'control-label'])); ?>

    <?php echo e(Form::select('parent_id', $sessionsParents, old('parent_id') , ['class' => 'form-control session_parent_id'])); ?>

    <span class="text-danger"><?php echo e($errors->first('parent_id')); ?></span>
</div>


<div class="form-group commitee_parent_id">
    <?php echo e(Form::label('parent_id', 'اختر اللجنة الفرعية', ['class' => 'control-label'])); ?>

    <?php echo e(Form::select('parent_id', $parents, old('parent_id') , ['class' => 'form-control commitee_parent_id'])); ?>

    <span class="text-danger"><?php echo e($errors->first('parent_id')); ?></span>
</div>

<div class="form-group start_date">
    <?php echo e(Form::label('start_date', 'تاريخ البداية', ['class' => 'control-label'])); ?>

    <?php echo e(Form::text('start_date', old('start_date') , ['class' => 'form-control hijri-datepicker-input'])); ?>

    <span class="text-danger"><?php echo e($errors->first('start_date')); ?></span>
</div>

<div class="form-group end_date">
    <?php echo e(Form::label('end_date', 'تاريخ النهاية', ['class' => 'control-label'])); ?>

    <?php echo e(Form::text('end_date', old('end_date') , ['class' => 'form-control hijri-datepicker-input rounded-0'])); ?>

    <span class="text-danger"><?php echo e($errors->first('end_date')); ?></span>
</div>

<div class="form-group">
    <?php echo e(Form::label('description', 'المهام', ['class' => 'control-label'])); ?>

    <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>

    <?php echo e(Form::textarea('description', old('description') , ['class' => 'form-control rounded-0', 'id' => 'article-ckeditor'])); ?>

    <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
</div>
<hr>

<div class="form-group">
    <?php echo e(Form::label('decision_name', 'رقم القرار ', ['class' => 'control-label'])); ?>

    <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>

    <?php echo e(Form::text('decision_name', old('decision_name') , ['class' => 'form-control secondFont rounded-0', ])); ?>

    <span class="text-danger"><?php echo e($errors->first('decision_name')); ?></span>
</div>

<div class="form-group">
    <?php echo e(Form::label('decision_image', 'صورة القرار ', ['class' => 'control-label'])); ?>

    <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>

    <?php echo e(Form::file('decision_image', ['class' => 'form-control rounded-0'])); ?>

    <br>
    <?php if($committee->decision_url == !null): ?>
    <a href="<?php echo e(asset($committee->decision_url)); ?>" target="_blank" class="reqimg">
        <i class="fa fa-image"></i>صورة القرار 
    </a>
    <?php endif; ?>
    <span class="text-danger"><?php echo e($errors->first('decision_image')); ?></span>
</div>
<hr>


<hr>
<?php echo $__env->make('committees::committees.add_new_member', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="form-group">
    <?php echo e(Form::submit('حفظ', ['class' => 'btn btn-success rounded-0'])); ?>

    <a href="<?php echo e(route('committees.index')); ?>" class="btn btn-danger text-white rounded-0">إلغاء</a>
</div>

<?php $__env->startSection('scripts'); ?>
    <?php echo $__env->make('committees::committees.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>