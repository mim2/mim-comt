<h5 class="text-right headtitle head3 mr-3">خامساً: توقيع الأعضاء والمدعوين</h5>

<table class="table text-center">
    <thead class="thcusme2 th4">
    <tr>
        <th scope="col">#</th>
        <th scope="col">اسم العضو</th>
        <th scope="col">رأي العضو</th>
        <th scope="col">أسباب التحفظ</th>
        <?php $currentMember = $session->committee->members->where('user_id', Auth::user()->user_id)->first() ?>
        <?php if($currentMember && in_array($currentMember->member_type_id, [1,2])): ?>
        <th scope="col">رسالة تذكير</th>
        <?php endif; ?>
    </tr>
    </thead>
    <tbody>

    <?php  $guests = $session->guests->pluck('user_id')->toArray() ?>
    <?php $__currentLoopData = $session->attendees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attendee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($attendee->attendanceStatus->id == 1): ?>
            <?php if(!in_array(@$attendee->member->user_id, $guests)): ?>

                <?php @$opinion = $session->opinions->where('user_id', $attendee->member->user_id)->first() ?>
                <?php if($attendee->member != null): ?>
                    <tr>
                        <td><?php echo e($loop->iteration); ?></td>
                        <td><?php echo e(@$attendee->member->user_name); ?></td>
                        <td><?php echo e($opinion ? $opinion->opinion == 1 ? 'موافق' : 'غير موافق' : 'لم يوقع'); ?></td>
                        <td><?php echo e($opinion ? $opinion->reservation_reason : ''); ?></td>
                        <?php if($currentMember && in_array($currentMember->member_type_id, [1,2])): ?>
                        <td class="">
                            <?php if(!$opinion): ?>
                            <button class="rounded-0 border-0" onclick="notify(<?php echo e(@$attendee->member->user_id); ?>, <?php echo e($session->id); ?>)">
                                <i class="fa fa-envelope fa-2x concolor"></i>
                            </button>
                            <?php endif; ?>
                        </td>
                        <?php endif; ?>
                    </tr>

                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </tbody>
</table>
<hr>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="remindedmessage" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-0">
            <div class="modal-body rounded-0 text-center">
                <img alt="اللجان" class=" ml-1" src="/assets/images/paper-plane.svg"><br>
                <span id="member_name"></span>
            </div>
            <div class="modal-footer justify-content-center">
                <button class="btn btn-secondary nonBtn rounded-0 border-0" data-dismiss="modal" type="button">
                    إغلاق
                </button>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="waiting" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-0">
            <div class="modal-body rounded-0 text-center">
                <i class="fa fa-spinner"></i>
                <span>فضلاً انتظر</span>
            </div>
            <div class="modal-footer justify-content-center">
                <button class="btn btn-secondary nonBtn rounded-0 border-0" data-dismiss="modal" type="button">
                    إغلاق
                </button>
            </div>
        </div>
    </div>
</div>

<h5 class="text-right headtitle head3 mr-3">سادساً: رأي المدعويين في المحضر</h5>
<table class="table text-center">
    <thead class="thcusme2 th5">
    <tr>
        <th scope="col">#</th>
        <th scope="col">اسم المدعو</th>
        <th scope="col">رأيه في المحضر</th>
        <th scope="col">أسباب التحفظ</th>
    </tr>
    </thead>
    <tbody>

    <?php $__currentLoopData = $session->guests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <?php $opinion = $session->opinions->where('user_id', $guest->user_id)->first() ?>

            <tr>
                <td><?php echo e($loop->iteration); ?></td>
                <td><?php echo e(@$guest->name); ?></td>
                <td><?php echo e($opinion ? $opinion->opinion == 1 ? 'موافق' : 'غير موافق' : 'لم يوقع'); ?></td>
                <td><?php echo e($opinion ? $opinion->reservation_reason : ''); ?></td>
            </tr>

    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </tbody>
</table>
<hr>

