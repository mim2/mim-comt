<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.plugin.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.plus.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.picker.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.picker-ar.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.ummalqura.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.ummalqura-ar.js"></script>


<script type="text/javascript">

    var calHj = $.calendars.instance('ummalqura', 'ar');
    $('.hijri-datepicker-input').calendarsPicker($.extend({
            calendar: calHj,
            dateFormat: 'dd-mm-YYYY',
        },
        $.calendarsPicker.regionalOptions['ar'])
    );

</script>