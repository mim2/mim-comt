<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>تسجيل الدخول</title>
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link href="/assets/css/icheck-bootstrap.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <link href="/images/favicon.ico" rel="shortcut icon">
</head>
<body>

<?php echo $__env->yieldContent('page'); ?>

<script src="/assets/js/main.js"></script>

</body>
</html>
