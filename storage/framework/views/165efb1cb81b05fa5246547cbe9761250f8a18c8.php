<link href="/vendor-assets/css/datatables.min.css" rel="stylesheet">
<link href="/vendor-assets/css/datatables.bootstrap.css" rel="stylesheet">
<link href="/vendor-assets/css/datatables.bootstrap-rtl.css" rel="stylesheet">
<link href="/vendor-assets/css/ummalqura.calendars.picker.css" rel="stylesheet">
<link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">


<?php $__env->startSection('page'); ?>

            



    <div class="container card mt-5 p-2 border-0">
        <div class="br-mainpanel">
            <h2 class="inPageTitle">تعديل الإجتماع</h2>
            <?php echo e(Form::model($session, ['route' => ['sessions.update', $session->id], 'files' => true, 'class' => 'form-horizontal', 'method' => 'patch'])); ?>

            <?php echo $__env->make('committees::sessions.form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo e(Form::close()); ?>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>