<div class="modal-body">
    <div class="row">
        <div class="form-group col border-0 rounded-0">
            <label for="session_title_id">
                <img class=" ml-1" src="/assets/images/bFile.png" alt="">
            </label>
            <?php echo e(Form::label('session_title_id', 'الإجتماع', ['class' => 'control-label'])); ?>

            <?php echo e(Form::select('session_title_id', $titles, $sessionTitleId , ['class' => 'form-control'])); ?>

            <span class="text-danger"><?php echo e($errors->first('session_title_id')); ?></span>
        </div>
        <div class="form-group col border-0 rounded-0">
            <label for="start_time">
                <img class=" ml-1" src="/assets/images/bTime.png" alt="">
                <span>الوقت</span>
            </label>
            <div class="row">
    <div class="form-group col engno">من
        <select name="start_time" class="form-control" style="font-family: arial;">
            <?php $__currentLoopData = $hours; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hour): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option <?php echo e(old('start_time')); ?>><?php echo e(@$hour->hours); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <span class="text-danger"><?php echo e($errors->first('start_time')); ?></span>
    </div>

    <div class="form-group col engno">الى
        <select name="end_time" class="form-control" style="font-family: arial;">
            <?php $__currentLoopData = $hours; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hour): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option <?php echo e(old('end_time')); ?>><?php echo e(@$hour->hours); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <span class="text-danger"><?php echo e($errors->first('end_time')); ?></span>
        </div>
</div>

        </div>
    </div>

    <div class="row">
        <div class="form-group col border-0 rounded-0">
            <label for="place">
                <img class="ml-1" src="/assets/images/bLocation.png" alt="">
                <span>المكان</span>
            </label>
            <?php echo e(Form::text('place', old('place') , ['class' => 'form-control'])); ?>

            <span class="text-danger"><?php echo e($errors->first('place')); ?></span>
        </div>
        <div class="form-group col border-0 rounded-0">

            <label for="date"><img class="ml-1" src="/assets/images/bCalender.png" alt="">التاريخ</label>
            
            

            <?php echo e(Form::text('date', old('date') , ['class' => 'form-control en-datepicker-input', 'autocomplete' => 'off'])); ?>

            <span class="text-danger"><?php echo e($errors->first('date')); ?></span>

        </div>

    </div>
    <div class="row mt-2">


</div>
</div>
<div class="modal-footer border-0 rounded-0">
    <?php echo e(Form::submit('إرسال', ['class' => 'btn sendBtn border-0 rounded-0 ml-2 pl-4 pr-4'])); ?>

    <a href="<?php echo e(route('sessions.index', $committee->id)); ?>"
       class="btn nonBtn border-0 rounded-0 ml-2 pl-4 pr-4">إلغاء</a>
</div>
</div>


<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.js"></script>
    <script type="text/javascript" src="/vendor-assets/js/date/jquery.plugin.js"></script>
    <script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.plus.js"></script>
    <script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.picker.js"></script>
    <script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.picker-ar.js"></script>
    <script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.ummalqura.js"></script>
    <script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.ummalqura-ar.js"></script>


    <script type="text/javascript">
        function typeChanged() {
            if ($("#type_id").val() == 4) {
                $('.parent_id').show();
            } else {
                $('.parent_id').hide();
            }
            if ($("#type_id").val() == 3) {
                $('.start_date').show();
                $('.end_date').show();
            } else {
                $('.start_date').hide();
                $('.end_date').hide();
            }
        }

        $("#type_id").change(typeChanged);
        typeChanged();

        $(":input[name='committee_or_council']").change(function () {
            var committee_or_council = $(":input[name='committee_or_council']:checked").val();
            if (committee_or_council == 2) {
                $('#type_id').val(2).attr('disabled', true);
            } else {
                $('#type_id').attr('disabled', false);
            }
        });


        // Datepicker
        var calHj = $.calendars.instance('ummalqura', 'ar');
        $('.hijri-datepicker-input').calendarsPicker($.extend({
                calendar: calHj,
                dateFormat: 'YYYY/mm/dd',
            },
            $.calendarsPicker.regionalOptions['ar'])
        );

    </script>
<?php $__env->stopSection(); ?>
