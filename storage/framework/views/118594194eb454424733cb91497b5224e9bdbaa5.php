<?php if($session->files->count()): ?>
<h5 class="text-right headtitle head3 mr-3">سادساً: ملفات خاصة بالمحضر</h5>
<table class="table text-center">
    <thead class="thcusme2 th6">
    <tr>
        <th scope="col">#</th>
        <th scope="col">عنوان الملف</th>
        <th scope="col">تنزيل الملف</th>
    </tr>
    </thead>
    <tbody>
    
    <?php $__currentLoopData = $session->files; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>
        <td scope="row"><?php echo e($loop->iteration); ?></td>
        <td><?php echo e($file->file_name); ?></td>
        <td class="">
            <a href="/<?php echo e($file->file_path); ?>"><i class="fa fa-download fa-2x concolor"></i></a>
        </td>
    </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    
    
    </tbody>
</table>
<?php endif; ?>
<hr>

