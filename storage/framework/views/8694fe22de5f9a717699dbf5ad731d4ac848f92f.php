<div class="form-group">
    
</div>

    <div class="form-group">
        <div class="aui-buttons input-group-btn" role="group" style="margin-left:12px;">
                <label for="status" class="control-label"> اختيار الرئيس</label>
            <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>
        
        
                <select name="manager" id="committee_members_mang" class="form_control select2">             
                    <option value="0">اختر</option>
                    <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($name->user_id); ?>"><?php echo e($name->user_name); ?>  ,    <?php echo e($name->user_idno); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                </select>
                <select name=""  id="" hidden>
                    <option value="1" hidden>رئيس</option>
                </select>
                <button  type="button"  id="add_committee_manager" data-url="<?php echo e(route('employees.getEmployees')); ?>">إضافة</button>
        </div>
        
        <div class="form-group">
            <table class="table table-striped mt-10" >
                <thead>
                    <tr>
                        <th scope="col">اسم الرئيس</th>
                        <th scope="col">رقم الهوية</th>
                        <th scope="col">رقم الجوال</th>
                        <th scope="col">البريد الإلكتروني</th>
                        
                    </tr>
                </thead>
                <tbody id="committeeMembersManager">
                    <?php $__currentLoopData = $commiteeMembersForManager; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                            <?php if($item->member_type_id == 1): ?>
                            <td><?php echo e(@$item->member->user_name); ?></td>
                            <td><?php echo e(@$item->member->user_idno); ?></td>
                            <td><?php echo e(@$item->member->user_mobile); ?></td>
                            <td><?php echo e(@$item->member->user_mail); ?></td>
                            <?php endif; ?>
                        
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>

        <hr>
        <div class="form-group">
            
        </div> 

    <div class="form-group">
        <div class="aui-buttons input-group-btn" role="group" style="margin-left:12px;">
                <label for="status" class="control-label"> اختيار أمين</label>
                
                <select name="vice" id="add_committee_members_vice" class="form_control select2">
                               
                    <option value="0">اختر</option>
                    <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(Input::old('vice') == $name->user_id): ?>
                            <option value="<?php echo e($name->user_id); ?>" selected><?php echo e($name->user_name); ?></option>
                        <?php else: ?>
                            <option value="<?php echo e($name->user_id); ?>"><?php echo e($name->user_name); ?>  ,    <?php echo e($name->user_idno); ?></option>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <select name=""  id="" hidden>
                        <option value="2" hidden>رئيس</option>
                    </select>
                <button  type="button"  id="add_vice" data-url="<?php echo e(route('employees.getEmployees')); ?>">إضافة</button>
        </div>
        <hr>
        <div class="form-group">
                <table class="table table-striped mt-10">
                    <thead>
                        <tr>
                            <th scope="col">اسم الأمين</th>
                            <th scope="col">رقم الهوية</th>
                            <th scope="col">رقم الجوال</th>
                            <th scope="col">البريد الإلكتروني</th>
                            
                        </tr>
                    </thead>
                        <tbody id="committeeMembersVice">
                        <?php $__currentLoopData = $commiteeMembersForVice; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            
                            <?php if($item->member_type_id == 2): ?>
                                <td><?php echo e(@$item->member->user_name); ?></td>
                                <td><?php echo e(@$item->member->user_idno); ?></td>
                                <td><?php echo e(@$item->member->user_mobile); ?></td>
                                <td><?php echo e(@$item->member->user_mail); ?></td>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                </table>
            </div>
    </div>
    <hr>
    <div id="secretaries" class="form-group">
        
    </div>
    <div id="secretary" class="form-group">
        <div class="aui-buttons input-group-btn" role="group" style="margin-left:12px;">
                <label for="status" class="control-label"> اختيار النوع</label>
                <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>
                <input type="hidden" value="<?php echo e(@$commiteeMembers[0]->committee_id); ?>" id="committeeIdFM">
                <select name="membersType[]" id="committee_members_type" class="input-group-prepend select2">
                    <option value="0">اختر</option>
                    <?php $__currentLoopData = $commiteeMemberType; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($type->id); ?>"><?php echo e($type->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <label for="status" class="control-label"> اختيار العضو</label>
                <select id="committee_members" class="input-group-prepend select2">
                    <option value="0">اختر</option>
                    <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($name->user_id); ?>"><?php echo e($name->user_name); ?>  ,    <?php echo e($name->user_idno); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <select name=""  id="" hidden>
                    <option value="3" hidden>رئيس</option>
                </select>
                <button  type="button"  id="add_committee_members" data-url="<?php echo e(route('employees.getEmployees')); ?>">إضافة</button>
        </div>
        <table class="table table-striped mt-10" id="committeeMembers">
            <thead>
                <tr>
                    <th scope="col">اسم العضو</th>
                    <th scope="col">رقم الهوية</th>
                    <th scope="col">رقم الجوال</th>
                    <th scope="col">البريد الإلكتروني</th>
                    
                    <th scope="col">النوع</th>
                    <th scope="col">خيارات</th>
                </tr>
            </thead>
            <?php $__currentLoopData = $commiteeMembersForMember; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                
                <?php if(($item->member_type_id == 3)
                || ($item->member_type_id == 4)
                ||($item->member_type_id == 5)
                ||
                ($item->member_type_id == 6)
                || ($item->member_type_id == 7)
                ): ?>
                    <tbody>
                        
                        
                        <td> <?php echo e(@$item->member->user_name); ?></td>
                        <td><?php echo e(@$item->member->user_idno); ?></td>
                        <td><?php echo e(@$item->member->user_mobile); ?></td>
                        <td><?php echo e(@$item->member->user_mail); ?></td>
                        
                        <td><?php echo e(@$item->memberType->name); ?></td>
                            <td> <button type="button" onClick="deleteRow(this)" class="btn btn-danger file-remove"
                                data-remove-url="<?php echo e(route('member.delete', $item)); ?>"
                                data-remove-row="#file-<?php echo e($item->id); ?>">
                            حذف
                        </button></td>
                    </tbody>
                <?php endif; ?>
                
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </table>
    </div>
<hr>
<div class="form-group">
    <h6 style="color:#dc3545" >
        <i class="fa fa-info"></i> ملحوظة: لن يتم حفظ أي تعديلات إلا بعد الضغط على حفظ أسفل الصفحة
    </h6>
</div>
<hr>


