<?php $__env->startSection('page'); ?>

  

    <div class="container mt-5 minhe">
        <!-- main row -->
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <a class="temp p-2 d-table" href="<?php echo e(route('sessions.index', $committee)); ?>" target="_self" title=" إدارة الإجتماع">
                                <i class="fa fa-gear"></i>إدارة الإجتماعات
                            </a>
                            <p class="sissionPage p-2 acolor">
                                <img alt="اللجان" class=" ml-1" src="/assets/images/attendance.png"/>
                                <span>تحضير الاعضاء</span>
                            </p>
                            <h5>
                                <img alt="اللجان" class=" ml-1" src="/assets/images/<?php echo e($session->committee->committee_or_council == 1 ? 'folder' : 'business-meeting'); ?>.svg">
                                <?php echo e($session->committee->committee_or_council == 1 ? 'لجنة' : 'مجلس'); ?>

                                <?php echo e($session->committee->name); ?> 
                                
                                - الإجتماع  
                                
                                <?php echo e($session->sessionTitle->name); ?>

                            </h5>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <?php echo e(Form::open(['route' => ['sessions.attendees.store', $committee->id, $session->id], 'class' => 'form-horizontal', 'method' => 'post'])); ?>

                        <table class="table userattenntabel text-center">
                            <thead class="thead-dark">
                            <tr>
                                <th> إسم العضو</th>
                                <th>الصفة</th>
                                <th> التحضير</th>
                                <th>سبب الإعتذار</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $members; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $member): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(@$member->member->user_id > 0): ?>
                                    <input type="hidden" value="<?php echo e($member->id); ?>" class="memberid">
                                    <?php $attendee = $session->getAttendee(@$member->member->user_id) ?>
                                   
                                    <?php if($member->displayForAttendance($attendee)): ?>
                                        <tr>
                                            <td><?php echo e(@$member->member->user_name); ?></td>
                                            <td><?php echo e(@$member->memberType->name); ?></td>
                                            <td class="<?php echo e($errors->has("attendees.{$member->member->user_id}.attendance_status_id") ? 'has-error' : ''); ?>">
                                                <?php echo e(Form::hidden("attendees[{$member->member->user_id}][user_id]", $member->member->user_id , ['class' => 'form-control'])); ?>

                                                <?php $__currentLoopData = $attendanceStatuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                    <div  class="form-check form-check-inline">
                                                        <input data-id="<?php echo e($member->id); ?>" class="form-check-input memberval"
                                                            name="attendees[<?php echo e($member->member->user_id); ?>][attendance_status_id]"
                                                            type="radio"
                                                            value="<?php echo e($key); ?>"
                                                            <?php echo e(optional($attendee)->attendance_status_id == $key ? 'checked' : ''); ?> id="member <?php echo e($member->id); ?>" />
                                                        <label class="form-check-label" for="inlineRadio1"><?php echo e($status); ?></label>
                                                    </div>

                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php echo e(Form::hidden("attendees[{$member->member->user_id}][user_id]", $member->member->user_id , ['class' => 'form-control', 'name' => 'radioval'])); ?>

                                                 <span class="has-error-span"> <?php echo e($errors->first("attendees.{$member->member->user_id}.attendance_status_id")); ?></span>

                                            </td>

                                            <td class="<?php echo e($errors->has("attendees.{$member->member->user_id}.apology_reason") ? 'has-error' : ''); ?>" >
                                
                                                <?php echo e(Form::text("attendees[{$member->member->user_id}][apology_reason]",
                                                            old("attendees.{$member->member->user_id}.apology_reason") ?? optional($attendee)->apology_reason  ,
                                                            ['class' => 'form-control user-as-member' , 'id' => 'session-attendance-apology-reason-' . $member->id, 'style' => @$attendee->attendance_status_id !== '3' ? 'display: none;' : '1'] )); ?>

                                                        
                                                <span class="has-error-span"> <?php echo e($errors->first("attendees.{$member->member->user_id}.apology_reason")); ?></span>
                                            </td>
                                        </tr>

                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <?php $__currentLoopData = $guests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php $attendee = $session->getGuestAttendee($guest->user_id) ?>

                            </span>
                            
                            
                                <?php if($guest->user_id): ?>
                            <span type="hidden" value="<?php echo e($guest->user_id); ?>" class="guestid">

                                    <tr>

                                        <td>
                                            <?php echo e(!empty($guest->name) ? $guest->name : $guest->user->name); ?>

                                        </td>

                                        <td>مدعو للإجتماع</td>

                                        <td id="status" class="<?php echo e($errors->has("attendees.{$guest->user_id}.attendance_status_id") ? 'has-error' : ''); ?>">

                                            <?php echo e(Form::hidden("attendees[{$guest->user_id}][user_id]", $guest->user_id ? : '-1' , ['class' => 'form-control'])); ?>

                                           
                                            <?php $__currentLoopData = $attendanceStatuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


                                                <div class="form-check form-check-inline">
                                                   
                                                    <input data-id="<?php echo e($guest->user_id); ?>"  class="form-check-input guestval"
                                                           name="attendees[<?php echo e($guest->user_id); ?>][attendance_status_id]"
                                                           type="radio"
                                                           value="<?php echo e($key); ?>"
                                                            <?php echo e(optional($attendee)->attendance_status_id == $key ? 'checked' : ''); ?> id="radio"/>
                                                    <label class="form-check-label" for="inlineRadio1"><?php echo e($status); ?></label>
                                                </div>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            <!-- <span class="has-error-span"> <?php echo e($errors->first("attendees.{$guest->id}.attendance_status_id")); ?></span> -->

                                        </td>

                                        <td class="<?php echo e($errors->has("attendees.{$guest->user_id}.apology_reason") ? 'has-error' : ''); ?>">
                                            
                                            <?php echo e(Form::text("attendees[{$guest->user_id}][apology_reason]",
                                            
                                            old("attendees[{$guest->user_id}][apology_reason]") ?? optional($attendee)->apology_reason ,
                                                        ['class' => 'form-control user-as-guest' , 'id' => 'session-attendance-apology-reason-' . $guest->user_id, 'style' => @$attendee->attendance_status_id!=='3'?'display: none;':''])); ?>


                                            <span class="has-error-span"> <?php echo e($errors->first("attendees.{$guest->user_id}.apology_reason")); ?></span>
                                            
                                        </td>

                                    </tr>
                                     <?php endif; ?>
                                    

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            
                            </tbody>
                        </table>
                        <button class="btn sendBtn border-0 rounded-0 ml-2 pl-4 pr-4" type="submit" id="target"><i class="fa fa-save ml-2"></i>   حفظ</button>
                        <button class="btn nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4" type="button"><i class="fa fa-plus ml-2"></i>   تحضير الكل</button>
                        <button class="btn nonBtne unnall border-0 rounded-0 ml-2 pl-4 pr-4" type="reset"> <i class="fa fa-trash ml-2"></i>   إلغاء تحضير  الكل</button>
                        <br /><br />
                        <a  href="<?php echo e(route('guests.index', [$session->committee, $session])); ?>"
                           class="btn btn-danger text-white nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button">
                            <i class="fa fa-arrow-right ml-2"></i> رجوع
                        </a>
                        
                        <a id="nextButton" style="background:green!important" href="<?php echo e(route('recommendations.edit',  $session)); ?>"
                           class="btn nonBtn  border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button" >
                            <i class="fa fa-arrow-left ml-2"></i> التالي
                        </a>
                    </div>
                </div>
                <?php echo e(Form::close()); ?>

            </div>
        </div>
        <br>
        
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>



        $( "#nextButton" ).click(function(e) {
                    $( "#target" ).click();

                    // alert("it's checked")
                    e.preventDefault();


                true;
            });


                $(".btnall").click(function () {
                if(  $("input[type=radio][value='1']").attr('checked', 'checked')){
                    $("#status").attr("disabled", true);
                }
                });
                $(".unnall").click(function () {
                    $("input[type=radio]").removeAttr('checked');
                });


                
        $(document).ready(function () {
            
            $(".guestval").on('change', function(e) {
                
                var guestval = $(this).attr('data-id');

                    if($(this).val()=="3") { //3rd radiobutton
                        $('#session-attendance-apology-reason-'  + guestval).show();
                    }
                    else {
                        $('#session-attendance-apology-reason-'  + guestval).hide();
                    }
            
            });

                              

            $(".memberval").on('click', function() {
                    
                var memberval = $(this).attr('data-id');
                    
                    if($(this).val()=="3") {                         
                        
                        $('#session-attendance-apology-reason-'  + memberval).show();
                           
                    }
                    else {

                        $('#session-attendance-apology-reason-'  + memberval).hide();

                        
                    }
                    

            });

        });

        
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>