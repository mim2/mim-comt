<?php $__env->startSection('page'); ?>

      


    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col categoryContainer">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                    
                        
                                    
                        <a class="nav-link rounded-0 commity p-4 active" data-toggle="tab" href="#commity" role="tab"><img alt="" class="ml-1" src="/assets/images/folder.svg"> اللجان</a>
        
                    </li>

                    <li class="nav-item">
                        <a class="nav-link seission rounded-0 p-4" data-toggle="tab" href="#seission" role="tab"><img alt="" class="ml-1" src="/assets/images/business-meeting.svg"> المجالس</a>
                        
                    </li>
                    
                    
                </ul>
              
                </br> 

               
                

                </br> 
                <hr> 
                

                <div class="tab-content text-right">
                    <div class="tab-pane fade in active show" id="commity" role="tabpanel">

                        <?php $__currentLoopData = $committees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $committee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                    
                            <?php if($committee->committee_or_council == 1): ?>
                                

                                <div class="col green p-2 mt-3 d-flex flex-column justify-content-between">
                                    <div class="maintitle d-flex justify-content-between">
                                        <h5><img alt="" class="ml-1" src="/assets/images/folderca_1.png"><?php echo e($committee->name); ?></h5>
                                        <span class="p-2 text-center details">
                                            <a href="<?php echo e(route('member.committees.show', $committee->id)); ?>">
                                                <img alt="" class="ml-1" src="/assets/images/info.svg"> تفاصيل اللجنة
                                            </a>
                                            <?php echo e(count($committees)); ?>

                                        </span>
                                    </div>
                                    <div class="buttonss flex-row pt-3 mt-3">
                                        
                                        <a class=" p-2 border-0 rounded-0 cate mr-2">
                                            <img alt="" class="ml-1" src="/assets/images/userCat.png"> عدد الاعضاء
                                            <span class="badge badge-light border-0 rounded-0"><?php echo e($committee->members->count()); ?></span>
                                        </a>
                                        <a class=" p-2 border-0 cate rounded-0 mr-2">
                                            <img alt="" class="ml-1" src="/assets/images/filescat.png"> الملفات
                                            <span class="badge badge-light border-0 rounded-0">0</span>
                                        </a>
                                        <a class=" p-2 border-0 cate mr-2">
                                            <img alt="" class="ml-1" src="/assets/images/commitCat.png"> الإجتماعات
                                            <span class="badge badge-light border-0 rounded-0"><?php echo e($committee->sessions->count()); ?></span>
                                        </a>

                                        

                                        <a class=" p-2 border-0 cate mr-2" href="<?php echo e(route('sessions.index', $committee)); ?>">
                                        <img alt="" class= ml-1" src="/assets/images/gearCat.png"> إدارة الإجتماعات
                                        </a>

                                        

                                    </div>
                                </div>
                                
                            <?php endif; ?>
                            
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>

                    <div class="tab-pane fade" id="seission" role="tabpanel">

                        <?php $__currentLoopData = $committees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $committee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(($committee->committee_or_council == 2) ^ ($committee->committee_or_council == 3)): ?>
                            
                        <div class="col green p-2 mt-3 d-flex flex-column justify-content-between">
                            <div class="maintitle d-flex justify-content-between">
                                <h5><img alt="" class=" ml-1" src="/assets/images/folderca_3.png"><?php echo e($committee->name); ?></h5>
                                <span class="p-2 text-center details">
                                    <a href="<?php echo e(route('member.committees.show', $committee->id)); ?>">
                                        <img alt="" class="ml-1" src="/assets/images/info.svg"> تفاصيل المجلس
                                    </a>
                                    
                                </span>
                            </div>
                            <div class="buttonss flex-row pt-3 mt-3">
                                
                                <button class="  p-2 border-0 cate mr-2">
                                    <img alt="" class="ml-1" src="/assets/images/userCat.png"> عدد الاعضاء
                                    <span class="badge badge-light border-0"><?php echo e($committee->members->count()); ?></span>
                                </button>
                                <button class="  p-2 border-0 cate mr-2">
                                    <img alt="" class="ml-1" src="/assets/images/filescat.png"> الملفات
                                    <span class="badge badge-light border-0">0</span></button>
                                <button class="  p-2 border-0 cate mr-2">
                                    <img alt="" class="ml-1 w-25" src="/assets/images/commitCat.png"> الإجتماعات
                                    <span class="badge badge-light border-0"><?php echo e($committee->sessions->count()); ?></span>
                                </button>

                                <?php if($committee->managingMemberIds()->contains(auth()->user()->user_id)): ?>

                                <a class="  p-2 border-0 cate mr-2" href="<?php echo e(route('sessions.index', $committee)); ?>">
                                    <img alt="" class="ml-1" src="/assets/images/gearCat.png"> إدارة الإجتماعات
                                </a>

                                <?php endif; ?>

                            </div>
                        </div>

                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>

                    

                    <div class="row">

                        <div class="col-12 text-center">

                            <?php echo e($committees->links()); ?>

                        </div>
                            
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>