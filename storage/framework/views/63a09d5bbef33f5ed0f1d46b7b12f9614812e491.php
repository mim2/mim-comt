<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf8_unicode_ci"/>
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="ie=edge" http-equiv="X-UA-Compatible"/>
    <title><?php echo e(@Modules\Core\Entities\Setting::first()->app_name); ?></title>
    <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
    <link href="/assets/css/icheck-bootstrap.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.css" rel="stylesheet"/>
    <link href="/assets/css/bootstrapp.css" rel="stylesheet"/>
    <link href="/assets/css/icheck-bootstrapp.css" rel="stylesheet">
    <link href="/assets/css/font-awesomep.css" rel="stylesheet"/>
    <link href="/assets/css/mainp.css?v=<?php echo e(rand(1000, 10000)); ?>" rel="stylesheet"/>
    <link href="/assets/fullCalendar/main.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" rel="stylesheet"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />


    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('select2/dist/css/select2.min.css')); ?>">

    <!-- Script -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>"/>

    <?php echo $__env->yieldContent('stylesheets'); ?>

</head>
<body>
    
    
        
            
            
        
        
            
        
    
    <div id="page-container">
        <div class="br-mainpanel" id="vue-app" >
        

            

                <?php echo $__env->make('layouts.main.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                
                
                <?php if($errors->count() > 0): ?>
                    <div id="ERROR_COPY" style="display: none;" class="alert alert-warning">
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php echo e($error); ?><br/>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                <?php endif; ?>
                <?php if($errors->count() > 0): ?>
                    <div id="ERROR_COPY" style="display: none;" class="alert alert-warning">
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php echo e($error); ?><br/>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                <?php endif; ?>

                <?php if(session()->has('success')): ?>
                    <div class="alert alert-success">
                        <?php echo e(session()->get('success')); ?>

                    </div>
                <?php endif; ?>

                <?php echo $__env->yieldContent('page'); ?>
                

                 <div class="footer lfooter   p-1 container-fluid">
                    
            </div>
        </div>
        

        <br>
        
    </div>

<?php echo $__env->make('layouts.main.js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script>
        window.onpageshow = function(evt) {
        if (evt.persisted) {
            document.body.style.display = "none";
            location.reload();
        }
    };
    </script>

<?php echo $__env->yieldContent('scripts'); ?>
</body>
</html>
