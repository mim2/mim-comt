<?php $__env->startSection('page'); ?>

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5><img class="ml-1" src="/assets/images/folder.svg" alt="">تعديل لجنة / مجلس</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <?php echo e(Form::model($committee, ['route' => ['committees.update', $committee->id], 'files' => true, 'class' => 'form-horizontal', 'method' => 'patch'])); ?>

                        <?php echo $__env->make('committees::committees.form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <?php echo e(Form::close()); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>