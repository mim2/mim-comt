<h5 class="text-right headtitle head3 mr-3">ثالثا: المناقشات </h5>

<table class="table text-center">
    <thead class="thcusme2 th3">
    <tr>
        <th scope="col">المناقشة</th>
    </tr>
    </thead>
    <tbody>

   
        <tr>
            
            <td style="text-align: justify; word-break: break-all;"><?php echo e(@$session->recommendation->recommendation); ?></td>
        </tr>

  

    </tbody>
</table>

<h5 class="text-right headtitle head3 mr-3">رابعاً: المهام / التوصيات</h5>
<table class="table text-center">
    <thead class="thcusme2 th5">
    <tr>
        <th scope="col">#</th>
        <th scope="col">التوصية</th>
        <th scope="col">المسؤول</th>
        <th scope="col">وقت الإستحقاق</th>
    </tr>
    </thead>
    <tbody>

    <?php $__currentLoopData = $session->tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

          
            <tr>
                <td><?php echo e($loop->iteration); ?></td>
                <td style="word-break: break-all;"><?php echo e(@$task->discussion->discussions); ?></td>
                <?php if(@$task->user_id != 99999): ?>
                <td><?php echo e(@$task->assignee->user_name); ?></td>
                <?php else: ?> 
                <td>جميع الأعضاء</td>
                <?php endif; ?>
                <td><?php echo e(@$task->time->name); ?></td>
            </tr>

    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </tbody>
</table>


