<div class="row">
        <div class="col maindetails p-2 mt-3">
            <table class="table text-center mt-2" id="datatable">
                <thead class="thcusme th5">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">اللجان </th>
                    <th scope="col">لجنة/مجلس</th>
                    <th scope="col">النوع</th>
                    <th scope="col">التصنيف</th>
                    <th scope="col">الإجتماعات</th>
                    <th scope="col">
                        <div class="pull-left">
                            <a href="<?php echo e(route('committees.create')); ?>" class="btn btn-success btn-sm">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody>

                <?php $__currentLoopData = $committees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $committee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <tr>
                        <td><?php echo e($loop->iteration); ?></td>
                        <td><a href="<?php echo e(route('committees.show', $committee->id)); ?>"
                               class="edia text-dark"><?php echo e($committee->name); ?></a></td>
                        <td class="edia text-dark"><?php echo e($committee->committee_or_council == 1 ? 'لجنة' : 'مجلس'); ?></td>
                        <td class="edia text-dark"><?php echo e($committee->type->name); ?></td>
                        <td class="edia text-dark"><?php echo e($committee->category == 1 ? 'خارجي' : 'داخلي'); ?></td>
                        <td class="edia text-dark" id="forActive" value="<?php echo e($committee->is_stopped); ?>">
                                <?php echo e($committee->is_stopped == 1 ? 'غير مفعلة' : 'مفعلة'); ?> 
                        </td>
                        <th>
                            <div class="pull-left">
                                <?php echo e(Form::open(['route' => ['committees.destroy', $committee->id], 'method' => 'delete', 'id' => $committee->id])); ?>

                                <?php if($committee->is_stopped): ?>
                                    <a href="<?php echo e(route('committees.resume', $committee->id)); ?>"
                                       class="btn btn-primary btn-sm">
                                        <i class="fa fa-play"></i>
                                    </a>
                                <?php else: ?>
                                    <button type="button" onclick="$('#id').val(<?php echo e($committee->id); ?>)"
                                            class="btn btn-primary btn-sm"
                                            data-toggle="modal" data-target="#exampleModal">
                                        <i class="fa fa-stop"></i>
                                    </button>
                                <?php endif; ?>
                                <a href="<?php echo e(route('committees.edit', $committee->id)); ?>"
                                   class="btn btn-warning btn-sm">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <button type="submit" onclick="return confirm('متأكد من الحذف؟')"
                                        class="btn btn-danger btn-sm">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                                <?php echo e(Form::close()); ?>

                            </div>
                        </th>
                    </tr>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </tbody>
            </table>
            <br/>
            
        </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
             aria-labelledby="myLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <?php echo e(Form::open(['route' => 'committees.stop', 'method' => 'post'])); ?>

                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">إيقاف لجنة</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" id="id" name="id" value=""/>
                            <?php echo e(Form::label('reason', 'سبب الإيقاف', ['class' => 'control-label'])); ?>

                            <?php echo e(Form::textarea('reason', old('reason'), ['class' => 'form-control'])); ?>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                    <?php echo e(Form::close()); ?>

                </div>
            </div>
        </div>
    </div>