<?php $__env->startSection('page'); ?>

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                    <div class="maintitle d-flex justify-content-between">
                        <h5><i class="fa fa-edit"></i> تعديل موضوع</h5>
                    </div>
                </div>
                <div class="col maindetails p-2 mt-3">
                    <?php echo e(Form::model($subject, ['route' => ['subjects.update', $subject], 'class' => 'form-horizontal', 'method' => 'patch'])); ?>


                    <?php echo $__env->make('committees::subjects.form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                    <?php echo e(Form::close()); ?>


                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>