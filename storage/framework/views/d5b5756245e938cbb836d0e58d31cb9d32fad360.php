<link href="/vendor-assets/css/datatables.min.css" rel="stylesheet">
<link href="/vendor-assets/css/datatables.bootstrap.css" rel="stylesheet">
<link href="/vendor-assets/css/datatables.bootstrap-rtl.css" rel="stylesheet">
<link href="/vendor-assets/css/ummalqura.calendars.picker.css" rel="stylesheet">
<link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">


<?php $__env->startSection('page'); ?>

             



    <div class="container mt-5 minhe">
        <div class="col SissionDetails">
            <div class="modal-header border-0 rounded-0 p-0">
                <h5><i class="fa fa-plus"></i> إضافة الإجتماع</h5>
            </div>
            <div class="row">
                <div class="col maindetails p-2 mt-3">
                    <?php echo e(Form::open(['route' => ['sessions.store', $committee->id], 'files' => true, 'class' => 'form-horizontal', 'method' => 'post'])); ?>

                    <?php echo $__env->make('committees::sessions.form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo e(Form::close()); ?>

                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>