<?php $__env->startSection('stylesheets'); ?>
    <style>
        input:-webkit-autofill,
        input:-webkit-autofill:hover,
        input:-webkit-autofill:focus
        input:-webkit-autofill,
        textarea:-webkit-autofill,
        textarea:-webkit-autofill:hover
        textarea:-webkit-autofill:focus,
        select:-webkit-autofill,
        select:-webkit-autofill:hover,
        select:-webkit-autofill:focus {
            -webkit-text-fill-color: inherit !important;
            -webkit-box-shadow: 0 0 0px 1000px #FFFFFF inset;
            transition: background-color 5000s ease-in-out 0s;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_title'); ?>
    <?php echo e(__('messages.sign_in') . ' | ' .  __('messages.maj_university')); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page'); ?>
    <div class="container-fluid p-0 login-fluid">
        <img alt="meeting" class="orgImg mt-2" src="/assets/images/MIM_Logo.png">
        <img alt="meeting" class="visionImg mt-2" src="/assets/images/2030.png">
        <img alt="meeting" class="bannerImg" src="/assets/images/banner4.png">
    </div>
    <div class="container  loginconta">
        <div class="col-lg-6 mx-auto col-sm-12">
            <!-- @ Start login box wrapper -->
            <div class="blmd-wrapp">
                <div class="blmd-color-conatiner ripple-effect-All"></div>
                <div class="blmd-header-wrapp">
                </div>
                <div class="blmd-continer">
                    <!-- @ Login form container -->
                    <?php echo e(Form::open(['route' => 'auth', 'class' => 'login-form', 'method' => 'post'])); ?>


                    <?php if(session('error_login')): ?>
                        <div class="alert alert-danger text-right errorMessaDes">
                            <button class="close" data-close="alert"></button>
                            <span style="text-align: left"><?php echo e(__('messages.login_error_message')); ?></span>
                        </div>
                    <?php endif; ?>

                    <?php if(session('success')): ?>
                        <div class="alert alert-info text-right errorMessaDes">
                            <button class="close" data-close="alert"></button>
                        <span style="text-align: left">تم تغيير كلمة المرور بنجاح ويمكنك الدخول بكلمة المرور الجديدة</span>
                        </div>
                    <?php endif; ?>

                    <?php if(session('not_active')): ?>
                        <div class="alert alert-danger text-right">
                            <button class="close" data-close="alert"></button>
                            <span style="text-align: left">حسابك غير مفعل. برجاء التواصل مع مدير النظام لتفعيل حسابك</span>
                        </div>
                    <?php endif; ?>

                    <div class="col-md-12">
                        <div class="input-group blmd-form">
                            <div class="blmd-line" style="font-size: inherit;">
                               
                                <input type="text" style="font-family: fangsong;" name="username" value="<?php echo e(old('username')); ?>" class="form-control" required autocomplete="off" />
                                <label class="label text-right">رقم الهوية</label>
                            </div>
                        </div>
                        <div class="input-group blmd-form">
                            <div class="blmd-line">

                                <input type="password" style="font-family: fangsong;" name="password" class="form-control" required autocomplete="off" />
                                <label class="label"><?php echo e(__('messages.password')); ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 text-center">
                        <?php echo e(Form::submit(__('messages.sign_in'), ['class' => 'btn btn-blmd ripple-effect rege-btn btn-lg btn-block rounded-0 border-0'])); ?>



                        
                    </div>
                    <br>
                    <?php echo Form::close(); ?>


                </div>
            </div>
            <h6 class="text-center mt-3 lfooter">جميع الحقوق محفوظة  © 2020</h6>
            <div class="foote_span text-center">
                للدعم الفني  بريد الكتروني: <a href="mailto:support@mim.gov.sa" title="راسلنا">support@mim.gov.sa</a>
            </div>
        </div>
    </div>
    
    <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade border-0" id="site-feedback"
         role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content rounded-0 border-0">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ضع اقتراحك</h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body text-right">
                    <form>
                        <div class="form-group">
                            <label class="col-form-label" for="recipient-name">الاسم:</label> <input
                                    class="form-control rounded-0" id="recipient-name" type="text">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label rounded-0" for="message-text">الرسالة:</label>
                            <textarea class="form-control rounded-0" id="message-text"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-block rounded-0" type="button">إرسال</button>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<script>
$(function(){
  $(".btn btn-primary").on("click",function(){
    $.notify({
      title: '<strong>تم الحفظ</strong>',
      icon: 'glyphicon glyphicon-ok',
    },{
      type: 'info',
      animate: {
		    enter: 'animated fadeInDown',
        exit: 'animated fadeOutUp'
      },
      placement: {
        from: "top",
        align: "left"
      },
      offset: 20,
      spacing: 10,
      z_index: 1031,
    });
  });
});
</script>

<?php echo $__env->make('layouts.login.index', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>