<h5 class="text-right headtitle head3 mr-3">أولا: أعضاء اللجنة</h5>
<table class="table table-bordered">
    <thead>
    <tr>
        <th class="tdtitle">#</th>
        <th class="tdtitle">اسم العضو</th>
        <th class="tdtitle">الصفة</th>
        <th class="tdtitle">الحالة</th>
         <th class="tdtitle">سبب الغياب</th> 
    </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $session->committee->members; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $member): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php $attendee = $session->attendees->where('user_id', $member->member->user_id)->first() ?>
            <tr>
                <td><?php echo e(@++$key); ?></td>
                <td><?php echo e($member->member->user_name); ?></td>
                <td><?php echo e($member->memberType->name); ?></td>
                <td><?php echo e($attendee ? $attendee->attendanceStatus->name : '-'); ?></td>
                <td><?php echo e($attendee ? $attendee->apology_reason : '-'); ?></td> 
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>