    <div class="container-fluid p-0 topMenu">
       <div class="row">
           <div class="col-lg-3 col-sm-12 logobg text-center pt-2">
               <img alt="meeting" class="p-1 ml-2 d-inline" src="/assets/images/meeting.svg"> <!-- <img alt="title" class="p-1 d-inline mr-3" src="assets/images/llogoIn.svg"> -->
               <div class=" lotitle d-inline-block">
               <a href="/" target="_self"><?php echo e(@Modules\Core\Entities\Setting::first()->app_name); ?></a>
               </div>
           </div>
           <div class="col-md-9 left-menu pl-5">
               <div class="username d-sm-block d-md-inline mr-3">
                   مرحبا بك: <?php echo e(Auth::user()->user_name); ?>

               </div>
               <div class="controlP d-inline mr-3">
                    <a href="/" target="_self">
                        <i class="fa fa-home rounded-circle p-2 ml-2"></i>الصفحة الرئيسية
                    </a>
                </div>
               
              
              

               <?php if( Auth::user()->is_super_admin == 1): ?>
                <div class="controlP d-inline mr-3">
                    <a href="/core/control" target="_self" title="لوحة التحكم"><i class="fa fa-cog rounded-circle p-2 ml-2"></i>لوحة التحكم</a>
                </div>
               <?php endif; ?>

               
                   
               

               <div class="exit d-inline mr-3">
                   <a href="<?php echo e(route('logout')); ?>" target="_self" title="<?php echo e(__('messages.logout')); ?>">
                       <i class="fa fa-sign-out rounded-circle p-2 ml-2"></i><?php echo e(__('messages.logout')); ?>

                   </a>
               </div>
               

               <a href="https://www.dmmr.gov.sa/">
                <img alt="وزارة الصناعة" class="mr-3 d-inline unLogo" src="/assets/images/mim-logo2.png">
            </a>
               <a href="https://vision2030.gov.sa/ar/node">
                   <img alt="رؤية ٢٠٣٠" class="mr-3 d-inline" src="/assets/images/2030.png">
               </a>
           </div>
       </div>

       
       
   </div><!-- end-topMenu -->
