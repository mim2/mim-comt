<!DOCTYPE html>
<html dir="rtl" lang="ar">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>اللجان</title>
    <style>
        body {
            direction: rtl;
            text-align: right;
            font-size: 14pt;
        }
    </style>

</head>

<body>

<br/>
<br/>
<br/>
<div style="text-align: center; font-size: 20px; margin-top: 100px">
    محضر الاجتماع
</div>
<br>

<table style="padding: 5px;">
    <tbody>
    <tr style="text-align: center; background-color: #7d7d7d; color: #fff; font-size: 20px;">
        <td style="border:3.5px solid #fff; border-collapse: collapse; padding: 10px;" colspan="2"><?php echo e($session->committee->name); ?></td>
    </tr>

    <tr style="background-color: #D9D9D9;">
        <td style="border:2.5px solid #fff;border-collapse: collapse;padding: 10px;">
            الإجتماع : <?php echo e($session->sessionTitle->name); ?>

        </td>
        <td style="border:2.5px solid #fff;border-collapse: collapse;padding: 10px;">
            التاريخ : <?php echo e(@$session->date); ?>

        </td>
    </tr>

    <tr style="background-color: #D9D9D9; ">
        <td style="border:2.5px solid #fff;
                           border-collapse: collapse;
                           padding: 10px;">
            المكان :
            <?php if(strpos(@$session->place,'http') !== false): ?>
                عن بعد
            <?php else: ?>
                <?php echo e(@$session->place); ?>

            <?php endif; ?>
        </td>
        <td style="border:2.5px solid #fff;
                    border-collapse: collapse;
                    padding: 10px;">
            الوقت :
            من
            <?php echo e(Carbon\Carbon::createFromFormat('H:i:s',$session->start_time)->format('h:i' )); ?>

            <?php echo e((Carbon\Carbon::createFromFormat('H:i:s',$session->start_time)->format('a' ) == 'am' ? 'ص' : 'م' )); ?>

            إلى

            <?php echo e(Carbon\Carbon::createFromFormat('H:i:s',$session->end_time)->format('h:i ')); ?>

            <?php echo e((Carbon\Carbon::createFromFormat('H:i:s',$session->end_time)->format('a' ) == 'am' ? 'ص' : 'م' )); ?>

        </td>
    </tr>

    </tbody>
</table>

<br/>
<br/>

<?php echo $__env->make('committees::partial.session_opinions_print', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<br/>
<br/>
<table style="padding: 5px;">
    <tr style="text-align: center; background-color: #D9D9D9; font-size: 20px; ">
        <th colspan="12" style="border:3.5px solid #fff;
                        border-collapse: collapse;
                        padding: 10px;">أجندة الإجتماع</th>
        <th colspan="1" style="border:3.5px solid #fff;
                           border-collapse: collapse;
                           padding: 10px;">#</th>
    </tr>

    <?php $__currentLoopData = $session->subjects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $subject): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td colspan="12"><?php echo e($subject->title); ?></td>
            <td align="center" colspan="1" style="border:2.5px solid #fff;
                            border-collapse: collapse;
                            padding: 10px;
                            background-color: #D9D9D9;"><?php echo e($index +1); ?></td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</table>

<br/>

<br/>

</body>

</html>