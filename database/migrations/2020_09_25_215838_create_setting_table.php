<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('app_name')->nullable();
            $table->text('app_logo')->nullable();
            $table->string('mail_account')->nullable();
            $table->string('mail_password')->nullable();
            $table->string('mail_port')->nullable();
            $table->string('mail_host')->nullable();
            $table->string('mail_driver')->nullable();
        

            $table->timestamps();
        });

        DB::table('setting')->insert([
            ['id' => 1, 'app_name' => 'نظام اللجان والمجالس'],
            
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}
