<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('types');
            $table->string('category');
            $table->timestamps();
        });

        DB::table('reports')->insert([
            'types' => 'تقرير تسجيل مستحقات اللجان والمجالس', 'category' => 'يختص باللجان والمجالس', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'نموذج حصر باللجان المؤقتة', 'category' => 'يختص باللجان والمجالس', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => ' تقرير احصائي باللجان', 'category' => 'يختص باللجان والمجالس', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير حصر اللجان الدائمة', 'category' => 'يختص باللجان والمجالس', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير حصر لجان الوكلاء', 'category' => 'يختص باللجان والمجالس', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'نموذج حصر اللجان الدائمة واللجان الفرعية لها', 'category' => 'يختص باللجان والمجالس', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير باللجان المفعلة وعدد الجلسات المعتمدة بداخلها', 'category' => 'يختص باللجان والمجالس', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقربر بالمجالس وعدد الجلسات المعتمدة بداخلها', 'category' => 'يختص باللجان والمجالس', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        
        DB::table('reports')->insert([
            'types' => 'تقرير بالاعضاء وساعات الحضور', 'category' => 'يختص بالأعضاء', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير باللجان والرؤساء', 'category' => 'يختص بالأعضاء', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'نموذج حصر عام بالجلسات والاعضاء والمدعويين لها', 'category' => 'يختص بالأعضاء', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير تفصيلي بالجلسات والاعضاء', 'category' => 'يختص بالأعضاء', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'نموذج حصر جلسات المدعويين', 'category' => 'يختص بالأعضاء', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير رؤساءاللجان الذين رشحو', 'category' => 'يختص بالأعضاء', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير بيانات اعضاء اللجان والمجالس', 'category' => 'يختص بالأعضاء', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير باعضاءالنظام ولجانهم', 'category' => 'يختص بالأعضاء', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير حصر بلجان ومجالس الاعضاء', 'category' => 'يختص بالأعضاء', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير بإعتذارات وغيابات اعضاءاللجان', 'category' => 'يختص بالأعضاء', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير بأرقام جوال وإيميلات رؤساء وأمناء اللجان الدائمة والفرعية لها', 'category' => 'يختص بالأعضاء', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'نموذج حصر الجلسات والأعضاء الفعليين فقط', 'category' => 'يختص بالأعضاء', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير بالجلسات المعتمدة داخل الاقسام', 'category' => 'يختص بالجلسات', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير بالجلسات ', 'category' => 'يختص بالجلسات', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'تقرير حصر الجلسات ', 'category' => 'يختص بالجلسات', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        DB::table('reports')->insert([
            'types' => 'نموذج بحث في المحضر', 'category' => 'يختص بالجلسات', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
