<?php

use Illuminate\Database\Seeder;
use Modules\Core\Entities\App;
use Modules\Auth\Entities\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('session_statuses')->truncate();
        // DB::table('session_statuses')->insert([
        //     ['id' => 1, 'name' => 'جديدة', 'color' => '#95C347'],
        //     ['id' => 2, 'name' => 'تم إعداد جدول الأعمال', 'color' => '#AAAAAA'],
        //     ['id' => 3, 'name' => 'تم إعداد المحضر للتوقيع', 'color' => '#FF6949'],
        //     ['id' => 4, 'name' => 'تم إرسال المحضر للاعتماد', 'color' => '#95C347'],
        //     ['id' => 5, 'name' => 'تم اعتماد المحضر', 'color' => '#95C347'],
        //     ['id' => 6, 'name' => 'للاستكمال', '#color' => '#0D6EB8'],
        //     ['id' => 7, 'name' => 'في العرض', 'color' => '#95C347'],
        //     ['id' => 8, 'name' => 'تم الاستكمال', 'color' => '#95C347'],
        //     ['id' => 9, 'name' => 'تم إرسال المحضر للاعتماد', 'color' => '#95C347'],
        //     ['id' => 10, 'name' => 'تم اعتماد المحضر', 'color' => '#95C347'],
        // ]);
        $this->call(AppsDatabaseSeeder::class);
        // $this->call(SessionHoursSeeder::class);
        // $this->call(\Modules\Committees\Database\Seeders\CommitteesDatabaseSeeder::class);
    }
}
