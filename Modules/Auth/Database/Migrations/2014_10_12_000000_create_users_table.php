<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('user_name');
            $table->string('full_name');
            $table->string('user_mobile');
            $table->string('user_idno')->nullable();
            $table->integer('user_dept');
            $table->integer('job_id')->nullable();
            $table->string('photo_url')->nullable();
            $table->boolean('is_active')->default(0);
            $table->string('user_mail')->unique();
            $table->string('password');
            $table->integer('gender');
            $table->boolean('is_super_admin')->default(false);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('users')->insert([
            [
                'user_id' => 1,
                'user_name' => 'حساب مدير النظام',
                'full_name' => 'مدير النظام',
                'user_mail' => 'admin@mim.gov.sa',
                'user_idno' => '1000000001',
                'user_mobile' => '0599223233',
                'user_dept' => '1',
                'gender' => "1",
                'deleted_at' => null,
                'is_super_admin' => true,
                'password' => Hash::make('secret')
            ],
            [
                'user_id' => 2,
                'user_name' => 'سديم المعجل',
                'full_name' => 'سديم المعجل',
                'user_mail' => 's.almujel@mim.gov.sa',
                'user_idno' => '1000000002',
                'user_mobile' => '0555990827',
                'user_dept' => '1',
                'gender' => "2",
                'deleted_at' => null,
                'is_super_admin' => true,
                'password' => Hash::make('secret')
            ],
            [
                'user_id' => 3,
                'user_name' => 'داليا القحطاني',
                'full_name' => 'داليا القحطاني',
                'user_mail' => 'd.alqahtani@mim.gov.sa',
                'user_idno' => '1000000003',
                'user_mobile' => '0599223233',
                'user_dept' => '1',
                'gender' => "2",
                'deleted_at' => null,
                'is_super_admin' => false,
                'password' => Hash::make('secret')
            ],
            [
                'user_id' => 4,
                'user_name' => 'فيصل العواسي',
                'full_name' => 'فيصل العواسي',
                'user_mail' => 'falowaisi@mim.gov.sa',
                'user_idno' => '1000000004',
                'user_mobile' => '0599223233',
                'user_dept' => '1',
                'gender' => "1",
                'deleted_at' => null,
                'is_super_admin' => false,
                'password' => Hash::make('secret')
            ],

            
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
