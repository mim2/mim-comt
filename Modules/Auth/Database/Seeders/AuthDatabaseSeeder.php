<?php

namespace Modules\Auth\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

       //  $this->call("AdminUserTableSeeder");

       
        
        DB::table('users')->insert([
            [
                'user_id' => 1,
                'user_name' => 'حساب مدير النظام',
                'full_name' => 'مدير النظام',
                'user_mail' => 'admin@mim.gov.sa',
                'user_mobile' => '599223233',
                'user_dept' => '3',
                'gender' => "1",
                'deleted_at' => null,
                'password' => md5('123456')
                ]
            
        ]);
   
    }
}
