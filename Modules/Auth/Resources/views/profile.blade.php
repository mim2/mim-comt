@extends('layouts.main.index')

@section('page')

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5><i class="fa fa-edit"></i> تحديث البيانات</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">

                        {{ Form::model(auth()->user(), ['route' => 'profile.update', 'class' => 'form-horizontal', 'method' => 'patch']) }}

                        <div class="form-group">
                            {{ Form::label('user_name', 'الاسم كاملاً', ['class' => 'control-label']) }}
                            {{ Form::text('user_name', old('user_name') , ['class' => 'form-control', 'required' => true]) }}
                            <span class="text-danger">{{ $errors->first('user_name') }}</span>
                        </div>

                        <div class="form-group">
                            {{ Form::label('job_id', 'الوظيفة', ['class' => 'control-label']) }}
                            {{ Form::select('job_id', $jobs, old('job_id') , ['class' => 'form-control']) }}
                            <span class="text-danger">{{ $errors->first('job_id') }}</span>
                        </div>

                        <div class="form-group">
                            {{ Form::label('user_dept', 'الإدارة', ['class' => 'control-label']) }}
                            {{-- {{ Form::select('user_dept', $departments, old('user_dept') , ['class' => 'form-control']) }} --}}
                            <span class="text-danger">{{ $errors->first('user_dept') }}</span>
                        </div>

                        <div class="form-group">
                            {{ Form::label('user_idno', 'رقم الهوية', ['class' => 'control-label']) }}
                            {{ Form::text('user_idno', old('user_idno') , ['class' => 'form-control', 'required' => true]) }}
                            <span class="text-danger">{{ $errors->first('user_idno') }}</span>
                        </div>

                        <div class="form-group">
                            {{ Form::label('user_mobile', 'رقم الجوال', ['class' => 'control-label']) }}
                            {{ Form::text('user_mobile', old('user_mobile') , ['class' => 'form-control', 'required' => true]) }}
                            <span class="text-danger">{{ $errors->first('user_mobile') }}</span>
                        </div>

                        <div class="form-group">
                            {{ Form::label('user_mail', 'البريد الإلكتروني', ['class' => 'control-label']) }}
                            {{ Form::email('user_mail', old('user_mail') , ['class' => 'form-control', 'required' => true]) }}
                            <span class="text-danger">{{ $errors->first('user_mail') }}</span>
                        </div>

                        <div class="form-group">
                            {{ Form::label('user_sex', 'الجنس', ['class' => 'control-label']) }}
                            <div class="radio">
                                <label class="radio-inline col-md-2">
                                    <input checked="checked" name="user_sex" type="radio" value="1" id="gender" class="form-check-input">
                                    <span style="padding-right: 20px;">ذكر</span></label>
                                <label class="radio-inline col-md-2">
                                  <input checked="checked" name="user_sex" type="radio" value="1" id="gender" class="form-check-input">
                                  <span style="padding-right: 20px;">أنثى</span></label>
                            </div>
                            <span class="text-danger">{{ $errors->first('user_sex') }}</span>
                        </div>

                        <div class="d-flex justify-content-end mainbuttons mt-4 mb-4">
                            {{ Form::submit('حفظ', ['class' => 'btn btn-info']) }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5><i class="fa fa-key"></i> تغيير كلمة المرور</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">

                        {{ Form::open(['route' => 'profile.password', 'class' => 'form-horizontal', 'method' => 'post']) }}

                        <div class="form-group">
                            {{ Form::label('password', 'كلمة المرور الجديدة', ['class' => 'control-label']) }}
                            {{ Form::password('password' , ['class' => 'form-control']) }}
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        </div>

                        <div class="form-group">
                            {{ Form::label('password_confirmation', 'تأكيد كلمة المرور', ['class' => 'control-label']) }}
                            {{ Form::password('password_confirmation' , ['class' => 'form-control']) }}
                            <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                        </div>

                        <div class="d-flex justify-content-end mainbuttons mt-4 mb-4">
                            {{ Form::submit('حفظ', ['class' => 'btn btn-info']) }}
                        </div>

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
