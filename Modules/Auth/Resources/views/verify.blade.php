<style>
    input:-webkit-autofill,
    input:-webkit-autofill:hover, 
    input:-webkit-autofill:focus
    input:-webkit-autofill, 
    textarea:-webkit-autofill,
    textarea:-webkit-autofill:hover
    textarea:-webkit-autofill:focus,
    select:-webkit-autofill,
    select:-webkit-autofill:hover,
    select:-webkit-autofill:focus {
    -webkit-text-fill-color: inherit !important;
    -webkit-box-shadow: 0 0 0px 1000px #FFFFFF inset;
    transition: background-color 5000s ease-in-out 0s;
}
</style>

@extends('layouts.login.index')

@section('page_title')
    {{ __('messages.sign_in') . ' | ' .  __('messages.maj_university')}}
@endsection

@section('page')
    <div class="container-fluid p-0 login-fluid">
        <img alt="meeting" class="orgImg mt-2" src="/assets/images/MIM_Logo.png">
        <img alt="meeting" class="visionImg mt-2" src="/assets/images/2030.png">
        <img alt="meeting" class="bannerImg" src="/assets/images/banner4.png">
    </div>
    <div class="container  loginconta">
        <div class="col-lg-6 mx-auto col-sm-12">
            <!-- @ Start login box wrapper -->
            <div class="blmd-wrapp">
                <div class="blmd-color-conatiner ripple-effect-All"></div>
                <div class="blmd-header-wrapp">
                </div>
                <div class="blmd-continer">
                    <!-- @ Login form container -->
                    {{ Form::open(['route' => 'check', 'class' => 'login-form', 'method' => 'post']) }}

                    @if (session('error_login'))
                        <div class="alert alert-danger text-right errorMessaDes">
                            <button class="close" data-close="alert"></button>
                            <span style="text-align: left">رمز التحقق خاطئ</span>
                        </div>
                    @endif

                    @if (session('not_active'))
                        <div class="alert alert-danger text-right">
                            <button class="close" data-close="alert"></button>
                            <span style="text-align: left">حسابك غير مفعل. برجاء التواصل مع مدير النظام لتفعيل حسابك</span>
                        </div>
                    @endif
                    <p style="color: #002060; text-align:center ">يرجى كتابة رمز التحقق المرسل على رقم جوالك</p>
                    <div class="col-md-12">
                        <div class="input-group blmd-form">
                            <div class="blmd-line">
                                <input name="number" type="number" class="form-control"  required/>
                                <label class="label text-right">رمز التحقق</label>
                            </div>
                        </div>
                        {{ Form::hidden('idno', $user->idno) }}
                 
                    </div>
                    <div class="col-sm-12 text-center">
                        {{Form::submit('تحقق', ['class' => 'btn btn-blmd ripple-effect rege-btn btn-lg btn-block rounded-0 border-0'])}}


                        {{--  <a class="btn btn-lg btn-block mt-4 forgetPass" href="#" target="_blank" title="تغير كلمة المرور">نسيت كلمة المرور؟</a>  --}}
                    </div>
                    <br>
                    {!! Form::close() !!}

                </div>
            </div>
            <h6 class="text-center mt-3 lfooter">جميع الحقوق محفوظة  © 2020</h6>
            <div class="foote_span text-center">
                للدعم الفني  بريد الكتروني: <a href="mailto:support@mim.gov.sa" title="راسلنا">support@mim.gov.sa</a>
            </div>
        </div>
    </div>
    {{--  <div class="site-feedback d-md-block d-none" data-target="#site-feedback" data-toggle="modal"
         style="display: none;" title="شاركنا برأيك">
        <i aria-hidden="true" class="fa fa-commenting"></i>
    </div>  --}}
    <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade border-0" id="site-feedback"
         role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content rounded-0 border-0">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ضع اقتراحك</h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body text-right">
                    <form>
                        <div class="form-group">
                            <label class="col-form-label" for="recipient-name">الاسم:</label> <input
                                    class="form-control rounded-0" id="recipient-name" type="text">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label rounded-0" for="message-text">الرسالة:</label>
                            <textarea class="form-control rounded-0" id="message-text"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-block rounded-0" type="button">إرسال</button>
                </div>
            </div>
        </div>
    </div>

@endsection
<script>
$(function(){
  $(".btn btn-primary").on("click",function(){
    $.notify({
      title: '<strong>تم الحفظ</strong>',
      icon: 'glyphicon glyphicon-ok',
    },{
      type: 'info',
      animate: {
		    enter: 'animated fadeInDown',
        exit: 'animated fadeOutUp'
      },
      placement: {
        from: "top",
        align: "left"
      },
      offset: 20,
      spacing: 10,
      z_index: 1031,
    });
  });
});
</script>
