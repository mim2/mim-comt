<?php

namespace Modules\Auth\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use  App\Classes\Sms\Mobily;
use Session;
class Otp extends Model
{
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'users_otp';

    protected $fillable = [
        'idno', 'code', 'phone_number', 'is_active'
    ];

    public static function saveRandom($data)
    {
        
        $getCodeData = self::where('idno', $data->user_idno)->first();

        $getCode = self::where('idno', $data->user_idno)->count();
        if ($getCode > 0 )
        {
            $delete = self::where('id', $getCodeData->id)->delete();
        }
        $digits = 4;
        $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
        
       
        $otp = New Otp;
        $otp->idno = $data->user_idno;
        $otp->phone_number = $data->user_mobile;
        $otp->code = $code;
        $otp->is_active = true;
        $otp->save();
       
        $sendSms = Mobily::send('رمز التحقق لنظام اللجان والمجالس' . $code, [$data->user_mobile]);


    }
    
    
}