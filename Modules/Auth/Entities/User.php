<?php

namespace Modules\Auth\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Auth\Traits\UserAuth;
use Modules\Committees\Entities\Authorize;
use Modules\Committees\Entities\Committee;
use Modules\Core\Entities\Department;
use Modules\Core\Entities\Job;
use Modules\Core\Entities\Employee;
use Modules\Core\Entities\Group;
use Modules\Core\Entities\Permission;
use Modules\Core\Traits\AuthorizeUser;
use Modules\Committees\Entities\Participant;


class User extends Authenticatable
{
    use Notifiable, UserAuth, AuthorizeUser;

    protected $primaryKey = 'user_id';
    protected $table = 'users';

   
    protected $fillable = [
        "user_id",
        "user_idno",
        "user_dept",
        "user_level",
        "user_title",
        "user_name",
        "password",
        "user_mail",
        "user_mobile",
        "gender",
        "created_at",
        "updated_at",
        "deleted_at",
        "is_super_admin",
        'password_change_at',
        'data_send_date'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];





    // Relations
    public function authorizations ()
    {
        return $this->hasMany(Authorize::class, 'from_user_id');
    }

    public function auths ()
    {
        return $this->hasMany(Authorize::class, 'user_id');
    }

    public function permissions()
    {
        return $this->morphMany(Permission::class, 'permissionable');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'es_core_users_groups', 'user_id', 'es_core_group_id');
    }

    public function participants()
    {
        return $this->belongsToMany(Participant::class, 'user_id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'user_dept', 'id');
    }

    public function committees()
    {
        return $this->belongsToMany(Committee::class, 'committee_members', 'user_id')->with('sessions', 'members');
    }






    // Functions
    public static function search($query)
    {
        return self::where('user_name', 'LIKE', '%' . $query . '%')
            ->orWhere('user_mail', 'LIKE', '%' . $query . '%')
            ->orWhere('user_mobile', 'LIKE', '%' . $query . '%')
            ->orWhere('user_idno', 'LIKE', '%' . $query . '%');
    }

    public function isMemberOfGroup($groupKey)
    {
        return Group::hasUserByKey($groupKey, $this);
    }









    // Scopes
    public  function scopeSearches($query, $queryInput)
    {
        return User::select(['user_name', 'user_idno', 'user_mail', 'user_id'])
            ->when($queryInput, function ($query) use ($queryInput) {
                return $query->where(function ($query) use ($queryInput) {
                    $query->Where('user_name', 'LIKE', '%' . $queryInput . '%')
                        ->orWhere('user_idno', 'LIKE', '%' . $queryInput . '%')
                        ->orWhere('user_mail', 'LIKE', '%' . $queryInput . '%');
                });
            });
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', '=', 1);
    }


}
