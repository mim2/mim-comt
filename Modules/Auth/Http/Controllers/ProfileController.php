<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Entities\User;
use Modules\Auth\Http\Requests\UpdateUser;
use Modules\Auth\Http\Requests\UpdateUserPassword;
use Modules\Core\Entities\Department;
use Modules\Core\Entities\Job;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $jobs = Job::all()->pluck('name', 'id');
        $departments = Department::all()->pluck('dep_arname', 'dep_no');
        return view('auth::profile', compact('jobs', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     * @param  UpdateUser $request
     * @return Response
     */
    public function update(UpdateUser $request)
    {
        // dd(Auth::user()->user_dept);

        Auth::user()->update($request->all());

        return back()->with('success', 'تم تحديث بياناتك.');
    }

    /**
     * Change current user password.
     *
     * @param UpdateUserPassword $request
     * @return Response
     */
    public function password(UpdateUserPassword $request)
    {
        $user = Auth::user();
        $user->password = Hash::make($request->password);
        $user->save();
        return back()->with('success', 'تم تحديث كلمة المرور.');
    }
}
