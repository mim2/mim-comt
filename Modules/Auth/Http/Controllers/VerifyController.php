<?php

namespace Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Modules\Auth\Entities\Otp;
use Modules\Auth\Entities\User;
use Illuminate\Support\Facades\Hash;
use Input;
use Session;
use Carbon\Carbon;
use App\Classes\Sms\Mobily;


class VerifyController extends Controller
{

    /**
     * Return view index.
     *
     * @return Response
     */
    public function index(Request $request)
    {
       
       $idno = Session::get('idno');

       $data = Otp::where('idno', $idno)->first();
       
        return view('auth::verify')->with('user',$data);

    }

    public function change(Request $request)
    {

       
        $idno = Session::get('idno');

       $data = User::where('user_idno', $idno)->first();

       return view('auth::change');
       
    }

     public function changePassword(Request $request)
    {
       

        $request->validate([
            'password' => 'required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'required'
        ]);


        $request->password_change_at = Carbon::now();
      
       
        $idno = Session::get('idno');
      
      
        $data = User::where('user_idno', $idno)->first();

        $update = New User;

        $update->where('user_idno', $idno)->update(['password' =>  Hash::make($request->password), 'password_change_at'=> Carbon::now()]);
       
     //   $otp = Otp::saveRandom($data);
     
        return redirect()->route('login')->with('success', 'تم تغيير كلمة المرور بنجاح ويمكنك الدخول بكلمة المرور الجديدة');

    }

    public function check(Request $request)
    {
       

        $check = Otp::where('idno', $request->idno)->first();
        
        $user = User::where('user_idno', $request->idno)->first();

        if ($check->code == $request->number )
        {
                Auth::login($user);
                
                return redirect()->route('home');
        
            } else {

            return redirect()->route('verify')
                             ->with('error_login', 'رمز التحقق خاطئ');

        }
       
    }

}
