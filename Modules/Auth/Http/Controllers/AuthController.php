<?php

namespace Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Validator;
use Modules\Auth\Entities\User;
use Modules\Auth\Entities\Otp;
use Auth;
use Modules\Auth\Classes\AzureAuthentication;
use Lcobucci\JWT\Parser;

use Session;
use DB;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth-user')->except(['showLoginForm', 'login', 'ssoAuthorize', 'apiLogin', 'apiLogout']);
        //$this->middleware('auth:api')->only(['apiLogout']);
    }

    /**
    * Show login form
    */
    public function showLoginForm()
    {
        if (Auth::check()) {
            return redirect()->route($this->redirectTo);
        }

        return view('auth::login');
    }

    /**
    * Login user
    */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'username' => 'required',
        'password' => 'required'
      ]);

        if ($validator->fails()) {
            return redirect()->route('login')
                             ->withErrors($validator)
                             ->with('error_login', 'invalid_login');
        }

        $userName = $request->username;

        
        $credentials = [
          'user_idno' => $userName,
          'password' => $request->password
      ];
     
        $user = User::isValidCredentials($credentials);

        if (env('APP_ENV') == 'production') {
        if ($user != false) {

         
            Session::put('idno', $userName);

            if ($user->password_change_at == null) {

                 return redirect()->route('change');
            } else {

                $otp = Otp::saveRandom($user);
                return redirect()->route('verify');

            }

        } else {

            return redirect()->route('login')
                             ->withInput($request->except('password'))
                             ->with('error_login', 'invalid_login');

        }
        } else {
        if ($user != false) {
            Auth::login($user);
            return redirect()->route($this->redirectTo);
        } else {
            return redirect()->route('login')
                             ->withInput($request->except('password'))
                             ->with('error_login', 'invalid_login');
        }

    }
    }


    public function logout(Request $request)
    {
        Session::flush();
        $request->session()->regenerate();

        Auth::logout();

        if (env('AUTH_METHOD') == 'azure') {
            return redirect(env('AZURE_LOG_OUT_URL'));
        } else {
            return redirect()->route('login');
        }

    }

    public function apiLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);
    
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $credentials = [
          'email' => $request->username,
          'password' => $request->password
      ];

        $user = User::isValidCredentials($credentials);

        if ($user != false) {

            Auth::login($user, true);
            $success['token'] =  $user->createToken('APIAPP')->accessToken;
 
            return response()->json($success, 200); 
        } else {

            return response()->json([ 'error' => 'Invalid login'], 401); 

        }
    }

    public function apiLogout(Request $request)
    {
        $value = $request->bearerToken();
        $id = (new Parser())->parse($value)->getHeader('jti');
        $revoked = DB::table('oauth_access_tokens')->where('id', '=', $id)->update(['revoked' => 1]);

        return response()->json(['success' => true], 200);
    }
    
}
