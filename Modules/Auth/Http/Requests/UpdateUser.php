<?php

namespace Modules\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateUser extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = Auth::user()->user_id ?? 0;
        return [
            'user_name' => 'required',
            'job_id' => 'required',
            'dep_arname' => 'required',
            'user_idno' => 'required|numeric|digits:10|unique:users,user_idno,' . $userId,
            'user_mobile' => 'required|numeric|digits:10|unique:users,user_mobile,' . $userId,
            'user_sex' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
