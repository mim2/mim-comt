<?php
namespace Modules\Auth\Classes;

use Modules\Auth\Interfaces\AuthenticationProvider;
use Modules\Auth\Entities\User;
use \Crypt;
use Auth;
use Illuminate\Support\Facades\Hash;
class DbAuthentication implements AuthenticationProvider
{

  /**
   * Authenticate user via db provider
   * @param Array $credentials['email' => $email, 'password' => $password]
   * @return Boolean
   */
    public static function authenticate($credentials)
    {
        $user = User::where('user_idno', '=', $credentials['user_idno'])->first();
        if ($user != null) {
           
      //      dd();
           // if ($user->password_change_at != null){
                
                if ($user->user_idno == $credentials['user_idno'] && Hash::check($credentials['password'], $user->password)) {
                   // if($user->password_change_at != null) {
                    return true;
                    // } else {
                    //     return redirect()->route('change');
                    // }
                }

           // }
        }

        return false;
    }
}
