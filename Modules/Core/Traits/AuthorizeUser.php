<?php
namespace Modules\Core\Traits;

use Modules\Core\Entities\Group;
use Modules\Core\Entities\Permission;
use Modules\Core\Entities\App;

trait AuthorizeUser
{
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var Object permission
     */
    public $currentPermission = null;

    /**
      * Set current permission
      * @param Permission $currentPermission
      */
    public function setCurrentPermission($currentPermission)
    {
        $this->currentPermission = $currentPermission;
    }

    /**
     * Return all user own permissions in addition to user groups permissions
     * @param Integer $app_id
     */
    public function allPermissions($app_id = false)
    {
        $userPermissions = $this->permissions();
        
        $groupPermissions = Permission::queryByGroupIds($this->groups()->pluck(Group::table().'.id'));
       
        if ($app_id != false) {
            $userPermissions =  $userPermissions->where('app_id', $app_id);
            $groupPermissions = $groupPermissions->where('app_id', $app_id);
        }

        $allPermissions = $userPermissions->union($groupPermissions);

        return $allPermissions;
    }


    /**
      * Check if user has a permission for a specified resource name
      * @param String namespaced resource name
      * @return permission if true
      * @return false if not found
      */
    public function hasPermission($resourceName)
    {

        
        $app = App::where('resource_name', $resourceName)->first();
        
        if ($app == null) {
            
            return false;
        }

        $permissions = $this->allPermissions($app->id)->get();
      
        $groupsPermissionsByAcessLevel = [];

        foreach ($permissions as $permission) {
            if ($permission->isUserPermission()) {
                return $permission;
            } elseif ($permission->isGroupPermission()) {
                $groupsPermissionsByAcessLevel[$permission->access_level] = $permission;
            }
        }


        if (array_key_exists("all", $groupsPermissionsByAcessLevel)) {
            return $groupsPermissionsByAcessLevel["all"];
        } elseif (array_key_exists("me", $groupsPermissionsByAcessLevel)) {
            return $groupsPermissionsByAcessLevel["me"];
        }
        

        return false;
    }

    /**
       * Check if user has a permission with specified access
       * @param String namespaced resource name
       * @param String access
       * @return permission if true
       * @return false if not found
       */
    public function hasPermissionWithAccess($reourceName, $accessLevel)
    {
        $permission = $this->hasPermission($reourceName);

        if ($permission) {
            if ($permission->access_level == $accessLevel) {
                return $permission;
            }
        }
        
        return false;
    }

    /**
     * Get User authorized apps ids
     * @param Array $excludedIds Ids to be execluded from arrray
     * @return Array authorized ids
     */
    public function authorizedAppsIds($excludedIds = [])
    {
        $ids = $this->allPermissions()->get()->pluck('app_id')->toArray();
        if (!empty($excludedIds)) {
            return array_diff($ids, $excludedIds);
        } else {
            return $ids;
        }
    }

    public function isAllAccess()
    {
        return optional($this->currentPermission)->access_level == "all";
    }

    public function isDirectManagerAccess()
    {
        return optional($this->currentPermission)->access_level == "direct_manager";
    }

    public function isParentManagerAccess()
    {
        return optional($this->currentPermission)->access_level == "parent_manager";
    }
}
