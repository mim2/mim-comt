@extends('layouts.main.index')

@section('page')
    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5>الصلاحيات</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3 pr-5">
                        @if($permissionableType == "users")
                            <h2><i class="fa fa-user"></i> {{$model->user_name}}</h2>
                        @elseif($permissionableType == "groups")
                            <h2><i class="fa fa-users"></i> {{$model->name}}</h2>
                        @endif

                        <core-app-wrapper permission-route-url="{{$routeUrl}}"
                                          is-permission=true
                                          permissionable-type="{{ $permissionableType }}"
                                          permissionable-id="{{ $permissionableId }}">
                        </core-app-wrapper>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
