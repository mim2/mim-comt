<div class="form-group">
    {{ Form::label('user_name', 'إسم البرنامج', ['class' => 'control-label']) }}
    {{ Form::text('app_name', @$setting->app_name , ['class' => 'form-control', 'required' => true]) }}
    
</div>



{{-- <div class="form-group">
    {{ Form::label('user_idno', 'صورة الوزارة', ['class' => 'control-label']) }}
    {{ Form::text('user_idno', old('user_idno') , ['class' => 'form-control', 'required' => true]) }}
    <span class="text-danger">{{ $errors->first('user_idno') }}</span>
</div> --}}




<div class="d-flex justify-content-end mainbuttons mt-4 mb-4">
    {{ Form::submit('حفظ', ['class' => 'btn btn-info']) }}
    <a href="{{ route('control') }}" class="btn btn-danger">إلغاء</a>
</div>