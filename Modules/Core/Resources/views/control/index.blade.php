@extends('layouts.main.index')

@section('stylesheets')

    <style type="text/css">
        .container {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        .mainclass {
            box-shadow: 0 6px 14px 0 rgba(52, 69, 88, 0.08) !important;
            background: #fff;
        }

        .hvr-bounce-to-bottom {
            vertical-align: middle;
            -webkit-transform: perspective(1px) translateZ(0);
            transform: perspective(1px) translateZ(0);
            /* box-shadow: 0 0 1px transparent; */
            position: relative;
            -webkit-transition-property: color;
            transition-property: color;
            -webkit-transition-duration: 0.5s;
            transition-duration: 0.5s;
        }

        .mainclass i {
            font-size: 40px;
            margin-bottom: 5px;
        }

        .seci {
            color: #009889;
        }

        .sei {
            color: #95c347;
        }

        .fri {
            color: #096eb9;
        }

        .foi {
            color: #cfa968;
        }

        .thi
        color: #fe3b6e

        ;
        }

        .fii {
            color: #ff6949;
        }
    </style>

@endsection

@section('page')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-sm-6 col-xl-2 pt-3 pb-3 m-2 mainclass hvr-bounce-to-bottom hvr-bounce-to-bottom">
              <a href="/" style="color: black;">
             <div class=" rounded overflow-hidden blue-2 min-h">
             <div class="pd-x-20 pd-t-20 text-center">
             <i class="fa fa-home tx-60 lh-0 tx-white m31 fri"></i>
             <br>الرئيسية
             </div>
             </div>
             </a>
            </div>

            <div class="col-sm-6 col-xl-2 pt-3 pb-3 m-2 mainclass hvr-bounce-to-bottom hvr-bounce-to-bottom">
              <a href="/core/departments" style="color:black;">
                <div class=" rounded overflow-hidden blue-2 min-h">
                    <div class="pd-x-20 pd-t-20 text-center">
                        <i class="fa fa-sitemap tx-60 lh-0 tx-white m31 seci"></i>
                      <br>الأقسام والإدارت
                    </div>
                </div>
                </a>
            </div>

            <div class="col-sm-6 col-xl-2 pt-3 pb-3 m-2 mainclass hvr-bounce-to-bottom hvr-bounce-to-bottom">
              <a href="/core/jobs" style="color:black;">
                <div class=" rounded overflow-hidden blue-2 min-h">
                    <div class="pd-x-20 pd-t-20 text-center">
                        <i class="fa fa-suitcase tx-60 lh-0 tx-white m31 thi"></i><br>
                        الأدوار الوظيفية
                    </div>
                </div>
                </a>
            </div>

            <div class="col-sm-6 col-xl-2 pt-3 pb-3 m-2 mainclass hvr-bounce-to-bottom hvr-bounce-to-bottom">
              <a href="/core/users" style="color:black;">
                <div class=" rounded overflow-hidden gold min-h">
                    <div class="pd-x-20 pd-t-20 text-center">
                        <i class="fa fa-cog tx-60 lh-0 tx-white m31 foi"></i><br>
                        إدارة المستخدمين
                    </div>
                </div>
                </a>
            </div>

            <div class="col-sm-6 col-xl-2 pt-3 pb-3 m-2 mainclass hvr-bounce-to-bottom hvr-bounce-to-bottom">
              <a href="/core/apps" style="color:black;">
                <div class=" rounded overflow-hidden gold min-h">
                    <div class="pd-x-20 pd-t-20 text-center">
                        <i class="fa fa-list tx-60 lh-0 tx-white m31 fii"></i><br>
                        تطبيقات الصلاحيات
                    </div>
                </div>
                </a>
            </div>

            <div class="col-sm-6 col-xl-2 pt-3 pb-3 m-2 mainclass hvr-bounce-to-bottom hvr-bounce-to-bottom">
              <a href="/core/groups" style="color:black;">
                <div class=" rounded overflow-hidden gold min-h">
                    <div class="pd-x-20 pd-t-20 text-center">
                        <i class="fa fa-group tx-60 lh-0 tx-white m31 sei"></i><br>
                        مجموعات الصلاحيات
                    </div>
                </div>
                </a>
            </div>

            {{-- <div class="col-sm-6 col-xl-2 pt-3 pb-3 m-2 mainclass hvr-bounce-to-bottom hvr-bounce-to-bottom">
              <a href="/committees/committees" style="color:black;">
                <div class=" rounded overflow-hidden gold min-h">
                    <div class="pd-x-20 pd-t-20 text-center">
                        <i class="fa fa-comments-o tx-60 lh-0 tx-white m31 fri"></i><br>
                        اللجان 
                    </div>
                </div>
                </a>
            </div> --}}

            <div class="col-sm-6 col-xl-2 pt-3 pb-3 m-2 mainclass hvr-bounce-to-bottom hvr-bounce-to-bottom">
              <a href="/core/documents" style="color:black;">
                <div class=" rounded overflow-hidden gold min-h">
                    <div class="pd-x-20 pd-t-20 text-center">
                        <i class="fa fa-file-o tx-60 lh-0 tx-white m31 seci"></i><br>
                        الوثائق
                    </div>
                </div>
                </a>
            </div>

            {{-- <div class="col-sm-6 col-xl-2 pt-3 pb-3 m-2 mainclass hvr-bounce-to-bottom hvr-bounce-to-bottom">
                <a href="/core/setting/create" style="color:black;">
                  <div class=" rounded overflow-hidden gold min-h">
                      <div class="pd-x-20 pd-t-20 text-center">
                          <i class="fa fa-file-o tx-60 lh-0 tx-white m31 seci"></i><br>
                          الإعدادات
                      </div>
                  </div>
                  </a>
              </div> --}}
        </div>
        <br>
        
    </div>

@endsection
