@extends('layouts.main.index')
@section('page')

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5>الوثائق</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">

                        <div  class="alert alert-info">
                            إختر الوثيقة والضغط على حفظ ليتم اضافة الوثيقة
                        </div>

                        {{ Form::open(['route' => ['documents.store'], 'class' => 'form-horizontal', 'method' => 'post', 'files' => true ]) }}

                        <div class="row">
                            <div class="col-md-4 {{$errors->has("file_path") ? 'has-error' : ''}}">

                                {!! Form::file('file_path', array('class' => 'form-control')) !!}

                                <span class="has-error-span"> {{$errors->first("file_path") }}</span>
                            </div>
                            <div class="col-md-4 {{$errors->has("file_name") ? 'has-error' : ''}}">

                                {!! Form::text('file_name', old('file_name'),array('class' => 'form-control', 'placeholder' => 'إسم الوثيقة')) !!}

                                <span class="has-error"> {{$errors->first("file_name") }}</span>

                            </div>
                            <div class="col-md-4">

                                {{ Form::submit('حفظ', ['class' => 'btn btn-info']) }}

                            </div>
                        </div>

                        {{ Form::close() }}

                        <div class="col ownertabelecon mt-4">
                            <table class="table table-bordered tb-b">
                                <thead class="ownertabele">
                                <tr>
                                    <th class="tth"> إسم الوثيقة</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($documents as $document)
                                    <tr>
                                        <th class="ttd">
                                            <a href="/{{ $document->file_path}}">
                                                {{ $document->file_name }}
                                            </a>
                                        </th>

                                        <th class="ttd">
                                            {{ Form::open(['route' => ['documents.destroy', $document->id], 'method' => 'delete', 'id' => $document->id]) }}
                                            <button type="submit"
                                                    onclick="return confirm('متأكد من الحذف؟')"
                                                    class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                            {{ Form::close() }}
                                        </th>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $documents->render() !!}
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <br>
        
            <a class="btn btn-danger" href="{{ route('control') }}"> رجوع</a>
    </div>

@stop
