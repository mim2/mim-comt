@extends('layouts.main.index')
@section('page')

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <span class="sissionPage p-2 acolor">الوثائق</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <div class="col ownertabelecon">
                            <table class="table table-bordered tb-b">
                                <thead class="thead-colored  th-cu">
                                <tr>
                                    <th> إسم الوثيقة</th>
                                    <th>تحميل</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($documents as $document)
                                    <tr>
                                        <td>
                                            <a href="/{{ $document->file_path}}">
                                                {{ $document->file_name }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/{{ $document->file_path}}">
                                                <i class="fa fa-download"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $documents->render() !!}
                        </div>

                        <div class="row">
                            <div class="col text-center">
                                <a href="{{ url()->previous() }}" class="btn btn-danger text-white rounded-0 goBack w-25">
                                    <i class="fa fa-reply fmr-3 vemid"></i> رجوع
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
