@extends('layouts.main.index')

@section('page')

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5>تعديل دور</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">

                        {{ Form::model($member_edit, ['route' => ['jobs.update', $member_edit->id], 'class' => 'form-horizontal', 'method' => 'patch']) }}

                        @include('core::jobs.form')

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
