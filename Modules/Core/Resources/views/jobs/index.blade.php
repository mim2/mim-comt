@extends('layouts.main.index')

@section('page')

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5>الأدوار الوظيفية</h5>

                            <div class="pull-left">
                                <a href="{{ route('jobs.create') }}" class="btn btn-success btn-sm">
                                    <i class="fa fa-plus"></i>إضافة أدوار
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <table class="table table-striped">
                            <tr>
                                <th>#</th>
                                <th>الدور</th>
                                <th>الحالة</th>
                                <th>
                                    <div class="pull-left">
                                    إجراءات
                                    </div>
                                    </th>
                                {{-- <th>
                                    <div class="pull-left">
                                        <a href="{{ route('jobs.create') }}" class="btn btn-success btn-sm">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </th> --}}
                            </tr>

                            @foreach($jobs as $job)

                                <tr>
                                    <td>{{ $job->id }}</td>
                                    <td>{{ $job->name }}</td>

                                    <td>{{ ($job->is_disable == 0 ? 'مفعل' : 'معطل') }}</td>
                                    <td>
                                        {{ Form::open(['route' => ['jobs.destroy', $job->id], 'method' => 'delete', 'id' => $job->id]) }}
                                        <div class="pull-left">
                                        @if ($job->is_disable)
                                            <a href="{{ route('jobs.resume', $job->id) }}"
                                            class="btn btn-danger btn-sm">
                                                <i class="fa fa-play"></i>
                                            </a>
                                        @else
                                            <a href="{{ route('jobs.stop', $job->id) }}"
                                                class="btn btn-primary btn-sm">
                                                <i class="fa fa-stop"></i>
                                            </a>
                                   
                                        @endif
                                    
                                            <a href="{{ route('jobs.edit', $job->id) }}" class="btn btn-warning btn-sm">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            @if ($job->id != 1 && $job->id != 2 && $job->id != 3)
                                             
                                            
                                            <button type="submit" onclick="return confirm('متأكد من الخذف؟')"
                                                    class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                            

                                            @endif
                                        </div>
                                            {{ Form::close() }} 
                                        
                                    </td>
                                </tr>

                            @endforeach

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
