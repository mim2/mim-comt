<div class="form-group">
    {{ Form::label('name', 'اسم الدور', ['class' => 'control-label']) }}
    {{ Form::text('name', old('name', @$member_edit->name) , ['class' => 'form-control', 'required' => true]) }}
</div>

<div class="d-flex justify-content-end mainbuttons mt-4 mb-4">
    {{ Form::submit('حفظ', ['class' => 'btn btn-info']) }}
    <a href="{{ route('jobs.index') }}" class="btn btn-danger">إلغاء</a>
</div>