<div class="form-group">
    {{ Form::label('user_name', 'الإسم كاملا', ['class' => 'control-label']) }}
    {{ Form::text('user_name', old('user_name', @$user->user_name) , ['class' => 'form-control', 'required' => true]) }}
    <span class="text-danger">{{ $errors->first('user_name') }}</span>
</div>


{{-- <div class="form-group">
    {{ Form::label('job_id', 'الوظيفة', ['class' => 'control-label']) }}
    {{ Form::select('job_id', $jobs, old('job_id') , ['class' => 'form-control']) }}
    <span class="text-danger">{{ $errors->first('job_id') }}</span>
</div>

<div class="form-group">
    {{ Form::label('department_id', 'الإدارة', ['class' => 'control-label']) }}
    {{ Form::select('department_id', $departments, old('parent_id') , ['class' => 'form-control']) }}
    <span class="text-danger">{{ $errors->first('department_id') }}</span>
</div> --}}

<div class="form-group">
    {{ Form::label('user_idno', 'رقم الهوية', ['class' => 'control-label']) }}
    {{ Form::text('user_idno', old('user_idno', @$user->user_idno) , ['class' => 'form-control', 'required' => true]) }}
    <span class="text-danger">{{ $errors->first('user_idno') }}</span>
</div>

<div class="form-group">
    {{ Form::label('user_mobile', 'رقم الجوال', ['class' => 'control-label']) }}
    {{ Form::text('user_mobile', old('user_mobile', @$user->user_mobile) , ['class' => 'form-control', 'required' => true]) }}
    <span class="text-danger">{{ $errors->first('user_mobile') }}</span>
</div>

<div class="form-group">
    {{ Form::label('user_mail', 'البريد الإلكتروني', ['class' => 'control-label']) }}
    {{ Form::email('user_mail', old('user_mail', @$user->user_mail) , ['class' => 'form-control', 'required' => true]) }}
    <span class="text-danger">{{ $errors->first('user_mail') }}</span>
</div>



<div class="form-group">
    {{ Form::label('user_dept', 'الإدارة', ['class' => 'control-label']) }}
    {{ Form::select('user_dept', $departments, old('user_dept') , ['class' => 'form-control']) }}
    <span class="text-danger">{{ $errors->first('user_dept') }}</span>
</div>




<div class="form-group">
    {{ Form::label('is_active', 'الحالة', ['class' => 'control-label']) }}
   

    <div class="radio">
        <label class="radio-inline col-md-2">
            {!! Form::radio('is_active', '1', (Input::old('is_active', @$user->is_active) ? 'checked="checked"' : ''), array('id'=>'is_active', 'class'=>'form-check-input')) !!}

            <span class="mr-4">معطل</span></label>
        
            <label class="radio-inline col-md-2">
                {!! Form::radio('is_active', '0', (Input::old('is_active', @$user->is_active) ? 'checked="checked"' : ''), array('id'=>'is_active', 'class'=>'form-check-input')) !!}
    
                <span class="mr-4">مفعل</span></label>
        <span class="text-danger">{{ $errors->first('is_active') }}</span>
    </div>
   
</div>

<div class="d-flex justify-content-end mainbuttons mt-4 mb-4">
    {{ Form::submit('حفظ', ['class' => 'btn btn-info']) }}
    <a href="{{ route('users.index') }}" class="btn btn-danger">إلغاء</a>
</div>