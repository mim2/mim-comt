@extends('layouts.main.index')

@section('page')

    {!! Form::open(['route' => ['user_superadmin', $userData->user_id], 'method' => 'put']) !!}

        <div class="panel panel-default" style="text-align: center">
            <br />
            <div class="panel-heading">صلاحية مدير النظام</div>
        
            <div class="panel-body" style="text-align: center">
                @if($userData->is_super_admin == 1)
                <p style="text-align: center">
                    هل انت متأكد من سحب  صلاحية مدير النظام من  <b>{{ app()->getLocale() == 'ar' ? $userData->user_name : $userData->user_enname }} </b>؟

                </p>
                @else 
                    هل انت متأكد من منح <b>{{ app()->getLocale() == 'ar' ? $userData->user_name : $userData->user_enname }} صلاحية مدير للنظام</b>؟
                @endif
            </div>
        
            <div class="panel-footer">
                <button type="submit" class="btn btn-danger">نعم</button>
                <a href="{{url('core/users')}}" class="btn btn-primary">لا</a>
            </div>
        </div>

    {!! Form::close() !!}

@endsection