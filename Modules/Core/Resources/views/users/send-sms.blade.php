@extends('layouts.main.index')

@section('page')

    {!! Form::open(['route' => ['users.send-sms', $userData->user_id], 'method' => 'put']) !!}

        <div class="panel panel-default" style="text-align: center">
            <br />
            <div class="panel-heading">إرسال بيانات  مستخدم</div>
        
            <div class="panel-body" style="text-align: center">
                
                    هل انت متأكد من إرسال بيانات الدخول ل <b>{{ app()->getLocale() == 'ar' ? $userData->user_name : $userData->user_enname }} </b>؟
            
            </div>
        
            <div class="panel-footer">
                <button type="submit" class="btn btn-danger">نعم</button>
                <a href="{{url('core/users')}}" class="btn btn-primary">لا</a>
            </div>
        </div>

    {!! Form::close() !!}

@endsection