<li style="background: #ebebeb; padding: 10px; padding-bottom: 10px">
    {{ Form::open(['route' => ['departments.destroy', $department->id], 'method' => 'delete', 'id' => $department->id]) }}
    {{ $department->name }}
    <div class="pull-left">
        
        <a href="{{ route('departments.edit', $department->id) }}" class="btn btn-warning btn-sm">
            <i class="fa fa-edit"></i>
        </a>
        <button type="submit" onclick="return confirm('متأكد من الخذف؟')" class="btn btn-danger btn-sm">
            <i class="fa fa-trash-o"></i>
        </button>
    </div>
    {{ Form::close() }}
</li>


