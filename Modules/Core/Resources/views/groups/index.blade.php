@extends('layouts.main.index')

@section('page')
    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5><i class="fa fa-users"></i>{{ __('messages.groups') }}</h5>

                            <div class="pull-right">
                         
                                <a href="{{ route('groups.create') }}"
                                   class="btn btn-primary">{{ __('messages.action_add')}}</a>
                  
                        </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        
                        <table id="table-ajax" class="table panel-body delete-object-modal-table" data-form="deleteForm"
                            data-url="/core/groups"
                            data-fields='[
                                {"data": "name","title":"{{ __('messages.general_name') }}","searchable":"true"},
                                {"data": "action","name":"actions","searchable":"false", "orderable":"false"}
                            ]'>
                        </table>

                        {{ $groupspagination->links() }}

                        
                    </div>
                </div>
                
            </div>
        </div>
        <br>
        
    </div>
@endsection
