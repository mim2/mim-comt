<div class="form-layout form-layout-4 fr-b">
<input type="hidden" name="parent_id" value="0">
  <div class="row {{ $errors->has('name') ? ' has-error' : '' }}">
      <label for="name" class="col-sm-2 form-control-label">{{ __('messages.name') }}</label>

      <div class="col-sm-10 mg-t-10 mg-sm-t-0">
        {{-- <input type="hidden" name="id"> --}}
          {!! Form::text('name',old('name'), ['class' => 'form-control']) !!}
          @if ($errors->has('name'))
              <span class="help-block">
                  <strong>{{ $errors->first('name') }}</strong>
              </span>
          @endif
      </div>
  </div>

<br/>


  <div class="row {{ $errors->has('name_en') ? ' has-error' : '' }}">
      <label for="name_en" class="col-sm-2 form-control-label">{{ __('messages.name_en') }}</label>

      <div class="col-sm-10 mg-t-10 mg-sm-t-0">

          {!! Form::text('name_en',old('name_en'), ['class' => 'form-control']) !!}
          @if ($errors->has('name_en'))
              <span class="help-block">
                  <strong>{{ $errors->first('name_en') }}</strong>
              </span>
          @endif
      </div>
  </div>


<br/>


  <div class="row {{ $errors->has('key') ? ' has-error' : '' }}">
      <label for="key" class="col-sm-2 form-control-label">{{ __('messages.key') }}</label>

      <div class="col-sm-10 mg-t-10 mg-sm-t-0">

          {!! Form::text('key',old('key'), ['class' => 'form-control']) !!}
          @if ($errors->has('key'))
              <span class="help-block">
                  <strong>{{ $errors->first('key') }}</strong>
              </span>
          @endif
      </div>
  </div>

</div>