
@extends('layouts.main.index')

@section('page')


  
    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5>{{ @$group->name}}</h5>
                       
                       
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        
                      <attach-user-to-group group-id="{{ $group->id }}"
                        attach-url="{{ route('attach_user_to_group', $group->id) }}"
                        detach-url="{{ route('detach_user_to_group', ['id' => $group->id, 'userId' => ""]) }}"
                        users-url="{{ route('group_users', $group->id)  }}">
                      </attach-user-to-group>
                        
                        
                    
                  
                        <a href="{{ url()->previous() }}" class="btn btn-danger text-white rounded-0 backbtn"> رجوع</a>
                        <br/>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
