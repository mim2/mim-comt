@extends('layouts.main.index')

@section('page')

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5>{{ __('messages.edit_group') }}</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">

                        {!! Form::model($group,['route' =>  ['groups.update', $group->id ],'method'=>'put' ]) !!}

                        @include('core::groups.form')

                        <div class="d-flex justify-content-end mainbuttons mt-4 mb-4">
                            <input type="submit" class="btn btn-primary btn-lg blue" value="{{ __('messages.save') }}"/>
                            <a href="{{ route('groups.index') }}" class="btn btn-danger">إلغاء</a>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
