<?php

header('Vary:X-Requested-With');

Route::group(['middleware' => 'web', 'prefix' => 'core', 'namespace' => 'Modules\Core\Http\Controllers'], function () {

    // Apps Routes
    Route::get('/control', 'ControlController@index')->name('control');
    Route::get('/apps', 'AppsController@index');
    Route::post('/apps', 'AppsController@store');
    Route::put('/apps/{id}', 'AppsController@update');
    Route::delete('/apps/{id}', 'AppsController@destroy');
    Route::get('/authorized-apps', 'AuthorizedAppsController@index');
    Route::get('/users/{id}/permissions', 'PermissionsController@index')->name('user_permissions');
    Route::get('/users/{id}/edit', 'UsersController@edit')->name('users.edit');
    Route::patch('/users/{id}/update', 'UsersController@update')->name('users.update');
    Route::match(['get', 'put'],'/users/{id}/send-sms', 'UsersController@sendSms')->name('users.send-sms');
    Route::match(['get', 'delete'],'/users/{id}/destroy-page', 'UsersController@destroyPage')->name('users.destroy-page');
    Route::get('/users', 'UsersController@index')->name('users.index');
    Route::get('/users/create', 'UsersController@create')->name('users.create');
    Route::post('/users/store', 'UsersController@store')->name('users.store');
    Route::get('/users-search', 'UsersController@search');
    Route::get('/users/{id}/employee_information', 'UsersController@employeeInformation')->name('user_employee_information');
    Route::match(['get', 'put'], '/users/{id}/superadmin', 'UsersController@makeSuperAdmin')->name('user_superadmin');
    // Permissions Routes
    Route::post('/{permissionable}/{permissionableId}/permissions', 'PermissionsController@store');
    Route::put('/{permissionable}/{permissionableId}/permissions/{id}', 'PermissionsController@update');
    Route::delete('/permissions/{id}', 'PermissionsController@destroy');
    Route::get('/access-levels', 'AccessLevelsController@index');

    //Groups Routes
    Route::resource('/groups', 'GroupsController');
    Route::get('/groups/{id}/permissions', 'PermissionsController@index')->name('group_permissions');
    Route::post('/groups/{id}/attach-user', 'GroupsController@attachUser')->name('attach_user_to_group');
    Route::delete('/groups/{id}/detach-user/{userId}', 'GroupsController@detachUser')->name('detach_user_to_group');
    Route::get('/groups/{id}/users', 'GroupsController@users')->name('group_users');

    Route::resource('departments', 'DepartmentsController');
    
    Route::resource('jobs', 'JobsController');
    Route::get('/jobs/resume/{id}', 'JobsController@resume')->name('jobs.resume');
    Route::get('/jobs/stop/{id}', 'JobsController@stop')->name('jobs.stop');
  //  Route::resource('users', 'UsersController');
    Route::get('user-documents', 'DocumentsController@user');
    Route::resource('documents', 'DocumentsController');

    Route::resource('setting', 'SettingController');
});
