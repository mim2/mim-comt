<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\UserBaseController;
use Modules\Core\Entities\Permission;
use Yajra\Datatables\Datatables;

class ControlController extends UserBaseController
{
    /**
     * Display a listing of the apps.
     * @return Response
     */
    public function index(Request $request)
    {
        return view('core::control.index');
    }
}
