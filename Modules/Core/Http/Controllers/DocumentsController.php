<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\UserBaseController;
use Modules\Core\Entities\Document;
use App\Traits\StoreFile;
use Modules\Core\Http\Requests\StoreDocumentRequest;
use App\Traits\ArabicToEnglish;

class DocumentsController extends UserBaseController
{
    use StoreFile;
    use ArabicToEnglish;

    /**
     * Display a listing of the resource.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        $documents = Document::paginate(25);
        
        $data = ['documents'];

        return view('core::documents.index', compact($data));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Illuminate\Http\Response
     */
    public function user()
    {
        $documents = Document::paginate(25);
        
        $data = ['documents'];

        return view('core::documents.index-for-user', compact($data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Illuminate\Http\Response
     */
    public function store(StoreDocumentRequest $request)
    {

        $file_name=self::arabicNumToEnglish($request->file_name);

        $document = new Document;
        $document->file_name = $file_name;
        $document->file_path = $this->verifyAndStoreFile($request, 'file_path', 'documents');
        $document->save();
        alert()->success('تم  إضافة وثيقة بنجاح !', 'حفظ ناجح')->autoclose(4500);
        return redirect(route('documents.index'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreCommitteeAttendees $request
     * @return Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        $document->delete();
        alert()->success('تم حذف الوثيقة بنجاح !', 'حذف ناجح')->autoclose(4500);
        return redirect(route('documents.index'));
    }
}
