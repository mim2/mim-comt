<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\UserBaseController;
use Modules\Core\Entities\Setting;
use App\Traits\StoreFile;
use Modules\Core\Http\Requests\StoreDocumentRequest;
use App\Traits\ArabicToEnglish;

class SettingController extends UserBaseController
{
    use StoreFile;
    use ArabicToEnglish;

    /**
     * Display a listing of the resource.
     *
     * @return Illuminate\Http\Response
     */
    

    /**
     * Display a listing of the resource.
     *
     * @return Illuminate\Http\Response
     */
    public function create(Setting $setting)
    {
        $setting = Setting::first();
        
        return view('core::setting.create', compact('setting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $findId = Setting::first();

        $setting = new Setting;
        $setting->app_name = $request->app_name;

        if($findId == null ) 
        {
        
        $setting->save();
        
        } else {

            $updateId = Setting::where('id', $findId->id)->first();
            $updateId->update(['app_name' => $request->app_name]);

        }

        alert()->success('تم  التغيير بنجاح !', 'حفظ ناجح')->autoclose(4500);
        return redirect(route('control'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreCommitteeAttendees $request
     * @return Illuminate\Http\Response
     */
    
}
