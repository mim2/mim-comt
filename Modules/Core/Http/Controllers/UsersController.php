<?php

namespace Modules\Core\Http\Controllers;

use App\Http\Controllers\UserBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Modules\Committees\Entities\CommitteeMember;
use Modules\Committees\Entities\Guest;
use Modules\Core\Entities\Department;
use Modules\Core\Entities\Job;
use Modules\Auth\Entities\User;
use Modules\Core\Transformers\User as UserTransformer;
use Modules\Core\Http\Requests\UserRequest;
use Yajra\Datatables\Datatables;
use App\Classes\Sms\Mobily;
use Session;

class UsersController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
       
        if ($request->wantsJson() || $request->ajax()) {
            $users = User::with('department')->select('*');

            return Datatables::of($users)
               ->addColumn('action', function ($user) {
                   return '
                  

                    <a style="color:white;" href="'.route('user_superadmin', ['id' => $user->user_id]).'" class="btn btn-xs btn-'. ($user->is_super_admin == 1 ? 'danger' : 'primary') .' makeSuperAdmin">
                        <i style="color:white;" class="fa fa-key"></i> Admin
                    </a>

                    <a style="color:white;" href="'.route('users.edit', ['id' => $user->user_id]).'" class="btn btn-xs btn-warning">
                    <i style="color:white;" class="fa fa-edit"></i> 
                    </a>
                    
                    <a style="color:white;" href="'.route('users.destroy-page', ['id' => $user->user_id]).'" class="btn btn-xs btn-danger">
                        <i style="color:white;" class="fa fa-trash"></i> 
                    </a>

                    <a style="color:white;" href="' . route('users.send-sms', ['id' => $user->user_id]) . '" 
                    class="btn btn-xs btn-info" title="'. $user->data_send_date .'">
                         إرسال بيانات الدخول
                    </a>


                   

                   

                   

                    

                    ';
               })
               ->make(true);
        } else {
            return view('core::users.index');
        }
    }
    // data_send_date
    public function search(Request $request)
    {
      $users = User::search($request->input('query'))
                   ->select('user_id', 'user_name')
                   ->get();

      return $users;
    }

    

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        // $jobs = Job::all()->pluck('name', 'id');
         $departments = Department::all()->pluck('name', 'id');
        return view('core::users.create')->with('departments', $departments);
    }

    /**
     * Store a newly created resource in storage.
     * @param  UserRequest $request
     * @return Response
     */
    public function store(UserRequest $request)
    {
        $pass = Hash::make($request->password);
        //User::create($request->all() + ['user_name' => $request->user_name, 'password' => $pass]);
        $newUser = New User;
        $newUser->user_name = $request->user_name;
        $newUser->password = $pass;
        $newUser->user_idno = $request->user_idno;
        $newUser->user_mobile = $request->user_mobile;
        $newUser->user_dept = $request->user_dept;
        $newUser->user_mail = $request->user_mail;
        $newUser->gender = 1;
        $newUser->save();

        // if (env('APP_ENV') == 'production') {
        // $massage = 'تم إضافتك لنظام اللجان والمجالس ، فضلاً الدخول على الرابط التالي https://committees.mim.gov.sa/ بوضع رقم الهوية وكلمة المرور 123456';
        // $send = Mobily::send($massage, [$request->user_mobile]);
        // }

        alert()->success('تم إنشاء المستخدم بنجاح !', 'حفظ ناجح')->autoclose(4500);
        return redirect(route('users.index'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param User $user
     * @return Response
     */
    public function edit($id)
    {

        $user = User::find($id);
        $departments = Department::all()->pluck('name', 'id');
        return view('core::users.edit', compact('user', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     * @param  UserRequest $request
     * @param User $user
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
     
        $user->update($request->all());
        
        return redirect(route('users.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param User $user
     * @return Response
     * @throws \Exception
     */
    public function destroy(User $user)
    {
       
        if ($count = CommitteeMember::where('user_id', $user->id)->get()->count()) {
            return back()->withErrors(['لا يمكن حذف المستخدم لأنه عضو في (' . $count . ') لجنة أو مجلس.']);
        }
        if ($count = Guest::where('user_id', $user->id)->get()->count()) {
            return back()->withErrors(['لا يمكن حذف المستخدم لأنه مدعو في (' . $count . ') الإجتماع.']);
        }
        $user->delete();
        return redirect(route('users.index'));
    }

    public function makeSuperAdmin(Request $request, $id)
    {
        $userData = User::findOrFail($id);

        if ($request->isMethod('put')) {
            $userData->is_super_admin = ($userData->is_super_admin == 0 ? 1 : 0);
            $userData->save();
            return redirect('core/users');
        }

        return view('core::users.make-super-admin', compact('userData'));
    }

    public function destroyPage(Request $request, $id)
    {
        $userData = User::findOrFail($id);

        if ($request->isMethod('delete')) {
            
            if ($count = CommitteeMember::where('user_id', $userData->id)->get()->count()) {
                return back()->withErrors(['لا يمكن حذف المستخدم لأنه عضو في (' . $count . ') لجنة أو مجلس.']);
            }
            if ($count = Guest::where('user_id', $userData->id)->get()->count()) {
                return back()->withErrors(['لا يمكن حذف المستخدم لأنه مدعو في (' . $count . ') الإجتماع.']);
            }
            $userData->delete();
            alert()->success('تم حذف المستخدم بنجاح !', 'حذف ناجح')->autoclose(4500);
            return redirect(route('users.index'));
        }

        return view('core::users.destroy-page', compact('userData'));
    }


    public function sendSms(Request $request, $id)
    {
        $userData = User::findOrFail($id);

        if ($request->isMethod('put')) {

            $digits = 6;
            $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
            $update = New User;
            $update->where('user_idno', $userData->user_idno)->update(['password' =>  Hash::make($code), 'data_send_date' => Carbon::now()]);
            $massage = 'تم إضافتك لنظام اللجان والمجالس ، فضلاً الدخول على الرابط التالي https://committees.mim.gov.sa/ بوضع رقم الهوية وكلمة المرور '. $code;
            $send = Mobily::send($massage, [$userData->user_mobile]);

            alert()->success('تم إرسال البيانات للمستخدم بنجاح   !', 'إرسال ناجح')->autoclose(4500);
            return redirect(route('users.index'));
        }

        return view('core::users.send-sms', compact('userData'));
    }
}
