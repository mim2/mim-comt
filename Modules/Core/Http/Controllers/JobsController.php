<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Modules\Core\Entities\Job;
use App\Http\Controllers\UserBaseController;
use Modules\Core\Http\Requests\JobRequest;
use Modules\Committees\Entities\MemberType;

class JobsController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $jobs = MemberType::paginate(25);
        return view('core::jobs.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('core::jobs.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  JobRequest $request
     * @return Response
     */
    public function store(JobRequest $request)
    {
       // dd($request->all());
        MemberType::create($request->all());
        return redirect(route('jobs.index'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param Job $job
     * @return Response
     */
    public function edit($id)
    {

        $member_edit = MemberType::find($id);
        
        return view('core::jobs.edit', compact('member_edit'));
    }

    /**
     * Update the specified resource in storage.
     * @param  JobRequest $request
     * @param Job $job
     * @return Response
     */
    public function update(Request $request, $id)
    {
        
        $member_edit = MemberType::find($id);
        $member_edit->update($request->all());
       
        return redirect(route('jobs.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param Job $job
     * @return Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $member_edit = MemberType::find($id);
        $member_edit->delete();
        return redirect(route('jobs.index'));
    }

    public function stop(Request $request)
    {
        
      
        $job = MemberType::findOrFail($request->id);
        $job->update(['is_disable' => 1]);
        alert()->info('تم ايقاف الدور', 'ايقاف الدور!');
        return redirect(route('jobs.index'));
    }

    /**
     * Set the specified resource to be resumed.
     *
     * @param Committee $committee
     * @return Response
     */
    public function resume(Request $request)
    {
        $job = MemberType::findOrFail($request->id);
        $job->update(['is_disable' => 0]);
        alert()->info('تم تفعيل الدور', 'تفعيل الدور!');
        return redirect(route('jobs.index'));
    }
}
