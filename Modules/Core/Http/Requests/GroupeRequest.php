<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\NameRule;
use App\Rules\DefinedNumber;
use App\Rules\OnlyArabic;
use App\Rules\OnlyEnglish;
class GroupeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', new OnlyArabic()],
            'name_en' => ['required', new OnlyEnglish()],
            'key' => ['required', new DefinedNumber()],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
