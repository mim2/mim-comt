<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\UserNameRule;
class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
     //   $userId = $this->route('user')->id ?? 0;
        // $userName = $this->route('user')->user_name ;
        return [
            'user_name' => 'required',
            'user_mail' => 'required|unique:users,user_mail',
            'user_idno' => 'required|numeric|digits:10|unique:users,user_idno',
            'user_mobile' => 'required|numeric|digits:10',
            'password' => 'required' 
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
