<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name', 'parent_id'];

    public function parent()
    {
        return $this->belongsTo(Department::class, 'dep_pid');
    }

   
}
