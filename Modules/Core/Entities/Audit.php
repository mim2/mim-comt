<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\Committees\Entities\CommitteeMember;

class Audit extends Model
{

    /**
     * Scope a query to only include commitee member.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommitteeMemberAll($query)
    {
        return $query->where('auditable_type', '=', CommitteeMember::class);
    }

    /**
     * Scope a query to only include commitee member by id.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommitteeMemberById($query, $committeeId)
    {
        return $query->committeeMemberAll()
                    ->where(function ($query) use ($committeeId) {
                        $query->where('new_values->committee_id', $committeeId)
                            ->orWhere('old_values->committee_id', $committeeId);
                });
    }

    public static function commiteeMemberForUser($userId, $committeeId = false) 
    {
        return self::committeeMember()
                   ->where(function ($query) use ($userId) {
                        $query->where('new_values->user_id', $userId)
                              ->orWhere('old_values->user_id', $userId);
                   });
    }
}
