<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'es_core_users_groups';
    protected $fillable = ['user_id', 'es_core_group_id'];

    public static function getUsersIds() 
    {
        $consultent = new self;
        $table = $consultent->getTable();
        print_r($table);
    }

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
