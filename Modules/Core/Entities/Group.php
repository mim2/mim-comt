<?php
namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;
use Modules\Core\Entities\Permission;

use Modules\Core\Entities\Employee;
use Modules\Core\Entities\HRDepartment;

class Group extends Model
{
  use \Modules\Core\Traits\SharedModel;
  public $incrementing = true;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'es_core_groups';

  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = ['id','name', 'name_en', 'key'];

  /**
   * unified groups keys
   *
   * @var array
   */
  public static $unifiedGroups = [
    'university' => 'es_university_group',
    'govern_employees' => 'es_university_govern_employees_group',
    'direct_managers' => 'es_univeristy_direct_managers',
    'parent_managers' => 'es_univeristy_parent_managers',
  ];


 /**
  * Get permissions specified for group
  */
 public function permissions()
 {
   return $this->morphMany(Permission::class, 'permissionable');
 }

 /**
  * Get group users
  */
 public function users()
 {
   return $this->belongsToMany(User::class, 'es_core_users_groups', 'es_core_group_id', 'user_id');
 }

 /**
  * Return Table Name
  */
  public static function table()
  {
      return with(new static)->getTable();
  }

  public static function findByKey($key)
  {
    return self::where('key', '=' ,$key)->first();
  }

  public static function findUnifiedGroupByName($name)
  {
    return self::findByKey(self::$unifiedGroups[$name]);
  }

 
  public static function hasUserByKey($key, $user)
  {

    
    $group = self::findByKey($key);
    
    return $group->hasUser($user);
  }

  public function hasUser($user)
  {
    
    $user = $this->users()
                  ->where('users.user_id', '=', $user->user_id)
                  ->first();
    
    return ($user == null ? false : true);
  }



 
/**
 * Group Id incrimen
 * 
 * @return int
 */
  public static function idIncr() {

    $id = (mt_rand(1000, 100000));
    return $id;

  }
}
