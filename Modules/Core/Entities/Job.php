<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = ['name'];
}
