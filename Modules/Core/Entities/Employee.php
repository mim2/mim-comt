<?php
namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\Core\Entities\HRDepartment;
use App;
use Modules\Auth\Entities\Core\User;

class Employee extends Model
{
    use \Modules\Core\Traits\SharedModel;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'erp.INT_GET_ALL_EMPLOYEE_INFO'; // hr_employees has been replaced with ERP.INT_GET_ALL_EMPLOYEE_INFO because it have some issues like it takes 40s in join!!

    public function parent()
    {
        return $this->belongsTo(HRDepartment::class, 'parent_id');
    }

    public function department()
    {
        return $this->belongsTo(HRDepartment::class, 'real_dept_code');
    }

    public function directDepartment()
    {
        return $this->belongsTo(HRDepartment::class, 'dept_code');
    }

    public function departmentsData($maxLevel = 0)
    {
        $departmentsData     = [];
        $currentDepartmentID = 0;
        
        $currentDepartment = $this->directDepartment()->first();
        $departmentsData['level'. $currentDepartmentID] = $currentDepartment;

        while($currentDepartment = optional($currentDepartment)->parent()->first()) {
            $currentDepartmentID++;
            $departmentsData['level'. $currentDepartmentID] = $currentDepartment;

            if($maxLevel != 0 && $currentDepartmentID == ($maxLevel-1)) {
                break;
            }
        }

        return $departmentsData;
    }

    public function user() 
    {
        return $this->belongsTo(User::class, 'national_id', 'user_idno');
    }

    

    

    

    

    

    

    
    
   

   
   

    /**
     * get employee data by employee id
     */
    public static function getEmployeeDataByEmployeeId($employeeId)
    {
        return self::where('employee_id', $employeeId)->first();
    }

    /**
     * get manager of
     */
    

    

    

    

    

    public function getJobInfoAttribute()
    {
        return $this->job_desc . ' - ' . $this->scale_desc;
    }
    
    

   

    
    

}
