<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['file_name', 'file_type'];
}
