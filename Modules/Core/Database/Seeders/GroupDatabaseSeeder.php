<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Report;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class ReportDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // DB::table(Report::table())->truncate();
        DB::table('reports')->truncate();

        DB::table('es_core_groups')->insert([
            'name' => 'تقرير تسجيل مستحقات اللجان والمجالس', 'name_en' => 'يختص باللجان والمجالس', 'key' => 'يختص باللجان والمجالس', 'parent_id' => 'يختص باللجان والمجالس', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        


        

    }
}