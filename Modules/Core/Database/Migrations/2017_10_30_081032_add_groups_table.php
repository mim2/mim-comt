<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('es_core_groups', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name')->unique();
        $table->string('name_en')->unique();
        $table->string('key')->unique()->nullable();
        $table->timestamps();
    });

    DB::table('es_core_groups')->insert([
      ['id' => 1, 'name' => 'مجموعة الأدوار الوظيفية للمستشار', 'name_en' => 'advisor', 'key' => 'advisor'],
      ['id' => 2, 'name' => 'مجموعة الأدوار الوظيفية للمشرف', 'name_en' => 'supervisor', 'key' => 'supervisor'],
      ['id' => 3, 'name' => 'مجموعة الأدوار الوظيفية للمثل', 'name_en' => 'secretary', 'key' => 'secretary'],
      ['id' => 4, 'name' => 'مجموعة الأدوار الوظيفية للأعضاء', 'name_en' => 'members', 'key' => 'members'],
      ['id' => 5, 'name' => 'مجموعة صاحب الصلاحية بالجهة', 'name_en' => 'rector', 'key' => 'rector'],
      ['id' => 6, 'name' => 'مجموعة منشئي اللجان', 'name_en' => 'committes creator', 'key' => 'committes_creator'],
      ['id' => 7, 'name' => 'مجموعة الإطلاع على التقارير ', 'name_en' => 'reports', 'key' => 'reports'],
      
  ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('es_core_groups');
    }
}
