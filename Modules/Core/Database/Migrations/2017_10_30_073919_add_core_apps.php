<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Core\Entities\App;

class AddCoreApps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('es_core_apps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('name_en')->nullable();
            $table->string('resource_name')->unique()->nullable();
            $table->string('icon')->nullable();
            $table->integer('sort')->default(0);
            $table->integer('parent_id')->default(0)->references('id')->on('es_core_apps');
            $table->string('frontend_path')->nullable();
            $table->string('li_color')->nullable();
            $table->boolean('is_main_root')->default(false);
            $table->boolean('displayed_in_menu')->default(false);
            $table->string('menu_type')->nullable();
            $table->timestamps();
        });

        $topParentId =  App::create([
            'resource_name' => 'Modules', 'name' => 'التطبيقات', 'name_en' => 'Apps', 'is_main_root' => 1,
            'icon' => 'icon icon-folder', 'sort' => 1, 'parent_id' => 0, 'frontend_path' => 'index',
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;

        $coreAppsModuleId = App::create([
            'resource_name' => 'Modules\Core\Http\Controllers', 'name' => 'المصادر الرئيسية', 'name_en' => 'Core Apps',
            'icon' => 'icon icon-folder', 'sort' => 1, 'parent_id' => $topParentId, 'frontend_path' => 'core', 'is_main_root' => 0,
            'displayed_in_menu' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;

        $coreAppsId = App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\AppsController', 'name' => 'إدارة التطبيقات', 'name_en' => 'Manage Apps',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $coreAppsModuleId, 'frontend_path' => 'core/apps', 'is_main_root' => 0,
            'displayed_in_menu' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\AppsController@index', 'name' => 'قراءة الكل', 'name_en' => 'Read List',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $coreAppsId, 'frontend_path' => 'core/apps', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\AppsController@show', 'name' => 'قراءة تفاصيل', 'name_en' => 'Read All',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $coreAppsId, 'frontend_path' => 'core/apps', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\AppsController@store', 'name' => 'إضافة', 'name_en' => 'Add',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $coreAppsId, 'frontend_path' => 'core/apps', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\AppsController@update', 'name' => 'تعديل', 'name_en' => 'Edit',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $coreAppsId, 'frontend_path' => 'core/apps', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\AppsController@destroy', 'name' => 'حذف', 'name_en' => 'Delete',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $coreAppsId, 'frontend_path' => 'core/apps', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);


        $usersId = App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\UsersController', 'name' => 'المستخدمين', 'name_en' => 'Manage Users',
            'icon' => 'icon icon-folder', 'sort' => 3, 'parent_id' => $coreAppsModuleId, 'frontend_path' => 'core/users', 'is_main_root' => 0,
            'displayed_in_menu' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;


        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\UsersController@index', 'name' => 'قراءة الكل', 'name_en' => 'Read List',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $usersId, 'frontend_path' => 'core/users', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\UsersController@search', 'name' => 'بحث', 'name_en' => 'Search',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $usersId, 'frontend_path' => 'core/apps', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        $groupsId = App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\GroupsController', 'name' => 'المجموعات', 'name_en' => 'Manage Groups',
            'icon' => 'icon icon-folder', 'sort' => 3, 'parent_id' => $coreAppsModuleId, 'frontend_path' => 'core/groups', 'is_main_root' => 0,
            'displayed_in_menu' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;


        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\GroupsController@index', 'name' => 'قراءة الكل', 'name_en' => 'Read List',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $groupsId, 'frontend_path' => 'core/groups', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\GroupsController@show', 'name' => 'اظهار بيانات', 'name_en' => 'Show details',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $groupsId, 'frontend_path' => 'core/groups', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\GroupsController@create', 'name' => 'صفحة انشاء جديد', 'name_en' => 'Add New Page',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $groupsId, 'frontend_path' => 'core/groups', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\GroupsController@store', 'name' => 'انشاء جديد', 'name_en' => 'Store Action',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $groupsId, 'frontend_path' => 'core/groups', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\GroupsController@edit', 'name' => 'صفحة التعديل', 'name_en' => 'ِEdit Page',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $groupsId, 'frontend_path' => 'core/groups', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\GroupsController@update', 'name' => 'تحديث', 'name_en' => 'Update Action',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $groupsId, 'frontend_path' => 'core/groups', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\GroupsController@destroy', 'name' => 'حذف', 'name_en' => 'Delete',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $groupsId, 'frontend_path' => 'core/groups', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\GroupsController@attachUser', 'name' => 'اضافة مستخدم', 'name_en' => 'Add User',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $groupsId, 'frontend_path' => 'core/groups', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\GroupsController@detachUser', 'name' => 'حذف مستخدم من المجموعة', 'name_en' => 'Delete User',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $groupsId, 'frontend_path' => 'core/groups', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);


        $permissionsId = App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\PermissionsController', 'name' => 'الصلاحيات', 'name_en' => 'Manage Permissions',
            'icon' => 'icon icon-folder', 'sort' => 3, 'parent_id' => $coreAppsModuleId, 'frontend_path' => 'core/permissions', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;


        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\PermissionsController@index', 'name' => 'قراءة الكل', 'name_en' => 'Read List',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $permissionsId, 'frontend_path' => 'core/permissions', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\PermissionsController@store', 'name' => 'اضافة', 'name_en' => 'Add',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $permissionsId, 'frontend_path' => 'core/permissions', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\PermissionsController@update', 'name' => 'تحديث', 'name_en' => 'Update',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $permissionsId, 'frontend_path' => 'core/permissions', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\PermissionsController@destroy', 'name' => 'حذف', 'name_en' => 'Delete',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $permissionsId, 'frontend_path' => 'core/permissions', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\AccessLevelsController@index', 'name' => 'مستويات الصلاحية', 'name_en' => 'Access Level',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $permissionsId, 'frontend_path' => 'core/permissions', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        $committeesAndCouncel = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\CommitteesController', 'name' => ' اللجان والمجالس', 'name_en' => ' committee update',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $topParentId, 'frontend_path' => 'committees/committees/{committee}', 'is_main_root' => 0,
            'displayed_in_menu' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        $committees = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\CommitteesController@index', 'name' => '   اللجان ', 'name_en' => ' committee show ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committeesAndCouncel, 'frontend_path' => 'committees/committees/{committee}', 'is_main_root' => 0,
            'displayed_in_menu' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\CommitteesController@show', 'name' => ' عرض لجنة او مجلس', 'name_en' => ' committee show ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committees, 'frontend_path' => 'committees/committees/{committee}', 'is_main_root' => 0,
            'displayed_in_menu' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\CommitteesController@update', 'name' => 'تحديث الجنة', 'name_en' => 'committee show',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committees, 'frontend_path' => 'committees/committees/{committee}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\CommitteesController@create', 'name' => 'انشاء الجنة', 'name_en' => 'committee show',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committees, 'frontend_path' => 'committees/committees/{committee}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\CommitteesController@destroy', 'name' => 'حذف الجنة', 'name_en' => 'committee show',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committees, 'frontend_path' => 'committees/committees/{committee}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\CommitteesController@store', 'name' => 'تخزين الجنة', 'name_en' => 'committee show',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committees, 'frontend_path' => 'committees/committees/{committee}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\CommitteesController@getEmployees', 'name' => ' بحث عن الاعضاء', 'name_en' => 'get emp',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committees, 'frontend_path' => '/employees/getEmployees/', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\CommitteesController@edit', 'name' => 'تعديل الجنة', 'name_en' => 'committee show',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committees, 'frontend_path' => 'committees/committees/{committee}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\CommitteesController@resume', 'name' => 'تفعيل الجنة', 'name_en' => 'committee resume',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committees, 'frontend_path' => 'committees/committees/resume/{committee}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\CommitteesController@stop', 'name' => 'ايقاف الجنة', 'name_en' => 'committee stop',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committees, 'frontend_path' => 'committees/committees/stop', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        $sessionFiles = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionFilesController@index', 'name' => 'ملفات الجلسة', 'name_en' => 'session files',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committeesAndCouncel, 'frontend_path' => 'committees/committees/{committee}/sessions/{session}/files', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionFilesController@store', 'name' => ' اضافة ملفات الجلسة ', 'name_en' => 'store session files',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionFiles, 'frontend_path' => 'committees/committees/{committee}/sessions/{session}/files', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionFilesController@delete', 'name' => ' حذف ملفات الجلسة ', 'name_en' => 'delete session files',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionFiles, 'frontend_path' => 'committees/committees/{committee}/sessions/{session}/files/{file}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionFilesController', 'name' => ' الكل ', 'name_en' => 'all',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionFiles, 'frontend_path' => 'committees/committees/{committee}/sessions/{session}/files/{file}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        $sessions = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsController@index', 'name' => ' الجلسات ', 'name_en' => 'sessions',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committeesAndCouncel, 'frontend_path' => 'committees/committees/{committee}/sessions', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsController', 'name' => ' الكل ', 'name_en' => 'all',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessions, 'frontend_path' => 'committees/committees/{committee}/sessions', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsController@create', 'name' => ' انشاء جلسة ', 'name_en' => 'create session',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessions, 'frontend_path' => 'committees/committees/{committee}/sessions/create', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsController@store', 'name' => ' تخزين جلسة ', 'name_en' => 'create session',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessions, 'frontend_path' => 'committees/committees/{committee}/sessions/create', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsController@show', 'name' => ' عرض جلسة ', 'name_en' => 'show session',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessions, 'frontend_path' => 'committees/committees/sessions/{session}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsController@edit', 'name' => ' تعديل جلسة ', 'name_en' => 'edit session',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessions, 'frontend_path' => 'committees/committees/sessions/{session}/edit', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsController@update', 'name' => ' تحديث جلسة ', 'name_en' => 'update session',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessions, 'frontend_path' => 'committees/committees/sessions/{session}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsController@destroy', 'name' => ' حذف جلسة ', 'name_en' => 'delete session',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessions, 'frontend_path' => 'committees/committees/sessions/{session}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsController@send', 'name' => ' ارسال جلسة ', 'name_en' => 'send session',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessions, 'frontend_path' => 'committees/sessions/{session}/send', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsController@elevate', 'name' => ' اعتماد جلسة ', 'name_en' => 'elevate session',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessions, 'frontend_path' => 'committees/sessions/{session}/elevate', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        $sessionManager = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsManagerController', 'name' => ' ادارة جلسة ', 'name_en' => 'manage session',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committeesAndCouncel, 'frontend_path' => 'committees/sessions/manage/committees/{committee}	', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsManagerController@index', 'name' => ' عرض  صفحة ادارة الجلسات ', 'name_en' => 'manage session index',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionManager, 'frontend_path' => 'committees/sessions/manage/committees/{committee}	', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsManagerController@show', 'name' => ' عرض  صفحة ادارة جلسة ', 'name_en' => 'manage session show',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionManager, 'frontend_path' => 'committees/sessions/manage/show/{session}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsManagerController@signed', 'name' => ' اعتماد محضر الجلسة ', 'name_en' => 'manage session signe',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionManager, 'frontend_path' => 'committees/sessions/manage/signed', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionsManagerController@store', 'name' => ' اضافة جلسة ', 'name_en' => 'manage session store',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionManager, 'frontend_path' => 'committees/sessions/manage/store', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        $sessionsSubjects = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SubjectsController', 'name' => '  موضوعات الجلسات الكل ', 'name_en' => 'all session subjects ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessions, 'frontend_path' => 'committees/committees/{committee}/sessions/{session}/subjects', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SubjectsController@index', 'name' => '  موضوعات الجلسات  ', 'name_en' => ' session subjects ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsSubjects, 'frontend_path' => 'committees/committees/{committee}/sessions/{session}/subjects', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SubjectsController@create', 'name' => '   انشاء موضوع للجلسة  ', 'name_en' => ' create subjict ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsSubjects, 'frontend_path' => 'committees/sessions/{session}/subjects/create', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SubjectsController@store', 'name' => '   اضافة موضوع للجلسة  ', 'name_en' => ' store subject ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsSubjects, 'frontend_path' => 'committees/sessions/{session}/subjects', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SubjectsController@edit', 'name' => '   تعديل موضوع للجلسة  ', 'name_en' => ' edit subject ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsSubjects, 'frontend_path' => 'committees/sessions/subjects/{subject}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SubjectsController@update', 'name' => '   تحديث موضوع للجلسة  ', 'name_en' => ' update subject ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsSubjects, 'frontend_path' => 'committees/sessions/subjects/{subject}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SubjectsController@destroy', 'name' => '   حذف موضوع للجلسة  ', 'name_en' => ' delete subject ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsSubjects, 'frontend_path' => 'committees/sessions/subjects/{subject}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        $sessionGuests = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\GuestsController', 'name' => '   اعضاء الجلسات  ', 'name_en' => ' all session guests ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessions, 'frontend_path' => 'committees/committees/{committee}/sessions/{session}/guests', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\GuestsController@index', 'name' => '   صفحة اعضاء الجلسة  ', 'name_en' => '  session guests index ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionGuests, 'frontend_path' => 'committees/committees/{committee}/sessions/{session}/guests', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\GuestsController@store', 'name' => '   تخزين اعضاء الجلسة  ', 'name_en' => '  session guests store ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionGuests, 'frontend_path' => 'committees/sessions/{session}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\GuestsController@destroy', 'name' => '   حذف اعضاء الجلسة  ', 'name_en' => '  session guests delete ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionGuests, 'frontend_path' => 'committees/sessions/{guest}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        $sessionAttendees = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionAttendeesController', 'name' => '   الحاضرين للجلسة  ', 'name_en' => '   session attendees ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessions, 'frontend_path' => 'committees/committees/{committee}/sessions/{session}/attendees', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionAttendeesController@index', 'name' => '   عرض الحاضرين للجلسة  ', 'name_en' => '  session attendees index ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionAttendees, 'frontend_path' => 'committees/committees/{committee}/sessions/{session}/attendees', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\SessionAttendeesController@store', 'name' => '   تخزين الحاضرين للجلسة  ', 'name_en' => '  session attendees store ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionAttendees, 'frontend_path' => 'committees/committees/{committee}/sessions/{session}/attendees', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        $sessionRecommendation = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\RecommendationsController', 'name' => '    توصيات الجلسة  ', 'name_en' => '  session rcommendation  ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessions, 'frontend_path' => 'committees/sessions/{session}/recommendations', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\RecommendationsController@edit', 'name' => '   تعديل توصيات الجلسة  ', 'name_en' => '  session rcommendation edit ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionRecommendation, 'frontend_path' => 'committees/sessions/{session}recommendations', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\RecommendationsController@store', 'name' => '   تخزين توصيات الجلسة للجلسة  ', 'name_en' => '  session rcommendation store ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionRecommendation, 'frontend_path' => 'committees/sessions/{session}/recommendations', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\RecommendationsController@update', 'name' => '   تحديث توصيات الجلسة للجلسة  ', 'name_en' => '  session rcommendation store ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionRecommendation, 'frontend_path' => 'committees/sessions/{session}/recommendations', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        $messages = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\MessagesController', 'name' => '  المراسلات ', 'name_en' => '  messages ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committeesAndCouncel, 'frontend_path' => 'committees/messages', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\MessagesController@index', 'name' => '  عرض المراسلات ', 'name_en' => '  messages index  ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $messages, 'frontend_path' => 'committees/messages', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\MessagesController@show', 'name' => '  عرض مراسلة ', 'name_en' => '  show messages  ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $messages, 'frontend_path' => 'committees/messages/{id}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        $Reports = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\ReportsController', 'name' => '  التقارير ', 'name_en' => '  reports ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committeesAndCouncel, 'frontend_path' => 'committees/reports', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\ReportsController@index', 'name' => '  عرض التقارير ', 'name_en' => '  reports index ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $Reports, 'frontend_path' => 'committees/reports', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\ReportsController@committeesReports', 'name' => '   تقارير اللجان ', 'name_en' => '  committee reports ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $Reports, 'frontend_path' => 'committees/reports/committees-reports', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\ReportsController@sessionsReports', 'name' => '   تقارير الجلسات ', 'name_en' => '  session reports ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $Reports, 'frontend_path' => 'committees/reports/sessions-reports', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\ReportsController@membersReports', 'name' => '   تقارير الاعضاء ', 'name_en' => '  member reports ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $Reports, 'frontend_path' => 'committees/reports/members-reports', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        $committeesMembers = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\MemberCommitteesController', 'name' => '  اعضاء اللجان ', 'name_en' => '  committees members ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committeesAndCouncel, 'frontend_path' => 'committees/member/committees', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\MemberCommitteesController@show', 'name' => '  عرض اعضاء اللجان ', 'name_en' => '  committees members show ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committeesMembers, 'frontend_path' => 'committees/member/committees/{committee}/session', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\MemberCommitteesController@index', 'name' => '  عرض اعضاء اللجان ', 'name_en' => '  committees members show ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committeesMembers, 'frontend_path' => 'committees/member/committees/{committee}/session', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\MemberCommitteesController@store', 'name' => '  تخزين اعضاء اللجان ', 'name_en' => '  committees members store ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committeesMembers, 'frontend_path' => 'committees/member/committees/{committee}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        $sessionsMembers = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\MemberSessionsController', 'name' => '  اعضاء الجلسات ', 'name_en' => '  sessions members ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committeesAndCouncel, 'frontend_path' => 'committees/member/sessions', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\MemberSessionsController@index', 'name' => ' عرض اعضاء الجلسات ', 'name_en' => '  sessions members index ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsMembers, 'frontend_path' => 'committees/member/sessions', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\MemberSessionsController@pending', 'name' => ' محاضر تنتظر التوقيع', 'name_en' => '  sessions members pending ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsMembers, 'frontend_path' => 'committees/member/sessions/pending', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\MemberSessionsController@approved', 'name' => ' المحاضر المعتمدة', 'name_en' => '  sessions members approved ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsMembers, 'frontend_path' => 'committees/member/sessions/approved', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\MemberSessionsController@show', 'name' => ' عرض المحضر', 'name_en' => '  sessions members show ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsMembers, 'frontend_path' => 'committees/member/sessions/{session}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\MemberSessionsController@print', 'name' => ' طباعة المحضر', 'name_en' => '  sessions members print ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsMembers, 'frontend_path' => 'committees/member/sessions/{session}/print', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\GuestSessionsController@opinion', 'name' => ' الرأي علي المحضر', 'name_en' => '  sessions members opinion ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsMembers, 'frontend_path' => 'committees/sessions/{session}/opinion', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        $documents = App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\DocumentsController', 'name' => '  الوثائق ', 'name_en' => '  documents ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committeesAndCouncel, 'frontend_path' => 'core/documents', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\DocumentsController@index', 'name' => ' عرض الوثائق ', 'name_en' => '  documents index ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $documents, 'frontend_path' => 'core/documents/create', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\DocumentsController@create', 'name' => ' انشاء الوثائق ', 'name_en' => '  documents create ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $documents, 'frontend_path' => 'core/documents', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\DocumentsController@store', 'name' => ' تخزين الوثائق ', 'name_en' => '  documents store ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $documents, 'frontend_path' => 'core/documents', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\DocumentsController@show', 'name' => ' عرض الوثائق ', 'name_en' => '  documents show ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $documents, 'frontend_path' => 'core/documents/{document}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\DocumentsController@edit', 'name' => ' تعديل الوثائق ', 'name_en' => '  documents edit ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $documents, 'frontend_path' => 'core/documents/{document}/edit', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\DocumentsController@update', 'name' => ' تحديث الوثائق ', 'name_en' => '  documents update ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $documents, 'frontend_path' => 'core/documents/{document}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Core\Http\Controllers\DocumentsController@destroy', 'name' => ' حذف الوثائق ', 'name_en' => '  documents delete ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $documents, 'frontend_path' => 'core/documents/{document}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        $sessionsApproval = App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\ApprovalSessionsController', 'name' => '  اعتماد  الجلسات وصفحة صاحب الصلاحية ', 'name_en' => '  sessions approve ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $committeesAndCouncel, 'frontend_path' => 'committees/approval/sessions', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ])->id;
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\ApprovalSessionsController@index', 'name' => '   صفحة صاحب الصلاحية ', 'name_en' => '  sessions approve ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsApproval, 'frontend_path' => 'committees/approval/sessions', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\ApprovalSessionsController@pending', 'name' => '     الجلسات المعتمدة ', 'name_en' => '  sessions approved ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsApproval, 'frontend_path' => 'committees/approval/sessions/approved', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\ApprovalSessionsController@years', 'name' => '     الجلسات للسنوات  السابقة ', 'name_en' => '  sessions in last year  ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsApproval, 'frontend_path' => 'committees/approval/sessions/years', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\ApprovalSessionsController@show', 'name' => ' عرض الجلسات لصاحب الصلاحية', 'name_en' => '  sessions show  ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsApproval, 'frontend_path' => 'committees/approval/sessions/{session}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\ApprovalSessionsController@print', 'name' => '   طباعة  ', 'name_en' => '  sessions print  ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsApproval, 'frontend_path' => 'committees/approval/sessions/{session}/print', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        App::create([
            'resource_name' => 'Modules\Committees\Http\Controllers\ApprovalSessionsController@update', 'name' => '   تحديث   ', 'name_en' => '  sessions update  ',
            'icon' => 'icon icon-folder', 'sort' => 2, 'parent_id' => $sessionsApproval, 'frontend_path' => 'committees/approval/sessions/{session}', 'is_main_root' => 0,
            'displayed_in_menu' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('es_core_apps');
    }
}
