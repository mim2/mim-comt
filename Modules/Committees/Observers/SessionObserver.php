<?php

namespace Modules\Committees\Observers;


use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\SessionStatusHistory;
use Illuminate\Support\Facades\Auth;

class SessionObserver
{
    /**
     * Handle to the User "created" event.
     *
     * @param Session $session
     * @return void
     */
    public function created(Session $session)
    {
        SessionStatusHistory::create([
            'session_id' => $session->id,
            'status_id' => 1,
            'user_id' => Auth::user()->user_id,
        ]);
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  Session $session
     * @return void
     */
    public function updated(Session $session)
    {
        if ($session->getOriginal('status_id') != $session->status_id) {
            SessionStatusHistory::create([
                'session_id' => $session->id,
                'status_id' => $session->status_id,
                'user_id' => Auth::user()->user_id,
            ]);
        }
    }
}
