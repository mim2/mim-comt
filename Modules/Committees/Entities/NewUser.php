<?php

namespace Modules\Committees\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class NewUser extends Model
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'users';

    protected $fillable = [
        'name', 'user_name', 'full_name', 'user_idno', 'job_id', 'department_id', 'email', 'password', 'gender', 'national_id', 'is_active'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // protected $primaryKey = 'user_id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = ['deleted_at'];

    /**
     * Scope a query to only active users
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */




    /**
     * Search for user by query token
     * @param $query token search
     * @return Collection users
     */
    public static function search($query)
    {
        return self::where('user_name', 'LIKE', '%'.$query.'%')
                  ->orWhere('user_enname', 'LIKE', '%'.$query.'%')
                  ->orWhere('user_mail', 'LIKE', '%'.$query.'%')
                  ->orWhere('user_mobile', 'LIKE', '%'.$query.'%')
                  ->orWhere('user_idno', 'LIKE', '%'.$query.'%');
    }
}