<?php

namespace Modules\Committees\Entities;

// use App\User;
use Illuminate\Database\Eloquent\Model;

class SessionStatusHistory extends Model
{

    protected $fillable = [
        'user_id', 'status_id', 'session_id'
    ];
    protected $guarded = [];
    protected $table = 'session_status_history';



    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function status()
    {
        return $this->belongsTo(SessionStatus::class);
    }
}
