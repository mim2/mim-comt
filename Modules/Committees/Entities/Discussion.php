<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    protected $table = 'mim_discussions';
   
    protected $fillable = ['discussions', 'commettees_id' , 'session_id'];
}
