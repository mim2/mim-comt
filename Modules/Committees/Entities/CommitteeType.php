<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;
use ProAI\Versioning\SoftDeletes;

class CommitteeType extends Model
{
    use SoftDeletes;
    protected $fillable = [];
}
