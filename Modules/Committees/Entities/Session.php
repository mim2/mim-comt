<?php

namespace Modules\Committees\Entities;

use App\Classes\Date\CarbonHijri;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class Session extends Model
{

    // protected $dates = ['date'];

    const Hours = [
         'select',
         '08:00 AM',
         '08:30 AM',
         '09:00 AM',
         '09:30 AM',
         '10:00 AM',
         '10:30 AM',
         '11:00 AM',
         '11:30 AM',
         '12:00 PM',
         '12:30 PM',
         '01:00 PM',
         '01:30 PM',
         '02:00 PM',
         '02:30 PM',
         '03:00 PM',
         '03:30 PM',
         '04:00 PM',
         '04:30 PM',
         '05:00 PM',
         '05:30 PM',
         '06:00 PM',
         '06:30 PM',
         '07:00 PM',
         '07:30 PM',
         '08:00 PM',
         '08:30 PM',
         '09:00 PM',
         '09:30 PM',
         '10:00 PM',
         '10:30 PM',
         '11:00 PM',
         '11:30 PM',
    ];
    protected $fillable = [
        'start_time', 'end_time', 'date', 'place', 'session_title_id', 'committee_id',
        'academic_year_id', 'user_id', 'notes', 'recommendations', 'status_id'
    ];

    protected $appends = ['title'];

    protected $time = ['start_time'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($session) {
            $session->files()->delete();
            $session->opinions()->delete();
            $session->subjects()->delete();
            $session->attendees()->delete();
            $session->guests()->delete();
            $session->history()->delete();
        });
    }

    // Relations

    public function committee()
    {
        return $this->belongsTo(Committee::class);
    }
    public function committeeMember()
    {
        return $this->belongsTo(CommitteeMember::class);
    }
    public function sessionTitle()
    {
        return $this->belongsTo(SessionTitle::class, 'session_title_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(SessionStatus::class);
    }

    public function history()
    {
        return $this->hasMany(SessionStatusHistory::class);
    }

    public function guests()
    {
        return $this->hasMany(Guest::class);
    }

    public function attendees()
    {
        return $this->hasMany(SessionAttendance::class);
    }

    public function subjects()
    {
        return $this->hasMany(Subject::class);
    }

    public function recommendation()
    {
        return $this->belongsTo(Recommendation::class, 'id', 'session_id');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function opinions()
    {
        return $this->hasMany(Opinion::class);
    }

    // Attributes

    public function getAttributeStartTimeHijri()
    {
        return CarbonHijri::toHijriFromMiladi($this->start_date, 'Y-m-d');
    }

    public function getTitleAttribute()
    {
        return $this->sessionTitle->name;
        $title = SessionTitle::where('id',$this->session_title_id)->first();
        return $title->name;
    }



    public function isSigned()
    {
        return $this->opinions->count() == $this->attendees->where('attendance_status_id', 1)->count() && $this->attendees->count();
    }



    public function hasOpinion()
    {
        return $this->opinions()
            ->where('opinion', '=', 1)
            ->get();
    }



    public function getAttendee($memberId)
    {
        return $this->attendees->filter(function ($attendee) use ($memberId) {
            return $attendee->user_id == $memberId;
        })->first();
    }

    public function getGuestAttendee($guestId)
    {
        return $this->attendees->filter(function ($attendee) use ($guestId) {
            return $attendee->user_id == $guestId;
        })->first();
    }

    public function files()
    {
        return $this->hasMany(SessionFile::class);
    }

    public static function deleteSessionIfNew()
    {
        $dayAgo = 2;
        $dayToCheck = Carbon::now()->subDays($dayAgo);
        $attendeeOpinion = DB::table('sessions')->where('sessions.status_id', 2)->whereDate('sessions.created_at',  $dayToCheck)->delete();
    }



    public function scopeTitle(Builder $query, $date): Builder
    {
        return $this->title;
        return $query->where('decision_name', $date);
    }



}
