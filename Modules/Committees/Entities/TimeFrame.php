<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;

class TimeFrame extends Model
{
    protected $table = 'mim_time_frames';

    protected $fillable = [];
}
