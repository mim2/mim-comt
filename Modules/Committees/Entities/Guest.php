<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;
use Modules\Committees\Entities\NewUser;
use Validator;
use Illuminate\Support\Facades\Auth;

class Guest extends Model
{
    protected $fillable = [
        'session_id',
        'attendance_status_id',
        'opinion',
        'name',
        'national_id',
        'email',
        'user_id',
        'created_by',
        'note'
    ];

    public static function createGuestRequest($request, $session)
    {
        foreach ($request->guests as $guest) {
            if (!isset($guest['subjects']) or !is_array($guest['subjects'])) {
                return redirect()->back()
                    ->withErrors('يجب اختيار موضوع الإجتماع');
            }

            $validator = Validator::make(
                $guest,
                [
                    'national_id' => 'required|national_id|unique:guests,national_id,NULL,id,session_id,' . $session->id,
                    'name' =>  "required|regex:/^[a-zA-Z0-9\p{Arabic}_]+ [a-zA-Z0-9\p{Arabic}_]+ [a-zA-Z0-9\p{Arabic}_]+$/u|unique:guests,name,NULL,id,session_id," . $session->id,
                    'email' => 'required|email|unique:guests,email,NULL,id,session_id,' . $session->id
                ]
            );


            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $validator->validate();
            }
            $customMessages = [
                'national_id' => 'الرجاء اخال رقم هوية صالح'
            ];

            $guestObject = Guest::create([
                'session_id' => $session->id,
                'note' => $request->note,
                'user_id' => Guest::userIdIncrement(),
                'name' => $guest['name'] ?? null,
                'email' => $guest['email'] ?? null,
                'national_id' => $guest['national_id'] ?? null,
                'created_by' => Auth::user()->user_id,
            ]);
            foreach ($guest['subjects'] as $subjectId) {
                GuestSubject::create([
                    'guest_id' => $guestObject->id,
                    'subject_id' => $subjectId,
                ]);
            }
            
            alert()->success('تم اضافة العضو بنجاح!', 'اضافة عضو')->autoclose(4500);

        }
    }

    public static function createExistUser($request, $session)
    {
        foreach ($request->guests ?? [] as $guest) {
            if (!isset($guest['subjects']) or !is_array($guest['subjects'])) {
                continue;
            }

            $validator = Validator::make($guest, [
                'name' =>  'required|regex:/^[\pL\s\-\.]+$/u|unique:guests,name,NULL,id,session_id,' . $session->id,
                'email' => 'required|unique:guests,email,NULL,id,session_id,' . $session->id
            ]);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $validator->validate();
            }

            $guestObject = Guest::create([
                'session_id' => $session->id,
                'note' => $request->note,
                'user_id' => $guest['user_id'],
                'name' => $guest['name'] ?? null,
                'email' => $guest['email'] ?? null,
                'national_id' => $guest['national_id'] ?? null,
                'created_by' => Auth::user()->user_id,
            ]);

            foreach ($guest['subjects'] as $subjectId) {
                GuestSubject::create([
                    'guest_id' => $guestObject->id,
                    'subject_id' => $subjectId,
                ]);
            }
            alert()->success('تم اضافة العضو بنجاح!', 'اضافة عضو')->autoclose(4500);
            // alert()->info('  الرجاء اضافة عضو');
            return back();
        }
    }

    public static function userIdIncrement()
    {
        $incr = (mt_rand(10, 100000));
        $user_id = ++$incr;
        return $user_id;
    }

    public function session()
    {
        return $this->belongsTo(Session::class);
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * Department join
     * 
     */
    public function department()
    {
        return $this->belongsTo(Department::class, 'user_dept', 'dep_no');
    }


    /**
     * The participant that belong to the gust.
     */
    public function participants()
    {
        return $this->belongsToMany(Participant::class, 'user_id');
    }



    public function session_attendance()
    {
        return $this->hasMany(SessionAttendance::class);
    }



    public function displayForGuestAttendance($attendeeGuest)
    {
        if ($attendeeGuest == null && $this->trashed()) {
            return false;
        }

        return true;
    }
}
