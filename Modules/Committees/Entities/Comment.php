<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;

class Comment extends Model
{
    protected $fillable = ['subject_id', 'user_id', 'comment'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
