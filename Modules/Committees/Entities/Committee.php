<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;
use ProAI\Versioning\Versionable;
use ProAI\Versioning\SoftDeletes;
use Modules\Committees\Entities\CommitteeMember;

class Committee extends Model
{
    use Versionable, SoftDeletes;
    protected $dates = ['start_date', 'end_date'];


    protected $fillable = [
        'id',
        'parent_id',
        'type_id',
        'committee_or_council',

        'name',
        'description',
        'decision_name',
        'decision_url',
        'user_id',
        'start_date',
        'end_date',

        'stop_reason',
        'is_stopped',
        'stop_date',
        'resume_date',
        'category',
        'privacy'
    ];

    protected $versioned = [
        'parent_id',
        'type_id',
        'committee_or_council',
        'name',
        'description',
        'decision_name',
        'decision_url',
        'user_id',
        'start_date',
        'end_date',
        'stop_reason',
        'is_stopped',
        'stop_date',
        'resume_date',
        'category',
        'privacy',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Get committee's type
     */
    public function type()
    {
        return $this->belongsTo(CommitteeType::class);
    }

    /**
     * Get main committees as select options.
     *
     * @return array
     */
    static public function mainCommittees()
    {
        return self::where('parent_id', 0)->where('type_id', 1)->get(['id', 'name'])->pluck('name', 'id');
    }
   

    static public function mainSession()
    {
        return self::where('parent_id', 0)->where('type_id', 2)->get()->pluck('name', 'id');
    }

    public function members()
    {
        return $this->hasMany(CommitteeMember::class)->orderBy('member_type_id', 'asc');
    }

    public function getManagerAttribute()
    {
        return CommitteeMember::where('committee_id', $this->id)->where('member_type_id', 1)->first();
    }

    public function getViceManagerAttribute()
    {
        return CommitteeMember::where('committee_id', $this->id)->where('member_type_id', 2)->first();
    }

    public function authorize()
    {
      
        return $this->hasMany(Authorize::class, 'committee_id', 'id');
    }

    /**
     * The committees that belong to the members.
     */
    public function committeeMember()
    {
        return $this->belongsToMany(CommitteeMember::class, 'committee_member');
    }

    public function managingMemberIds()
    {

        
            return $this->members->whereIn('member_type_id', [1, 2, 4])->pluck('user_id');
    }

    public function managingMemberIdsForSessions()
    {
        return $this->members->whereIn('member_type_id', [1, 2, 4, 5, 6, 7])->pluck('user_id');
    }

    public function managingMemberIdsForSign()
    {
        return $this->members->whereIn('member_type_id', [1, 2, 3, 4, 5, 6, 7])->pluck('user_id');
    }

    public function managingMemberIdsForRecomendations()
    {
        return $this->members->whereIn('member_type_id', [1, 4, 7, 5, 6, 2, 3])->pluck('user_id');
    }

    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    public function sessionSequence()
    {
        return $this->hasMany(Session::class)->orderBy('session_title_id', 'desc');

    }
    

    public function canAddSessions()
    {
        return !$this->is_stopped && $this->sessions->count() < SessionTitle::count();
    }
    public function cfiles()
    {
        return $this->hasMany(CommitteeFile::class);
    }

    public static function updateRequest($request, $committee)
    {
        CommitteeMember::viceUpdateRequest($request, $committee);
        CommitteeMember::managerUpdateRequest($request, $committee);
        CommitteeMember::memberUpdateRequest($request, $committee);
    }

    // this is a recommended way to declare event handlers
    public static function boot()
    {
        parent::boot();

        static::deleting(function ($committee) { // before delete() method call this
            $committee->sessions()->delete();
            $committee->cfiles()->delete();
            $committee->members()->delete();
        });
    }


    // Scopes


    public function scopeType(Builder $query, $date): Builder
    {
        return $query->where('committee_or_council', $date);
    }

    public function scopeDecision(Builder $query, $date): Builder
    {
        return $query->where('decision_name', $date);
    }


}    // class
