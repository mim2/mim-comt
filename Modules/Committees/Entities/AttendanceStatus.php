<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class AttendanceStatus extends Model
{
    use SoftDeletes;

    protected $fillable = [];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}
