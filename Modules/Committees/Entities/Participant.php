<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Committees\Entities\Guest;
use Modules\Auth\Entities\User;


class Participant extends Model
{
    protected $fillable = [];

    /**
     * The committeeMember that belong to the participant.
     */
    public function committeeMembers()
    {
        return $this->belongsToMany(User::class, 'user_id')->where('role', 'committeeMember');
    }

     /**
     * The committeeMember that belong to the participant.
     */
    public function invitedMembers()
    {
        return $this->belongsToMany(User::class, 'user_id')->where('role', 'member');
    }

     /**
     * The guest that belong to the participant.
     */
    public function guests()
    {
        return $this->belongsToMany(Guest::class, 'user_id')->where('role', 'guest');
    }
    

}
