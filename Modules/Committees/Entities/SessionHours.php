<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;

class SessionHours extends Model
{
    protected $table = ['session_hours'];
}
