<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
class MemberType extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'is_disable'];

    public static $typesIds = [
        'manager' => [1],
        'member' => [3,5]
    ];

    public function session()
    {
        return $this->belongsTo(Session::class);
    }
}
