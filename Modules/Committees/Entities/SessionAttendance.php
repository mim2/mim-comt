<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\AttendanceStatus;
use Modules\Auth\Entities\User;
use OwenIt\Auditing\Contracts\Auditable;
use Modules\Core\Entities\Department;


class SessionAttendance extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $fillable = [
        'session_id',
        'attendance_status_id',
        'user_id',
        'apology_reason'
    ];

    protected $appends = ['dept'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function session()
    {
        return $this->belongsTo(Session::class, 'id', 'session_id');
    }

    public function member()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getDeptAttribute()
    {
        $dept = Department::where('id',$this->member->user_dept)->first();
      //  dd($dept);
        return $dept->name;
       // return $this->belongsTo(Department::class, $this->member->user_dept, 'id');
    }


    public function attendanceStatus()
    {
        return $this->belongsTo(AttendanceStatus::class);
    }
}
