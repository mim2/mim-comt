<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model
{
    protected $table = 'mim_recommendations';

    protected $fillable = ['recommendation', 'commettees_id' , 'session_id'];
}
