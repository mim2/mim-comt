<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\MemberType;
use Modules\Auth\Entities\User;
use Illuminate\Notifications\Notifiable;
use Modules\Committees\Events\CommitteeMemberCreated;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Modules\Committees\Classes\CommitteesModule;
use Modules\Core\Traits\SharedModel;

class CommitteeMember extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable, Notifiable, SoftDeletes, SharedModel;

    protected $primaryKey = 'id';
    private static $committeeId;
    private static $input;

    protected $fillable = [
        'user_id',
        'committee_id',
        'member_type_id',
        'created_by_user_id'
    ];


    protected $appends = ['user_name', 'auth_name'];

    public function getUserNameAttribute()
    {
      
        $data = User::select('user_name')->where('user_id', $this->user_id)->first();

        return $data->user_name;
       
    }

    public function getAuthNameAttribute()
    {
      
      
        $data = Authorize::select('user_id')->where('from_user_id', $this->user_id)->where('committee_id', $this->committee_id)->first();
        
        if($data != null)  {
        $name = User::select('user_name')->where('user_id', $data->user_id)->first();
        return '(المفوض ' .  $name->user_name .')';
        }
        return '';

     //   return $this->belongsTo(Authorize::class, 'user_id', 'user_id');
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = ['deleted_at'];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => CommitteeMemberCreated::class,
        'restored' => CommitteeMemberCreated::class,
    ];


    public function committee()
    {
        return $this->belongsTo(Committee::class);
    }

    /**
     * The members that belong to the committee.
     */
    public function memberCommittee()
    {
        return $this->belongsToMany(Committee::class, 'committee_member');
    }

    public function member()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function memberType()
    {
        return $this->belongsTo(MemberType::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by_user_id');
    }

    public static function managerStoreRequest($request)
    {
        CommitteesModule::CommitteeMemberStore($request);

    }

   


    public function transformAudit(array $data): array
    {
        $data['old_values']['user_id'] = intval($this->getOriginal('user_id'));
        $data['old_values']['committee_id'] = intval($this->getOriginal('committee_id'));
        $data['old_values']['member_type_id'] = intval($this->getOriginal('member_type_id'));
        $data['new_values']['user_id'] = intval($this->getAttribute('user_id'));
        $data['new_values']['committee_id'] = intval($this->getAttribute('committee_id'));
        $data['new_values']['member_type_id'] = intval($this->getAttribute('member_type_id'));

        return $data;
    }

    public function displayForAttendance($attendee)
    {
        if ($attendee == null && $this->trashed()) {
            return false;
        }

        return true;
    }

    public function displayForGuestAttendance($attendee)
    {
        if ($attendee == null && $this->trashed()) {
            return false;
        }

        return true;
    }

    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    public static $memberUserId = ([
        'user_id' => [
            'unique:committee_members,user_id,NULL,id,committee_id,'
        ],
    ]);
}
