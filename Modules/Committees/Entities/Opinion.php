<?php

namespace Modules\Committees\Entities;
use Modules\Committees\Entities\Session;
use DB;
use Carbon\Carbon;


use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{
    protected $guarded = [];

    public static function signitureSend( $dayToCheck) 
    {
        // $opinions = DB::table('sessions')->join('opinions', 'sessions.id', '=', 'opinions.session_id')->select('opinions.user_id')->first();
        $attendeeOpinion = DB::table('sessions')->join('session_attendances', 'sessions.id', '=', 'session_attendances.session_id')->whereDate('sessions.created_at',  $dayToCheck)->get();

        foreach($attendeeOpinion as $key => $a)
        {
                $data_to_insert = [
                     'session_id' => $a->session_id,
                    'opinion' => 1,
                ];

                $save = Opinion::firstOrNew([
                    'user_id' => $a->user_id,
                ], $data_to_insert);
                $save->save();

        }
    }

    
}
