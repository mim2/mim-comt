<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;

class Authorize extends Model
{
    protected $fillable = ['from_user_id', 'user_id', 'type', 'degree', 'committee_id', 'session_id'];


    public function committee()
    {
        return $this->belongsTo(Committee::class);
    }

    public function session()
    {
        return $this->belongsTo(Session::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    public function auther()
    {
        return $this->belongsTo(User::class, 'from_user_id');
    }

}
