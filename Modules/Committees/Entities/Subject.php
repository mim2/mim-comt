<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

use Modules\Committees\Entities\Session;
use ProAI\Versioning\SoftDeletes;

class Subject extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'recommendation', 'law_document', 'relevant_jurisdiction', 'source', 'user_id','session_id'
    ];

    public function session()
    {
        return $this->belongsTo(Session::class);
    }

    public function guests()
    {
        return $this->belongsToMany(Guest::class, 'guest_subject');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public static function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                utf8_encode_deep($value);
            }
    
            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));
    
            foreach ($vars as $var) {
                utf8_encode_deep($input->$var);
            }
        }
    }
}
