<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;

class GuestSubject extends Model
{
    protected $table = 'guest_subject';
    protected $fillable = ['guest_id', 'subject_id'];

    public function guest()
    {
        return $this->belongsTo(Guest::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
}
