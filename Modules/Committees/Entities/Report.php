<?php

namespace Modules\Committees\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\View;

class Report extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'reports';

    public function scopeAlways($request)
    {
        return Committee::with([
            'members' => function ($query) {
                $query->select(['id', 'committee_id', 'member_type_id', 'user_id']);
                $query->with([
                    'member' => function ($query) {
                        $query->select(['user_id', 'user_name', 'user_idno']);
                    },
                    'memberType' => function ($query) {
                        $query->select(['id', 'name']);
                    }
                ]);
            },
            'type' => function ($query) {
                $query->select(['id', 'name']);
            }

        ])->where('type_id', 1)

            ->get(['id', 'name', 'type_id']);
        $dataWithAttendences = ['types', 'committees', 'phases', 'sessions', 'attendanceForReport', 'reports', 'committeeMembers', 'attendandeeMembers'];
        $view = $request->print == '1' ? 'committees::reports.print' : 'committees::reports.members_reports';
        return View::make($view, compact($dataWithAttendences));
    }

    public function scopeTemporary($request)
    {
        return Committee::with([
            'members' => function ($query) {
                $query->select(['id', 'committee_id', 'member_type_id', 'user_id']);
                $query->with([
                    'member' => function ($query) {
                        $query->select(['user_id', 'user_name', 'user_idno']);
                    },
                    'memberType' => function ($query) {
                        $query->select(['id', 'name']);
                    }
                ]);
            },
            'type' => function ($query) {
                $query->select(['id', 'name']);
            }

        ])->where('type_id', 4)

            ->get(['id', 'name', 'type_id']);
        $dataWithAttendences = ['types', 'committees', 'phases', 'sessions', 'attendanceForReport', 'reports', 'committeeMembers', 'attendandeeMembers'];
        $view = $request->print == '1' ? 'committees::reports.print' : 'committees::reports.members_reports';
        return View::make($view, compact($dataWithAttendences));
    }

    public function scopeIndependent($request)
    {
        return Committee::with([
            'members' => function ($query) {
                $query->select(['id', 'committee_id', 'member_type_id', 'user_id']);
                $query->with([
                    'member' => function ($query) {
                        $query->select(['user_id', 'user_name', 'user_idno']);
                    },
                    'memberType' => function ($query) {
                        $query->select(['id', 'name']);
                    }
                ]);
            },
            'type' => function ($query) {
                $query->select(['id', 'name']);
            }

        ])->where('type_id', 2)

            ->get(['id', 'name', 'type_id']);
        $dataWithAttendences = ['types', 'committees', 'phases', 'sessions', 'attendanceForReport', 'reports', 'committeeMembers', 'attendandeeMembers'];
        $view = $request->print == '1' ? 'committees::reports.print' : 'committees::reports.members_reports';
        return View::make($view, compact($dataWithAttendences));
    }

    public function scopeFrom()
    {
        return Committee::with([
            'members' => function ($query) {
                $query->select(['id', 'committee_id', 'member_type_id', 'user_id']);
                $query->with([
                    'member' => function ($query) {
                        $query->select(['user_id', 'user_name', 'user_idno']);
                    },
                    'memberType' => function ($query) {
                        $query->select(['id', 'name']);
                    }
                ]);
            },
            'type' => function ($query) {
                $query->select(['id', 'name']);
            },


        ])->where('type_id', 3)

            ->get(['id', 'name', 'type_id']);
    }
}
