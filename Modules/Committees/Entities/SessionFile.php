<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Contracts\Auditable;

use Modules\Committees\Entities\Session;

use Illuminate\Database\Eloquent\SoftDeletes;

class SessionFile extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $fillable = ['file_name', 'file_path', 'session_id'];

    public function session()
    {
        return $this->belongsTo(Session::class);
    }

}
