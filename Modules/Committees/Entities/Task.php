<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;

class Task extends Model
{
    protected $table = 'mim_tasks';

    protected $fillable = ['parent_id', 'time_id', 'commettees_id', 'session_id', 'user_id'];


    public function assignee()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    public function time()
    {
        return $this->belongsTo(TimeFrame::class, 'time_id', 'id');
    }

    public function discussion()
    {
        return $this->belongsTo(Discussion::class, 'parent_id', 'id');
    }


}
