<?php

namespace Modules\Committees\Entities;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Contracts\Auditable;

use Modules\Committees\Entities\Committee;

use Illuminate\Database\Eloquent\SoftDeletes;

class CommitteeFile extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $fillable = ['file_name', 'file_path', 'committee_id'];

    public function committee()
    {
        return $this->belongsTo(Committee::class);
    }

}
