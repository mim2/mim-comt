<?php

namespace Modules\Committees\Classes\Reports;

// use Illuminate\Database\Eloquent\Builder;

interface SessionReports
{
    /**
     * Apply a given search value to the builder instance.
     * 
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function count($request);
}