<?php

namespace Modules\Committees\Classes\Reports;

use App\Classes\Date\CarbonHijri;
use Illuminate\Support\Facades\View;
use Modules\Committees\Entities\Session;

class Sessions implements SessionReports
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function count($request)
    {
        return  Session::with([
            'sessionTitle' => function ($query) {
                return $query->select(['id', 'name']);
            },
            'attendees' => function ($query) {
                $query->select(['id', 'session_id', 'attendance_status_id', 'user_id']);
                $query->with([
                    'attendanceStatus' => function ($query) {
                        return $query->select(['id', 'name']);
                    },
                    'member'  => function ($query) {
                        return $query->select(['user_id', 'user_name', 'user_idno']);
                    },

                ]);
            },
            'opinions' => function ($query) {
                $query->select(['id', 'session_id', 'opinion']);
            },
            'history',
            'committee' => function ($query) {
                $query->select(['id', 'name']);
                $query->with([
                    'members' => function ($query) {
                        $query->select(['id', 'committee_id', 'member_type_id', 'user_id']);
                        $query->with([
                            'member' => function ($query) {
                                $query->select(['user_id', 'user_name', 'user_idno']);
                            },
                            'memberType' => function ($query) {
                                $query->select(['id', 'name']);
                            }
                        ]);
                    }
                ]);
            }



        ])->where('committee_id', $request->committee)
            ->whereHas('history', function ($query) use ($request) {
                $start_date = CarbonHijri::toMiladiFromHijri($request->start_date);
                $end_date = CarbonHijri::toMiladiFromHijri($request->end_date);
                return $query->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            })->get(['id', 'user_id', 'committee_id', 'start_time', 'end_time', 'date', 'session_title_id', 'status_id']);

            $dataWithAttendences = ['types', 'committees', 'phases', 'sessions', 'attendanceForReport', 'reports', 'committeeMembers'];
            $view = $request->print == '1' ? 'committees::reports.print' : 'committees::reports.sessions_reports';
            return View::make($view, compact($dataWithAttendences));
    }
}