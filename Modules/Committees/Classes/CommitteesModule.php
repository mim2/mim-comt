<?php

namespace Modules\Committees\Classes;

use App\Classes\Date\CarbonHijri;
use App\Classes\Sms\Mobily;
use App\Traits\ArabicToEnglish;
use App\Traits\StoreImage;
use Carbon\Carbon;
use Modules\Auth\Entities\User;
use Modules\Committees\Classes\CommitteeInterface;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\CommitteeMember;
use File;
use Illuminate\Support\Facades\Input;
use Modules\Committees\Entities\AcademicYear;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Modules\Committees\Emails\NotifyMemberAddingNewSession;
use Modules\Committees\Entities\Session;
use Modules\Core\Entities\Group;
use Modules\Core\Entities\UserGroup;

class CommitteesModule implements CommitteeInterface
{

    use StoreImage;
    use ArabicToEnglish;
    /**
     * Apply a given search value to the builder instance.
     * 
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function CommitteeStore($request)
    {
        $decision_name = self::arabicNumToEnglish($request->decision_name);
        $start_date = CarbonHijri::toHijriFromMiladi(Carbon::now());
        $end_date = CarbonHijri::toHijriFromMiladi(Carbon::now());
        if ($request->type_id == 3) {
            try {
                $start_date = CarbonHijri::toHijriFromMiladi($request->start_date);
                $end_date   = CarbonHijri::toHijriFromMiladi($request->end_date);
                $request->merge(['parent_id' => $request->parent_id]);
            } catch (\Exception $e) {
                return redirect()->back()->withInput();
            }
        }

        if ($request->type_id != 3) $request->parent_id = 0;

        if ($request->committee_or_council == 2) {
            $request->parent_id = 0;
            $request->merge(['type_id' => 2]);
        }
        $request->merge([
            'user_id' => auth()->user()->user_id,
            'decision_url' => self::verifyAndStoreImage($request, 'decision_image', 'decisions'),
            'start_date' => $start_date,
            'decision_name' => $decision_name = $decision_name,
            'end_date' => $end_date,
            'parent_id' => $request->parent_id,
            'privacy' => $request->privacy
        ]);
        // CommitteeMember::CommitteeMemberStoreMembers($request);
    }

    public static function CommitteeUpdate($request, $committee)
    {

        $start_date = CarbonHijri::toHijriFromMiladi(Carbon::now());
        // dd($start_date);
        $end_date = CarbonHijri::toHijriFromMiladi(Carbon::now());
        if ($request->type_id == 3) {
            try {
                $start_date = CarbonHijri::toHijriFromMiladi($request->start_date);
                $end_date   = CarbonHijri::toHijriFromMiladi($request->end_date);
            } catch (\Exception $e) {
                return redirect()->back()->withInput();
            }
        }

        if ($request->type_id != 4) {
            $request->parent_id = 0;
        }

        $request->merge([
            'decision_url' => self::verifyAndStoreImage($request, 'decision_image', 'decisions'),
            'start_date' => $start_date,
            'end_date' => $end_date,
            'parent_id' => $request->parent_id,
        ]);

        // delete old file
        if ($request->decision_url != null) {
            File::delete($committee->decision_url);
        } else {
            $request->merge([
                'decision_url' => $committee->decision_url
            ]);
        }
        $committee->update($request->all());
    }

    public static function CommitteeMemberStore($request)
    {
        $input = $request->all();
        // dd($input);

        $committee_id = Committee::create($input)->id;
        $membersGroupId = Group::where('key', 'members')->first()->id;
        $aminGroupId = Group::where('key', 'secretary')->first()->id;
        $presidentGroupId = Group::where('key', 'supervisor')->first()->id;

        if ($request->input('manager_user_id') != null) {
        $commiteeMemberForManager = new CommitteeMember();
        $commiteeMemberForManager->committee_id = $committee_id;
        $commiteeMemberForManager->user_id = $request->input('manager_user_id');
        $commiteeMemberForManager->member_type_id = 1;
        $commiteeMemberForManager->created_by_user_id = $request->user()->user_id;
        $commiteeMemberForManager->save();
        if ($commiteeMemberForManager->member_type_id == 1) {
            $membersGroup[] = [
                'es_core_group_id'  => $presidentGroupId,
                'user_id'    => $request->input('manager_user_id'),
                'committee_id'    => $committee_id,
            ];
        }

        }

        if ($request->input('vice_manager_user_id') != null) {
        $commiteeMemberForvice = new CommitteeMember();
        $commiteeMemberForvice->committee_id = $committee_id;
        $commiteeMemberForvice->user_id = $request->input('vice_manager_user_id');
        $commiteeMemberForvice->member_type_id = 2;
        $commiteeMemberForvice->created_by_user_id = $request->user()->user_id;
        $commiteeMemberForvice->save();
        if ($commiteeMemberForvice->member_type_id == 2) {
            $membersGroup[] = [
                'es_core_group_id'  => $aminGroupId,
                'user_id'    => $request->input('vice_manager_user_id'),
                'committee_id'    => $committee_id,
            ];
        }
        }

        if ( $request->commMembers != null) {
        if ( $input['category'] == 0) {
        foreach ($request->commMembers as $selected_member) {
            $commiteeMember = new CommitteeMember();
            $commiteeMember->committee_id = $committee_id;
            $commiteeMember->user_id = $selected_member['user_id'];
            $commiteeMember->member_type_id = $selected_member['member_type_id'];
            $commiteeMember->created_by_user_id = $request->user()->user_id;
            $commiteeMember->save();
            if ($selected_member['member_type_id'] == 3) {
                $membersGroup[] = [
                    'es_core_group_id'  => $membersGroupId,
                    'user_id'    => $selected_member['user_id'],
                    'committee_id'    => $committee_id,
                ];
            } 
        }
    }
    }
        // dd($commiteeMember);
      //  CommitteeMember::create($input);
      if ( $request->commMembers != null || $request->input('vice_manager_user_id') != null || $request->input('manager_user_id') != null){
        DB::table('es_core_users_groups')->insert($membersGroup);
      }
    }

    public static function CommitteeMemberUpdateForManeger($request, $committee)
    {
        if (($request->input('manager')) != 0) {
            $currentCommitteeManager = $committee->manager;

            if($currentCommitteeManager != null) $currentCommitteeManager->delete();

            $commiteeMemberForManager = new CommitteeMember;
            $commiteeMemberForManager->committee_id = $committee->id;
            $commiteeMemberForManager->user_id = $request->input('manager');
            $commiteeMemberForManager->member_type_id = 1;
            $commiteeMemberForManager->created_by_user_id = $request->user()->user_id;
            $commiteeMemberForManager->save();

            alert()->success('تم تعديل اللجنة بنجاح !', 'تعديل ناجح')->autoclose(1000);
        }
    }

    public static function CommitteeMemberUpdateForVice($request, $committee)
    {
        if (($request->input('vice')) != 0) {
            $currentViceCommitteeManager = $committee->vice_manager;
            if($currentViceCommitteeManager != null) $currentViceCommitteeManager->delete();

            $commiteeMemberForVice = new CommitteeMember;
            $commiteeMemberForVice->committee_id = $committee->id;
            $commiteeMemberForVice->user_id = $request->input('vice');
            $commiteeMemberForVice->member_type_id = 2;
            $commiteeMemberForVice->created_by_user_id = $request->user()->user_id;
            $commiteeMemberForVice->save();

            alert()->success('تم تعديل اللجنة بنجاح !', 'تعديل ناجح')->autoclose(1000);
        }
    }

    public static function CommitteeMemberUpdateForMembers($request, $committee)
    {
        if (($request->input('commMembers')) != 0) {
            if (is_array($request->commMembers)) {
                foreach ($request->commMembers as $memberId => $selected_member) {
                    $existsMember = CommitteeMember::where('user_id', $selected_member['user_id'])->where('committee_id', $committee->id)->count();
                    $commiteeMembers = $committee->members()->with('member')->withTrashed()->get();
                    if ($existsMember == null) {
                        foreach ($commiteeMembers as $commiteeMember) {
                            $commiteeMember->where('user_id', 0)->delete();
                        }
                        $data = Input::all();
                        $commiteeMember = new CommitteeMember;
                        $commiteeMember->committee_id = $committee->id;
                        $commiteeMember->user_id = $selected_member['user_id'];
                        $commiteeMember->member_type_id = $selected_member['member_type_id'];
                        $commiteeMember->created_by_user_id = $request->user()->user_id;
                        $commiteeMember->save();
                        // $commiteeMember->memberCommittee()->attach(['id', 'committee_id']);
                        alert()->success('تم تعديل اللجنة بنجاح !', 'تعديل ناجح')->autoclose(1000);
                    } else {
                        alert()->warning('  الرجاء اختيار نائب جديد  !', 'لم يتم التعديل')->autoclose(1000);
                    }
                }
            }
        }
    }

    public static function SessionStore($committee, $request, $session)
    {

        $academicYear = AcademicYear::where('is_active', true)->first()->code;
        $request->merge([
            'user_id' => Auth::user()->user_id,
            'committee_id' => $committee->id,
            'academic_year_id' => $academicYear
        ]);
        $requestData = $request->all();
        $requestData['start_time'] = date("H:i:s", strtotime($request->start_time));
        $requestData['end_time'] = date("H:i:s", strtotime($request->end_time));
       // $requestData['date'] = CarbonHijri::toMiladiFromHijri($request->date)->toDateString();
        $requestData['date'] = $request->date;
        $session = Session::create($requestData);
        // dd($requestData);
        foreach ($committee->members as $member) {
            if($request->has('send_mail')) {
                Mail::to($member->member->user_mail)
                    ->queue(new NotifyMemberAddingNewSession($member->member, $session));
            }
            // if($request->has('send_sms')) {
            //     $message = $committee->name . ' ' .  'تم اضافة  الجلسة' . $session->sessionTitle->name  . ' في لجنة  ';
            //     Mobily::send($message, [$member->member->user_mobile]);
            // }
        }
        alert()->success(' تم الحفظ بنجاح والارسال الي الاعضاء !', 'حفظ ناجح');
    }

    public static function SessionUpdate($request, $session)
    {
        $requestData = $request->all();
        $requestData['start_time'] = date("H:i:s", strtotime($request->start_time));
        $requestData['end_time'] = date("H:i:s", strtotime($request->end_time));

        $session->update($requestData);
    }
}
