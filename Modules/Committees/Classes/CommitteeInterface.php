<?php

namespace Modules\Committees\Classes;


interface CommitteeInterface
{
   /**
    * committee requests
    */
    public static function CommitteeStore($request);

    public static function CommitteeUpdate($request, $committee);

    public static function CommitteeMemberStore($request);

    public static function CommitteeMemberUpdateForManeger($request, $committee); 

    public static function CommitteeMemberUpdateForVice($request, $committee);

    public static function CommitteeMemberUpdateForMembers($request, $committee);
    /**
     * ==========================================================================
     */

    /**
    * session requests
    */
    public static function SessionStore($committee, $request, $session);

    public static function SessionUpdate($request, $session);
    /**
     * ==========================================================================
     */
}