<?php

namespace Modules\Committees\Classes;


interface SessionsInterface
{
   
    public static function SessionStore($request);

    public static function SessionUpdate($request, $committee);
}