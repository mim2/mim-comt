<?php

namespace Modules\Committees\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CommitteesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('committee_types')->truncate();
        DB::table('committee_types')->insert([
            ['id' => 1, 'name' => 'دائمة', 'deleted_at' => null],
            ['id' => 4, 'name' => 'مؤقتة', 'deleted_at' => null],
            ['id' => 3, 'name' => 'فرعية', 'deleted_at' => null],
            ['id' => 2, 'name' => ' مجلس مستقل ', 'deleted_at' => null],
            ['id' => 5, 'name' => ' مجلس فرعي ', 'deleted_at' => null],
            ['id' => 6, 'name' => ' مجلس كلي ', 'deleted_at' => null],
        ]);

        // Member Types Database Seeder
        DB::table('member_types')->truncate();
        DB::table('member_types')->insert([
            ['id' => 1, 'name' => 'مشرف', 'deleted_at' => null],
            ['id' => 2, 'name' => 'أمين', 'deleted_at' => null],
            ['id' => 3, 'name' => 'عضواً', 'deleted_at' => null],
            ['id' => 4, 'name' => 'نائباً للرئيس', 'deleted_at' => null],
            ['id' => 5, 'name' => 'نائباً للرئيس وعضوأ', 'deleted_at' => null],
            ['id' => 6, 'name' => 'نائباً للرئيس ومشرف ', 'deleted_at' => null],
            ['id' => 7, 'name' => 'سكرتير', 'deleted_at' => null],
        ]);

        // Session Titles Database Seeder
        DB::table('session_titles')->truncate();
        DB::table('session_titles')->insert([
            ['id' => 1, 'name' => 'الأولى'],
            ['id' => 2, 'name' => 'الثانية'],
            ['id' => 3, 'name' => 'الثالثة'],
            ['id' => 4, 'name' => 'الرابعة'],
            ['id' => 5, 'name' => 'الخامسة'],
            ['id' => 6, 'name' => 'السادسة'],
            ['id' => 7, 'name' => 'السابعة'],
            ['id' => 8, 'name' => 'الثامنة'],
            ['id' => 9, 'name' => 'التاسعة'],
            ['id' => 10, 'name' => 'العاشرة'],
            ['id' => 11, 'name' => 'الحادية عشر'],
            ['id' => 12, 'name' => 'الثانية عشر'],
            ['id' => 13, 'name' => 'الثالثة عشرة'],
            ['id' => 14, 'name' => 'الرابعة عشرة'],
            ['id' => 15, 'name' => 'الخامسة عشرة'],
            ['id' => 16, 'name' => 'السادسة عشرة'],
            ['id' => 17, 'name' => 'السابعة عشرة'],
            ['id' => 18, 'name' => 'الثامنة عشرة'],
            ['id' => 19, 'name' => 'التاسعة عشرة'],
            ['id' => 20, 'name' => 'العشرون'],
        ]);

        // Session Statuses Database Seeder
        DB::table('session_statuses')->truncate();
        DB::table('session_statuses')->insert([
            ['id' => 1, 'name' => 'جديدة', 'color' => '#95C347'],
            ['id' => 2, 'name' => 'تم إعداد جدول الأعمال', 'color' => '#AAAAAA'],
            ['id' => 3, 'name' => 'تم إعداد المحضر للتوقيع', 'color' => '#FF6949'],
            ['id' => 4, 'name' => 'تم إرسال المحضر للاعتماد', 'color' => '#95C347'],
            ['id' => 5, 'name' => 'تم اعتماد المحضر', 'color' => '#95C347'],
            ['id' => 6, 'name' => 'للاستكمال', '#color' => '#0D6EB8'],
            ['id' => 7, 'name' => 'في العرض', 'color' => '#95C347'],
            ['id' => 8, 'name' => 'تم الاستكمال', 'color' => '#95C347'],
            ['id' => 9, 'name' => 'تم إرسال المحضر للاعتماد', 'color' => '#95C347'],
            ['id' => 10, 'name' => 'تم اعتماد المحضر', 'color' => '#95C347'],
        ]);

        // Academic Years Database Seeder
        DB::table('academic_years')->truncate();
        DB::table('academic_years')->insert([
            ['id' => 1, 'name' => 1437, 'code' => 37, 'is_active' => false],
            ['id' => 2, 'name' => 1438, 'code' => 38, 'is_active' => false],
            ['id' => 3, 'name' => 1439, 'code' => 39, 'is_active' => true],
            ['id' => 4, 'name' => 1440, 'code' => 40, 'is_active' => false],
            ['id' => 5, 'name' => 1441, 'code' => 41, 'is_active' => false],
            ['id' => 6, 'name' => 1442, 'code' => 42, 'is_active' => false],
            ['id' => 7, 'name' => 1443, 'code' => 43, 'is_active' => false],
            ['id' => 8, 'name' => 1444, 'code' => 44, 'is_active' => false],
            ['id' => 9, 'name' => 1445, 'code' => 45, 'is_active' => false],
            ['id' => 10, 'name' => 1446, 'code' => 46, 'is_active' => false],
            ['id' => 11, 'name' => 1447, 'code' => 47, 'is_active' => false],
            ['id' => 12, 'name' => 1448, 'code' => 48, 'is_active' => false],
            ['id' => 13, 'name' => 1449, 'code' => 49, 'is_active' => false],
            ['id' => 14, 'name' => 1450, 'code' => 50, 'is_active' => false],
        ]);

        DB::table('attendance_statuses')->truncate();
        DB::table('attendance_statuses')->insert([
            ['id' => 1, 'name' => 'حاضر', 'deleted_at' => null ],
            ['id' => 2, 'name' => 'غائب', 'deleted_at' => null ],
            ['id' => 3, 'name' => 'معتذر', 'deleted_at' => null],
        ]);
    }
}
