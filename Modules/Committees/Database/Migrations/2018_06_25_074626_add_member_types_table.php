<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddMemberTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('member_key');
            $table->integer('is_disable');
            $table->timestamps();
            $table->softDeletes();
        });
        // Member Types Database Seeder
        DB::table('member_types')->insert([
            ['id' => 1, 'name' => 'مشرف', 'member_key' => 'supervisor','is_disable' => false, 'deleted_at' => null],
            ['id' => 2, 'name' => 'أمين', 'member_key' => 'secretary','is_disable' => false, 'deleted_at' => null],
            ['id' => 3, 'name' => 'عضواً', 'member_key' => 'members','is_disable' => false, 'deleted_at' => null],
            ['id' => 4, 'name' => 'نائباً للرئيس', 'member_key' => 'viceـpresident', 'is_disable' => true, 'deleted_at' => null],
            ['id' => 5, 'name' => 'نائباً للرئيس وعضوأ', 'member_key' => 'viceـpresident_and_members', 'is_disable' => true, 'deleted_at' => null],
            ['id' => 6, 'name' => 'نائباً للرئيس وأمين ', 'member_key' => 'viceـpresident_and_secretary', 'is_disable' => true, 'deleted_at' => null],
            ['id' => 7, 'name' => 'سكرتير', 'member_key' => 'members_mim', 'is_disable' => true, 'deleted_at' => null],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_types');
    }
}
