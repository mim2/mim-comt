<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionAttendancesTable extends Migration
{
    public $incrementing = false;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('session_id');
            $table->unsignedInteger('attendance_status_id');
            $table->unsignedInteger('user_id');
            $table->string('apology_reason')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('session_id')
                ->references('id')->on('sessions')
                ->onDelete('cascade');

            $table->foreign('attendance_status_id')
                ->references('id')->on('attendance_statuses')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_attendances');
    }
}
