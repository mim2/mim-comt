<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('start_time');
            $table->string('end_time');
            $table->string('date');
            $table->unsignedInteger('session_title_id');
            $table->unsignedInteger('committee_id');
            $table->unsignedInteger('status_id')->default(1);
            $table->unsignedInteger('academic_year_id');
            $table->string('file_url')->nullable();
            $table->string('outgoing_code')->nullable();
            $table->string('place');
            $table->text('notes')->nullable();
            $table->string('approved_date')->nullable();
            $table->integer('completed')->nullable();
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
