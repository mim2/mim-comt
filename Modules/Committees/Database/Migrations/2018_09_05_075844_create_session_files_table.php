<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_name');
            $table->string('file_path');

            $table->unsignedInteger('session_id');
            $table->foreign('session_id')
                  ->references('id')->on('sessions')
                  ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_files');
    }
}
