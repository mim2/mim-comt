<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCommitteeTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('committee_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('committee_types')->insert([
            ['id' => 1, 'name' => 'دائمة', 'deleted_at' => null],
            ['id' => 4, 'name' => 'مؤقتة', 'deleted_at' => null],
            ['id' => 3, 'name' => 'فرعية', 'deleted_at' => null],
            ['id' => 2, 'name' => ' مجلس مستقل ', 'deleted_at' => null],
            ['id' => 5, 'name' => ' مجلس فرعي ', 'deleted_at' => null],
            ['id' => 6, 'name' => ' مجلس كلي ', 'deleted_at' => null],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('committee_types');
    }
}
