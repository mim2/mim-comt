<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsToMulitbleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dateTime('data_send_date')->nullable()->after('is_super_admin');
        });

        Schema::table('committees_version', function (Blueprint $table) {
            $table->string('privacy')->nullable()->after('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('data_send_date');
        });

        Schema::table('committees_version', function (Blueprint $table) {
            $table->dropColumn('privacy');
        });
    }
}
