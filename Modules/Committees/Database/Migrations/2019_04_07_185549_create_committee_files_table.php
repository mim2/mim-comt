<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommitteeFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('committee_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_name');
            $table->string('file_path');

            $table->unsignedInteger('committee_id');
            $table->foreign('committee_id')
                  ->references('id')->on('committees')
                  ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('committee_files');
    }
}
