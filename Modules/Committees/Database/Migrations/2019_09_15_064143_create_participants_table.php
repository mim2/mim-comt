<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('session_id');
            $table->unsignedInteger('user_id')->nullable();
            // $table->unsignedInteger('committee_member_id')->nullable();
            // $table->unsignedInteger('guest_id')->nullable();
            $table->string('role')->nullable();
            $table->unsignedInteger('created_by');

            //     $table->foreign('user_id')->references('user_id')->on('users')

            // ->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
