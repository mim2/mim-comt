<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSessionTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_titles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

         // Session Titles Database Seeder
         DB::table('session_titles')->insert([
            ['id' => 1, 'name' => 'الأولى'],
            ['id' => 2, 'name' => 'الثانية'],
            ['id' => 3, 'name' => 'الثالثة'],
            ['id' => 4, 'name' => 'الرابعة'],
            ['id' => 5, 'name' => 'الخامسة'],
            ['id' => 6, 'name' => 'السادسة'],
            ['id' => 7, 'name' => 'السابعة'],
            ['id' => 8, 'name' => 'الثامنة'],
            ['id' => 9, 'name' => 'التاسعة'],
            ['id' => 10, 'name' => 'العاشرة'],
            ['id' => 11, 'name' => 'الحادية عشر'],
            ['id' => 12, 'name' => 'الثانية عشر'],
            ['id' => 13, 'name' => 'الثالثة عشرة'],
            ['id' => 14, 'name' => 'الرابعة عشرة'],
            ['id' => 15, 'name' => 'الخامسة عشرة'],
            ['id' => 16, 'name' => 'السادسة عشرة'],
            ['id' => 17, 'name' => 'السابعة عشرة'],
            ['id' => 18, 'name' => 'الثامنة عشرة'],
            ['id' => 19, 'name' => 'التاسعة عشرة'],
            ['id' => 20, 'name' => 'العشرون'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_titles');
    }
}
