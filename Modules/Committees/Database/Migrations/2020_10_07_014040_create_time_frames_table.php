<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeFramesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mim_time_frames', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });

        DB::table('mim_time_frames')->insert([
            ['id' => 1, 'name' => 'يومين عمل'],
            ['id' => 2, 'name' => '٤ أيام عمل'],
            ['id' => 3, 'name' => '٧ أيام عمل'],
            ['id' => 4, 'name' => 'أسبوعين عمل'],
            ['id' => 5, 'name' => '٣ أسابيع عمل'],
            ['id' => 6, 'name' => 'شهر'],
           
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_frames');
    }
}
