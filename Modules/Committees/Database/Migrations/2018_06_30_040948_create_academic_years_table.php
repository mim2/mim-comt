<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAcademicYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_years', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('name');
            $table->integer('code');
            $table->boolean('is_active');
            $table->timestamps();
        });
        // Academic Years Database Seeder
        DB::table('academic_years')->insert([
            ['id' => 1, 'name' => 1437, 'code' => 37, 'is_active' => false],
            ['id' => 2, 'name' => 1438, 'code' => 38, 'is_active' => false],
            ['id' => 3, 'name' => 1439, 'code' => 39, 'is_active' => true],
            ['id' => 4, 'name' => 1440, 'code' => 40, 'is_active' => false],
            ['id' => 5, 'name' => 1441, 'code' => 41, 'is_active' => false],
            ['id' => 6, 'name' => 1442, 'code' => 42, 'is_active' => false],
            ['id' => 7, 'name' => 1443, 'code' => 43, 'is_active' => false],
            ['id' => 8, 'name' => 1444, 'code' => 44, 'is_active' => false],
            ['id' => 9, 'name' => 1445, 'code' => 45, 'is_active' => false],
            ['id' => 10, 'name' => 1446, 'code' => 46, 'is_active' => false],
            ['id' => 11, 'name' => 1447, 'code' => 47, 'is_active' => false],
            ['id' => 12, 'name' => 1448, 'code' => 48, 'is_active' => false],
            ['id' => 13, 'name' => 1449, 'code' => 49, 'is_active' => false],
            ['id' => 14, 'name' => 1450, 'code' => 50, 'is_active' => false],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic_years');
    }
}
