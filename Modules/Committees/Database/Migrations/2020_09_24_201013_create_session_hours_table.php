<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_hours', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hours');
            $table->timestamps();
        });

        DB::table('session_hours')->insert([
            'id' => 1, 'hours' => 'select'
        ]);
        DB::table('session_hours')->insert([
            'id' => 2,'hours' => '08:00 AM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 3,'hours' => '08:30 AM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 4,'hours' => '09:00 AM'
        ]);
        DB::table('session_hours')->insert([
            'id' =>5,'hours' => '09:30 AM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 6,'hours' => '10:00 AM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 7,'hours' => '10:30 AM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 8,'hours' => '11:00 AM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 9,'hours' => '11:30 AM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 10,'hours' => '12:00 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 11,'hours' => '12:30 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 12,'hours' => '01:00 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 13,'hours' => '01:30 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 14,'hours' => '02:00 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 15,'hours' => '02:30 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 16,'hours' => '03:00 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 17,'hours' => '03:30 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 18,'hours' => '04:00 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 19,'hours' => '04:30 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 20,'hours' => '05:00 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 21,'hours' => '05:30 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 22,'hours' => '06:00 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 23,'hours' => '06:30 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 24,'hours' => '07:00 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 25,'hours' => '07:30 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 26,'hours' => '08:00 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 27,'hours' => '08:30 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 28,'hours' => '09:00 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 29,'hours' => '09:30 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 30,'hours' => '10:00 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 31, 'hours' => '10:30 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 32, 'hours' => '11:00 PM'
        ]);
        DB::table('session_hours')->insert([
            'id' => 33,'hours' => '11:30 PM'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_hours');
    }
}
