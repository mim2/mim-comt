<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOldUserIdToCommitteeMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('committee_members', function (Blueprint $table) {
            $table->unsignedInteger('old_user_id')->nullable();
            $table->unsignedInteger('old_member_id')->nullable();
            $table->unsignedInteger('old_member_type_id')->nullable();
            $table->unsignedInteger('old_committee_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('committee_members', function (Blueprint $table) {

        });
    }
}
