<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommitteeMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('committee_member', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('committee_id');
            // $table->unsignedInteger('committee_member_id')->nullable();
            $table->unsignedInteger('user_id');

            $table->timestamps();

            // $table->unique(['committee_id', 'committee_member_id']);
            
            $table->foreign('committee_id')
                  ->references('id')->on('committees')
                  ->onDelete('cascade');

                  $table->foreign('user_id')
                  ->references('user_id')->on('users')
                  ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('committee_member');
    }
}
