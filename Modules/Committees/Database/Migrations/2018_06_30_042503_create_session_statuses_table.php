<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSessionStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        
        
        DB::table('session_statuses')->insert([
            ['id' => 1, 'name' => 'جديدة'],
            ['id' => 2, 'name' => 'تم إعداد جدول الأعمال'],
            ['id' => 3, 'name' => 'تم إعداد المحضر للتوقيع'],
            ['id' => 4, 'name' => 'تم إرسال المحضر للاعتماد'],
            ['id' => 5, 'name' => 'تم اعتماد المحضر'],
            ['id' => 6, 'name' => 'للاستكمال'],
            ['id' => 7, 'name' => 'في العرض'],
            ['id' => 8, 'name' => 'تم الاستكمال'],
            ['id' => 9, 'name' => 'تم إرسال المحضر للاعتماد'],
            ['id' => 10, 'name' => 'تم اعتماد المحضر'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_statuses');
    }
}
