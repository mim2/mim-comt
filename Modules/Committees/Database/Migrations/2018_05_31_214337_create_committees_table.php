<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCommitteesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('committees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('latest_version');
            $table->timestamp('created_at');
        });

        Schema::create('committees_version', function (Blueprint $table) {
            $table->integer('ref_id');
            $table->integer('version');
            $table->integer('parent_id')->default(0);
            $table->integer('committee_or_council');
            $table->integer('type_id');
            $table->string('name');
            $table->text('description');

            $table->string('decision_name');
            $table->string('decision_url');
            $table->integer('user_id');

            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();

            $table->string('stop_reason')->nullable();
            $table->integer('is_stopped')->default(0);
            $table->dateTime('stop_date')->nullable();
            $table->dateTime('resume_date')->nullable();

            $table->integer('category')->default(0);
            $table->timestamp('updated_at');
            $table->softDeletes();
            $table->primary(['ref_id', 'version']);
        });
  
    }
    
       
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('committees');
        Schema::dropIfExists('committees_version');
    }
}
