<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommitteeMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('committee_members', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('committee_id');
            $table->unsignedInteger('member_type_id');
            $table->unsignedInteger('created_by_user_id');

            $table->foreign('user_id')

                  ->references('user_id')->on('users')

                  ->onDelete('cascade');

            $table->foreign('committee_id')
                  ->references('id')->on('committees')
                  ->onDelete('cascade');


            $table->foreign('member_type_id')
                  ->references('id')->on('member_types')
                  ->onDelete('cascade');

            
            $table->foreign('created_by_user_id')
                  ->references('user_id')->on('users')
                  ->onDelete('cascade');

            $table->softDeletes();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('committee_members');
    }
}
