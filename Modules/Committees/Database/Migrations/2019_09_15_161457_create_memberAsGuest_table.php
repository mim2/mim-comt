<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberAsGuestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberAsGuest', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('session_id');
            $table->unsignedInteger('attendance_status_id')->default(1);
            $table->text('note')->nullable();

            $table->string('name')->nullable();
            $table->string('national_id', 10)->nullable();
            $table->string('email', 128)->nullable();
            $table->unsignedInteger('user_id')->nullable();

            $table->unsignedInteger('created_by');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberAsGuest');
    }
}
