<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOldSessionIdToSessionAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('session_attendances', function (Blueprint $table) {
            $table->unsignedInteger('old_session_id')->nullable();
            $table->unsignedInteger('old_attendance_status_id')->nullable();
            $table->text('old_apology_reason')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('session_attendances', function (Blueprint $table) {

        });
    }
}
