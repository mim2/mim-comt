<?php

namespace Modules\Committees\Providers;

use App\Events\AddNewSessionToCommittee;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use Modules\Committees\Events\CommitteeMemberCreated;
use Modules\Committees\Listeners\NotifyCommitteeMemberCreatedByEmail;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        CommitteeMemberCreated::class => [
            NotifyCommitteeMemberCreatedByEmail::class,
        ],
        AddNewSessionToCommittee::class => [
            \Modules\Committees\Listeners\NotifyMembersForNewSessionByEmail::class,
        ],
    ];
}