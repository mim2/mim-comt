<?php

namespace Modules\Committees\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Modules\Committees\Entities\CommitteeMember;
use Modules\Committees\Entities\Session;

class AddNewSessionToCommittee
{
    use SerializesModels, Dispatchable, InteractsWithSockets;

    public $committeeMembers;

    public $sessions;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(CommitteeMember $committeeMembers, Session $sessions)
    {
        $this->committeeMembers = $committeeMembers;
        $this->sessions = $sessions;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
