<?php

namespace Modules\Committees\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Committees\Entities\CommitteeMember;

class CommitteeMemberCreated
{
    use SerializesModels;


    public $committeeMember;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(CommitteeMember $committeeMember)
    {
        $this->committeeMember = $committeeMember;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
