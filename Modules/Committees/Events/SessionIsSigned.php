<?php

namespace Modules\Committees\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Committees\Entities\Session;

class SessionIsSigned
{
    use SerializesModels;


    public $session;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
