 
<h5 class="text-right headtitle head3 mr-3 ">خامساً: توقيع الأعضاء</h5>
<table class="table table-bordered ">
    <tr>
        <th class="tdtitle">العضو</th>
        <th class="tdtitle">رأي العضو</th>
        <th class="tdtitle">أسباب التحفظ</th>
    </tr>
    @foreach($committees->sessions[0]->attendees as $attendee)
    @if (@$attendee->member->user_name)
        @if ($attendee->attendanceStatus->id == 1)
            <?php @$opinion = $committees->sessions[0]->opinions->where('user_id', $attendee->member->user_id)->first() ?>
            <tr>
                <td>{{ @$attendee->member->user_name }}</td>
                <td>{{ $opinion ? $opinion->opinion == 1 ? 'موافق' : 'غير موافق' : 'لم يوقع'}}</td>
                <td>{{ $opinion ? $opinion->reservation_reason : '' }}</td>
            </tr>
        @endif
        @endif
    @endforeach
</table>
<h5 class="text-right headtitle head3 mr-3 "> توقيع المدعوين</h5>
<table class="table table-bordered ">
    <tr>
        <th class="tdtitle">المدعو</th>
        <th class="tdtitle">رأي المدعو</th>
        <th class="tdtitle">أسباب التحفظ</th>
    </tr>
    @foreach($committees->sessions[0]->guests->where('user_id', null) as $guest)
        <tr>
            <td>{{ $guest->name }}</td>
            <td>موافق</td>
            {{-- <td></td> --}}
        </tr>
    @endforeach
</table>
