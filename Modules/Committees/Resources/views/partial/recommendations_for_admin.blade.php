<h5 class="text-right headtitle head3 mr-3">ثالثا: المناقشات </h5>

<table class="table text-center">
    <thead class="thcusme2 th3">
    <tr>
        <th scope="col">المناقشة</th>
    </tr>
    </thead>
    <tbody>

   
        <tr>
            
            <td style="text-align: justify; word-break: break-all;">{{ @$session->recommendation->recommendation }}</td>
        </tr>

  

    </tbody>
</table>

<h5 class="text-right headtitle head3 mr-3">رابعاً: المهام / التوصيات</h5>
<table class="table text-center">
    <thead class="thcusme2 th5">
    <tr>
        <th scope="col">#</th>
        <th scope="col">التوصية</th>
        <th scope="col">المسؤول</th>
        <th scope="col">وقت الإستحقاق</th>
    </tr>
    </thead>
    <tbody>

    @foreach($session->tasks as $task)

          
            <tr>
                <td>{{ $loop->iteration  }}</td>
                <td style="word-break: break-all;">{{ @$task->discussion->discussions }}</td>
                @if (@$task->user_id != 99999)
                <td>{{ @$task->assignee->user_name }}</td>
                @else 
                <td>جميع الأعضاء</td>
                @endif
                <td>{{ @$task->time->name }}</td>
            </tr>

    @endforeach

    </tbody>
</table>

{{-- <script src="assets/js/main.js"></script> --}}
