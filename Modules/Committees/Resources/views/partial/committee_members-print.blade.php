<h5 class="text-right headtitle head3 mr-3">أولا: أعضاء اللجنة</h5>
<table>
    <thead>
    <tr  style="background-color: #b5b5b5;">
        <th align="right">سبب الغياب</th>
        <th align="right">الحالة</th>
        <th align="right">الصفة</th>
        <th align="right">اسم العضو</th>
        <th align="right" >#</th>
        
       
        
          
    </tr>
    </thead>
    <tbody>
        @foreach($session->committee->members as $member)
        <?php $attendee = $session->attendees->where('user_id', $member->member->user_id)->first() ?>
            <tr>
                <td>{{ $attendee ? $attendee->apology_reason : '-' }}</td>
                <td>{{ $attendee ? $attendee->attendanceStatus->name : '-' }}</td>
                <td>{{ $member->memberType->name }}</td>
                <td>{{ $member->member->user_name }}</td>
                <td>{{ @++$key }}</td>
            </tr>
        @endforeach
    </tbody>
</table>