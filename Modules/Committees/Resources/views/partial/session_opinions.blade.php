<h5 class="text-right headtitle head3 mr-3">خامساً: توقيع الأعضاء والمدعوين</h5>

<table class="table text-center">
    <thead class="thcusme2 th4">
    <tr>
        <th scope="col">#</th>
        <th scope="col">اسم العضو</th>
        <th scope="col">رأي العضو</th>
        <th scope="col">أسباب التحفظ</th>
        <?php $currentMember = $session->committee->members->where('user_id', Auth::user()->user_id)->first() ?>
        @if ($currentMember && in_array($currentMember->member_type_id, [1,2]))
        <th scope="col">رسالة تذكير</th>
        @endif
    </tr>
    </thead>
    <tbody>

    <?php  $guests = $session->guests->pluck('user_id')->toArray() ?>
    @foreach($session->attendees as $attendee)
        @if ($attendee->attendanceStatus->id == 1)
            @if(!in_array(@$attendee->member->user_id, $guests))

                <?php @$opinion = $session->opinions->where('user_id', $attendee->member->user_id)->first() ?>
                @if($attendee->member != null)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ @$attendee->member->user_name  }}</td>
                        <td>{{ $opinion ? $opinion->opinion == 1 ? 'موافق' : 'غير موافق' : 'لم يوقع'}}</td>
                        <td>{{ $opinion ? $opinion->reservation_reason : '' }}</td>
                        @if ($currentMember && in_array($currentMember->member_type_id, [1,2]))
                        <td class="">
                            @if(!$opinion)
                            <button class="rounded-0 border-0" onclick="notify({{ @$attendee->member->user_id }}, {{ $session->id }})">
                                <i class="fa fa-envelope fa-2x concolor"></i>
                            </button>
                            @endif
                        </td>
                        @endif
                    </tr>

                @endif
            @endif
        @endif
    @endforeach

    </tbody>
</table>
<hr>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="remindedmessage" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-0">
            <div class="modal-body rounded-0 text-center">
                <img alt="اللجان" class=" ml-1" src="/assets/images/paper-plane.svg"><br>
                <span id="member_name"></span>
            </div>
            <div class="modal-footer justify-content-center">
                <button class="btn btn-secondary nonBtn rounded-0 border-0" data-dismiss="modal" type="button">
                    إغلاق
                </button>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="waiting" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-0">
            <div class="modal-body rounded-0 text-center">
                <i class="fa fa-spinner"></i>
                <span>فضلاً انتظر</span>
            </div>
            <div class="modal-footer justify-content-center">
                <button class="btn btn-secondary nonBtn rounded-0 border-0" data-dismiss="modal" type="button">
                    إغلاق
                </button>
            </div>
        </div>
    </div>
</div>

<h5 class="text-right headtitle head3 mr-3">سادساً: رأي المدعويين في المحضر</h5>
<table class="table text-center">
    <thead class="thcusme2 th5">
    <tr>
        <th scope="col">#</th>
        <th scope="col">اسم المدعو</th>
        <th scope="col">رأيه في المحضر</th>
        <th scope="col">أسباب التحفظ</th>
    </tr>
    </thead>
    <tbody>

    @foreach($session->guests as $guest)

            <?php $opinion = $session->opinions->where('user_id', $guest->user_id)->first() ?>

            <tr>
                <td>{{ $loop->iteration  }}</td>
                <td>{{ @$guest->name }}</td>
                <td>{{ $opinion ? $opinion->opinion == 1 ? 'موافق' : 'غير موافق' : 'لم يوقع'}}</td>
                <td>{{ $opinion ? $opinion->reservation_reason : '' }}</td>
            </tr>

    @endforeach

    </tbody>
</table>
<hr>

