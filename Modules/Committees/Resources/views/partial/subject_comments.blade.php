{{-- <span class="p-2 text-center details" >
    <a class="standing2 p-2" data-toggle="collapse" data-target="#comments-{{ $subject->id }}" aria-controls="collapseExample">
        <i class="fa fa-comments-o"></i> جميع الأراء
    </a>
</span> --}}
{{-- <span class="p-2 text-center details">
    <a class="standing1 p-2" data-target="#add_comment-{{ $subject->id }}" data-toggle="modal" href="" target="_self" title="اضافة رأي">
        <i class="fa fa-commenting-o"></i> إضافة رأي
    </a>
</span> --}}
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="add_comment-{{ $subject->id }}" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content border-0 rounded-0">
            <div class="modal-header border-0 rounded-0 p-0">
            </div>
            {{ Form::open(['method' => 'POST', 'route' => ['comments.store', $subject->id]]) }}
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col border-0 rounded-0">
                        <label for="exampleFormControlSelect1"><i class="fa fa-plus"></i> إضافة رأي </label>
                        <textarea style="min-height: 150px;" class="form-control textsty" name="comment"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer border-0 rounded-0">
                <button class="btn sendBtn border-0 rounded-0 ml-2 pl-4 pr-4" type="submit">
                    إرسال
                </button>
                <button class="btn nonBtn border-0 rounded-0 ml-2 pl-4 pr-4" data-dismiss="modal" type="button">
                    إلغاء
                </button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<div class="collapse" id="comments-{{ $subject->id }}" style="margin-top: 20px;">
    <table class="table text-center">
        <thead class="thcusme2 th3">
        <tr>
            <th scope="col">الاسم</th>
            <th scope="col" style="width: 75%;"> التعليق</th>
        </tr>
        </thead>
        <tbody>
        @foreach($subject->comments as $comment)
            <tr>
                <td>{{ $comment->user->name }}</td>
                <td>{{ $comment->comment }}</td>
            </tr>
        @endforeach
        @unless($subject->comments->count())
            <tr>
                <td colspan="2">لا توجد آراء حالياً.</td>
            </tr>
        @endunless
        </tbody>
    </table>
</div>
