<hr>
<h5 class="text-right headtitle head3 mr-3 mt-5">آراء المفوض عنهم</h5>

@if(auth()->user()->auths->count() < 1)
    <p class=" sour text-right mr-3 ml-1">
        لا يوجد لديك مفوض عنهم
    </p>
@endif

@foreach(auth()->user()->auths as $auth)
    <?php $opinion = $session->opinions->where('user_id', $auth->from_user_id)->first() ?>
    <p class=" sour text-right mr-3 ml-1">
        حالة توقيع
        <u>{{ $auth->auther->full_name }}</u>
        :
        {{ $opinion ? $opinion->opinion == 1 ? 'موافق' : 'غير موافق' : 'لم تدلي بالرأي بعد في المحضر'}}
    </p>
    @if($session->status_id == 3)
        @if(!$opinion  )
            <form action="{{ route('sessions.opinion', $session) }}" method="post">
                @csrf
                <input type="hidden" name="user_id" value="{{ $auth->auther->user_id }}">
                <table class="table text-center">
                    <tbody>
                    <tr>
                        <td>ماهو رأي <u>{{ $auth->auther->full_name }}</u> في المحضر</td>
                        <td class="iftrached">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input ml-2" id="agreeBtn{{ $loop->index }}a" name="opinion"
                                       type="radio" value="1">
                                <label class="form-check-label" for="agreeBtn{{ $loop->index }}a">موافق</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input ml-2" id="agreeBtn{{ $loop->index }}b" name="opinion"
                                       type="radio" value="2">
                                <label class="form-check-label" for="agreeBtn{{ $loop->index }}b">غير موافق</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>هل لدى <u>{{ $auth->auther->full_name }}</u> تحفظات</td>
                        <td class="iftrached">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input ml-2" id="reservation{{ $loop->index }}a"
                                       name="has_reservation" type="radio" value="2"
                                       onclick="hideNote('#note{{ $loop->index }}')">
                                <label class="form-check-label" for="reservation{{ $loop->index }}a">لا</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input ml-2" id="reservation{{ $loop->index }}b"
                                       name="has_reservation" type="radio" value="1"
                                       onclick="showNote('#note{{ $loop->index }}')">
                                <label class="form-check-label" for="reservation{{ $loop->index }}b">نعم</label>
                            </div>
                        </td>
                    </tr>
                    <tr id="note{{ $loop->index }}" style="display: none">
                        <td>الرجاء كتابة التحفظات</td>
                        <td>
                            <div class=" form-group col border-0 rounded-0">
                                <textarea name="reservation_reason" class="form-control textsty" placeholder="سبب التحفظ" style="height: 200px;">
                                </textarea>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div class="d-flex flex-row mb-3 justify-content-center text-center">
                    <div class="col sendsign">

                        <button type="submit" class="btn  submitBtn border-0 rounded-0 ml-2 pl-4 pr-4 mt-2">
                            <i class="fa fa-pencil acolor"></i> إرسال التوقيع
                            عن {{ $auth->auther->full_name }}
                        </button>
                        <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade"
                             id="signetuersend"
                             role="dialog"
                             tabindex="-1">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content rounded-0">
                                    <div class="modal-body rounded-0 text-center">
                                        <img alt="اللجان" class=" ml-1" src="/assets/images/paper-plane.svg"><br>
                                        <br>
                                        <span>تم ارسال توقيعك بنجاح</span>
                                        <br>
                                        <br>
                                        <a href="{{ url()->previous() }}">
                                            <button class="btn btn-secondary nonBtn rounded-0 border-0" type="button">
                                                إغلاق
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>


        @endif




    @endif

@endforeach




