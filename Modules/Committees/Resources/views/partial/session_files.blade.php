@if ($session->files->count())
<h5 class="text-right headtitle head3 mr-3">سادساً: ملفات خاصة بالمحضر</h5>
<table class="table text-center">
    <thead class="thcusme2 th6">
    <tr>
        <th scope="col">#</th>
        <th scope="col">عنوان الملف</th>
        <th scope="col">تنزيل الملف</th>
    </tr>
    </thead>
    <tbody>
    
    @foreach($session->files as $file)
    <tr>
        <td scope="row">{{ $loop->iteration }}</td>
        <td>{{ $file->file_name }}</td>
        <td class="">
            <a href="/{{ $file->file_path}}"><i class="fa fa-download fa-2x concolor"></i></a>
        </td>
    </tr>
    @endforeach
    
    
    </tbody>
</table>
@endif
<hr>

