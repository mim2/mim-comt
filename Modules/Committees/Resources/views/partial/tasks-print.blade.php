
<br />
<br />
<br />
<table>
    <tr style="text-align: center; background-color: #7d7d7d; color: #fff; font-size: 24px;">
        <th style="border:3.5px solid #fff; border-collapse: collapse; padding: 10px;" >المهام / التوصيات</th>
           
     
    </tr>
</table>

<table >
    <thead>
  

    <tr style="text-align: center; background-color: #D9D9D9; font-size: 20px; ">

        <th colspan="4">وقت الإستحقاق</th>
        <th colspan="4">المسؤول</th>
        <th colspan="4">المهمة / التوصية</th>
        <th>#</th>

    </tr>

    </thead>
    <tbody>
      
        @foreach ($session->tasks as $item)
            <tr>
                <td colspan="4" style="text-align: center"  align="center">{{ $item->time->name }}</td>
                @if (@$item->user_id != 99999)
                <td colspan="4" style="text-align: center"  align="center">{{ $item->assignee->user_name }}</td>
                @else 
                <td colspan="4" style="text-align: center"  align="center">جميع الأعضاء</td>
                @endif
                <td colspan="4" style="text-align: center"  align="center">{{ $item->discussion->discussions }}</td>

                <td align="center" colspan="1" style="border:2.5px solid #fff;
                border-collapse: collapse;
                padding: 10px; background-color: #D9D9D9;">{{ $loop->iteration }}</td>
            </tr>
        @endforeach
 
    </tbody>
</table>

