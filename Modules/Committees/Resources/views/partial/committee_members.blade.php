<h5 class="text-right headtitle head3 mr-3">أولا: أعضاء اللجنة</h5>
<table class="table table-bordered">
    <thead>
    <tr>
        <th class="tdtitle">#</th>
        <th class="tdtitle">اسم العضو</th>
        <th class="tdtitle">الصفة</th>
        <th class="tdtitle">الحالة</th>
         <th class="tdtitle">سبب الغياب</th> 
    </tr>
    </thead>
    <tbody>
        @foreach($session->committee->members as $member)
        <?php $attendee = $session->attendees->where('user_id', $member->member->user_id)->first() ?>
            <tr>
                <td>{{ @++$key }}</td>
                <td>{{ $member->member->user_name }}</td>
                <td>{{ $member->memberType->name }}</td>
                <td>{{ $attendee ? $attendee->attendanceStatus->name : '-' }}</td>
                <td>{{ $attendee ? $attendee->apology_reason : '-' }}</td> 
            </tr>
        @endforeach
    </tbody>
</table>