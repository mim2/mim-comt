<?php $opinion = $session->opinions->where('user_id', auth()->user()->user_id)->first() ?>

<h5 class="text-right headtitle head3 mr-3">رأيك في المحضر</h5>
<p class=" sour text-right mr-3 ml-1">
    حالة توقيعك: {{ $opinion ? $opinion->opinion == 1 ? 'موافق' : 'غير موافق' : 'لم تدلي برأيك بعد في المحضر'}}
</p>

@if($session->status_id == 3)
    @if(!$opinion  )

    <table class="table text-center">
        <tbody>
        <tr>
            <td>ماهو رأيك في المحضر</td>
            <td class="iftrached">
                <div class="form-check form-check-inline">
                    <input class="form-check-input ml-2" id="inlineRadio1" name="opinion" type="radio" value="1">
                    <label class="form-check-label" for="inlineRadio1">موافق</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input ml-2" id="inlineRadio3" name="opinion" type="radio" value="2">
                    <label class="form-check-label" for="inlineRadio3">غير موافق</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>هل لديك تحفظات</td>
            <td class="iftrached">
                <div class="form-check form-check-inline">
                    <input class="form-check-input ml-2" id="inlineRadio3" name="has_reservation" type="radio" value="2">
                    <label class="form-check-label" for="inlineRadio3">لا</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input ml-2" id="inlineRadio1" name="has_reservation" type="radio" value="1">
                    <label class="form-check-label" for="inlineRadio1">نعم</label>
                </div>
            </td>
        </tr>
        <tr id="note">
            <td>الرجاء كتابة تحفظاتك</td>
            <td>
                <div class=" form-group col border-0 rounded-0">
                    <textarea id="notes" name="reservation_reason" class="form-control textsty"
                              placeholder="سبب التحفظ" style="height: 200px;"></textarea>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

    @endif
    <div class="d-flex flex-row mb-3 justify-content-center text-center">
        <div class="col sendsign">
            <a href="{{ route('member.sessions.show-draft', [$session->committee, $session]) }}" target="_blank" class="btn removebtn sendBtn border-0 rounded-0 ml-2 pl-4 pr-4 mt-2" type="button">
                <i class="fa fa-print acolor"></i> طباعة المسودة
            </a>
            @if(!$opinion  )

            <button id="submit" class="btn  submitBtn border-0 rounded-0 ml-2 pl-4 pr-4 mt-2" type="submit">
                <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
                <i class="fa fa-pencil acolor"></i> إرسال التوقيع
            </button>
            @endif
            <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="signetuersend"
                 role="dialog"
                 tabindex="-1">
                <div class="modal-dialog" role="document">
                    <div class="modal-content rounded-0">
                        <div class="modal-body rounded-0 text-center">
                            <img  alt="اللجان" class=" ml-1" src="/assets/images/paper-plane.svg" ><br>
                            <br>
                            <span>تم ارسال توقيعك بنجاح</span>
                            <br>
                            <br>
                            <a href="{{ url()->previous() }}">
                                <button class="btn btn-secondary nonBtn rounded-0 border-0" type="button">
                                إغلاق
                                </button>
                            </a>
                        </div>
                        <div class="modal-footer justify-content-center">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endif

