@extends('layouts.main.index')

@section('page')

@section('stylesheet')

    <style>
        .table-bordered th {
            background: #000 !important;
        }
    </style>

@endsection

    {{-- @include('committees::partial.category') --}} 



<div class="container mt-5 minhe">
    <div class="row mt-2">
        <div class="col CommityDetails">
            <div class="row">
                <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                    <div class="maintitle d-flex justify-content-between">
                        <span class="sissionPage p-2 acolor">الرسائل</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col maindetails p-2 mt-3">
                    <div class="col ownertabelecon">
                        <table class="table table-bordered tb-b">
                            <thead class="ownertabele">
                            <tr>
                                <th class="tht">#</th>
                                <th class="tht"><i class="fa fa-envelope-o"></i></th>
                                <th class="tht">الأجندة</th>
                                <th class="tht">التاريخ</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($messages as $message)

                                <tr class="{{ $message->read ? '' : 'table-white' }}">
                                    <td class="ttd">{{ $message->id }}</td>
                                    <td class="ttd">
                                        <i class="fa {{ $message->read ? 'fa-envelope-open-o' : 'fa-envelope-o' }}"></i>
                                    </td>
                                    <td class="ttd">
                                        <a href="{{ route('messages.show', $message->id) }}">{{ $message->subject }}</a>
                                    </td>
                                    <td class="ttd">{{ $message->date }}</td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                        {{ $messages->links() }}
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
