<br>
<hr>
<div class="container card  mt-5 p-2 border-0">
    @isset($sessions)
        @if ($sessions->count())

            <table width="100%" class=" mt-5">
                <tr>
                    <td>
                        اسم اللجنة / المجلس: {{ $sessions->first()->committee->name }}
                    </td>
                    <td>
                        @if (request()->has('phase') && request()->phase)
                            المرحلة {{ \Modules\Committees\Entities\SessionTitle::find(request()->phase)->name }}
                        @endif
                    </td>
                </tr>
            </table>

            <table class="table table-striped text-center">
                <tr>
                    <td>م</td>
                    <td>رقم الإجتماع</td>
                    <td>عنوان الإجتماع</td>
                    <td>تاريخ انعقادها</td>
                    <td>وقت انعقادها</td>
                    <td>تاريخ اعتمادها</td>
                </tr>

                @foreach($sessions as $row)

                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $row->session_title_id }}</td>
                        <td>{{ $row->sessionTitle->name }}</td>
                        <td>{{ $row->date }}</td>
                        <td>من {{ $row->start_time }} إلى {{ $row->end_time }}</td>
                        <td>{{ App\Classes\Date\CarbonHijri::toHijriFromMiladi($row->history->first()->created_at) }}</td>
                    </tr>

                @endforeach

            </table>

            <h5 class="mt-5">الأعضاء:</h5>
            <table class="table table-striped text-center">
                <tr>
                    <td>م</td>
                    <td>رقم الهوية</td>
                    <td>العضو</td>
                    <td> نوع العضو </td>
                    <td>عدد الإجتماعات خارج وقت الدوام</td>
                </tr>                
                @foreach($sessions->first()->committee->members as $row)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ @$row->member->user_idno }}</td>
                        <td>{{ @$row->member->user_name }}</td>
                        <td>{{ @$row->memberType->name }}</td>
                        <td>0</td>
                    </tr>
                @endforeach        
            </table>


        @else
            <h2 class="m-4 text-center">لا توجد نتائج</h2>
        @endif
    @endisset
</div>
