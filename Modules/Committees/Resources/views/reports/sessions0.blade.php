<div class="container card  mt-5 p-2 border-0">
    {{ Form::open(['route' => 'reports.sessions.reports', 'method' => 'get']) }}
        <div class="row">
            <div class="col-md-4">
                {{ Form::label('report_id', 'التقرير', ['class' => 'control-label float-right']) }}
                {{ Form::select('report_id', [ 0 => 'إختر'] + $reports,  old('report_id', @$_GET['report_id']) , ['class' => 'form-control text-right-align select2']) }}

                {{-- <select id="report_id" name="report_id" class="form-control text-right-align select2">
                    <option >إختر</report_id>
                    @foreach ($reports as $value => $name)
                        <option value="{{ $value }}"
                            @if ($name != 'تقرير حصر  ')
                                disabled
                            @endif
                        >
                            {{ $name }}
                        </option>
                    @endforeach
                </select> --}}
                <span class="text-danger">{{ $errors->first('report_id') }}</span>
            </div>

            @if (request()->input('report_id') == 23)
                <div class="col-md-4">
                    {{ Form::label('type_id', 'النوع', ['class' => 'control-label float-right']) }}
                    {{ Form::select('type_id', [ null => 'إختر'] + $types, old('type_id', @$_GET['type_id']) , ['class' => 'form-control select2']) }}
                    <span class="text-danger">{{ $errors->first('type_id') }}</span>
                </div>
            @endif
            @if ($committees != null)

            <div class="col-md-4">
                {{ Form::label('committee', 'اللجنة/المجلس', ['class' => 'control-label float-right']) }}
                {{ Form::select('committee', [ null => 'إختر'] + $committees, old('committees', @$_GET['committee']) , ['class' => 'form-control']) }}
                <span class="text-danger">{{ $errors->first('committee') }}</span>
            </div>
            {{-- <div class="col-md-4">
                {{ Form::label('phase', 'المرحلة', ['class' => 'control-label float-right']) }}
                {{ Form::select('phase', [ null => ''] + $phases, old('phase', @$_GET['phase']) , ['class' => 'form-control']) }}
                <span class="text-danger">{{ $errors->first('phase') }}</span>
            </div> --}}

        @endif

    </div>

        @if (request()->has('committee'))
            <div class="col-md-4 mt-3">
                {{ Form::label('start_date', 'تاريخ البداية', ['class' => 'control-label float-right']) }}
                {{ Form::text('start_date', old('start_date', @$_GET['start_date']) , ['class' => 'form-control hijri-datepicker-input']) }}
                <span class="text-danger">{{ $errors->first('start_date') }}</span>
            </div>
            <div class="col-md-4 mt-3">
                {{ Form::label('end_date', 'تاريخ النهاية', ['class' => 'control-label float-right']) }}
                {{ Form::text('end_date', old('end_date', @$_GET['end_date']) , ['class' => 'form-control hijri-datepicker-input rounded-0']) }}
                <span class="text-danger">{{ $errors->first('end_date') }}</span>
            </div>
        @endif

    <div>
        <br/>
        <button type="submit" class="btn btn-primary">عرض التقرير</button>
    </div>
            <div class="row">

                <div class="col-md-4">
                    <br/>
                    {{-- @if ($committeeMembers)
                        <a href="{{ request()->fullUrl() }}&print=1" class="btn btn-secondary btn-sm">طباعة التقرير</a>
                    @endif --}}
                </div>
                    
            </div>
            
        </div>
        @isset($sessions)
            @if ($sessions->count())
            <div class="container card">
                <table width="100%" class=" mt-5">
                    <tr>
                        <td>
                            اسم اللجنة / المجلس: {{ $sessions->first()->committee->name }}
                        </td>
                        <td>
                            @if (request()->has('phase') && request()->phase)
                                المرحلة {{ \Modules\Committees\Entities\SessionTitle::find(request()->phase)->name }}
                            @endif
                        </td>
                    </tr>
                </table>

                <table class="table table-striped text-center">
                    <tr>
                        <td>م</td>
                        <td>رقم الإجتماع</td>
                        <td>عنوان الإجتماع</td>
                        <td>تاريخ انعقادها</td>
                        <td>وقت انعقادها</td>
                        <td>تاريخ اعتمادها</td>
                    </tr>

                    @foreach($sessions as $row)

                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->session_title_id }}</td>
                            <td>{{ $row->sessionTitle->name }}</td>
                            <td>{{ $row->date }}</td>
                            <td>من {{ $row->start_time }} إلى {{ $row->end_time }}</td>
                            <td>{{ App\Classes\Date\CarbonHijri::toHijriFromMiladi($row->history->first()->created_at) }}</td>
                        </tr>

                    @endforeach

                </table>                

                <h5 class="mt-5 text-right">الأعضاء:</h5>
                <table class="table table-striped text-center">
                    <tr>
                        <td>م</td>
                    
                        <td>العضو</td>
                        <td>رقم الهوية</td>
                        <td> نوع العضو </td>
                        <th scope="col">الحالة</th>

                    </tr>                
                    @foreach($sessions[0]->committee->members as $member)
                    <?php $attendee = $sessions[0]->attendees->where('user_id', $member->member->user_id)->first() ?>
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                
                                <td>{{ @$member->member->user_name }}</td>                        
                                <td>{{ @$member->member->user_idno }}</td>
                                <td>{{ @$member->memberType->name }}</td>
                                <td>{{ $attendee ? $attendee->attendanceStatus->name : '-' }}</td>

                            </tr>
                            
                    @endforeach        
                </table>

            </div>
            @else
                <h2 class="m-4 text-center">لا توجد نتائج</h2>
            @endif
            
        @endisset

        <hr>
{{--        <div class="col-md-2">--}}
{{--            @if ($sessions)--}}
{{--                <a href="{{ request()->fullUrl() }}&print=1" class="btn btn-success">طباعة التقرير</a>--}}
{{--            @endif                --}}
{{--        </div>--}}
    {{ Form::close() }}
</div>


