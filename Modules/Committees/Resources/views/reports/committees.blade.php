@extends('layouts.main.index')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="/vendor-assets/css/ummalqura.calendars.picker.css">
@endsection

@section('page')
    </br>
    </br>
    </br>
    <div class="container">
        <div class="row">
            <div class="reports-header">
                <div class="col-1">
                    <a href="{{ route('reports.index') }}" class="btn btn-outline-light">التقارير</a>
                </div>
                <div class="col-11">
                    <h1>تقارير اللجان و المجالس</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container card  mt-5 p-2 border-0">
        <form action="{{ route('committees.reports.filters') }}" method="post">
            @csrf
            <input type="hidden" name="report_type" value="committees">
            <div class="overflow-hidden" style="overflow:hidden !important;">
                <div class="form-group col-3 float-right text-right">
                    <label>الاسم</label>
                    <input name="filter[name]" class="form-control" />
                </div>
                <div class="form-group col-3 float-right text-right">
                    <label>النوع</label>
                    <select name="filter[type]" class="form-control">
                        <option value=""></option>
                        <option value="1">لجنة</option>
                        <option value="2">مجلس</option>
                    </select>
                </div>
                <div class="form-group col-3 float-right text-right">
                    <label>التصنيف</label>
                    <select name="filter[category]" class="form-control">
                        <option value=""></option>
                        <option value="0">داخلي</option>
                        <option value="1">خارجي</option>
                    </select>
                </div>
                <div class="form-group col-3 float-right text-right">
                    <label>النوع</label>
                    <select name="filter[type_id]" class="form-control">
                        <option value=""></option>
                        @foreach($types as $key => $type)
                            <option value="{{ $key }}">{{ $type }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-3 float-right text-right">
                    <label>الخصوصية</label>
                    <select name="filter[privacy]" class="form-control">
                        <option value=""></option>
                        @foreach(\App\Classes\Utils::privacyList() as $key => $privacy)
                            <option value="{{ $key }}">{{ $privacy }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-3 float-right text-right">
                    <label>رقم القرار</label>
                    <input name="filter[decision]" class="form-control" />
                </div>

                <div class="form-group col-3 float-right text-right">
                    <label>الحالة</label>
                    <select name="filter[is_stopped]" class="form-control">
                        <option value=""></option>
                        <option value="0">نشطة</option>
                        <option value="1">غير نشطة</option>
                    </select>
                </div>

            </div>
            <div class="col-12">
                <button type="submit" class="btn btn-primary">بحث</button>
            </div>
        </form>


        <hr>


        <div class="container card  mt-5 p-2 border-0">
            <h5 class="mt-5 text-right">النتائج:
                {{ $committees->count() }} نتيجة
            </h5>

            <table class="table table-hover text-right">
                <thead>
                    <tr class="bg-secondary text-white">
                        <th>الاسم</th>
                        <th> نوع اللجنة او المجلس</th>
                        <th>حالة اللجنة</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($committees as $committee)
                    <tr>
                        <td>{{ $committee->name }}</td>
                        <td>{{ $committee->type->name }}</td>
                        <td>{{ $committee->is_stopped == 0 ? 'نشطة' : 'غير نشطة '}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>



    </div>
@endsection
