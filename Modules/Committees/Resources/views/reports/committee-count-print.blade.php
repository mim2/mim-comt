<div class="container card  mt-5 p-2 border-0">
    
    @isset($committeeCount)
        @if ($committeeCount->count())
            </table>
            <h5 class="mt-5 text-right">الأعضاء:</h5>
            <table class="table table-striped text-center">
                <tr>
                    <td>  اسم اللجنة</td>
                    <td> نوع اللجنة او المجلس</td>
                    <td>حالة اللجنة</td>
                    <td> العضو </td>
                    <td> صفة العضو </td>
                </tr>    
                @foreach($committeeCount as $committe_id => $members)
                    @foreach ($members->whereIn('member_type_id', [1, 2, 4]) as $item)
                        <tr>
                            <td>{{ @$item->committee->name }}</td>
                            <td>{{ @$item->committee->type->name }}</td>
                            <td>{{ @$item->committee->is_stopped == 0 ? 'مفعلة' : 'متوقفة '}}</td>
                            <td>{{ @$item->member->user_name}}</td>   
                            <td>{{ @$item->memberType->name}}</td>                              
                        </tr>
                    
                    @endforeach  
                @endforeach
            </table>
        @else
            <h2 class="m-4 text-center">لا توجد نتائج</h2>
        @endif


    @endisset
    

</div>


