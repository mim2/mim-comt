<div class="container card  mt-5 p-2 border-0">

    @isset($statndingCommitteesCount)
        {{-- @if ($statndingCommitteesCount->count()) --}}
            </table>
            <h5 class="mt-5 text-right">الأعضاء:</h5>
            <table class="table table-striped text-center">
                <tr>
                    <td>  اسم اللجنة</td>
                    <td>   عدد الإجتماعات وقت الدوام </td>
                    <td> عدد الاعضاء</td>
                    <td>  عدد الإجتماعات خارج الدوام </td>
                    <td>   عدد الإجتماعات </td>
                </tr>    
                @foreach($statndingCommitteesCount as $standig)
                

                    <tr>
                        <td>{{ @$standig->name }}</td>
                            @if (@$standig->sessions[0]->start_time <= "12:30:00")
                                <td>{{ @$standig->sessions->count() }}</td>
                            @else
                                <td>لا يوجد محاضر</td>
                            @endif
                            <td>{{ @$standig->members->count() }}</td> 
                            @if (@$standig->sessions[0]->start_time >= "12:30:00")
                                <td>{{ @$standig->sessions->count() }}</td>
                            @else
                            <td>{{ @$standig->sessions->count() }}</td>                                
                            @endif
                            <td>{{ @$standig->sessions->count() }}</td>
                    </tr>
                @endforeach
            </table>
        {{-- @else
            <h2 class="m-4 text-center">لا توجد نتائج</h2>
        @endif --}}


    @endisset
    

</div>


