<div class="container card  mt-5 p-2 border-0">

    {{ Form::open(['route' => 'reports.committees.reports', 'method' => 'get']) }}
        <div class="row">
            <div class="col-md-4">
                {{ Form::label('report_id', 'التقرير', ['class' => 'control-label pull-right']) }}
                {{ Form::select('report_id', [ null => 'إختر'] + $reports, old('report_id', @$_GET['report_id']) , ['class' => 'form-control text-right-align select2']) }}
                <span class="text-danger">{{ $errors->first('report_id') }}</span>
            </div>
            
        
        {{-- @if (request()->input('report_id'))
            <div class="col-md-4">
                {{ Form::label('type_id', 'النوع', ['class' => 'control-label float-right']) }}
                {{ Form::select('type_id', [ null => 'إختر'] + $types, old('type_id', @$_GET['type_id']) , ['class' => 'form-control select2']) }}
                <span class="text-danger">{{ $errors->first('type_id') }}</span>
            </div>
        @endif
        @if ($committees != null)
            <div class="col-md-4">
                {{ Form::label('committee', 'اللجنة/المجلس', ['class' => 'control-label float-right']) }}
                {{ Form::select('committee', [ null => 'إختر'] + $committees, old('committees', @$_GET['committee']) , ['class' => 'form-control select2']) }}
                <span class="text-danger">{{ $errors->first('committee') }}</span>
            </div>
        @endif --}}
            @if (request()->input('report_id'))
                <div class="col-md-4 mt-3">
                    {{ Form::label('start_date', 'تاريخ البداية', ['class' => 'control-label float-right']) }}
                    {{ Form::text('start_date', old('start_date', @$_GET['start_date']) , ['class' => 'form-control hijri-datepicker-input']) }}
                    <span class="text-danger">{{ $errors->first('start_date') }}</span>
                </div>
                <div class="col-md-4 mt-3">
                    {{ Form::label('end_date', 'تاريخ النهاية', ['class' => 'control-label float-right']) }}
                    {{ Form::text('end_date', old('end_date', @$_GET['end_date']) , ['class' => 'form-control hijri-datepicker-input rounded-0']) }}
                    <span class="text-danger">{{ $errors->first('end_date') }}</span>
                </div>

            @endif
        </div>

    <div class="">
        <br/>
        <button type="submit" class="btn btn-primary">عرض التقرير</button>
        @if ($sessions)
            <a href="{{ request()->fullUrl() }}&print=1" class="btn btn-success">طباعة التقرير</a>
        @endif
    </div>

        </div>
        @isset($statndingCommitteesCount)
            {{-- @if ($statndingCommitteesCount->count()) --}}
                </table>
                <h5 class="mt-5 text-right">الأعضاء:</h5>
                <table class="table table-striped text-center">
                    <tr>
                        <td>  اسم اللجنة</td>
                        <td>   عدد الإجتماعات وقت الدوام </td>
                        <td> عدد الاعضاء</td>
                        <td>  عدد الإجتماعات خارج الدوام </td>
                        <td>   عدد الإجتماعات </td>
                    </tr>    
                    @foreach($statndingCommitteesCount as $standig)
                    

                        <tr>
                            <td>{{ @$standig->name }}</td>
                                @if (@$standig->sessions[0]->start_time <= "12:30:00")
                                    <td>{{ @$standig->sessions->count() }}</td>
                                @else
                                    <td>لا يوجد محاضر</td>
                                @endif
                                <td>{{ @$standig->members->count() }}</td> 
                                @if (@$standig->sessions[0]->start_time >= "12:30:00")
                                    <td>{{ @$standig->sessions->count() }}</td>
                                @else
                                <td>{{ @$standig->sessions->count() }}</td>                                
                                @endif
                                <td>{{ @$standig->sessions->count() }}</td>
                        </tr>
                    @endforeach
                </table>
            {{-- @else
                <h2 class="m-4 text-center">لا توجد نتائج</h2>
            @endif --}}
    
            <a href="{{ request()->fullUrl() }}&print=1" class="btn btn-success">طباعة التقرير</a>
    
        @endisset
        
    {{ Form::close() }}
    </div>
    
    
    