<div class="container card  mt-5 p-2 border-0">

        
        {{ Form::open(['route' => 'reports.members.reports', 'method' => 'get']) }}
            <div class="row">
                <div class="col-md-4">
                    {{ Form::label('report_id', 'التقرير', ['class' => 'control-label float-right']) }}
                    {{ Form::select('report_id', [ null => 'إختر'] + $reports, old('report_id', @$_GET['report_id']) , ['class' => 'form-control text-right-align select2']) }}
                    <span class="text-danger">{{ $errors->first('report_id') }}</span>
                </div>

                @if (request()->input('report_id'))
                    <div class="col-md-4">
                        {{ Form::label('type_id', 'النوع', ['class' => 'control-label float-right']) }}
                        {{ Form::select('type_id', [ null => 'إختر'] + $types, old('type_id', @$_GET['type_id']) , ['class' => 'form-control select2']) }}
                        <span class="text-danger">{{ $errors->first('type_id') }}</span>
                    </div>
                @endif
                

                @if ($committees != null)

                <div class="col-md-4">
                    {{ Form::label('committee', 'اللجنة/المجلس', ['class' => 'control-label float-right']) }}
                    {{ Form::select('committee', [ null => 'إختر'] + $committees, old('committees', @$_GET['committee']) , ['class' => 'form-control select2']) }}
                    <span class="text-danger">{{ $errors->first('committee') }}</span>
                </div>
                {{-- <div class="col-md-4">
                    {{ Form::label('phase', 'المرحلة', ['class' => 'control-label float-right']) }}
                    {{ Form::select('phase', [ null => ''] + $phases, old('phase', @$_GET['phase']) , ['class' => 'form-control select2']) }}
                    <span class="text-danger">{{ $errors->first('phase') }}</span>
                </div> --}}

            @endif

                    @if (request()->input('committee'))
                        <div class="col-md-4 mt-3">
                            {{ Form::label('start_date', 'تاريخ البداية', ['class' => 'control-label float-right']) }}
                            {{ Form::text('start_date', old('start_date', @$_GET['start_date']) , ['class' => 'form-control hijri-datepicker-input']) }}
                            <span class="text-danger">{{ $errors->first('start_date') }}</span>
                        </div>
                        <div class="col-md-4 mt-3">
                            {{ Form::label('end_date', 'تاريخ النهاية', ['class' => 'control-label float-right']) }}
                            {{ Form::text('end_date', old('end_date', @$_GET['end_date']) , ['class' => 'form-control hijri-datepicker-input rounded-0']) }}
                            <span class="text-danger">{{ $errors->first('end_date') }}</span>
                        </div>
                    @endif
        </div>
    <div>
        <br/>
        <button type="submit" class="btn btn-primary">عرض التقرير</button>
    </div>
        @isset($sessions)
            @if ($sessions->count())

                <table width="100%" class=" mt-5">
                    <tr>
                        <td>
                            اسم اللجنة / المجلس: {{ $sessions->first()->committee->name }}
                        </td>
                        <td>
                            @if (request()->has('phase') && request()->phase)
                                المرحلة {{ \Modules\Committees\Entities\SessionTitle::find(request()->phase)->name }}
                            @endif
                        </td>
                    </tr>
                </table>

                <table class="table table-striped text-center">
                    <tr>
                        <td>م</td>
                        <td>رقم الإجتماع</td>
                        <td>عنوان الإجتماع</td>
                        <td>تاريخ انعقادها</td>
                        <td>وقت انعقادها</td>
                        <td>تاريخ اعتمادها</td>
                    </tr>

                    @foreach($sessions as $row)

                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->session_title_id }}</td>
                            <td>{{ $row->sessionTitle->name }}</td>
                            <td>{{ $row->date }}</td>
                            <td>من {{ $row->start_time }} إلى {{ $row->end_time }}</td>
                            <td>{{ App\Classes\Date\CarbonHijri::toHijriFromMiladi($row->history->first()->created_at) }}</td>
                        </tr>

                    @endforeach

                </table>

                <h5 class="mt-5">الأعضاء:</h5>
                <table class="table table-striped text-center">
                    <tr>
                        <td>م</td>
                        <td>رقم الهوية</td>
                        <td>العضو</td>
                        <td> نوع العضو </td>
                        <td>عدد الإجتماع خارج وقت الدوام</td>
                    </tr>                
                    @foreach($sessions->first()->committee->members as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ @$row->member->user_idno }}</td>
                            <td>{{ @$row->member->user_name }}</td>
                            <td>{{ @$row->memberType->name }}</td>
                            <td>0</td>
                        </tr>
                    @endforeach        
                </table>


            @else
                <h2 class="m-4 text-center">لا توجد نتائج</h2>
            @endif
        @endisset
        <hr>
        <div class="col-md-2">
            @if ($sessions)
                <a href="{{ request()->fullUrl() }}&print=1" class="btn btn-success">طباعة التقرير</a>
            @endif                
        </div>
    {{ Form::close() }}
</div>

