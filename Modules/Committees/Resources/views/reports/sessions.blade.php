@extends('layouts.main.index')

@section('page')
</br>
</br>
</br>
<div class="container">
    <div class="row">
        <div class="reports-header">
            <div class="col-1">
                <a href="{{ route('reports.index') }}" class="btn btn-outline-light">التقارير</a>
            </div>
            <div class="col-11">
                <h1>تقارير الاجتماعات</h1>
            </div>
        </div>
    </div>
</div>

<div class="container card  mt-5 p-2 border-0">
    <form action="{{ route('committees.reports.filters') }}" method="post">
        @csrf
        <input type="hidden" name="report_type" value="sessions">
        <div class="overflow-hidden" style="overflow:hidden !important;">
            <div class="form-group col-3 float-right text-right">
                <label>العنوان</label>
                <input name="append[title]" class="form-control" />
            </div>

            <div class="form-group col-3 float-right text-right">
                <label>الاسم</label>
                <input name="full_name" class="form-control" />
            </div>

            <div class="form-group col-3 float-right text-right">
                <label>الجوال</label>
                <input name="user_mobile" class="form-control" />
            </div>

            <div class="form-group col-3 float-right text-right">
                <label>رقم الهوية</label>
                <input name="user_idno" class="form-control" />
            </div>

            <div class="form-group col-3 float-right text-right">
                <label>البريد الإلكتروني</label>
                <input name="user_mail" class="form-control" />
            </div>

            <div class="form-group col-3 float-right text-right">
                <label>اللجنة/المجلس</label>
                <select name="committee_id" class="form-control">
                    <option value=""></option>
                    @foreach($committees as $committee)
                        <option value="{{ $committee->ref_id }}">{{ $committee->name }}</option>
                    @endforeach
                </select>
            </div>



        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-primary">بحث</button>
        </div>
    </form>


    <hr>


    <div class="container card  mt-5 p-2 border-0">
        <h5 class="mt-5 text-right">النتائج:
            {{ $sessions->count() }} نتيجة
        </h5>

        <table class="table table-hover text-right">
            <thead>
            <tr class="bg-secondary text-white">
                <th>الاسم</th>
                <th>رقم الهوية</th>
                <th>رقم الجوال</th>
                <th>البريد الإلكتروني</th>
                <th>الحالة</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sessions as $session)
                <tr>
                    <td>{{ $session->title }}</td>
                    <td>{{ $session->user_idno }}</td>
                    <td>{{ $session->user_mobile }}</td>
                    <td>{{ $session->user_mail }}</td>
                    <td>{{ $session->is_active }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>



</div>


@endsection
