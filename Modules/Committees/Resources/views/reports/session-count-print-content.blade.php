<div class="container card  mt-5 p-2 border-0">
   
    @isset($sessions)
        @if ($sessions->count())
        <div class="container card">
            <table width="100%" class=" mt-5">
                <tr>
                    <td>
                        اسم اللجنة / المجلس: {{ $sessions->first()->committee->name }}
                    </td>
                    <td>
                        @if (request()->has('phase') && request()->phase)
                            المرحلة {{ \Modules\Committees\Entities\SessionTitle::find(request()->phase)->name }}
                        @endif
                    </td>
                </tr>
            </table>

            <table class="table table-striped text-center">
                <tr>
                    <td>م</td>
                    <td>رقم الإجتماع</td>
                    <td>عنوان الإجتماع</td>
                    <td>تاريخ انعقادها</td>
                    <td>وقت انعقادها</td>
                    <td>تاريخ اعتمادها</td>
                </tr>

                @foreach($sessions as $row)

                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $row->session_title_id }}</td>
                        <td>{{ $row->sessionTitle->name }}</td>
                        <td>{{ $row->date }}</td>
                        <td>من {{ $row->start_time }} إلى {{ $row->end_time }}</td>
                        <td>{{ App\Classes\Date\CarbonHijri::toHijriFromMiladi($row->history->first()->created_at) }}</td>
                    </tr>

                @endforeach

            </table>                

            <h5 class="mt-5 text-right">الأعضاء:</h5>
            <table class="table table-striped text-center">
                <tr>
                    <td>م</td>
                
                    <td>العضو</td>
                    <td>رقم الهوية</td>
                    <td> نوع العضو </td>
                    <th scope="col">الحالة</th>

                </tr>                
                @foreach($sessions[0]->committee->members as $member)
                <?php $attendee = $sessions[0]->attendees->where('user_id', $member->member->user_id)->first() ?>
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            
                            <td>{{ @$member->member->user_name }}</td>                        
                            <td>{{ @$member->member->user_idno }}</td>
                            <td>{{ @$member->memberType->name }}</td>
                            <td>{{ $attendee ? $attendee->attendanceStatus->name : '-' }}</td>

                        </tr>
                        
                @endforeach        
            </table>

        </div>
        @else
            <h2 class="m-4 text-center">لا توجد نتائج</h2>
        @endif
        
    @endisset

    <hr>
    
</div>


