<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.plugin.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.plus.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.picker.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.picker-ar.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.ummalqura.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.ummalqura-ar.js"></script>
<script type="text/javascript">
    var calHj = $.calendars.instance('ummalqura', 'ar');
    $('.hijri-datepicker-input').calendarsPicker($.extend({
            calendar: calHj,
            dateFormat: 'yyyy-mm-dd',
        },
        $.calendarsPicker.regionalOptions['ar'])
    );


    $('#type_id, #committee').on('change', function (e) {
        $(this).closest('form').submit();
    });
    $("#report_id option").removeAttr('disabled').
    filter( "[value='10'], [value='11'], [value='12'], [value='14'], [value='16'], [value='17'], [value='18'], [value='19'], [value='20']" ).
    attr( 'disabled', 'disabled' );

    
    $('#report_id').on('change', function (e) {
        if ($(this).val() == 15) {
            $(this).closest('form').submit();
        }
        
    });
    $('#report_id').on('change', function (e) {
        if ($(this).val() == 13) {
            $(this).closest('form').submit();
        }
        
    });
    $('#report_id').on('change', function (e) {
        if ($(this).val() == 9) {
            $(this).closest('form').submit();
        }
        
    });
    $( document ).ready(function() {
        $('#start_date, #end_date').attr('autocomplete','off');
    });

    $(document).ready(function () {
    
        $('#report_id, #type_id, #committee, #phase').select2();
        
    })

</script>