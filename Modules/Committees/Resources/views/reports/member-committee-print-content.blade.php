<div class="container card  mt-5 p-2 border-0">
    
    @isset($sessions)
        @if ($sessions->count())

            </table>
            

            <h5 class="mt-5 text-right">الأعضاء:</h5>
            <table class="table table-striped text-center">
                <tr>
                    <td>م</td>
                    <td>العضو</td>
                    <td>رقم الهوية</td>
                    <td> نوع العضو </td>
                    <td>  اسم اللجنة</td>
                    <td> نوع اللجنة او المجلس</td>
                   
                    
                </tr>                
                @foreach($sessions as $committee)
                    @foreach($committee->members as $row)

                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ @$row->member->user_name }}</td>                        
                            <td>{{ @$row->member->user_idno }}</td>
                            <td>{{ @$row->memberType->name }}</td>
                            <td>{{ @$committee->name }}</td>
                            <td>{{ @$committee->type->name }}</td>
                            
                            
                        </tr>
                        
                    @endforeach        
                @endforeach        
            </table>


        @else
            <h2 class="m-4 text-center">لا توجد نتائج</h2>
        @endif
    @endisset

    <hr>
    
</div>


