@extends('layouts.main.index')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="/vendor-assets/css/ummalqura.calendars.picker.css">
@endsection

@section('page')
</br>
</br>
</br>
    <div class="container">
        <div class="row">
            <div class="reports-header">
                <div class="col-1">
                    <a href="{{ route('reports.index') }}" class="btn btn-outline-light">التقارير</a>
                </div>
                <div class="col-11">
                    <h1>تقارير اللجان و المجالس</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container card  mt-5 p-2 border-0">
        <form action="{{ route('committees.reports.filters') }}" method="post">
            @csrf

            <div class="overflow-hidden">
                <div class="form-group col-3 float-right">
                    <label>الاسم</label>
                    <input name="name" class="form-control" />
                </div>

                <div class="form-group col-3 float-right">
                    <label>النوع</label>
                    <select name="type" class="form-control">
                        <option value=""></option>
                        <option value="1">لجنة</option>
                        <option value="2">مجلس</option>
                    </select>
                </div>
                <div class="form-group col-3 float-right">
                    <label>التصنيف</label>
                    <select name="type" class="form-control">
                        <option value=""></option>
                        <option value="0">داخلي</option>
                        <option value="1">خارجي</option>
                    </select>
                </div>

                <div class="form-group col-3 float-right">
                    <label>النوع</label>
                    <select name="type_id" class="form-control">
                        <option value=""></option>
                        @foreach($types as $key => $type)
                            <option value="{{ $key }}">{{ $type }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-3 float-right">
                    <label>الخصوصية</label>
                    <select name="privacy" class="form-control">
                        <option value=""></option>
                        @foreach(\App\Classes\Utils::privacyList() as $key => $privacy)
                            <option value="{{ $key }}">{{ $privacy }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-3 float-right">
                    <label>رقم القرار</label>
                    <input name="decision" class="form-control" />
                </div>


            </div>

            <div class="form-group overflow-hidden">
                <button type="submit" class="btn btn-primary">بحث</button>
            </div>

        </form>

        {{ $committees->count() }}
{{--    @if (request()->input('report_id') == 3 || request()->input('report_id') == 0)--}}
{{--        @include('committees::reports.committee')--}}
{{--    @elseif (request()->input('report_id') == 1 || request()->input('report_id') == 0)--}}
{{--        @include('committees::reports.is_intitelment')--}}
{{--    @elseif (request()->input('report_id') == 4 || request()->input('report_id') == 0)--}}
{{--        @include('committees::reports.standing_committees')--}}
{{--    @elseif (request()->input('report_id') == 8 || request()->input('report_id') == 0)--}}
{{--        @include('committees::reports.approved_transcripts')--}}
{{--    @endif--}}

    
    </div>
@endsection

@section('scripts')
    @include('committees::reports.committee_scripts')
@endsection