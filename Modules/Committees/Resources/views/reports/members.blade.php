@extends('layouts.main.index')

@section('page')
</br>
</br>
</br>
<div class="container">
    <div class="row">
        <div class="reports-header">
            <div class="col-1">
                <a href="{{ route('reports.index') }}" class="btn btn-outline-light">التقارير</a>
            </div>
            <div class="col-11">
                <h1>تقارير الأعضاء</h1>
            </div>
        </div>
    </div>
</div>

<div class="container card  mt-5 p-2 border-0">
    <form action="{{ route('committees.reports.filters') }}" method="post">
        @csrf
        <input type="hidden" name="report_type" value="members">
        <div class="overflow-hidden" style="overflow:hidden !important;">
            <div class="form-group col-3 float-right text-right">
                <label>اسم المستخدم</label>
                <input name="filter[user_name]" class="form-control" />
            </div>

            <div class="form-group col-3 float-right text-right">
                <label>الاسم</label>
                <input name="filter[full_name]" class="form-control" />
            </div>

            <div class="form-group col-3 float-right text-right">
                <label>الجوال</label>
                <input name="filter[user_mobile]" class="form-control" />
            </div>

            <div class="form-group col-3 float-right text-right">
                <label>رقم الهوية</label>
                <input name="filter[user_idno]" class="form-control" />
            </div>

            <div class="form-group col-3 float-right text-right">
                <label>البريد الإلكتروني</label>
                <input name="filter[user_mail]" class="form-control" />
            </div>

            <div class="form-group col-3 float-right text-right">
                <label>الجنس</label>
                <select name="filter[gender]" class="form-control">
                    <option value=""></option>
                    <option value="1">ذكر</option>
                    <option value="2">انثى</option>
                </select>
            </div>

            <div class="form-group col-3 float-right text-right">
                <label>القسم</label>
                <select name="filter[user_dept]" class="form-control">
                    <option value=""></option>
                    @foreach($departments as $department)
                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                    @endforeach
                </select>
            </div>


        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-primary">بحث</button>
        </div>
    </form>


    <hr>


    <div class="container card  mt-5 p-2 border-0">
        <h5 class="mt-5 text-right">النتائج:
            {{ $users->count() }} نتيجة
        </h5>

        <table class="table table-hover text-right">
            <thead>
            <tr class="bg-secondary text-white">
                <th>الاسم</th>
                <th>القسم</th>
                <th>رقم الهوية</th>
                <th>رقم الجوال</th>
                <th>البريد الإلكتروني</th>
                <th>الحالة</th>
                <th>عدد اللجان</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->full_name }}</td>
                    <td>{{ $user->department->name }}</td>
                    <td>{{ $user->user_idno }}</td>
                    <td>{{ $user->user_mobile }}</td>
                    <td>{{ $user->user_mail }}</td>
                    <td>{{ $user->is_active }}</td>
                    <td>{{ $user->committees->count() == 0 ? 'لا يوجد' : $user->committees->count() }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>



</div>


@endsection
