<!DOCTYPE html>
<html lang="en"
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>اللجان</title>
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link href="/assets/css/icheck-bootstrap.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <link href="/vendor-assets/css/datatables.min.css" rel="stylesheet">
    <link href="/vendor-assets/css/datatables.bootstrap.css" rel="stylesheet">
    <link href="/vendor-assets/css/datatables.bootstrap-rtl.css" rel="stylesheet">
    <link href="/vendor-assets/css/ummalqura.calendars.picker.css" rel="stylesheet">
    <link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="images/favicon.ico" rel="shortcut icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="text-right">
<div class="br-mainpanel" id="vue-app" >

    <div class="container card border-0">
        <img src="{{ asset('assets/images/logoforPdf.png') }}">
        @if(request()->input('report_id') == 1 || request()->input('report_id') == 0)  
             @include('committees::reports.is_intitelment-print')
        @elseif(request()->input('report_id') == 3 || request()->input('report_id') == 0)
             @include('committees::reports.committee-count-print')
        @elseif(request()->input('report_id') == 4 || request()->input('report_id') == 0)
             @include('committees::reports.standing_committees-count-print')
        @elseif (request()->input('report_id') == 8 || request()->input('report_id') == 0)
             @include('committees::reports.approved_transcripts-print') 
        @endif

        {{--  <img src="{{ asset('assets/images/footerforpdf.png') }}">  --}}
    </div>

</div>
</body>
</html>
