<div class="container card  mt-5 p-2 border-0">

        {{ Form::open(['route' => 'reports.members.reports', 'method' => 'get']) }}
            <div class="row">
                <div class="col-md-4">
                    {{ Form::label('report_id', 'التقرير', ['class' => 'control-label float-right']) }}
                    {{ Form::select('report_id', [ null => 'إختر'] + $reports, old('report_id', @$_GET['report_id']) , ['class' => 'form-control text-right-align select2']) }}
                    <span class="text-danger">{{ $errors->first('report_id') }}</span>
                </div>
    
             
                
{{--                <div class="row">--}}
{{--                        --}}
{{--                    <div class="col-md-4">--}}
{{--                        <br/>--}}
{{--                        @if ($attendandeeMembers)--}}
{{--                            <a href="{{ request()->fullUrl() }}&print=1" class="btn btn-secondary btn-sm">طباعة التقرير</a>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                        --}}
{{--                </div>--}}
                
            </div>
            @isset($attendandeeMembers)
                @if ($attendandeeMembers->count())
    
                    </table>
                    
    
                    <h5 class="mt-5 text-right">الأعضاء:</h5>
                    <table class="table table-striped text-center">
                        <tr>
                            <td>م</td>
                            <td>  اسم اللجنة</td>
                            <td> نوع اللجنة او المجلس</td>
                            <td>العضو</td>
                            <td>رقم الهوية</td>
                            <td> نوع العضو </td>
                            
                        </tr>                
                        @foreach($attendandeeMembers as $committee)
                            @foreach($committee->members as $row)
    
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ @$committee->name }}</td>
                                    <td>{{ @$committee->type->name }}</td>
                                    <td>{{ @$row->member->user_name }}</td>                        
                                    <td>{{ @$row->member->user_idno }}</td>
                                    <td>{{ @$row->memberType->name }}</td>
                                    
                                </tr>
                                
                            @endforeach        
                        @endforeach        
                    </table>
    
    
                @else
                    <h2 class="m-4 text-center">لا توجد نتائج</h2>
                @endif
            @endisset
    
            <hr>
            <div class="col-md-2">
                @if ($attendandeeMembers)
                    <a href="{{ request()->fullUrl() }}&print=1" class="btn btn-success">طباعة التقرير</a>
                @endif                
            </div>
        {{ Form::close() }}
    </div>
    
    
    