@extends('layouts.main.index')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="/vendor-assets/css/ummalqura.calendars.picker.css">
@endsection

@section('page')
    </br>
    </br>
    </br>
    <div class="container">
        <div class="row">
            <div class="reports-header">
                <div class="col-1">
                    <a href="{{ route('reports.index') }}" class="btn btn-outline-light">التقارير</a>
                </div>
                <div class="col-11">
                    <h1>تقارير الأعضاء</h1>
                </div>
            </div>
        </div>
    </div>
    @if (request()->input('report_id') == 15 || request()->input('report_id') == 0)
        @include('committees::reports.members-committee')  
    @elseif ((request()->input('report_id') == 13 || request()->input('report_id') == 0))   
        @include('committees::reports.sessions-count')  
    @elseif ((request()->input('report_id') == 9 || request()->input('report_id') == 0))   
        @include('committees::reports.attendee-members') 
        
    @endif

        
@endsection

@section('scripts')
    @include('committees::reports.member_scripts')
@endsection