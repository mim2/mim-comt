@extends('layouts.main.index')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="/vendor-assets/css/ummalqura.calendars.picker.css">

    <style>
        #menu {
            float: right;
            text-align: right;
            margin: 0;
            padding: 0;
            width: auto;
        }
        #menu ul {
            margin: 0;
            padding: 0;
            width: 100%;
        }

        #menu li {
            list-style-type: none;
            padding: 2px 4px;
        }

        #menu .menu-header {
            cursor: pointer;
            background: #0c6eb9;
            color: #fff;
        }

        #menu .menu-header ul {
            display: none;
            background: #0c6eb9;
            position: absolute;
            z-index: 999999
        }

        #menu .menu-header li {
            cursor: pointer;
            text-align: right;
            font-weight: normal;
        }

        #menu .menu-header li:hover {
            background-color: #0c6eb9;
            color: #62555c;
        }

        #menu .menu-header:hover ul {
            display: block;
            z-index: 500;
            /* position: fixed; */
            max-width: 380px;
        }
        .nav-link {
            color: #6d9529;
        }
        #menu .menu-header li {
            background: #3196e3;
            border-right: 5px solid #0c6eb9;
            margin: 5px 0;
        }
        #menu .menu-header li a {
            color: #fff;
        }
        img {
            width: 30px;

        }
        .ico-container .ico-h-2 {
            border: 2px #e2c889 solid;

        }
        .ico-container .ico-h-2:hover, .ico-container .ico-h-3:hover, .ico-container .ico-h-4:hover, .ico-container .ico-h-5:hover, .ico-container .ico-h-6:hover, .ico-container .ico-h-7:hover, .ico-container .ico-h-8:hover, .ico-container .ico-h-9:hover {
            background: #c39e45;
        }

        .mainMenu li:hover {
            background: #173d84;
        }
        .mainMenu li {
            border-left: 1px solid #183d83;
            transition: all 0.5s ease;
        }
    </style>
@endsection


@section('page')
</br>
</br>
</br>
    <div class="container text-center ico-container">
            <div class="col-md-1 col-sm-12 title" style="min-width:180px; padding:10px; color:#fff; top:30px; background: rgb(195 158 71); font-size:20px; margin-bottom: 30px;box-shadow: 10px 0px 11px -15px rgba(120,120,120,0.8);">
                <img alt="" class="p-1 ml-1 mb-0" src="/assets/images/clipboard.svg"> التقارير
            </div>
        <div class="row mt-2 pt-2 pb-3 commit-tasks">

            <div class="row w-100 justify-content-center">
                <div class="col-sm-4 ico text-center">
                    <a class="ico-h-2 p-3 mt-3 mb-1 rounded-circle mx-auto" href="{{ route('reports.committees.reports') }}">
                        <img alt="" class="p-1 ml-1 mb-0" src="/assets/images/icons/file.svg">
                    </a>
                    تقارير اللجان والمجالس
                </div>
                <div class="col-sm-4 ico text-center">
                    <a class="ico-h-2 p-3 mt-3 mb-1 rounded-circle mx-auto" href="{{ route('reports.members.reports') }}">
                        <img alt="" class="p-1 ml-1 mb-0" src="/assets/images/icons/avatar.svg">
                    </a>
                  تقارير الأعضاء
                </div>
            

                <div class="col-sm-4 ico text-center">
                    <a class="ico-h-2 p-3 mt-3 mb-1 rounded-circle mx-auto" href="{{ route('reports.sessions.reports') }}">
                        <img alt="" class="p-1 ml-1 mb-0" src="/assets/images/icons/meetin2.svg">
                    </a>
                 تقارير الإجتماعات
                </div>
            </div>
        </div>
    </div>

    

@endsection

