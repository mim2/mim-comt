
<div class="container card  mt-5 p-2 border-0">
    @isset($committeeIsIntitlements)
        @if ($committeeIsIntitlements->count())
            </table>
            <h5 class="mt-5 text-right">الأعضاء:</h5>
            <table class="table table-striped text-center">
                <tr>
                    <td>  اسم اللجنة</td>
                    <td> نوع اللجنة او المجلس</td>
                    <td>التصنيف</td>
                    <td> من تاريخ </td>
                    <td>  الي تاريخ </td>
                </tr>    
                @foreach($committeeIsIntitlements as $committee)

                    <tr>
                        <td>{{ @$committee->name }}</td>
                        @if ($committee->type_id == 1 || 3 || 4)
                            <td>لجنة</td>
                        @else
                            <td>مجلس</td>
                        @endif
                        <td>{{ @$committee->category == 0 ? 'داخلي' : 'خارجي'}}</td>
                        @if ($committee->start_date == null)
                            <td>منتهية</td>
                        @else
                            <td>{{ @$committee->start_date->format('d-m-Y') }}</td>  
                        @endif 
                        @if ($committee->end_date == null)
                            <td>منتهية</td>
                        @else
                            <td>{{ @$committee->end_date->format('d-m-Y') }}</td>  
                        @endif                                
                        
                    </tr>
                @endforeach
            </table>
        @else
            <h2 class="m-4 text-center">لا توجد نتائج</h2>
        @endif

    @endisset
    
</div>


