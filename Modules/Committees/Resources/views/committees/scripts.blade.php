<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.plugin.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.plus.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.picker.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.picker-ar.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.ummalqura.js"></script>
<script type="text/javascript" src="/vendor-assets/js/date/jquery.calendars.ummalqura-ar.js"></script>
{{-- <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script>
    $('textarea').ckeditor();
    $('.textarea').ckeditor(); // if class is prefered.
</script> --}}
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
{{-- <script type="text/javascript" src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script> --}}
    <script type="text/javascript">
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
<script type="text/javascript">
    function typeChanged() {
        if ($("#type_id").val() == 4) {
            $('.parent_id').show();
        } else {
            $('.parent_id').hide();
        }
        if ($("#type_id").val() == 3) {
            $('.parent_id').show();
            $('.start_date').show();
            $('.end_date').show();
        } else {
            $('.parent_id').hide();
            $('.start_date').hide();
            $('.end_date').hide();
        }
        if ($("#type_id").val() == 5) {
            $('.session_parent_id').show();
        } else {
            $('.session_parent_id').hide();
        }
        if ($("#type_id").val() == 6) {
            $('.commitee_parent_id').show();
        } else {
            $('.commitee_parent_id').hide();
        }
    }

    function kindChanged() {
        var committee_or_council = $(":input[name='committee_or_council']:checked").val();
        if (committee_or_council == 2) {
            $('#type_id option[value=1]').hide();
            $('#type_id option[value=5]').show();
            $('#type_id option[value=3]').hide();
            $('#type_id option[value=4]').hide();
            $('#type_id option[value=6]').show();
            $('#type_id option[value=2]').show();
            $('#type_id').val(2).attr('disabled', false);

            //   $('#type_id option[value=7]').hide();  
        } else {
            $('#type_id option[value=2]').hide();
            $('#type_id option[value=5]').hide();
            $('#type_id option[value=6]').hide();
            $('#type_id option[value=1]').show();
            $('#type_id option[value=3]').show();
            $('#type_id option[value=4]').show();
            $('#type_id').val(1).attr('disabled', false);
        }
        if (committee_or_council == 3) {
            $('#type_id option[value=1]').hide();
            $('#type_id option[value=5]').show();
            $('#type_id option[value=3]').hide();
            $('#type_id option[value=4]').hide();
            $('#type_id option[value=6]').show();
            $('#type_id option[value=2]').hide();
            $('.session_parent_id').show();

            $('#type_id').val(5).attr('disabled', false);

        } else {
            // $('#type_id option[value=2]').hide();
        }
        
    }
    function reset() {
        
            $('.parent_id').hide();
            $('.commitee_parent_id').hide();
            $('.session_parent_id').hide();
            $('.start_date').hide();
            $('.end_date').hide();
    }   
    $(":input[name='committee_or_council']").click(function () {
        reset()
        kindChanged();

        
    });
    $("#type_id").change(typeChanged);
    
    typeChanged();
    kindChanged();

    // Datepicker
    var calHj = $.calendars.instance('ummalqura', 'ar');
    $('.hijri-datepicker-input').calendarsPicker($.extend({
            calendar: calHj,
            dateFormat: 'yyyy-mm-dd',
        },
        $.calendarsPicker.regionalOptions['ar'])
    );
         // select2
        $(document).ready(function () {
            let counter = 0;

            $('.select2').select2({
                width: '30%',
                placeholder: $(this).attr('data-placeholder') ? $(this).attr('data-placeholder') : ''
            }); 

            $('#itemName').select2({
                placeholder: 'ابحث عن عضو',
                ajax: {
                url: "{{ route('select2.ajax') }}",
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                    results:  $.map(data, function (item) {
                            return {
                                text: [item.user_name, item.user_idno],
                                id: item.user_id,
                                page: item.page ,
                                pagination : {
                                    more : item.page < data.total// Total number of pages is 10, so 1-9 pages can be refreshed drop-down
                                }                        }
                        })
                    };
                },
                cache: true
                },
                minimumInputLength: 3,

            });
        });
        $(document).ready(function () {
        // Add manager to committee
            $(document).on('click', '#add_committee_manager', function () {
                const url = $(this).attr('data-url');
                const user_id = $('#committee_members_mang').val();            
                $.get({
                    url: url,
                    data: {'user_id':user_id},
                    success: function (response) {
                        console.log(response);
                        const employees = response.employees;
                        let mang_user_id = $('#committee_members_mang').val();
                        // let vice_user_id = $('#add_committee_members_vice').val();
                        // let member_user_id = $('#committee_members').val();
                        let selectedMemberOption = $('#committee_members').find(":selected")[0];
                        // let selectedOptionOne = $('#committee_members').find(":selected")[1];
                        const val = $(selectedMemberOption).val();
                        let selectedManagerOption = $('#committee_members_mang').find(":selected")[0];
                        const val_manager = $(selectedManagerOption).val();
                        let user_id_vice = $('#user_id_vice').val();
                        let user_member_id = $('#user_member_id').val();
                        let tableManagersIds =new Array() ;
                        $('#committeeMembersManager').find("tr").each(function () { 
                            if (this.dataset.id != undefined || tableManagersIds.includes(this.dataset.id)) {
                                tableManagersIds.push(this.dataset.id );    
                            }
                        });
                        if ((!tableManagersIds.includes(val_manager)) && (mang_user_id != user_id_vice ) && (mang_user_id != user_member_id )){
                            let trow = '';
                            trow = `
                                <tr id="trow-${val_manager}" data-id="${val_manager}">
                                <td>
                                    <input id="" class="userId" name="manager_user_id" hidden value="${val_manager}">
                                    <input id="user_id_manager" name="" hidden value="${val_manager}">${employees.user_name ? employees.user_name : '' }
                                </td>
                                    <td>${employees.user_idno ? employees.user_idno : '' }</td>
                                    <td>${employees.user_mobile ? employees.user_mobile : '' }</td>
                                    <td>${employees.user_mail ? employees.user_mail : '' }</td>
                                </tr>
                            `;
                            $('#committeeMembersManager').html(trow);
                        // $('#add_committee_members_vice').val(0).trigger('change');

                        }else {
                            swal("  الرئيس مضاف سابقا ", {
                                icon: "warning",
                            });
                        }
                    }
                });
            });
        });
        $(document).ready(function () {
            // Add vice Manager to committee
            $(document).on('click', '#add_vice', function () {
                let formData = $('#add_committee_members_vice').serialize();
                const url = $(this).attr('data-url');
                const user_id = $('#add_committee_members_vice').val();
                $.get({
                    url: url,
                    data: {'user_id':user_id},
                    success: function (response) {
                        console.log(response);
                        const employees = response.employees;
                        let vice_user_id = employees.user_id;
                        let selectedMemberOption = $('#committee_members').find(":selected")[0];
                        let selectedManagerOption = $('#committee_members_mang').find(":selected")[0];
                        let selectedViceOption = $('#add_committee_members_vice').find(":selected")[0];
                        // let selectedOptionOne = $('#committee_members').find(":selected")[1];
                        const val = $(selectedMemberOption).val();
                        const val_vice = $(selectedViceOption).val();
                        // var member_user_id = $('#committee_members').val();
                        const user_id_manager = $('#user_id_manager').val();
                        const user_member_id = $('#user_member_id').val();
                        let tableVicesIds =new Array() ;
                        $('#committeeMembersManager').find("tr").each(function () { 
                            if (this.dataset.id != undefined || tableVicesIds.includes(this.dataset.id)) {
                                tableVicesIds.push(this.dataset.id );    
                            }
                        });
                        if ((!tableVicesIds.includes(val_vice)) && (user_member_id != vice_user_id ) && (user_id_manager != vice_user_id )){
                            let trow = '';
                            trow = `
                                <tr id="trow-${val_vice}" data-id="${val_vice}">
                                    <td>
                                        <input id="" class="" name="vice_manager_user_id" hidden value="${val_vice}">
                                        <input id="user_id_vice" name="" hidden value="${val_vice}">${employees.user_name ? employees.user_name : '' }
                                    </td>
                                    <td>${employees.user_idno ? employees.user_idno : '' }</td>
                                    <td>${employees.user_mobile ? employees.user_mobile : '' }</td>
                                    <td>${employees.user_mail ? employees.user_mail : '' }</td>
                                </tr>
                            `;
                            $('#committeeMembersVice').html(trow);
                        }else {
                            swal("  الأمين مضاف سابقا ", {
                                icon: "warning",
                            });
                        }
                    }
                });
            });
        });
        //add member to committee
        $(document).ready(function () {
            let counter = 0;
            $(document).on('click', '#add_committee_members', function () {
                const url = $(this).attr('data-url');
                const user_id = $('#committee_members').val();
                const member_type_id = $('#committee_members_type').val();
                const member_type = $( "#committee_members_type option:selected" ).text();
                $.get({
                    url: url,
                    data: {
                        'user_id':user_id,
                        'member_type_id':member_type_id
                    },
                    success: function (response) {
                        console.log(response);
                        const employees = response.employees;
                        const member_user_id = employees.user_id;
                        let selectedOption = $('#committee_members').find(":selected")[0];
                        let selectedTypeOption = $('#committee_members_type').find(":selected")[0];
                        const val = $(selectedOption).val();
                        const valType = $(selectedTypeOption).val();
                        let type = $('#committee_members_type').val();
                        const member_type_name = $('#memberTypeName').val();
                        const typeId = $('#typesofmembers').val();
                        const userId = $('.userId').val();
                        const user_id_vice = $('#user_id_vice').val();
                        const user_id_manager = $('#user_id_manager').val();
                        let tableMembersIds = [];
                        let tableMembersTypeIds = [];
                        //get all members ids in the table 
                        $('#committeeMembers').find("tr").each(function () {
                            if (this.dataset.id != undefined || tableMembersIds.includes(this.dataset.id)) {
                                tableMembersIds.push(this.dataset.id );
                                
                            }
                        });
                        $('#committeeMembers').find("tr").each(function () {
                            tableMembersTypeIds.push(this.dataset.user);
                        });

                        //check if the seected member added to the table
                        if ((!tableMembersIds.includes(val)) ^ (member_user_id != user_id_vice ) ^ (member_user_id != user_id_manager )){
                            if (type == 0) {
                                swal("  يجب اختيار النوع ", {
                                        icon: "warning",
                                });
                                
                            } else if (((tableMembersTypeIds.includes("4")) && (type.includes("4"))) || ((tableMembersTypeIds.includes("5")) && (type.includes("5"))) || ((tableMembersTypeIds.includes("6")) && (type.includes("6")))) {
                                    swal(" لا يمكن اختيار اكثر من نائب", {
                                    icon: "warning",
                                    });
                            } else{
                                
                                // append new row to the tabl
                                counter = counter + 1;
                                let trow = '';
                                trow = `
                                    <tr id="trow-${val}" data-id="${val}" data-user="${type}" name="members_user_id[]">
                                        <td>
                                            <input id="user_member_id" class="userId" name="commMembers[${counter}][user_id]" hidden value="${val}">
                                            <input  value="${valType}" name="commMembers[${counter}][member_type_id]" hidden>
                                            ${employees.user_name }
                                        </td>
                                        <td>${employees.user_idno ? employees.user_idno : '' }</td>
                                        <td>${employees.user_mobile ? employees.user_mobile : '' }</td>
                                        <td>${employees.user_mail ? employees.user_mail : '' }</td>
                                        <td>
                                            <input id="typesofmembers" data-id="${val}" value="${valType}" name="commMembers[${counter}][member_type_id]" hidden>
                                            ${member_type}
                                        </td>
                                        <td><button type="button" class="btn btn-danger trow-remove" data-id="${val}" data-remove-row="#trow-${val}">حذف</button></td>
                                    </tr>
                                `;
                                $('#committeeMembers').append(trow);
                            }


                        } else  {
                        swal("  العضو مضاف سابقا ", {
                                icon: "warning",
                            });
                        }
                            
                    }
                });
            });
        });
        //delete row
        $(document).on('click', '.trow-remove', function () {
            const row = $(this).attr('data-remove-row');
            const memberId = $(this).attr('data-id');
            $(row).remove();

        });
        // Helper function:
        function upTo(el, tagName) {
            tagName = tagName.toLowerCase();

            while (el && el.parentNode) {
                el = el.parentNode;
                if (el.tagName && el.tagName.toLowerCase() == tagName) {
                return el;
                }
            }
            return null;
        } 
        function deleteRow(el) {
            swal({
                    title: "هل انت متأكد من حذف هذا العضو ؟",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {

                    var row = upTo(el, 'tr')
                    if (row) row.parentNode.removeChild(row);

                    swal("تم الحذف !", {
                    icon: "success",
                    });
                } else {
                    swal("لم يتم الحذف");
                }
            });
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('click', '.file-remove',         function () {
            let btn = $(this);
            let url = $(btn).attr('data-remove-url');
            $.post({
                url: url,
                method: 'delete',
                success: function (response) {
                    let trow = $(btn).attr('data-remove-row');
                    console.log('asdsdsa');
                    $(trow).remove();
                },
                error: function (request) {
                    let errors = request.responseJSON.errors;
                    let keys = Object.keys(errors);
                    Swal.fire({
                        title: 'حدث خطأ',
                        text: errors[keys[0]][0], // First Error is enough
                        type: 'error',
                        showCancelButton: false,
                        confirmButtonText: 'حسنا',
                    });
                }
            })
        });


        // catogary


        $(document).ready(function () {

           
       
            $("#categories").change(function () {
          console.log($('#categories').val())
            if ($("#category").val() == 1)
            {

                $('#secretary').hide();
                $('#secretaries').hide();

            } else  {
                $('#secretary').show();
            }
        });
        });

</script>
