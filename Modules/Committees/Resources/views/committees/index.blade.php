@section('stylesheets')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<style>
    .edia {
        color: #c73b1e !important;
    }
</style>
@endsection

@extends('layouts.main.index')

@section('page')

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5><img class="ml-1" src="/assets/images/folder.svg" alt="">اللجان / المجالس</h5>
                        </div>
                        
                    </div>
                    
                </div>
                @include('committees::committees.committees_datatables')
            </div>
        </div>
    </div>
    
    
@stop
@section('scripts')
    @include('committees::committees.index-scripts')
@endsection
