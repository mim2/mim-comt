
<h6 style="color:#dc3545" >
    <i class="fa fa-info"></i> ملحوظة: لن يتم حفظ أي تعديلات إلا بعد الضغط على حفظ أسفل الصفحة
</h6>
<add-member-to-committee search-users-url="{{ route('committee_users_to_add', (isset($committee->id) ? $committee->id : 'new'))  }}"
                         committee-members-url="{{ route('committee_members', ['id' => $committee->id]) }}"
                         is-new="{{ ( isset($committee->id) ? true : false ) }}"
                         selected-members-old="{{ old('selected_members_old') }}">
</add-member-to-committee>

<br/>
