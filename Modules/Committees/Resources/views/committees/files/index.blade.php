@extends('layouts.main.index')
@section('page')

 {{-- @include('committees::partial.category') --}}


    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <a class="temp p-2 d-table" href="" target="_self" title=" إدارة الإجتماعات">
                                <i class="fa fa-gear"></i>
                                <span>إدارة الإجتماعات</span>
                            </a>
                            <p class="sissionPage p-2 acolor">
                                <img alt="" class=" ml-1" src="/assets/images/addFile.png">
                                <span>الملفات</span>
                            </p>
                            <h5>
                                <img alt="اللجان" class=" ml-1" src="/assets/images/folder.svg">
                                {{ $session->committee->name }}
                            </h5>
                            <span class="p-2 text-center details">
                                <a class="standing p-2" data-target="#newsision" data-toggle="modal" href=""
                                   target="_self" title="إجتماع جديد">
                                    <i class="fa fa-plus"></i>
                                    <span>اضافة ملف</span>
                                </a>
                            </span>
                        </div>
                        <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="newsision"
                             role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-lg" role="document">
                                {{ Form::open(['route' => ['sessions.files.store', $committee->id, $session->id], 'class' => 'form-horizontal', 'method' => 'post', 'files' => true ]) }}
                                <div class="modal-content border-0 rounded-0">
                                    <div class="modal-header border-0 rounded-0 p-0">
                                        <h5 id="exampleModalLabel"><i class="fa fa-plus"></i> ملف جديد</h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row"></div>
                                        <div class="row">
                                            <div class="form-group col border-0 rounded-0">
                                                <div class="custom-file">
                                                    <input name="file_path" class="custom-file-input" id="customFile"
                                                           type="file">
                                                    <label class="custom-file-label" for="customFile">ارفع ملف</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col border-0 rounded-0">
                                                <input class="form-control" name="file_name" type="text"
                                                       placeholder="إسم الملف">   
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <div class="modal-footer border-0 rounded-0">
                                        <button type="submit" class="btn sendBtn border-0 rounded-0 ml-2 pl-4 pr-4">
                                            إضافة
                                        </button>
                                        <button class="btn nonBtn border-0 rounded-0 ml-2 pl-4 pr-4" data-dismiss="modal" type="button">
                                            إلغاء
                                        </button>

                                        
                                    </div>
                                    
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col maindetails p-2 mt-3">

                        @foreach($files as $file)

                            <div class="d-flex flex-row mb-3 justify-content-between text-center topDetails">
                                <div class="p-1 m-2 sissionplace filestab">
                                    <span class="spTitle"><img alt="meeting" class="p-1 ml-1 mb-0"
                                                               src="/assets/images/file.svg">الملف :</span>
                                    <a href="/{{ $file->file_path}}">{{ $file->file_name }}</a>
                                </div>
                                <div class="p-1 mt-2 mb-2 sissionTime">
                                    <span>وقت الإضافة :</span> <img class="ml-1" src="/assets/images/clock.png" alt="">
                                    {{ $file->created_at->format('h:i a') }}
                                </div>
                                <div class="p-1 mt-2 mb-2 sissionTime">
                                    <img class="ml-1" src="/assets/images/calender.png" alt="">
                                    {{ $file->created_at->format('Y/m/d') }}
                                </div>
                                {{ Form::open(['route' => ['sessions.files.delete', $committee->id, $session->id, $file->id], 'method' => 'delete', 'id' => $file->id]) }}
                                <button type="submit" class="p-1 m-2 sissionremove trashed"
                                        onclick="return confirm('متأكد من الحذف؟')">
                                    <i class="fa fa-trash ml-2"></i>
                                </button>
                                {{ Form::close() }}
                            </div>

                        @endforeach
                        
                        <div class="row">
                            <div class="form-check form-check-inline">
                                    <input id="inlineCheckbox1" type="checkbox" class="form-check-input" name="checkbox" onclick="change_url(this)" checked>
                                    <label for="inlineCheckbox1" class="form-check-label"> لا يوجد ملفات </label>   
                            </div>
                        </div>
                        <a  href="{{ route('subjects.index', [$session->committee, $session]) }}"
                        class="btn btn-danger text-white nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button">
                            <i class="fa fa-arrow-left ml-2"></i> رجوع
                        </a>

                        @if(!count($files))  
                            <a id="next_action" data-information="{{route('guests.index', [$session->committee, $session]) }}" style="background:green!important"  href="{{ route('sessions.files.index', [$session->committee, $session]) . '?file=no' }}"
                                class="btn nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button">
                                 <i class="fa fa-arrow-left ml-2"></i> التالي
                            </a>
                        
                        
                            @if ($message = Session::get('warning'))
                            
                                <div class="alert alert-warning alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>	
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endif 
                        @else

                             <a style="background:green!important"  href="{{route('guests.index', [$session->committee, $session]) }}"
                                class="btn nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button">
                                 <i class="fa fa-arrow-left ml-2"></i> التالي
                             </a>
                      

                        
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')

    <script>

        

    function change_url(event)
    {
        url = $('#next_action').attr('href');
        url2 =  $('#next_action').attr('data-information');
        if ($('#inlineCheckbox1').is(":checked"))
        {
        console.log(url2);

            //parts = url.split("?"),
           $('#next_action').attr('href', url2);
        }
        else
        {
            parts = url.split("?"),
            $('#next_action').attr('href', parts[0] + '?file=no');
        }        
    }

    </script>
@endsection

