@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="/vendor-assets/css/ummalqura.calendars.picker.css">



@endsection

<div class="form-group" style="border-coler: gold">
    {{ Form::label('name', 'اسم المجلس', ['class' => 'control-label']) }}
    <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>
    {{ Form::text('name', old('name') , ['class' => 'form-control rounded-0']) }}
    <span class="text-danger">{{ $errors->first('name') }}</span>
</div>
<hr>
<div class="form-group">

  

    {{ Form::label('category', 'التصنيف', ['class' => 'control-label rounded-0']) }}
    <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>

    <div class="radio">
        <label class="radio-inline col-md-2">
            {!! Form::radio('category', '1', (Input::old('category') == '1'), array('id'=>'category', 'class'=>'form-check-input')) !!}
            <span class="mr-4">خارجي</span></label>
        <label class="radio-inline col-md-4">
            {!! Form::radio('category', '0', (Input::old('category') == '0'), array('id'=>'category', 'class'=>'form-check-input')) !!}
            <span class="mr-4">داخلي</span></label>
        <span class="text-danger">{{ $errors->first('category ') }}</span>
    </div>
</div>


<div class="form-group">
    {{ Form::label('type_id', 'النوع', ['class' => 'control-label']) }}
    {{ Form::select('type_id', $types, old('type_id') , ['class' => 'form-control']) }}

    <span class="text-danger">{{ $errors->first('type_id') }}</span>

    
</div>


<hr>
<div class="form-group parent_id">
    {{ Form::label('parent_id', 'اختر اللجنة الرئيسية', ['class' => 'control-label']) }}
    {{ Form::select('parent_id', $parents, old('parent_id') , ['class' => 'form-control parent_id']) }}
    <span class="text-danger">{{ $errors->first('parent_id') }}</span>
</div>

<div class="form-group session_parent_id">
    {{ Form::label('parent_id', 'اختر المجلس الرئيسي', ['class' => 'control-label']) }}
    {{ Form::select('parent_id', $sessionsParents, old('parent_id') , ['class' => 'form-control session_parent_id']) }}
    <span class="text-danger">{{ $errors->first('parent_id') }}</span>
</div>


<div class="form-group commitee_parent_id">
    {{ Form::label('parent_id', 'اختر اللجنة الفرعية', ['class' => 'control-label']) }}
    {{ Form::select('parent_id', $parents, old('parent_id') , ['class' => 'form-control commitee_parent_id']) }}
    <span class="text-danger">{{ $errors->first('parent_id') }}</span>
</div>

<div class="form-group start_date">
    {{ Form::label('start_date', 'تاريخ البداية', ['class' => 'control-label']) }}
    {{ Form::text('start_date', old('start_date') , ['class' => 'form-control hijri-datepicker-input']) }}
    <span class="text-danger">{{ $errors->first('start_date') }}</span>
</div>

<div class="form-group end_date">
    {{ Form::label('end_date', 'تاريخ النهاية', ['class' => 'control-label']) }}
    {{ Form::text('end_date', old('end_date') , ['class' => 'form-control hijri-datepicker-input rounded-0']) }}
    <span class="text-danger">{{ $errors->first('end_date') }}</span>
</div>

<div class="form-group">
    {{ Form::label('description', 'مهام المجلس', ['class' => 'control-label']) }}
    <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>

    {{ Form::textarea('description', old('description') , ['class' => 'form-control rounded-0', 'id' => 'article-ckeditor']) }}
    <span class="text-danger">{{ $errors->first('description') }}</span>
</div>
<hr>

<div class="form-group">
    {{ Form::label('decision_name', 'رقم القرار ', ['class' => 'control-label']) }}
    <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>

    {{ Form::text('decision_name', old('decision_name') , ['class' => 'form-control secondFont rounded-0', ]) }}
    <span class="text-danger">{{ $errors->first('decision_name') }}</span>
</div>

<div class="form-group">
    {{ Form::label('decision_image', 'صورة القرار ', ['class' => 'control-label']) }}
    <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>

    {{ Form::file('decision_image', ['class' => 'form-control rounded-0']) }}
    <br>
    @if ($committee->decision_url == !null)
    <a href="{{ asset($committee->decision_url) }}" target="_blank" class="reqimg">
        <i class="fa fa-image"></i>صورة القرار 
    </a>
    @endif
    <span class="text-danger">{{ $errors->first('decision_image') }}</span>
</div>
<hr>


<hr>
@include('committees::committees.add_new_member')
<div class="form-group">
    {{ Form::submit('حفظ', ['class' => 'btn btn-success rounded-0']) }}
    <a href="{{ route('committees.index') }}" class="btn btn-danger text-white rounded-0">إلغاء</a>
</div>

@section('scripts')
    @include('committees::committees.scripting')
@endsection