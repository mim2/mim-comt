<style>

    .reqimg {
        background: #fd3b6e;
        padding: 5px;
        color: #fff;
    }

    .reqimg:hover {
        background: #0c6eb9;
        color: #fff !important;
        text-decoration: none;
    }

    a:hover {

    }

    .enl {
        font: 600 14px sans-serif;
    }

    .colorrin {
        color: #ff680c;
    }

</style>

@extends('layouts.main.index')

@section('page')
 

  
    <div class="container mt-5">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            
                
                           
                        <p class="standing p-2"><span class="p-2 text-center details">{{ $committee->type->name }}</span></p>
                         
						
							<span class="p-2 text-center details">
								{{-- <a class="standing p-2"  href="{{ route('member.committees.print', $committee) }}"><img alt="اللجان" class=" ml-1" src="/assets/images/folder.svg">طباعة</a>
                                 --}}
                                <a href="{{ asset($committee->decision_url) }}" target="_blank" class="standing p-2">
                                    <i class="fa fa-image"></i> صورة القرار 
                                </a>
                            </span>
                            

                        </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <h4><i class="fa fa-folder ml-2"></i>الإسم</h4>
                        <p class="d-flex flex-column p-3 form-group">{{ $committee->name }}</p>
                        <h4><i class="fa fa-list ml-2"></i>التصنيف</h4>
                        <p class="d-flex flex-column p-3 form-group">{{ $committee->category == 1 ? 'خارجي' : 'داخلي'}}</p>
                        @if($committee->type_id == 3)
                        <h4><i class="fa fa-calendar ml-2"></i>تاريخ البداية</h4>
                        <p class="d-flex flex-column p-3 form-group">{!! $committee->start_date->format('Y-m-d') !!}</p>
                        <h4><i class="fa fa-calendar ml-2"></i>تاريخ النهاية</h4>
                        <p class="d-flex flex-column p-3 form-group">{!! $committee->end_date->format('Y-m-d') !!}</p>
                        @endif
                        <h4><i class="fa fa-briefcase ml-2"></i>المهام</h4>
						<p class="d-flex flex-column p-3 form-group">{!! $committee->description !!}</p>
						<h4><i class="fa fa-briefcase ml-2"></i> رقم القرار </h4>
                        <p class="d-flex flex-column p-3 form-group">{!! $committee->decision_name !!}</p>
                        <h4><i class="fa fa-user ml-2"></i>الاعضاء</h4>

                        @foreach($committee->members as $member)

                            <div class="d-flex text-center">
                                <div class="col p-3 userspro">
                                    <i class="fa fa-user ml-2"></i>
                                    {{ $member->member->user_name }}
                                </div>
                                <div class="col p-3 userspro">
                                    {{ $member->memberType->name }}
								</div>
								
								
								
                                {{-- <a class="col p-3 userspro" href="" target="_self" title="ملف العضو"><img alt="اللجان" class=" ml-1" src="/assets/images/userpro.png"> ملف العضو</a> --}}
                            </div>

                        @endforeach

                        <h4 class="mt-4 titleIn">الإجتماعات</h4>
                            
                        @foreach($committee->sessions as $session)

                            <div class="d-flex text-center">
                                <div class="col p-3 userspro">
                                    {{ $session->sessionTitle->name }}
                                </div>
                                <div class="col p-3 userspro">
                                    {{ $session->status->name }}
                                </div>
                                <div class="col p-3 userspro">
                                    <a href="{{ route('member.sessions.show', [$session->committee, $session]) }}">
                                        محضر الإجتماع
                                    </a>
                                </div>
                            </div>

                        @endforeach
						
						
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>



@stop



