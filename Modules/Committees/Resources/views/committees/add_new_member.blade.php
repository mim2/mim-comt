<div class="form-group">
    {{-- <h6 style="color:#dc3545" >
        <i class="fa fa-info"></i> ملحوظة: يجب اختيار مشرف واحد  
    </h6> --}}
</div>

    <div class="form-group">
        <div class="aui-buttons input-group-btn" role="group" style="margin-left:12px;">
                <label for="status" class="control-label"> اختيار الرئيس</label>
            <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>
        
        
                <select name="manager" id="committee_members_mang" class="form_control select2">             
                    <option value="0">اختر</option>
                    @foreach($employees as $key => $name)
                        <option value="{{ $name->user_id }}">{{ $name->user_name }}  ,    {{ $name->user_idno }}</option>
                    @endforeach 
                </select>
                <select name=""  id="" hidden>
                    <option value="1" hidden>رئيس</option>
                </select>
                <button  type="button"  id="add_committee_manager" data-url="{{ route('employees.getEmployees') }}">إضافة</button>
        </div>
        
        <div class="form-group">
            <table class="table table-striped mt-10" >
                <thead>
                    <tr>
                        <th scope="col">اسم الرئيس</th>
                        <th scope="col">رقم الهوية</th>
                        <th scope="col">رقم الجوال</th>
                        <th scope="col">البريد الإلكتروني</th>
                        {{-- <th scope="col">الادارة</th> --}}
                    </tr>
                </thead>
                <tbody id="committeeMembersManager">
                    @foreach ($commiteeMembersForManager as $item)
                        
                            @if($item->member_type_id == 1)
                            <td>{{ @$item->member->user_name }}</td>
                            <td>{{ @$item->member->user_idno }}</td>
                            <td>{{ @$item->member->user_mobile }}</td>
                            <td>{{ @$item->member->user_mail }}</td>
                            @endif
                        
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

        <hr>
        <div class="form-group">
            {{-- <h6 style="color:#dc3545" >
                <i class="fa fa-info"></i> ملحوظة: يجب اختيار  أمين لم يتم اختياره كعضو او مشرف
            </h6> --}}
        </div> 

    <div class="form-group">
        <div class="aui-buttons input-group-btn" role="group" style="margin-left:12px;">
                <label for="status" class="control-label"> اختيار أمين</label>
                
                <select name="vice" id="add_committee_members_vice" class="form_control select2">
                               
                    <option value="0">اختر</option>
                    @foreach($employees as $key => $name)
                        @if (Input::old('vice') == $name->user_id)
                            <option value="{{ $name->user_id }}" selected>{{ $name->user_name }}</option>
                        @else
                            <option value="{{ $name->user_id }}">{{ $name->user_name }}  ,    {{ $name->user_idno }}</option>
                        @endif
                    @endforeach
                </select>
                <select name=""  id="" hidden>
                        <option value="2" hidden>رئيس</option>
                    </select>
                <button  type="button"  id="add_vice" data-url="{{ route('employees.getEmployees') }}">إضافة</button>
        </div>
        <hr>
        <div class="form-group">
                <table class="table table-striped mt-10">
                    <thead>
                        <tr>
                            <th scope="col">اسم الأمين</th>
                            <th scope="col">رقم الهوية</th>
                            <th scope="col">رقم الجوال</th>
                            <th scope="col">البريد الإلكتروني</th>
                            {{-- <th scope="col">الادارة</th> --}}
                        </tr>
                    </thead>
                        <tbody id="committeeMembersVice">
                        @foreach ($commiteeMembersForVice as $item)
                            
                            @if($item->member_type_id == 2)
                                <td>{{ @$item->member->user_name }}</td>
                                <td>{{ @$item->member->user_idno }}</td>
                                <td>{{ @$item->member->user_mobile }}</td>
                                <td>{{ @$item->member->user_mail }}</td>
                            @endif
                        @endforeach
                        </tbody>
                </table>
            </div>
    </div>
    <hr>
    <div id="secretaries" class="form-group">
        {{-- <h6 style="color:#dc3545" >
            <i class="fa fa-info"></i> ملحوظة: يجب اختيار عضو جديد 
        </h6> --}}
    </div>
    <div id="secretary" class="form-group">
        <div class="aui-buttons input-group-btn" role="group" style="margin-left:12px;">
                <label for="status" class="control-label"> اختيار النوع</label>
                <label style="position: absolute;text-align: center;font-size: large; color: #e32;display:inline;">*</label>
                <input type="hidden" value="{{ @$commiteeMembers[0]->committee_id }}" id="committeeIdFM">
                <select name="membersType[]" id="committee_members_type" class="input-group-prepend select2">
                    <option value="0">اختر</option>
                    @foreach($commiteeMemberType as $key => $type)
                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                    @endforeach
                </select>
                <label for="status" class="control-label"> اختيار العضو</label>
                <select id="committee_members" class="input-group-prepend select2">
                    <option value="0">اختر</option>
                    @foreach($employees as $key => $name)
                        <option value="{{ $name->user_id }}">{{ $name->user_name }}  ,    {{ $name->user_idno }}</option>
                    @endforeach
                </select>
                <select name=""  id="" hidden>
                    <option value="3" hidden>رئيس</option>
                </select>
                <button  type="button"  id="add_committee_members" data-url="{{ route('employees.getEmployees') }}">إضافة</button>
        </div>
        <table class="table table-striped mt-10" id="committeeMembers">
            <thead>
                <tr>
                    <th scope="col">اسم العضو</th>
                    <th scope="col">رقم الهوية</th>
                    <th scope="col">رقم الجوال</th>
                    <th scope="col">البريد الإلكتروني</th>
                    {{-- <th scope="col">الادارة</th> --}}
                    <th scope="col">النوع</th>
                    <th scope="col">خيارات</th>
                </tr>
            </thead>
            @foreach ($commiteeMembersForMember as $item)
                
                @if(($item->member_type_id == 3)
                || ($item->member_type_id == 4)
                ||($item->member_type_id == 5)
                ||
                ($item->member_type_id == 6)
                || ($item->member_type_id == 7)
                )
                    <tbody>
                        
                        {{-- <p id="memberTypeTd" value="{{ @$item->member->user_id }}" hidden></p> --}}
                        <td> {{ @$item->member->user_name }}</td>
                        <td>{{ @$item->member->user_idno }}</td>
                        <td>{{ @$item->member->user_mobile }}</td>
                        <td>{{ @$item->member->user_mail }}</td>
                        {{-- <td>{{ @$item->member->dep_arname }}</td> --}}
                        <td>{{ @$item->memberType->name }}</td>
                            <td> <button type="button" onClick="deleteRow(this)" class="btn btn-danger file-remove"
                                data-remove-url="{{ route('member.delete', $item) }}"
                                data-remove-row="#file-{{ $item->id }}">
                            حذف
                        </button></td>
                    </tbody>
                @endif
                
            @endforeach
        </table>
    </div>
<hr>
<div class="form-group">
    <h6 style="color:#dc3545" >
        <i class="fa fa-info"></i> ملحوظة: لن يتم حفظ أي تعديلات إلا بعد الضغط على حفظ أسفل الصفحة
    </h6>
</div>
<hr>


