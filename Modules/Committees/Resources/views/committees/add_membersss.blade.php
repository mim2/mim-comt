<div class="container" id="app">
        <div class="row">
            <div class="col-sm-8">
                <h1>Autocomplete Vue js using Laravel</h1>
                <div class="panel panel-default">
                    <div class="panel-heading">Please Enter for Search</div>
                    <div class="panel-body">
                        <autocomplete></autocomplete>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <script type="application/javascript">
    
    
        Vue.component('autocomplete', {
          template: '<div><input type="text" placeholder="what are you looking for?" v-model="searchquery" v-on:keyup="autoComplete" class="form-control"><div class="panel-footer" v-if="data_results.length"><ul class="list-group"><li class="list-group-item" v-for="result in data_results">@{{ result.user_name }}</li></ul></div></div>',
          data: function () {
            return {
              searchquery: '',
              data_results: []
            }
          },
          methods: {
            autoComplete(){
            this.data_results = [];
            if(this.searchquery.length > 2){
             axios.get('vuejs/autocomplete/search',{params: {searchquery: this.searchquery}}).then(response => {
                console.log(response);
              this.data_results = response.data;
             });
            }
           }
          },
        })
    
    
        const app = new Vue({
            el: '#app'
        });
    </script>