@extends('layouts.main.index')

@section('page')

            {{-- @include('committees::partial.category') --}}



    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <span class="sissionPage p-2 acolor">
                                <img src="/assets/images/file.svg"> إجتماع انت مدعو لها
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <div class="col ownertabelecon">

                            @if ($sessions->count())

                                <table class="table">
                                    <thead class="ownertabele">
                                    <tr>
                                        <th>#</th>
                                        <th>الإجتماع</th>
                                        <th>اللجنة/المجلس</th>
                                        <th>الحالة</th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    @unless($sessions->count())

                                        <tr>
                                            <td colspan="4" class="ttd">لا توجد محاضر بانتظار التوقيع.</td>
                                        </tr>

                                    @endunless

                                    @foreach($sessions as $session)

                                        <tr>
                                            <th class="ttd">{{ $loop->iteration}}</th>
                                            <th class="ttd">{{ $session->sessionTitle->name }}</th>
                                            <th class="ttd">{{ @$session->committee->name }}</th>
                                            <th class="ttd">{{ $session->status->name }}</th>
                                            <th class="ttd">
                                                <a href="{{ route('guest.sessions.show', [$session->committee, $session]) }}">
                                                    <i class="fa fa-eye"></i> محضر الإجتماع
                                                </a>
                                            </th>
                                        </tr>

                                    @endforeach

                                </table>

                            @else

                                <div class="col CommityDetails">
                                    <div class="row mt-2">
                                        <div class="col maindetails p-2 mt-3">
                                            <div class="mb-3 text-center ">
                                                <h4 class="text-center m-5">
                                                    <i class="fa fa-exclamation-triangle fa-4x mb-3 d-block gray"></i>
                                                    لا توجد محاضر تنتظر توقيعك
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endif
                            {{-- <div class="row">
                                <div class="col text-center">
                                    <a href="{{ url()->previous() }}" class="btn btn-danger text-white rounded-0 goBack w-25">
                                        <i class="fa fa-reply fmr-3 vemid"></i> رجوع
                                    </a>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
