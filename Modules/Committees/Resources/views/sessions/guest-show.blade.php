@extends('layouts.main.index')

@section('stylesheets')
    <style>
        .table {
            background-color: rgb(249, 252, 254) !important;

        }

        .titleIn {
            background: rgb(246, 246, 246);
            padding: 10px;
            font-size: 18px !important;
            border-top: 1px solid #3196e3;
        }
    </style>

@endsection

@section('page')

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            
                         
                            <a class="temp p-2 d-table"  href="{{ route('guest.sessions.index') }}">
                              
                              <img src="/assets/images/file.svg" >إجتماع أنت مدعو له 
                         </a>

                         <a class=" p-2 d-table"  >
                              
                           محضر الإجتماع
                       </a>

                         <a class="temp p-2 d-table"  href="{{ route('sessions.manage.show', $session->id) }}">
                              
                            <img src="/assets/images/link.png" >معلومات الإجتماع 
                       </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <div class="d-flex flex-column mb-3 justify-content-between topDetails resulttable">
                            <table class="table text-center">
                                <thead class="thcusme">
                                <tr>
                                    <th scope="col">اسم {{ @$session->committee->committee_or_council == 1 ? 'اللجنة' : 'المجلس'}}</th>
                                    <th scope="col">رقم الإجتماع</th>
                                    <th scope="col">الوقت</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ @$session->committee->name }}</td>
                                    <td class="secondfont">{{ $session->sessionTitle->id }}</td>
                                    <td class="secondfont">{{ $session->date }} {{ $session->start_time }}</td>
                                </tr>
                                </tbody>
                            </table>
                            <hr>


                            @include('committees::partial.committee_members')

                            <hr>
                            <h5 class="text-right headtitle head3 mr-3">ثانيا: جدول أعمال اللجنة</h5>
                            <table class="table text-center">
                                <thead class="thcusme2 th3">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">الأجندة</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($session->subjects as $subject)

                                    <tr>
                                        <td>{{ @++$key }}</td>
                                        <td>{{ $subject->title }}</td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                            @if($session->status_id >= 3)

                            <br />
                                @include('committees::partial.recommendations')
                                <br />
                                @include('committees::partial.session_opinions')
                                <br />
                                @include('committees::partial.session_files')
                                <br />
                                @include('committees::partial.your_opinions')

                            @endif
{{-- 
                            <a href="{{ url()->previous() }}" class="btn btn-danger text-white rounded-0 backbtn"> رجوع</a> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>
        $('input:radio[name="has_reservation"]').change(function () {
            if (this.value == 2) {
                $('.textsty').prop('disabled', true);
                $('.textsty').val('');
                $('#note').hide();
            } else {
                $('.textsty').prop('disabled', false);
                $('#note').show();
            }
        });
        $('.textsty').prop('disabled', true);
        $('#note').hide();

        $('#submit').click(function () {
            var opinion = $('input:checked[name="opinion"]').val();
            if (!opinion) {
                alert('ماهو رأيك في المحضر؟');
                return false;
            }
            var reservation = $('input:checked[name="has_reservation"]').val();
            if (!reservation) {
                alert('هل لديك تحفظات على المحضر؟');
                return false;
            }
            if (reservation == 1 && $('#notes').val() == '') {
                alert('الرجاء كتابة تحفظاتك');
                return false;
            }
            var url = '{{ route('guest.opinion', $session) }}';
            var data = {
                opinion: opinion,
                has_reservation: reservation,
                reservation_reason: $('.textsty').val(),
                _token: '{{ csrf_token() }}',
            };

            $.post(url, data).done(function () {
                $('#signetuersend').modal();
            });
            return false;
        });
    </script>

@endsection
