@extends('layouts.main.index')
@section('page')

 {{-- @include('committees::partial.category') --}} 

    <div class="container mt-5 minhe">
        <!-- main row -->
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <a class="temp p-2 d-table" href="{{ route('sessions.index', $committee) }}" target="_self" title=" إدارة الإجتماع">
                                <i class="fa fa-gear"></i>إدارة الإجتماعات
                            </a>
                            <p class="sissionPage p-2 acolor">
                                <img alt="اللجان" class=" ml-1" src="/assets/images/attendance.png"/>
                                <span>تحضير الاعضاء</span>
                            </p>
                            <h5>
                                <img alt="اللجان" class=" ml-1" src="/assets/images/{{ $session->committee->committee_or_council == 1 ? 'folder' : 'business-meeting' }}.svg">
                                {{ $session->committee->committee_or_council == 1 ? 'لجنة' : 'مجلس' }}
                                {{ $session->committee->name }} 
                                
                                - الإجتماع  
                                
                                {{ $session->sessionTitle->name }}
                            </h5>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        {{ Form::open(['route' => ['sessions.attendees.store', $committee->id, $session->id], 'class' => 'form-horizontal', 'method' => 'post']) }}
                        <table class="table userattenntabel text-center">
                            <thead class="thead-dark">
                            <tr>
                                <th> إسم العضو</th>
                                <th>الصفة</th>
                                <th> التحضير</th>
                                <th>سبب الإعتذار</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($members as $member)
                                @if(@$member->member->user_id > 0)
                                    <input type="hidden" value="{{$member->id}}" class="memberid">
                                    <?php $attendee = $session->getAttendee(@$member->member->user_id) ?>
                                   
                                    @if ($member->displayForAttendance($attendee))
                                        <tr>
                                            <td>{{ @$member->member->user_name }}</td>
                                            <td>{{ @$member->memberType->name }}</td>
                                            <td class="{{$errors->has("attendees.{$member->member->user_id}.attendance_status_id") ? 'has-error' : ''}}">
                                                {{ Form::hidden("attendees[{$member->member->user_id}][user_id]", $member->member->user_id , ['class' => 'form-control']) }}
                                                @foreach($attendanceStatuses as $key => $status)

                                                    <div  class="form-check form-check-inline">
                                                        <input data-id="{{$member->id}}" class="form-check-input memberval"
                                                            name="attendees[{{ $member->member->user_id }}][attendance_status_id]"
                                                            type="radio"
                                                            value="{{ $key }}"
                                                            {{ optional($attendee)->attendance_status_id == $key ? 'checked' : '' }} id="member {{$member->id}}" />
                                                        <label class="form-check-label" for="inlineRadio1">{{ $status }}</label>
                                                    </div>

                                                @endforeach
                                                {{ Form::hidden("attendees[{$member->member->user_id}][user_id]", $member->member->user_id , ['class' => 'form-control', 'name' => 'radioval']) }}
                                                 <span class="has-error-span"> {{$errors->first("attendees.{$member->member->user_id}.attendance_status_id") }}</span>

                                            </td>

                                            <td class="{{$errors->has("attendees.{$member->member->user_id}.apology_reason") ? 'has-error' : ''}}" >
                                
                                                {{ Form::text("attendees[{$member->member->user_id}][apology_reason]",
                                                            old("attendees.{$member->member->user_id}.apology_reason") ?? optional($attendee)->apology_reason  ,
                                                            ['class' => 'form-control user-as-member' , 'id' => 'session-attendance-apology-reason-' . $member->id, 'style' => @$attendee->attendance_status_id !== '3' ? 'display: none;' : '1'] ) }}
                                                        
                                                <span class="has-error-span"> {{$errors->first("attendees.{$member->member->user_id}.apology_reason") }}</span>
                                            </td>
                                        </tr>

                                    @endif
                                @endif
                            @endforeach

                            @foreach($guests as $guest)
                            <?php $attendee = $session->getGuestAttendee($guest->user_id) ?>

                            </span>
                            {{--  @if ($guest->displayForGuestAttendance($attendee))  --}}
                            
                                @if ($guest->user_id)
                            <span type="hidden" value="{{$guest->user_id}}" class="guestid">

                                    <tr>

                                        <td>
                                            {{ !empty($guest->name) ? $guest->name : $guest->user->name }}
                                        </td>

                                        <td>مدعو للإجتماع</td>

                                        <td id="status" class="{{$errors->has("attendees.{$guest->user_id}.attendance_status_id") ? 'has-error' : ''}}">

                                            {{ Form::hidden("attendees[{$guest->user_id}][user_id]", $guest->user_id ? : '-1' , ['class' => 'form-control']) }}
                                           
                                            @foreach($attendanceStatuses as $key => $status)


                                                <div class="form-check form-check-inline">
                                                   
                                                    <input data-id="{{$guest->user_id}}"  class="form-check-input guestval"
                                                           name="attendees[{{ $guest->user_id }}][attendance_status_id]"
                                                           type="radio"
                                                           value="{{ $key }}"
                                                            {{ optional($attendee)->attendance_status_id == $key ? 'checked' : '' }} id="radio"/>
                                                    <label class="form-check-label" for="inlineRadio1">{{ $status }}</label>
                                                </div>

                                            @endforeach

                                            <!-- <span class="has-error-span"> {{$errors->first("attendees.{$guest->id}.attendance_status_id") }}</span> -->

                                        </td>

                                        <td class="{{$errors->has("attendees.{$guest->user_id}.apology_reason") ? 'has-error' : ''}}">
                                            {{--  @if($loop->count)  --}}
                                            {{ Form::text("attendees[{$guest->user_id}][apology_reason]",
                                            
                                            old("attendees[{$guest->user_id}][apology_reason]") ?? optional($attendee)->apology_reason ,
                                                        ['class' => 'form-control user-as-guest' , 'id' => 'session-attendance-apology-reason-' . $guest->user_id, 'style' => @$attendee->attendance_status_id!=='3'?'display: none;':'']) }}

                                            <span class="has-error-span"> {{$errors->first("attendees.{$guest->user_id}.apology_reason") }}</span>
                                            {{--  @endif  --}}
                                        </td>

                                    </tr>
                                     @endif
                                    {{--  @endif  --}}

                            @endforeach

                            
                            </tbody>
                        </table>
                        <button class="btn sendBtn border-0 rounded-0 ml-2 pl-4 pr-4" type="submit" id="target"><i class="fa fa-save ml-2"></i>   حفظ</button>
                        <button class="btn nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4" type="button"><i class="fa fa-plus ml-2"></i>   تحضير الكل</button>
                        <button class="btn nonBtne unnall border-0 rounded-0 ml-2 pl-4 pr-4" type="reset"> <i class="fa fa-trash ml-2"></i>   إلغاء تحضير  الكل</button>
                        <br /><br />
                        <a  href="{{ route('guests.index', [$session->committee, $session]) }}"
                           class="btn btn-danger text-white nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button">
                            <i class="fa fa-arrow-right ml-2"></i> رجوع
                        </a>
                        {{-- @if  --}}
                        <a id="nextButton" style="background:green!important" href="{{ route('recommendations.edit',  $session) }}"
                           class="btn nonBtn  border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button" >
                            <i class="fa fa-arrow-left ml-2"></i> التالي
                        </a>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
        <br>
        
    </div>
@stop

@section('scripts')
    <script>



        $( "#nextButton" ).click(function(e) {
                    $( "#target" ).click();

                    // alert("it's checked")
                    e.preventDefault();


                true;
            });


                $(".btnall").click(function () {
                if(  $("input[type=radio][value='1']").attr('checked', 'checked')){
                    $("#status").attr("disabled", true);
                }
                });
                $(".unnall").click(function () {
                    $("input[type=radio]").removeAttr('checked');
                });


                
        $(document).ready(function () {
            
            $(".guestval").on('change', function(e) {
                
                var guestval = $(this).attr('data-id');

                    if($(this).val()=="3") { //3rd radiobutton
                        $('#session-attendance-apology-reason-'  + guestval).show();
                    }
                    else {
                        $('#session-attendance-apology-reason-'  + guestval).hide();
                    }
            
            });

                              

            $(".memberval").on('click', function() {
                    
                var memberval = $(this).attr('data-id');
                    
                    if($(this).val()=="3") {                         
                        
                        $('#session-attendance-apology-reason-'  + memberval).show();
                           
                    }
                    else {

                        $('#session-attendance-apology-reason-'  + memberval).hide();

                        
                    }
                    

            });

        });

        
    </script>
@endsection