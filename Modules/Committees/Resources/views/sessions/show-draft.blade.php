<!DOCTYPE html>
<html dir="rtl" lang="ar">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>اللجان</title>
    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <style>
        body {
            direction: rtl;
            text-align: right;
            
        }
        </style>


</head>
<body>
    <br />
    <br />
    <br />

    <div style="text-align: center; font-size: 20px;">
        مسودة  
    </div>

    <br />
 

    <table  style="padding: 5px;">
        <tbody>

            <tr style="text-align: center; background-color: #7d7d7d; color: #fff; font-size: 20px;">
                <td style="border:3.5px solid #fff; border-collapse: collapse; padding: 10px;" colspan="2" >{{ $session->committee->name }}</td>

            </tr>

            <tr style="background-color: #D9D9D9;">
                
                <td style="border:2.5px solid #fff;
                           border-collapse: collapse;
                           padding: 10px;" >
                الإجتماع : {{ $session->sessionTitle->name }}</td>
                <td style="border:2.5px solid #fff;
                           border-collapse: collapse;
                           padding: 10px;" >
                    التاريخ : {{ @$session->committee->date }}</td>

                
                
            </tr>

            <tr style="background-color: #D9D9D9; ">
       
                <td style="border:2.5px solid #fff;
                           border-collapse: collapse;
                           padding: 10px;" >
                    المكان : {{ $session->place }}</td>
                    <td style="border:2.5px solid #fff;
                    border-collapse: collapse;
                    padding: 10px;" >
                    الوقت : 
                    من
                    {{ Carbon\Carbon::createFromFormat('H:i:s',$session->start_time)->format('h:i' )}}
                    {{ (Carbon\Carbon::createFromFormat('H:i:s',$session->start_time)->format('a' ) == 'am' ? 'ص' : 'م' )}}
                    إلى 

                    {{ Carbon\Carbon::createFromFormat('H:i:s',$session->end_time)->format('h:i ')}}
                    {{ (Carbon\Carbon::createFromFormat('H:i:s',$session->end_time)->format('a' ) == 'am' ? 'ص' : 'م' )}}
                </td>
                
            </tr>
            
        </tbody>
    </table>

    <br />
    <br />
    
    
    @include('committees::partial.session_opinions_print')

    <br />
    <br />

    <table style="padding: 5px;">
        <tr style="text-align: center; background-color: #D9D9D9; font-size: 26px; ">
            
            <th colspan="12" style="border:3.5px solid #fff;
            border-collapse: collapse;
            padding: 10px;" >
               أجندة الإجتماع
            </th>
            <th colspan="1" style="border:3.5px solid #fff;
               border-collapse: collapse;
               padding: 10px;" >#</th>
           
        </tr>

        @foreach($session->subjects as $index => $subject)

            <tr>
                
                <td colspan="12">{{ $subject->title }}</td>
                <td align="center" colspan="1"  style="border:2.5px solid #fff;
                border-collapse: collapse;
                padding: 10px;
                background-color: #D9D9D9;">{{ $index +1 }}</td>
                
                
            </tr>

        @endforeach

    </table>

            

                @include('committees::partial.recommendations_for_admin-print')

               
              



</body>

</html>