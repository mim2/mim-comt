
<link href="/vendor-assets/css/datatables.min.css" rel="stylesheet">
<link href="/vendor-assets/css/datatables.bootstrap.css" rel="stylesheet">
<link href="/vendor-assets/css/datatables.bootstrap-rtl.css" rel="stylesheet">
<link href="/vendor-assets/css/ummalqura.calendars.picker.css" rel="stylesheet">
<link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
@extends('layouts.main.index')

@section('page')

            {{-- @include('committees::partial.category') --}}



    <div class="container card mt-5 p-2 border-0">
        <div class="br-mainpanel">
            <h2 class="inPageTitle">تعديل الإجتماع</h2>
            {{ Form::model($session, ['route' => ['sessions.update', $session->id], 'files' => true, 'class' => 'form-horizontal', 'method' => 'patch']) }}
            @include('committees::sessions.form')
            {{ Form::close() }}
        </div>
    </div>

@stop
