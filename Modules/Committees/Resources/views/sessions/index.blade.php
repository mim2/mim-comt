@extends('layouts.main.index')


<style>

    .usersfrom {
        padding: .75rem;
        vertical-align: top;
        border: 1px solid #ececec;
    }

    .h_h4 {
        padding: .75rem;
        vertical-align: top;
        background-color: #ddd;
        text-align: right;
        color: #444;
        font-size: 20px;
    }

    .has-search .form-control-feedback {
        position: absolute;
        z-index: 2;
        display: block;
        width: 2.375rem;
        height: 2.375rem;
        line-height: 2.375rem;
        text-align: center;
        pointer-events: none;
        color: #aaa;
        left: 37px;
        top: 0;
        bottom: 0;
        margin-top: 41px;
    }

    .th7 th {
        background-color: #c39e46 !important;
        border-color: #c39e45;
        border: 1px solid #ead5a2;
    }



</style>


@section('page')




    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            {{-- <a class="temp p-2 d-table" href="" target="" title="">
                                <i class="fa fa-gear"></i>
                                <span>إدارة الإجتماعات</span>
                            </a> --}}
                            <h5>
                                <img alt="" class=" ml-1" src="/assets/images/{{ $committee->committee_or_council == 1 ? 'folder' : 'business-meeting' }}.svg">
                                {{ $committee->committee_or_council == 1 ? 'لجنة' : 'مجلس' }}
                                {{ $committee->name }}
                            </h5>
                            @if ($committee->managingMemberIds()->contains(auth()->user()->user_id))
                                <span class="p-2 text-center details">
                                <a class="standing p-2" href="{{ route('sessions.create', $committee) }}">
                                    <i class="fa fa-plus"></i>
                                    <span>إجتماع جديد</span>
                                </a>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                @foreach($sessions as $session)

                    <?php $class = @$session->status->id == 1 ? 'sissionNew' : (@$session->status->id == 2 ? 'sissionsu' : (@$session->status->id == 3 ? 'sissiontoSig' : (@$session->status->id == 6 ? 'sissiontoToCont' : (@$session->status->id == 9 ? 'sissiontoFInished' : 'sissionNew')))) ?>
                    <?php $class2 = @$session->status->id == 6 ? 5 : @$session->status->id ?>

                    <div class="row">
                        <div class="col maindetails p-2 mt-3">
                            <div class="row">
                                <div class="col-md-12">

                                    <table class="table text-center mt-2">
                                        <thead class="thcusme2 th7" style="">
                                        <tr>

                                            <th scope="col">عنوان الإجتماع</th>
                                            <th scope="col">الوقت</th>
                                            <th scope="col">التاريخ</th>
                                            <th scope="col">حالة الإجتماع</th>
                                            <th scope="col">الاجراء</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>

                                            <td width="10%" class="usersfrom align-middle">{{ @$session->sessionTitle->name }}</td>
                                            <td width="10%" class="usersfrom align-middle"> {{ Carbon\Carbon::createFromFormat('H:i:s',$session->start_time)->format('h:i')}}</td>
                                            {{-- <td class="usersfrom align-middle">{{ @App\Classes\Date\CarbonHijri::toHijriFromMiladi($session->date) }}</td> --}}

                                            <td width="10%" class="usersfrom align-middle">{{ $session->date }}</td>
                                            <td width="20%" class="usersfrom align-middle"> {{ @$session->status->name }}</td>
                                            <td width="50%" class="usersfrom">
                                                {{ Form::open(['route' => ['sessions.destroy', $session->id], 'method' => 'delete', 'id' => $session->id]) }}
                                                <div class="col-md-4 float-right">
                                                    <a href="{{ route('sessions.manage.show', @$session->id) }}"
                                                       class="btn showBtn border-0 rounded-0">
                                                        <i class="fa fa-eye ml-2"></i>
                                                        عرض الإجتماع
                                                    </a>
                                                </div>

                                                    <div class="col-md-4 float-right">
                                                        <a href="{{ route('subjects.print', @$session->id) }}"
                                                           class="btn showBtn border-0 rounded-0">
                                                            <i class="fa fa-print ml-2"></i>
                                                            طباعة الأجندة
                                                        </a>
                                                    </div>


                                                @if ($committee->managingMemberIds()->contains(auth()->user()->user_id))
                                                    <div class="col-md-4 float-right">
                                                        <a class="" href="{{ route('sessions.edit', $session->id) }}">
                                                            <i style="color: #002060" class="fa fa-edit ml-2"
                                                               style="cursor: pointer;"></i>
                                                        </a>
                                                    </div>

                                                    @if ($session->status_id == 2 || $session->status_id == 1)
                                                        <button type="submit" style="background:#fff " class="btn"
                                                                onclick="return confirm('متأكد من الحذف؟')">
                                                            <i style="color: red; " class="fa fa-trash ml-2"></i>
                                                        </button>
                                                    @endif


                                                @endif
                                                {{ Form::close() }}


                                            </td>


                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>


                            @include('committees::sessions.manage.actions-menu')

                        </div>
                    </div>

                @endforeach

            </div>
        </div>
        <hr>
        <div class="row">

            <div class="col-12 text-center">

                {{ $sessions->links() }}
            </div>

        </div>
        <br>
    </div>

@stop

@section('scripts')
    @include('committees::sessions.scripts')
@endsection