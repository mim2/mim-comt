@extends('layouts.main.index')
@section('page')

            {{-- @include('committees::partial.category') --}}



    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <a class="temp p-2 d-table" href="{{ route('sessions.index', $committee) }}" target="_self" title=" إدارة الإجتماع">
                                <i class="fa fa-gear"></i>
                                <span>إدارة الإجتماعات</span>
                            </a>
                            
                         
                            {{-- <span class="p-2 text-center details">
                                <a id="file_uploade" class="standing p-2" data-target="#newsision" data-toggle="modal" href=""
                                   target="_self" title="اضافة ملف">
                                    <i class="fa fa-plus"></i>
                                    <span>اضافة ملف</span>
                                </a>
                            </span> --}}

                            <p class="sissionPage p-2 acolor">
                                <img alt="اللجان" class=" ml-1" src="/assets/images/addFile.png"> الملفات
                            </p>

                            <h5>
                                <img alt="اللجان" class=" ml-1" src="/assets/images/{{ $session->committee->committee_or_council == 1 ? 'folder' : 'business-meeting' }}.svg">
                                {{ $session->committee->committee_or_council == 1 ? 'لجنة' : 'مجلس' }}
                                {{ $session->committee->name }} 
                                
                                - الإجتماع  
                                
                                {{ $session->sessionTitle->name }}
                            </h5>
                        </div>
                        <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="newsision"
                             role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-lg" role="document">
                                @if ($errors->any())
                                    <div class="alert alert-warning">
                                        {{ __('messages.files_errors') }}
                                    </div>
                                @endif
                                {{ Form::open(['route' => ['sessions.files.store', $committee->id, $session->id], 'class' => 'form-horizontal', 'method' => 'post', 'files' => true ]) }}
                                {{--  <form action="{{route('sessions.files.store', [$committee->id, $session->id])}}" name="deleteFiles" class="form-horizontal" method="post" onsubmit="return confirm_update();">  --}}

                                <div class="modal-content border-0 rounded-0">
                                    <div class="modal-header border-0 rounded-0 p-0">
                                        <h5 id="exampleModalLabel"> ملف جديد</h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row"></div>
                                        <div class="row">
                                         <div id="fileUploade" class="form-group col border-0 rounded-0">
                                            {{ Form::label('file_path', 'اختر الملف', ['class' => 'control-label']) }}
                                            {{ Form::file('file_path', ['class' => 'form-control rounded-0']) }}
                                        </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="form-group col border-0 rounded-0">
                                                <input class="form-control" name="file_name" type="text"
                                                       placeholder="إسم الملف">   
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <div class="modal-footer border-0 rounded-0">
                                        <button type="submit" class="btn sendBtn border-0 rounded-0 ml-2 pl-4 pr-4" id="submit">
                                            إضافة
                                        </button>
                                        <button class="btn nonBtn border-0 rounded-0 ml-2 pl-4 pr-4" data-dismiss="modal" type="button">
                                            إلغاء
                                        </button>

                                        
                                    </div>
                                    
                                </div>
                                {{--  </form>  --}}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        {{ Form::open(['route' => ['sessions.files.store', $committee->id, $session->id], 'class' => 'form-horizontal', 'method' => 'post', 'files' => true ]) }}
                        <add-file
                        file_path="{{ old('file_path') }}"
                        file_name="{{ old('file_name') }}"
                                ></add-file>
                        {{ Form::close() }}
                        @foreach($files as $file)

                            <div class="d-flex flex-row mb-3 justify-content-between text-center topDetails">
                                <div class="p-1 m-2 sissionplace filestab">
                                    <span class="spTitle"><img alt="meeting" class="p-1 ml-1 mb-0"
                                                               src="/assets/images/file.svg">الملف :</span>
                                    <a href="/{{ $file->file_path}}">{{ $file->file_name }}</a>
                                </div>
                                {{-- <div class="p-1 mt-2 mb-2 sissionTime">
                                    <span>وقت الإضافة :</span> <img class="ml-1" src="/assets/images/clock.png" alt="">
                                    {{ $file->created_at->format('h:i a') }}
                                </div>
                                <div class="p-1 mt-2 mb-2 sissionTime">
                                    <img class="ml-1" src="/assets/images/calender.png" alt="">
                                    {{ $file->created_at->format('Y/m/d') }}
                                </div> --}}
                                {{ Form::open(['route' => ['sessions.files.delete', $committee->id, $session->id, $file->id], 'method' => 'delete', 'id' => $file->id]) }}
                                <button type="submit" class="p-1 m-2 sissionremove "
                                        onclick="return confirm('متأكد من الحذف؟')">
                                    <i class="fa fa-trash ml-2"></i>
                                </button>
                                {{ Form::close() }}
                            </div>
                        @endforeach

                       


                        <a  href="{{ route('subjects.index', [$session->committee, $session]) }}"
                        class="btn btn-danger text-white nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button">
                            <i class="fa fa-arrow-right ml-2"></i> رجوع
                        </a>
     

                             <a id="next_action2" style="background:green!important"  href="{{route('guests.index', [$session->committee, $session]) }}"
                                class="btn nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button">
                                 <i class="fa fa-arrow-left ml-2"></i> التالي
                             </a>
                      

                        
                        

                    </div>
                </div>
            </div>
        </div>
        
    </div>
@stop

@section('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            url = $('#next_action').attr('href');
                url2 =  $('#next_action').attr('data-information');
            $('input[type="checkbox"]').click(function(){
                if($(this).prop("checked") == true){
                       // $('#file_uploade').hide();
                        $('#next_action').attr('href', url2);
                }else if($(this).prop("checked") === false){
                    $('#next_action').attr('href', url);
                }
            });
        });

    </script>

@endsection

