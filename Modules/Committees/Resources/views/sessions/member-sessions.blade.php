@extends('layouts.main.index')

<style>

    .th9 th  {
        text-align: center !important;
    }
</style>
@section('page')

            {{-- @include('committees::partial.category') --}}



    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">

                <div class="row">
                        <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                            <div class="maintitle d-flex justify-content-between">
                                <div class="sissionPage p-2 acolor">

                                    <h5>
                                        <img alt="" class="ml-1"
                                             src="/assets/images/{{ @$sessions->committee->committee_or_council == 1 ? 'folder' : 'business-meeting' }}.svg">
                        @foreach($sessions as $session)
                                             
                                        {{ @$sessions->committee->name }}
                                    </h5>
                        @endforeach

                                </div>
                            </div>
                        </div>
                </div>
            <div class="row mt-3">
                <div class="col ownertabelecon">
                    <table class="table">
                        <thead class="ownertabele">
                        <tr class="th9">
                            <th>#</th>
                            <th>الإجتماع</th>
                            <th>اللجنة</th>
                            <th>الحالة</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            @if($sessions->count() == null)
                                <tr>
                                    <td style="text-align: center"  colspan="4">لا توجد محاضر بانتظار التوقيع</td>
                                </tr>
                            @else
                                @foreach($sessions as $session)

                                    <tr class="th9">
                                        <th >{{ $loop->iteration }}</th>
                                        <th >{{ $session->sessionTitle->name }}</th>
                                        <th>{{ $session->committee->name }}</th>
                                        <th>{{ $session->status->name }}</th>
                                        <th>
                                            <a href="{{ route('member.sessions.show', [$session->committee, $session]) }}">
                                                <i class="fa fa-eye"></i> محضر الإجتماع
                                            </a>
                                        </th>
                                    </tr>

                                @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

@stop
