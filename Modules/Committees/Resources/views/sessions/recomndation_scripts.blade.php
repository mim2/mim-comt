<script>
    $(document).ready(function () {
    var counter = 100;
    $(document).on('click', '.rowfy-addrow', function(){
    
    var rowfyable = $(this).closest('table');
    //   alert(counter);
    var lastRow = $('tbody tr:last', rowfyable).clone();
    $('input', lastRow).val('');
    // $('tbody', rowfyable).append(lastRow);
    counter = counter + 1;
    let trow = '';
    trow = `
    
       
        <tr>
            <td>      
                <div class="form-group">
                    <label class="col-form-label">التوصية</label>
                    <div class="changefont">
                        <input class="form-control" name="signed_services[${counter}][recommendation]"  placeholder="التوصية">
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <label class="col-form-label">التاريخ</label>
                    <div class="changefont">
                        <input class="form-control" name="signed_services[${counter}][date]"  placeholder="01 / 02 / 1442">
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                        <div class="form-check form-check-inline noteAll">
                            <h6 style="color:#dc3545" >
                                <i class="fa fa-info"></i>  ملحوظة: اختيار جميع الاعضاء تلقائيا
                            </h6>
                        </div>
                    <div id="myDiv" class=" form-group border-0 rounded-0">
                       
                    </div>
                </div>
            </td>
            <td><button type="button" class="btn rowfy-addrow btn sendBtn border-0 rounded-0 pl-4 pr-4"><i class="fa fa-plus"></i> اضافة توصية </button></td>
        </tr>
    `;
    $('tbody', rowfyable).append(trow);
    
    console.log(counter);
    // $('#myTable > tbody').append('<tr><td>my data</td><td>more data</td></tr>');
    // myFunction();
    // var f1 = $('#subjectId').val();
    //     $('#signedSubjectId').val(f1);
    //     console.log(f1);
    $(this).removeClass('rowfy-addrow btn sendBtn border-0 rounded-0 pl-4 pr-4').addClass('rowfy-deleterow btn nonBtn btnall border-0 rounded-0 pl-4 pr-4').text('حذف التوصية');
  });
  });

</script>

  