@extends('layouts.main.index')

@section('page')

            {{-- @include('committees::partial.category') --}} 



    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between ownerSection">
                        <div class="maintitle d-flex flex-column justify-content-between">
                            <div class="row">
                                <div class="col-2 owtitle text-center mr-3">
                                    <a class="" href="">
                                        <img alt="اللجان" class=" mx-auto d-block" src="/assets/images/new-file.svg">
                                        <span>المحاضر الجديدة</span>
                                    </a>
                                </div>
                                <form action="?">
                                <div class="col text-right">
                                    <label>لجان/مجالس</label>
                                    <select name="type" id="selsubject" onchange="$('form').submit()">
                                        <option value="0">الكل</option>
                                        <option value="1" {{ @$_GET['type'] == 1 ? 'selected':'' }}>لجنة</option>
                                        <option value="2" {{ @$_GET['type'] == 2 ? 'selected':'' }}>مجلس</option>
                                    </select>
                                </div>
                                <div class="col text-right">
                                    <label>اللجنة /المجلس</label>
                                    {!! Form::select('committee', $committees, @$_GET['committee'], ['id' => 'selsubject', 'onchange' => "$('form').submit()"]) !!}
                                </div>
                                </form>
                            </div>

                            <div class="row mt-3">
                                <div class="col ownertabelecon">
                                    <table class="table">
                                        <thead class="ownertabele">
                                        <tr>
                                            <th>اللجنة أو المجلس</th>
                                            <th>الإجتماع</th>
                                            <th>تاريخ الانعقاد</th>
                                            <th>حالة الإجتماع</th>
                                            <th></th>
                                        </tr>
                                        </thead>

                                        @if($sessions->count() == null)

                                            <tr><td colspan="5" class="ttd">لا توجد محاضر بانتظار الاعتماد.</td></tr>

                                        @else

                                            @foreach($sessions as $session)

                                                <tr>
                                                    <th class="ttd">{{ @$session->committee->name }}</th>
                                                    <th class="ttd">{{ @$session->sessionTitle->name }}</th>
                                                    <th class="ttd">{{ @$session->date }}</th>
                                                    <th class="ttd inused">{{ @$session->status->name }}</th>
                                                    <th class="ttd">
                                                        <a class="text-dark" href="{{ route('approval.sessions.show', [$session->committee, $session]) }}">محضر الإجتماع</a>
                                                    </th>
                                                </tr>

                                            @endforeach
                                        @endif

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
