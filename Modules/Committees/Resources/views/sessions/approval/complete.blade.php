@extends('layouts.main.index')

@section('page')

            {{-- @include('committees::partial.category') --}} 



    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between ownerSection">
                        <div class="maintitle d-flex flex-column justify-content-between">
                           
                                        <span style="text-align: center">المحاضر التي تنتظر الاعتماد </span>
                                
                            
                          
                           
                        </div>
                    </div>
                </div>
                            <div class="row mt-3">
                                <div class="col ownertabelecon">
                                    <table class="table">
                                        <thead class="ownertabele2">
                                        <tr align="center">
                                            <th>اللجنة أو المجلس</th>
                                            <th>الإجتماع</th>
                                            <th>تاريخ الانعقاد</th>
                                            <th>حالة الإجتماع</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        @unless($sessions->count())

                                            <tr align="center">
                                                <td colspan="4" class="ttd">لا توجد محاضر تنتظر الاعتماد.</td>
                                            </tr>

                                        @endunless

                                        @foreach($sessions as $session)

                                            <tr align="center">
                                                <th class="ttd">{{ $session->committee->name }}</th>
                                                <th class="ttd">{{ $session->sessionTitle->name }}</th>
                                                <th class="ttd">{{ $session->date }}</th>
                                                <th class="ttd inused">{{ $session->status->name }}</th>
                                                <th class="ttd">
                                                    <a class="text-dark" href="{{ route('approval.sessions.show', [$session->committee, $session]) }}">محضر الإجتماع</a>
                                                </th>

                                                
                                            </tr>

                                        @endforeach

                                    </table>
                                </div>
                            </div>
                       
                   
                
            </div>
        </div>
    </div>

@stop
