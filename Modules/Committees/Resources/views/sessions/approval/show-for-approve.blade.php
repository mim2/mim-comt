

{{ Form::open(['route' => ['approval.sessions.update', $session], 'method' => 'patch']) }}
<h5 class="text-right headtitle head3 mr-3">اعتماد صاحب الصلاحية</h5>
<div class="form-group row" style="background-color: #FBFBFB">
    <label for="has_reservation" class="col-md-3">القرار:</label>
    <div class="col-md-9">
        <div class="radio">
            <div class="form-check form-check-inline">
                <input type="radio" value="10" name="status_id" class="approve form-check-input">
                <label class="form-check-label mr-2">اعتماد</label>
            </div>
        </div>
        <div class="radio">
            <div class="form-check form-check-inline">
                <input type="radio" value="10" name="status_id" class="form-check-input">
                <label class="form-check-label mr-2">اعتماد مع ملاحظة</label>
            </div>
        </div>
        <div class="radio">
            <div class="form-check form-check-inline">
                <input type="radio" value="6" name="status_id" class="form-check-input">
                <label class="form-check-label mr-2">للاستكمال</label>
            </div>
        </div>
    </div>
</div>
<div class="form-group row" id="note" style="background-color: #FBFBFB">
    {{ Form::label('note', 'ملاحظات', ['class' => 'control-label col-md-3']) }}
    <div class=" form-group col border-0 rounded-0" style="background-color: #FBFBFB">
        {{ Form::textarea('notes', old('notes'), ['class' => 'form-control textsty', 'id' => 'notes', 'required' => true]) }}
    </div>
</div>
<div class="d-flex justify-content-end mainbuttons mt-4 mb-4">
    <button type="submit" class="btn sendBtn border-0 rounded-0 ml-2 pl-4 pr-4">إرسال</button>
</div>
{{ Form::close() }}
