@extends('layouts.main.index')

@section('page')

            {{-- @include('committees::partial.category') --}} 



    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <a class="sissionPage p-2 acolor" href="#">
                                <img src="/assets/images/link.png" alt="">
                                <span>صفحة الإجتماع</span>
                            </a>
                          <a href="{{ route('member.sessions.print', [$session->committee, $session]) }}" class="sissionpaper p-2 acolor">
                                <img class=" ml-1" src="/assets/images/file.svg" alt="">
                                <span>محضر اجتماع ال{{ $session->committee->committee_or_council == 1 ? 'لجنة' : 'مجلس' }}</span></a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <div class="d-flex flex-column mb-3 justify-content-between topDetails resulttable">
                            <table class="table text-center">
                                <thead class="thcusme">
                                <tr>
                                    <th scope="col">اسم {{ @$session->committee->committee_or_council == 1 ? 'اللجنة' : 'المجلس'}}</th>
                                    <th scope="col">رقم الإجتماع</th>
                                    <th scope="col">الوقت</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $session->committee->name }}</td>
                                    <td class="secondfont">{{ $session->sessionTitle->name }}</td>
                                    <td class="secondfont">{{ $session->start_time }} {{ $session->date }}</td>
                                </tr>
                                </tbody>
                            </table>
                            <hr>

                            @include('committees::partial.committee_members')

                            <h5 class="text-right headtitle head3 mr-3">ثانيا: جدول أعمال اللجنة</h5>
                            <table class="table text-center">
                                <thead class="thcusme2 th3">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">الموضوع</th>
                                </tr>
                                </thead>
                                <tbody>
                                @unless($session->subjects->count())

                                            <tr>
                                                <td colspan="4" class="ttd">لم يتم جدول الاعمال.</td>
                                            </tr>

                                        @endunless

                                @foreach($session->subjects as $subject)

                                    <tr>
                                        <td>{{ @++$key }}</td>
                                        <td>{{ $subject->title }}</td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                            <hr>
                            @include('committees::partial.recommendations_for_admin')

                            <hr>

                            @include('committees::partial.session_opinions')

                            @if ($session->files->count())
                                <h4 class="mt-4 titleIn">الملفات</h4>
                                <div class="col-md-12">
                                    @foreach($session->files as $file)
                                        <ul>
                                            <li>
                                                <i class="fa fa-download"></i>
                                                <a href="/{{ $file->file_path}}">{{ $file->file_name }}</a>
                                            </li>
                                        </ul>
                                    @endforeach
                                </div>
                            @endif
                            @if ($session->status_id == 4)
                            @include('committees::sessions.approval.show-for-approve')
                            @endif
                            

                            <h5 class="text-right headtitle head3 mr-3">إرشيف حالات الإجتماع</h5>
                            <table class="table text-center">
                                <thead class="thcusme2 th5">
                                <tr>
                                    <th>الحالة</th>
                                    <th>اسم المستخدم</th>
                                    <th>التاريخ</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($history as $item)

                                    <tr>
                                        <td>{{ $item->status->name }}</td>
                                        <td>{{ $item->user_name }}</td>
                                        <td>{{ $item->created_at }}</td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>
        $('input:radio[name="status_id"]').change(function () {
            if (this.className == 'approve form-check-input') {
                $('#notes').prop('disabled', true);
                $('#note').hide();
            } else {
                $('#notes').prop('disabled', false);
                $('#note').show();
            }
        });
    </script>

@endsection
