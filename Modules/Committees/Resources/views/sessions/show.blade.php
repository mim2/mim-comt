@extends('layouts.main.index')

@section('page')
    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                             <a class="temp p-2 d-table"  href="{{ route('sessions.manage.show', $session->id) }}">
                                    <span><img src="/assets/images/link.png" >معلومات الإجتماع</span>
                             </a>
                             <a class="temp p-2 d-table" target="_blank"  href="{{ route('member.sessions.print', [$session->committee, $session]) }}" >
                                <span>
                                    <img alt="" class=" ml-1" src="/assets/images/file.svg"> محضر اجتماع اللجنة
                                </span>
                             </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <div class="d-flex flex-column mb-3 justify-content-between topDetails resulttable">
                            <table class="table text-center">
                                <thead class="thcusme">
                                    <tr>
                                        <th scope="col">اسم {{ @$session->committee->committee_or_council == 1 ? 'اللجنة' : 'المجلس'}}</th>
                                        <th scope="col">رقم الإجتماع</th>
                                        <th scope="col">التاريخ</th>
                                        <th scope="col">الساعة</th>
                                        <th scope="col">رقم القرار</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $session->committee->name }}</td>
                                        <td class="secondfont">{{ $session->sessionTitle->id }}</td>
                                        <td class="secondfont">{{ $session->date }} </td>
                                        <td class="secondfont">{{ $session->start_time }}</td>
                                        <td class="secondfont">{{ $session->committee->decision_name }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr>

                            @include('committees::partial.committee_members')

                            <hr>
                            <h5 class="text-right headtitle head3 mr-3">ثانيا: جدول أعمال اللجنة</h5>
                            <table class="table text-center">
                                <thead class="thcusme2 th3">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">الأجندة</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($session->subjects as $subject)
                                    <tr>
                                        <td>{{ @++$key }}</td>
                                        <td>{{ $subject->title }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            @if($session->status_id >= 3)

                                <hr>
                                @include('committees::partial.recommendations_for_admin')
                                @include('committees::partial.session_opinions')
                                @include('committees::partial.session_files')
                                @include('committees::partial.your_opinions')
                                @include('committees::partial.auths_opinions')

                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script type="text/javascript">
        $('input:radio[name="has_reservation"]').change(function () {
            if (this.value == 2) {

                $('#notes').prop('disabled', true);
                $('#notes').val('');
                $('#note').hide();
            } else {
                $('#notes').prop('disabled', false);
                $('#note').show();
            }
        });
        $('#notes').prop('disabled', true);
        $('#note').hide();

        $('#submit').click(function () {
            var opinion = $('input:checked[name="opinion"]').val();
            if (!opinion) {
                alert('ماهو رأيك في المحضر؟');
                return false;
            }
            var reservation = $('input:checked[name="has_reservation"]').val();
            if (!reservation) {
                alert('هل لديك تحفظات على المحضر؟');
                return false;
            }
            if (reservation == 1 && $('#notes').val() == '') {
                alert('الرجاء كتابة تحفظاتك');
                return false;
            }
            var url = '{{ route('sessions.opinion', $session) }}';
            var data = {
                user_id: {{ auth()->user()->user_id }},
                opinion: opinion,
                has_reservation: reservation,
                reservation_reason: $('#notes').val(),
                '_token': $('input[name="_token"]').val(),
            };

            $.post(url, data).done(function () {
                $('#signetuersend').modal();
            });
            return false;
        });


        function notify(user, session) {
            $('#waiting').modal();
            $.get('/committees/sessions/' + session + '/notify/' + user).done(function (data) {
                $('#waiting').modal('hide');
                $('#member_name').html(data);
                $('#remindedmessage').modal();
                $('#waiting').modal('hide');
    
            });
        }

        function hideNote(elmtName) {
            console.log(elmtName)
            // $(elmtName).prop('disabled', true);
            $(elmtName).hide(300);
            $(elmtName).val('');
        }

        function showNote(elmtName) {
            console.log(elmtName)
            $(elmtName).show(300);
            $(elmtName).val('');
        }
    
    </script>

@endsection
