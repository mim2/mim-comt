

@if ($committee->managingMemberIds()->contains(auth()->user()->user_id))

<div class="d-flex flex-row mt-5 mb-3 justify-content-center text-center bigIco">

    @if (!$session->isSigned())

    @if ($session->status_id == 2 || $session->status_id == 1 || $session->status_id == 6 )

            <a class="m-2" href="{{ route('subjects.index', [$session->committee, $session]) }}">
                <img class="ml-1 orangBtn d-block p-4 mb-3 {{ $session->subjects->count() > 0 ? 'bg_orangiconicheck' : '' }}"
                    src="/assets/images/subject.png" alt="">
                <span> إضافة أجندة</span>
            </a>
            
            <div class="checkbox icheck-success">
                <input type="checkbox" id="success2" name="success" {{ $session->subjects->count() > 0 ? 'checked' : '' }}>
                <label for="exampleRadios1"></label>
            </div>
            @else
            <a class="m-2" >
                    <img class="ml-1 grayBtn d-block p-4 mb-3 {{ $session->subjects->count()  ? 'bg_grayiconicheck' : '' }}"
                        src="/assets/images/subject.png" alt="">
                    <span> إضافة أجندة</span>
                </a>
                
                <div class="checkbox icheck-success">
                    <input type="checkbox" id="success2" name="success" {{ $session->subjects->count() > 0 ? 'checked' : '' }}>
                    <label for="exampleRadios1"></label>
                </div>
        
     @endif

            @if ($session->status_id == 2 || $session->status_id == 1 || $session->status_id == 6 )
                <a class="m-2" href="{{ route('sessions.files.index',  [$session->committee, $session]) }}">
                    <img class="ml-1 orangBtn d-block p-4 mb-3 {{ $session->files->count() > 0 ? 'bg_orangiconicheck' : '' }}"
                        src="/assets/images/addFile.png" alt="">
                    <span>إضافة ملفات</span>
                </a>
                <div class="checkbox icheck-success">
                    <input type="checkbox" id="success2" name="success" {{ $session->files->count() > 0 ? 'checked' : '' }}>
                    <label for="exampleRadios1"></label>
                </div>
            @else
                <a class="m-2">
                        <img class="ml-1 grayBtn d-block p-4 mb-3 {{ $session->files->count() > 0 ? 'bg_grayiconicheck' : '' }}"
                             src="/assets/images/addFile.png" alt="">
                        <span>إضافة ملفات</span>
                    </a>
                    <div class="checkbox icheck-success">
                        <input type="checkbox" id="success2" name="success" {{ $session->files->count() > 0 ? 'checked' : '' }}>
                        <label for="exampleRadios1"></label>
                    </div>
            @endif


    @else

        <a class="m-2">
            <img class="ml-1 grayBtn d-block p-4 mb-3 {{ $session->subjects->count() > 0 ? 'bg_grayiconicheck' : '' }}"
                 src="/assets/images/subject.png" alt="">
            <span>إضافة أجندة</span>
        </a>
        <div class="checkbox icheck-success">
            <input type="checkbox" id="success2" name="success"  }}>
            <label for="exampleRadios1"></label>
        </div>

        {{-- <a class="m-2">
            <img class="ml-1 grayBtn d-block p-4 mb-3 {{ $session->files->count() > 0 ? 'bg_grayiconicheck' : '' }}"
                 src="/assets/images/addFile.png" alt="">
            <span>إضافة ملفات</span>
        </a>
        <div class="checkbox icheck-success">
            <input type="checkbox" id="success2" name="success" {{ $session->files->count() > 0 ? 'checked' : '' }}>
            <label for="exampleRadios1"></label>
        </div> --}}

    @endif

    @if ($session->status_id == 2 || $session->status_id == 1 || $session->status_id == 6)

        <a class="m-2" href="{{ route('guests.index', [$session->committee, $session]) }}">
            <img class="ml-1 orangBtn d-block p-4 mb-3 {{ $session->guests->count() ? 'bg_orangiconicheck' : '' }}"
                 src="/assets/images/addusers.png" alt="">
            <span>إضافة مدعويين</span>
        </a>

    @else

        <a class="m-2">
            <img class="ml-1 grayBtn d-block p-4 mb-3 {{ $session->guests->count() ? 'bg_grayiconicheck' : '' }}"
                 src="/assets/images/addusers.png" alt="">
            <span>إضافة مدعويين</span>
        </a>

    @endif
    <div class="checkbox icheck-success">
        <input type="checkbox" id="success2"
               name="success" {{ $session->guests->count() ? 'checked' : '' }}>
        <label for="exampleRadios1"></label>
    </div>

    <?php $flag = $session->attendees->count() == $session->committee->members->count() + $session->guests->count() ?>

    @if ( $session->status_id == 2 || $session->status_id == 1 || $session->status_id == 6)
    
        <a class="m-2" href="{{ route('sessions.attendees.index', [$session->committee, $session]) }}">
            <img alt="" class="ml-1 orangBtn d-block p-4 mb-3 {{ $flag ? 'bg_orangiconicheck' : '' }}"
                 src="/assets/images/attendance.png">
            <span>التحضير</span>
        </a>

    @else

        <a class="m-2">
            <img alt="" class="ml-1 grayBtn d-block p-4 mb-3 {{ $flag ? 'bg_grayiconicheck' : '' }}"
                 src="/assets/images/attendance.png">
            <span>التحضير</span>
        </a>

    @endif
    <div class="checkbox icheck-success">
        <input type="checkbox" id="success2"
               name="success" {{ $session->attendees->count()  ? 'checked' : '' }}>
        <label for="exampleRadios1"></label>
    </div>

    @if ($session->status_id == 6)

        @if ($session->recommendation != null)
        <a class="m-2" href="{{ route('recommendations.change', [$session,$session->recommendation->id]) }}">
            <img class="ml-1 orangBtn d-block p-4 mb-3 {{ strlen($session->recommendations) > 0 ? 'bg_orangiconicheck' : '' }}"
                 src="/assets/images/attendesfolder.png" alt="">
            <span>بناء المحضر</span>
        </a>
        @else 

        <a class="m-2" href="{{ route('recommendations.edit',  $session) }}">
            <img class="ml-1 orangBtn d-block p-4 mb-3 {{ strlen($session->recommendations) > 0 ? 'bg_orangiconicheck' : '' }}"
                 src="/assets/images/attendesfolder.png" alt="">
            <span>بناء المحضر</span>
        </a>
        @endif


    @else

        <a class="m-2">
            <img class="ml-1 grayBtn d-block p-4 mb-3 {{ strlen($session->recommendations) > 0 ? 'bg_grayiconicheck' : '' }}"
                 src="/assets/images/attendesfolder.png" alt="">
            <span>بناء المحضر</span>
        </a>

    @endif
    <div class="checkbox icheck-success">
        <input type="checkbox" id="success2"
               name="success" {{ strlen($session->recommendation) > 0 ? 'checked' : '' }}>
        <label for="exampleRadios1"></label>
    </div>

     {{-- @if ( $session->status_id == 3 ) --}}
    @if ( $session->recommendation != null   && $session->status_id == 6) 

        <a class="m-2" href="{{ route('sessions.elevate', $session) }}">
                <img alt="" class="ml-1 redBtn d-block p-4 mb-3" src="/assets/images/sendf.png">
                <span>إرسال المحضر للتوقيع </span>
            </a>
        

            
          {{-- @endif  --}}
    @else

        <a class="m-2">
            <img alt="" class="ml-1 badBtn d-block p-4 mb-3" src="/assets/images/sendf.png">
            <span>تم إرسال المحضر للتوقيع
            </span>

            
        </a>

        


    @endif

    

    @if ($session->status_id == 10) 

        <a class="m-2" target="_blanc" href="{{ route('member.sessions.print', [$session->committee, $session]) }}">
                <img alt="" class="ml-1 orangBtn d-block p-4 mb-3" src="/assets/images/SessionSchedule.png">
                <span>طباعة المحضر</span>
            </a>

            
        

          {{-- @endif  --}}
    @endif

    

    


</div>

@endif


  