@extends('layouts.main.index')

@section('stylesheets')
    <style>

        .th7 th {
            background: #c39e46 !important;
            border-color: #c39e45 !important;
            border: 1px solid #ead5a2 !important;
        }


        .sendBtns {
            background: #002060 !important;
            color: #fff !important;
        }

        .btn-info:hover {
            color: #fff;
            background-color: #c39e47;
            border-color: #c39e47;
        }

        .btn:focus, .btn.focus {
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgba(255, 255, 255, 0.25);
        }
    </style>
@endsection
@section('page')

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <a class="temp p-2 d-table" href="{{ route('sessions.index', $session->committee->id)  }}"
                               target="" title="">
                                <i class="fa fa-gear"></i>
                                <span>إدارة الإجتماعات</span>
                            </a>


                            <h5>
                                <span class="sissionPage p-2 text-center">معلومات الإجتماع</span>
                            </h5>

                            <h5>
                                <img alt="" class="ml-1"
                                     src="/assets/images/{{ $session->committee->committee_or_council == 1 ? 'folder':'business-meeting' }}.svg">
                                {{ $session->committee->committee_or_council == 1 ? 'لجنة' : 'مجلس' }}
                                {{ $session->committee->name }}
                            </h5>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table text-center mt-2">
                                    <thead class="thcusme2 th7" style="background:red">
                                    <tr>
                                        <th scope="col">عنوان الإجتماع</th>
                                        <th scope="col" class="list">الوقت</th>
                                        <th scope="col">التاريخ</th>
                                        <th scope="col">مكان الإجتماع</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="usersfrom align-middle">{{ @$session->sessionTitle->name }}</td>
                                        <td class="usersfrom align-middle"> {{ Carbon\Carbon::createFromFormat('H:i:s',$session->start_time)->format('h:i')}}</td>
                                        <td class="usersfrom align-middle">{{ $session->date }}</td>
                                        <td class="usersfrom align-middle"><a style="color: black" target="_blanc"
                                                                              href="{{ @$session->place }}">{{ @$session->place }}</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="p-1 m-2 usertittle">
                            <img class="ml-1" src="/assets/images/icons/usersBlue.png" alt=""> أعضاء اللجنة
                        </div>

                        <div class="d-flex flex-row mb-3 sissonallDetails p-2">
                            <div class="col flex-column userentryTime">

                                @foreach($session->committee->members as $member)
                                    <?php $attendee = $session->attendees->where('user_id', $member->member->user_id)->first() ?>

                                    <div class="mainuser d-flex flex-row mb-2 p-2 text-right">
                                        <div class="username">
                                            <img class="ml-1" src="/assets/images/icons/userCat.png" alt="">
                                            {{ $member->member->user_name }}
                                        </div>
                                        <div class=" timeent">{{ $member->memberType->name }}</span>
                                        </div>
                                    </div>

                                @endforeach

                            </div>
                            <div class="col leftBtn flex-column mt-4 justify-content-between text-center">
                                <div class="sissionPageIn p-2 mb-2">
                                    <a href="{{ route('member.committees.show', [$session->committee, $session]) }}">
                                        <img class="ml-1" src="/assets/images/infoIn.png" alt="">
                                        {{ $session->committee->committee_or_council == 1 ? 'معلومات اللجنة' : 'معلومات المجلس' }}
                                    </a>
                                </div>
                                <div style="    background: #002060;" class="sissionpaper p-2 mb-2">
                                    <a href="{{ route('member.sessions.show', [$session->committee, $session]) }}">
                                        <img alt="" class="ml-1" src="/assets/images/file.svg"> محضر الإجتماع
                                    </a>
                                </div>
                                @if ( $userOpinion != 1)
                                    <div class="sissionappology p-2 mb-2" data-target="#appologymodel"
                                         data-toggle="modal">
                                        <img alt="الأولى" class="ml-1" src="/assets/images/appology.png"> اعتذار عن
                                        الحضور
                                    </div>
                                @endif
                                <div aria-hidden="true" aria-labelledby="appologymodelLabel" class="modal fade"
                                     id="appologymodel" role="dialog" tabindex="-1">

                                    {{ Form::open(['route' => ['sessions.manage.store'], 'class' => 'form-horizontal', 'method' => 'post']) }}
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content border-0 rounded-0">
                                            <div class="modal-header border-0 rounded-0 p-0">
                                                <h5 class="modal-title sissionappology p-2" id="appologymodelLabel"><img
                                                            alt="" class="ml-1" src="/assets/images/appology.png">الاعتذار
                                                    عن الإجتماع</h5>
                                                <button aria-label="Close" class="close" data-dismiss="modal"
                                                        type="button"><span aria-hidden="true">&times;</span></button>
                                            </div>

                                            <div class="modal-body">
                                                <input type="hidden" name="user_id"
                                                       value="{{ $member->member->user_id }}">
                                                {{ Form::textarea("apology_reason",
                                                old("attendees.{$member->member->user_id}.apology_reason") ?? optional($attendee)->apology_reason  ,
                                                ['class' => 'form-control' , 'placeholder' => 'الرجاء كتابة سبب الاعتذار', 'id' => 'session-attendance-apology-reason-' . $member->member->user_id]) }}
                                                <input type="hidden" name="session_id" value="{{ $session->id }}">


                                                <span class="has-error-span"> {{$errors->first("attendees.{$member->member->user_id}.apology_reason") }}</span>
                                            </div>
                                            <div class="modal-footer justify-content-center">

                                                <button class="btn sendBtn border-0 rounded-0 ml-2 pl-4 pr-4"
                                                        type="submit">موافق
                                                </button>
                                                <button class="btn nonBtn border-0 rounded-0 ml-2 pl-4 pr-4"
                                                        data-dismiss="modal" type="button">إلغاء
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </div>

                            </div>
                        </div>

                        <div class="d-flex flex-row text-right">
                            <div class="SessionSchedule p-2 mb-2" style="background: #fff; color: black">
                                <i class="fa fa-tasks " style="color: #002060; font-size:24px;"></i>
                                جدول أعمال الإجتماع
                            </div>
                        </div>
                        <div class="d-flex flex-column text-right Sessiondiss justify-content-between">


                            @foreach($session->subjects as $subject)
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="mainuser d-flex flex-row mb-2 p-2 text-right lastcommtitle">
                                            <div class="suname">
                                                <img class="ml-1" src="/assets/images/bFile.png" alt="">
                                                <a
                                                        style="color: black"
                                                        data-toggle="collapse"
                                                        href="#collapseExample{{$subject->id}}"
                                                        aria-controls="collapseExample">
                                                    {{ $subject->title }}
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    @if ( $userOpinion != 1)
                                        <div class="col-md-2">
                                            <a class="btn sendBtns border-0 rounded-0 btn-block collapsed"
                                               data-toggle="collapse"
                                               href="#collapseExample{{$subject->id}}"
                                               role="button"
                                               aria-expanded="false"
                                               aria-controls="collapseExample"
                                               id="testFun"
                                               dataOpinionId="{{ $subject->id }}"
                                               style="padding: 15px;">

                                                <i class="fa fa-comments-o"></i>
                                                مرئيات
                                            </a>
                                        </div>
                                    @else

                                        <div class="col-md-2">
                                            <a class="btn sendBtns border-0 rounded-0 btn-block" data-toggle="collapse"
                                               href="#collapseExample{{$subject->id}}" role="button"
                                               aria-expanded="false" aria-controls="collapseExample"
                                               style="padding: 15px;">
                                                <i class="fa fa-comments-o"></i>
                                                المرئيات
                                            </a>
                                        </div>
                                    @endif
                                </div>

                                <div class="collapse" id="collapseExample{{$subject->id}}">
                                    @if(count($subject->comments) > 0)
                                        @foreach ($subject->comments as $comment)
                                            <div class="mainuser d-flex flex-row mb-2 p-2 text-right lastcommtitle"
                                                 style="background: #f8f9fc !important;">
                                                <div class="suname">
                                                    <p style="margin-right: 80px;">{{ $comment->comment }}</p>
                                                </div>
                                                <div class=" sutime mt-2">
                                                    <p class="font-italic text-muted"
                                                       style="font-size:12px; font-family: GE Dinar Two Medium;">مرئيات
                                                        : {{ $comment->user->user_name }}
                                                        - {{ $comment->created_at }}</p>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else

                                        <div class="mainuser d-flex flex-row mb-2 p-2 text-right lastcommtitle"
                                             style="background: #f8f9fc !important;">
                                            <div class="suname">
                                                <p style="text-align: center">لايوجد مرئيات</p>
                                            </div>

                                        </div>

                                    @endif
                                    @if ( $userOpinion != 1)
                                        {{ Form::open(['route' => ['comments.store', $subject->id], 'class' => 'login-form', 'method' => 'post']) }}
                                        <div class="d-flex flex-row mb-2 p-2 text-right comment">
                                            <textarea required name="comment" class="form-control rounded-0"
                                                      placeholder="يرجي اضافة مرئيات ان وجد" rows="4"></textarea>
                                        </div>
                                        {{-- {{ Form::hidden('subject_id', $subject->id) }} --}}

                                        <div class="d-flex flex-row mb-2 p-2 text-right justify-content-center">
                                            <button class="btn sendBtn border-0 rounded-0 ml-2 pl-4 pr-4"
                                                    data-dismiss="modal" type="submit">إضافة
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    @endif
                                </div>


                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
