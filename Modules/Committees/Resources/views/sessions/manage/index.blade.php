@extends('layouts.main.index')

<style>

    .usersfrom{
        padding: .75rem;vertical-align: top;border: 1px solid #ececec;
        border: 1px #c39e47 solid !important;
    }
    .h_h4{
        padding: .75rem;
        vertical-align: top;
        background-color: #ddd;
        text-align: right;
        color: #444;
        font-size: 20px;
    }
    
    .has-search .form-control-feedback {
        position: absolute;
        z-index: 2;
        display: block;
        width: 2.375rem;
        height: 2.375rem;
        line-height: 2.375rem;
        text-align: center;
        pointer-events: none;
        color: #aaa;
        left: 37px;
        top: 0;
        bottom: 0;
        margin-top: 41px;
    }
    
    .th7 th {
        background-color: #c39e46 !important;
        border-color: #c39e45 !important;
        border: 1px #c39e47 solid !important;
    }
    .sendBtn {
        background: #002060 !important;
        color: #fff;
        min-width: 200px;
    }
    .th13 th {
        background: #fff !important;
        color: #c39e47 !important;
        border: 1px #c39e47 solid !important;
    }
    
    </style>

@section('page')



            {{-- @include('committees::partial.category') --}}
            

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            
            <div class="col SissionDetails">

                <div   class="row">
                    <div  class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div   class="maintitle d-flex justify-content-between">
                            <a  href="{{ url()->previous() }}" target="" title="">
                               
                            </a>

                            <a  class="" href="{{ url()->previous() }}" target="" title="">
                                <img alt="" class=" ml-1" src="/assets/images/sent_members.svg">
                                <span >محاضر تنتظر الارسال لتوقيع الاعضاء</span>
                            </a>

                            <a   href="{{ url()->previous() }}" target="" title="">
                              
                            </a>
                        
                        
                        </div>


                    </div>
                </div>
            
                <br />
                @if($sessions->count())
                    @foreach($sessions as $session)
                        <?php $class = $session->status->id == 1 ? 'sissionNew' : ($session->status->id == 2 ? 'sissionsu' : ($session->status->id == 3 ? 'sissiontoSig' : ($session->status->id == 6 ? 'sissiontoToCont' : ($session->status->id == 8 ? 'sissiontoFInished' :'sissiontoToApprov')))) ?>
                        <?php $class2 = $session->status->id == 6 ? 5 : $session->status->id ?>

                      

                        <div class="col maindetails p-2 mt-3">
                            <div class="d-flex flex-row mb-3 justify-content-between text-center topDetails">
                             

                                <table class="table text-center mt-2">
                                    <thead class="thcusme2 th13" style="">
                                        <tr>
    
                                            <th colspan="12" class="w-25"> 
                                            
                                                <img style="backgroung: red" alt="" class=" ml-1" src="/assets/images/icons/{{ $session->committee->committee_or_council == 1 ? 'folder1' : 'business-meeting1'  }}.svg">
                                               
                                                {{ $session->committee->committee_or_council == 1 ? 'لجنة' : 'مجلس' }}
                                                {{ $session->committee->name }}
                                            </th>
                                          
                                            
                                        </tr>
                                    </thead>

                                    <thead class="thcusme2 th7" style="">
                                        <tr>
    
                                            <th scope="col" class="w-25">عنوان الإجتماع</th>
                                            <th scope="col">الوقت</th>
                                            <th scope="col">التاريخ</th>
                                            <th scope="col">حالة الإجتماع</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                           
                                            <td class="usersfrom align-middle">{{ @$session->sessionTitle->name }}</td>
                                            <td class="usersfrom align-middle"> {{ Carbon\Carbon::createFromFormat('H:i:s',$session->start_time)->format('h:i')}}</td>
                                            <td class="usersfrom align-middle">{{ @$session->date }}</td>
                                            <td class="usersfrom align-middle"> {{ @$session->status->name }}</td>
                                            

                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            @include('committees::sessions.manage.actions-menu')
                            

                        </div>
                        

                    @endforeach
                @else

                    <div class="col CommityDetails">
                        <div class="row mt-2">
                            <div class="col maindetails p-2 mt-3">
                                <div class="mb-3 text-center ">
                                    <h6 class="text-center m-5">
                                        <i class="fa fa-exclamation-triangle fa-4x mb-3 d-block gray"></i>
                                        لا توجد محاضر تنتظر تنتظر الارسال لتوقيع الاعضاء
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>

@stop
