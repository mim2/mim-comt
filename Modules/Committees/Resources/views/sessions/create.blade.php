<link href="/vendor-assets/css/datatables.min.css" rel="stylesheet">
<link href="/vendor-assets/css/datatables.bootstrap.css" rel="stylesheet">
<link href="/vendor-assets/css/datatables.bootstrap-rtl.css" rel="stylesheet">
<link href="/vendor-assets/css/ummalqura.calendars.picker.css" rel="stylesheet">
<link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
@extends('layouts.main.index')

@section('page')

            {{-- @include('committees::partial.category') --}} 



    <div class="container mt-5 minhe">
        <div class="col SissionDetails">
            <div class="modal-header border-0 rounded-0 p-0">
                <h5><i class="fa fa-plus"></i> إضافة الإجتماع</h5>
            </div>
            <div class="row">
                <div class="col maindetails text-dark p-2 mt-3">
                    {{ Form::open(['route' => ['sessions.store', $committee->id], 'files' => true, 'class' => 'form-horizontal', 'method' => 'post']) }}
                    @include('committees::sessions.form')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@stop
