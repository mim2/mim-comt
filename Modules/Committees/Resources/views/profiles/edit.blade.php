@extends('layouts.main.index')

@section('page')

    {{-- @include('committees::partial.category') --}}

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col categoryContainer">
                <div class="tab-content text-right">
                    <div class="tab-pane fade in active show" id="commity" role="tabpanel">

                        <div class="col green p-2 mt-3 d-flex flex-column justify-content-between">
                            <div class="maintitle d-flex justify-content-between">
                                <h5>
                                    <i class="fa fa-edit fa-2x mim-icons-primary"></i>
                                    تغيير كلمة المرور
                                </h5>
                            </div>
                            <div class="buttonss flex-row pt-3 mt-3">

                                <form action="{{ route('committees.profile.update', auth()->user()->user_id) }}" method="post">
                                    @csrf
                                    @method('put')
                                    <div class="form-group">
                                        <label>كلمة المرور الحالية</label>
                                        <input type="password" class="form-control" name="old_password" required>
                                    </div>

                                    <div class="form-group">
                                        <label>كلمة المرور الجديدة</label>
                                        <input type="password" class="form-control" name="password" required>
                                    </div>

                                    <div class="form-group">
                                        <label>تأكيد كلمة المرور الجديدة</label>
                                        <input type="password" class="form-control" name="password_confirmation" required>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">تغيير</button>
                                    </div>
                                </form>

                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>

@stop
