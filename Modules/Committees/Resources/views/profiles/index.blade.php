@extends('layouts.main.index')

@section('page')

    {{-- @include('committees::partial.category') --}}

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col categoryContainer">
                <div class="tab-content text-right">
                    <div class="tab-pane fade in active show" id="commity" role="tabpanel">

                        <div class="col green p-2 mt-3 d-flex flex-column justify-content-between">
                            <div class="maintitle d-flex justify-content-between">
                                <h5>
                                    <i class="fa fa-user fa-2x mim-icons-primary"></i>
                                    {{ auth()->user()->full_name }}
                                </h5>
                                <span class="p-2 text-center details">
                                    <a href="{{ route('committees.profile.edit', auth()->user()->user_id) }}">
                                        تحديث البيانات
                                    </a>
                                </span>
                            </div>
                            <div class="buttonss flex-row pt-3 mt-3">



                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>

@stop
