@extends('layouts.mailers.master')
@section('page')

<td align="left" class="esd-structure es-p20t es-p20r es-p20l" style=" background: #fafafa; border: 1px solid #ededed;">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td align="right" class="esd-stripe esd-checked es-p20t es-p20b es-p20r" style=" background: #0a64a8;color:#fff">
                    إضافة إلى لجنة
                </td>
            </tr>
            <tr>
                <td align="center" class="esd-container-frame" valign="top" width="600">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" class="esd-block-text es-p10b">
                                    <br>
                                           <h2 style=" color: #74bb1e;">سعادة العضو : 
                                            {{ @$commiteeMember->member->full_name }}
                                    </h2>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="esd-block-text es-p15t es-p10b">
                                    <p style="font-size: 18px;">تم إضافتك
                                            {{  @$commiteeMember->memberType->name }}

                                            إلى
                                            {{  @$commiteeMember->committee->committee_or_council == 1 ? 'لجنة' : 'مجلس' }} : <br/>
                                            {{  @$commiteeMember->committee->name }}
                                            <br></p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="esd-block-text">
                                    <br>
                                    <br>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</td>
@endsection