@extends('layouts.mailers.master')
@section('page')

<td align="left" class="esd-structure es-p20t es-p20r es-p20l" style=" background: #fafafa; border: 1px solid #ededed;">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td align="right" class="esd-stripe esd-checked es-p20t es-p20b es-p20r" style=" background: #0a64a8;color:#fff">
                    إضافة إجتماع جديدة </td>
            </tr>
            <tr>
                <td align="center" class="esd-container-frame" valign="top" width="600">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" class="esd-block-text es-p10b">
                                    <br>
                                           <h2 style=" color: #74bb1e;">سعادة العضو : 
                                            {{$committeeMember->user_name  }}
                                            
                                    </h2>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="esd-block-text es-p10b">
                                    <br>
                                    <h3 style=" color: #74bb1e;">   إضافة إجتماع جديدة ضمن اللجنة: 
                                        {{ $session->committee->name }}
                                            
                                    </h3>
                                    <h3 style=" color: #74bb1e;">الإجتماع  : 
                                        {{ $session->sessionTitle->name }}
                                            
                                    </h3>
                                </td>
                            </tr>
                           
                            <tr>
                                <td align="left" class="esd-block-text">
                                    <br>
                                    <br>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</td>
@endsection