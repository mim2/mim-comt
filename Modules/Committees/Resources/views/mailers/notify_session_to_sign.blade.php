
@extends('layouts.mailers.master')
@section('page')
<td align="left" class="esd-structure es-p20t es-p20r es-p20l" style=" background: #fafafa; border: 1px solid #ededed;">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td align="right" class="esd-stripe esd-checked es-p20t es-p20b es-p20r" style=" background: #0a64a8;color:#fff">
                   محضر جاهز للتوقيع
                </td>
            </tr>
            <tr>
                <td align="center" class="esd-container-frame" valign="top" width="600">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" class="esd-block-text es-p10b">
                                    <br>
                                           <h2 style=" color: #74bb1e;">
                                               سعادة:  
                                               {{ $recieverUser->full_name }}
                                    </h2>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="esd-block-text es-p15t es-p10b">
                                        <p>
                                                نفيدكم علماً بان محضر
                                                {{ $session->committee->committee_or_council == 2 ? 'مجلس' : 'لجنة'}}
                                                {{ $session->committee->name }}
                                                الإجتماع
                                                {{ $session->sessionTitle->name }}
                                                جاهز للتوقيع ، يرجى منكم التكرم بالمبادرة بالتوقيع على المحضر.
                                        </p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="esd-block-text">
                                    <br>
                                    <br>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</td>
@endsection
