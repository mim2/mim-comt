@extends('layouts.main.index')

@section('page')

    {{-- @include('committees::partial.category') --}}

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col categoryContainer">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link rounded-0 commity p-4 active" data-toggle="tab" href="#commity"
                           role="tab"><img alt="" class="ml-1" src="/assets/images/folder.svg"> اللجان</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link seission rounded-0 p-4" data-toggle="tab" href="#seission" role="tab"><img
                                    alt="" class="ml-1" src="/assets/images/business-meeting.svg"> المجالس</a>
                    </li>
                </ul>

                <hr>

                <div class="tab-content text-right">
                    <div class="tab-pane fade in active show" id="commity" role="tabpanel">

                        @foreach($committees as $committee)

                            @if($committee->committee_or_council == 1)
                                {{-- @foreach($committee->sessions as $session) --}}

                                <div class="col green p-2 mt-3 d-flex flex-column justify-content-between">
                                    <div class="maintitle d-flex justify-content-between">
                                        <h5>
                                            <i class="fa fa-folder-open-o mim-icons-primary"></i>
                                            {{ $committee->name }}
                                        </h5>
                                        <span class="p-2 text-center details">
                                            <a href="{{ route('member.committees.show', $committee->id) }}">
                                                <img alt="" class="ml-1" src="/assets/images/info.svg"> تفاصيل اللجنة
                                            </a>
                                        </span>
                                    </div>
                                    <div class="buttonss flex-row pt-3 mt-3">

                                        <a class=" p-2 border-0 rounded-0 cate mr-2">
                                            <i class="fa fa-user mim-icons-secondry"></i>
                                            عدد الاعضاء
                                            <span class="badge badge-light border-0 rounded-0 mim-badge">{{ $committee->members->count() }}</span>
                                        </a>
                                        <a class=" p-2 border-0 cate rounded-0 mr-2">
                                            <i class="fa fa-file mim-icons-secondry"></i>
                                            الملفات
                                            <span class="badge badge-light border-0 rounded-0 mim-badge">0</span>
                                        </a>
                                        <a class="p-2 border-0 cate mr-2">
                                            <i class="fa fa-users mim-icons-secondry"></i>
                                            الإجتماعات
                                            <span class="badge badge-light border-0 rounded-0 mim-badge">{{ $committee->sessions->count() }}</span>
                                        </a>

                                        {{--  @if ($committee->managingMemberIds()->contains(auth()->user()->user_id))  --}}

                                        <a class=" p-2 border-0 cate mr-2"
                                           href="{{ route('sessions.index', $committee) }}">
                                            <i class="fa fa-gear mim-icons-secondry"></i>
                                            إدارة الإجتماعات
                                        </a>

                                        {{--  @endif  --}}

                                    </div>
                                </div>
                                {{-- @endforeach --}}
                            @endif

                        @endforeach

                    </div>

                    <div class="tab-pane fade" id="seission" role="tabpanel">

                        @foreach($committees as $committee)
                            @if(($committee->committee_or_council == 2) ^ ($committee->committee_or_council == 3))


                                <div class="col green p-2 mt-3 d-flex flex-column justify-content-between">
                                    <div class="maintitle d-flex justify-content-between">
                                        <h5>
                                            <i class="fa fa-users mim-icons-primary"></i>
                                            {{ $committee->name }}
                                        </h5>
                                        <span class="p-2 text-center details">
                                    <a href="{{ route('member.committees.show', $committee->id) }}">
                                        <img alt="" class="ml-1" src="/assets/images/info.svg"> تفاصيل المجلس
                                    </a>
                                    
                                </span>
                                    </div>
                                    <div class="buttonss flex-row pt-3 mt-3">

                                        <button class="p-2 border-0 cate mr-2">
                                            <i class="fa fa-user mim-icons-secondry"></i>
                                             عدد الاعضاء
                                            <span class="badge badge-light border-0 rounded-0 mim-badge">{{ $committee->members->count() }}</span>
                                        </button>
                                        <button class="p-2 border-0 cate mr-2">
                                            <i class="fa fa-file mim-icons-secondry"></i>
                                             الملفات
                                            <span class="badge badge-light border-0 rounded-0 mim-badge">0</span></button>
                                        <button class="p-2 border-0 cate mr-2">
                                            <i class="fa fa-users mim-icons-secondry"></i>
                                             الإجتماعات
                                            <span class="badge badge-light border-0 rounded-0 mim-badge">{{ $committee->sessions->count() }}</span>
                                        </button>

                                        @if ($committee->managingMemberIds()->contains(auth()->user()->user_id))
                                            <a class="  p-2 border-0 cate mr-2"
                                               href="{{ route('sessions.index', $committee) }}">
                                                <i class="fa fa-gear mim-icons-secondry"></i>
                                                 إدارة الإجتماعات
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
