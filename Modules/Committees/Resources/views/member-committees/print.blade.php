{{-- @extends('layouts.main.index') --}}
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>اللجان</title>
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <link href="/vendor-assets/css/datatables.min.css" rel="stylesheet">
    <link href="/vendor-assets/css/datatables.bootstrap.css" rel="stylesheet">
    <link href="/vendor-assets/css/datatables.bootstrap-rtl.css" rel="stylesheet">
    <link href="/vendor-assets/css/ummalqura.calendars.picker.css" rel="stylesheet">
    <link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="images/favicon.ico" rel="shortcut icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            background: white;
            font: 14px "Tahoma";
        }
        * {
            box-sizing: flexbox;
            /* display: flexbox; */
            -moz-box-sizing: border-box;
        }
        .page {
            width: 70%;
            border: 5px black solid;

            height: 90%;
            padding: 20mm;
            margin: 10mm auto;
            border: 1px black solid;
            border-radius: 5px;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            page-break-after: before;
        }
        .subpage {
            padding: 1cm;
            border: 5px black solid;
            min-height: 325mm;
            /* outline: 2cm #FFEAEA solid; */
        }
        
        /* @page {
            size: A4;
            margin: 0;
        } */
        @media print {
            /* html, body {
                width: 210mm;
                height: 297mm;        
            } */
            @page  {
                border: 3px black solid;
            }
            .page {
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                height: 90%;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
        }
            /* {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            } */
            .headimg {
                max-height: 170px;
                width: 100%;
            }

            .welcome {
                text-align: right;
                width: 80%;
                font-size: 15px;
                font-display: Sans-serif;
            }
            .paragraph {
                text-align: center;
                /* width: 80%; */
                font-size: 15px;
                font-display: Sans-serif;
            }
            .wel {
                width: 80%;
                font-size: 15px;

            }
            .center {
            
                margin: 180px 0 0 -25px;
            }
            .left {
                text-align: center;
                width: 80%;
            }
        

    </style>
</head>
<body >
{{-- @section('page') --}}

     {{-- @include('committees::partial.category') --}}


    <div class="container mt-5" id="printableArea">
        <div class="row mt-2">
            <div class="col CommityDetails">
                <div class="row">
                    <div class="col CommityTitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <h5>
                                <img alt="" class="ml-1" src="/assets/images/{{ $committee->committee_or_council == 1 ? 'folder' : 'business-meeting' }}.svg">
                                {{ $committee->name }}
                            </h5>
                            <span class="p-2 text-center details"></span>

                            @if($committee->type_id == 1)
                            <p class="standing p-2"><span class="p-2 text-center details">دائمة</span></p>
                            @endif
                            @if($committee->type_id == 3)
                            <p class="temp p-2"><span class="p-2 text-center details">مؤقتة</span></p>
                            @endif
                            @if($committee->type_id == 4)
                            <p class="sub p-2"><span class="p-2 text-center details">فرعية</span></p>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <h4><i class="fa fa-briefcase ml-2"></i>المهام</h4>
						<p class="d-flex flex-column p-3 form-group">{!! $committee->description !!}</p>
						<h4><i class="fa fa-briefcase ml-2"></i> رقم القرار </h4>
                        <p class="d-flex flex-column p-3 form-group">{!! $committee->decision_name !!}</p>
                        <h4><i class="fa fa-user ml-2"></i>الاعضاء</h4>

                        @foreach($committee->members as $member)

                            <div class="d-flex text-center">
                                <div class="col p-3 userspro">
                                    <i class="fa fa-user ml-2"></i>
                                    {{ $member->member->user_name }}
                                </div>
                                <div class="col p-3 userspro">
                                    {{ $member->memberType->name }}
                                </div>
                            </div>

                        @endforeach

                        <h4 class="mt-4 titleIn">الإجتماعات</h4>

                        @foreach($committee->sessions as $session)

                            <div class="d-flex text-center">
                                <div class="col p-3 userspro">
                                    {{ $session->sessionTitle->name }}
                                </div>
                                <div class="col p-3 userspro">
                                    {{ $session->status->name }}
                                </div>
                               
                            </div>

                        @endforeach
						<!-- popup buttons -->
						
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>

    <div class="left">
            <button  onclick="printPage('printableArea')">طباعة</button>
        </div>
        <br>
        <br>
        <br>
        
    
    </body>
    <script>
        function printPage(printableArea) {
            var printContents = document.getElementById(printableArea).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    </html>

{{-- @stop --}}
