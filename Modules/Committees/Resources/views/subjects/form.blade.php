<div class="modal-body">
    <div class="row">
        <div class="form-group col border-0 rounded-0">
            {{utf8_encode('')}}

            {{ Form::label('title', 'الأجندة', ['class' => 'control-label']) }}
            {{ Form::text('title', old('title') , ['class' => 'form-control textsty']) }}
            <span class="text-danger">{{ $errors->first('title') }}</span>
        </div>
    </div>

    <div class="row">
        {{-- <div class="form-group col border-0 rounded-0">
             {{utf8_encode('')}}
            {{ Form::label('source', 'جهة الأجندة', ['class' => 'control-label']) }}
            {{ Form::text('source', old('source') , ['class' => 'form-control textsty']) }}
            <div class="form-check form-check-inline ">
                <input class="form-check-input" id="inlineCheckbox2" type="checkbox" name="inlineCheckbox2" value="option-subject-sent" @if(old('inlineCheckbox2')) checked @endif >
                <br>
                <label class="form-check-label text-dark p-1" for="inlineCheckbox2">إرسال بريد إلكتروني</label>
            </div>
            <span class="text-danger">{{ $errors->first('source') }}</span>
        </div> --}}
    </div>
</div>
<div class="modal-footer border-0 rounded-0">
    <button class="btn sendBtn border-0 rounded-0 ml-2 pl-4 pr-4" type="submit">تعديل</button>
</div>



