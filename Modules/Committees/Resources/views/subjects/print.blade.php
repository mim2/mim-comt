<!DOCTYPE html>
<html dir="rtl" lang="ar">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>اللجان</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        body {
            direction: rtl;
            text-align: right;
            font-size: 14pt;
        }
        table tr td {
            direction: rtl;
        }
        .agenda-table {
            margin-top: 20px;
            padding: 5px;
        }
        .agenda-table tr {
            margin-bottom: 10px;
        }
        .agenda-head-td {
            border:2px solid #fff;
            border-collapse: collapse;
            background-color: #D9D9D9;
            text-align: center;
            width: 10%;
            padding: 20px;
        }
        .agenda-text-td {
            border:2px solid #fff;
            border-collapse: collapse;
            background-color: #D9D9D9;
            padding: 20px;
            width: 90%;
        }
    </style>
</head>

<body>
<br/>
<br/>

<table style="padding: 5px;">
    <tbody>
    <tr style="text-align: center; background-color: #7d7d7d; color: #fff; font-size: 20px;">
        <td style="border:3.5px solid #fff; border-collapse: collapse; padding: 10px;" colspan="2">{{ $session->committee->name }}</td>
    </tr>

    <tr style="background-color: #D9D9D9;">
        <td style="border:2.5px solid #fff;border-collapse: collapse;padding: 10px;">
            الإجتماع : {{ $session->sessionTitle->name }}
        </td>
        <td style="border:2.5px solid #fff;border-collapse: collapse;padding: 10px;">
            التاريخ : {{ @$session->date }}
        </td>
    </tr>

    <tr style="background-color: #D9D9D9; ">
        <td style="border:2.5px solid #fff;border-collapse: collapse;padding: 10px;">
            المكان :
            @if (strpos(@$session->place,'http') !== false)
                عن بعد
            @else
                {{ @$session->place }}
            @endif
        </td>
        <td style="border:2.5px solid #fff;border-collapse: collapse;padding: 10px;">
            الوقت :
            من
            {{ Carbon\Carbon::createFromFormat('H:i:s',$session->start_time)->format('h:i' )}}
            {{ (Carbon\Carbon::createFromFormat('H:i:s',$session->start_time)->format('a' ) == 'am' ? 'ص' : 'م' )}}
            إلى

            {{ Carbon\Carbon::createFromFormat('H:i:s',$session->end_time)->format('h:i ')}}
            {{ (Carbon\Carbon::createFromFormat('H:i:s',$session->end_time)->format('a' ) == 'am' ? 'ص' : 'م' )}}
        </td>
    </tr>

    </tbody>
</table>


<br>
<div style="text-align: center; font-size: 20px;">
    الأجندة
</div>
<br>
<table class="agenda-table">
    <tbody>
    @foreach($session->subjects as $index => $subject)
        <tr>
            <td class="agenda-text-td">{{ $subject->title }}</td>
            <td class="agenda-head-td">{{ $index +1 }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</body>

</html>