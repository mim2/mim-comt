@extends('layouts.main.index')
    
@section('page')

    {{-- @include('committees::partial.category') --}}


    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            
                                <a class="temp p-2 d-table" href="{{ route('sessions.index', $committee) }}" target="_self" title=" إدارة الإجتماع">
                                        <i class="fa fa-gear"></i>
                                        <span>إدارة الإجتماعات</span>
                                    </a>
                                
                                    <p class="sissionPage p-2 acolor">
                                        <img alt="اللجان" class=" ml-1" src="/assets/images/subject.png"> الأجندة
                                    </p>
                                   
                                    <h5>
                                        <img alt="اللجان" class=" ml-1" src="/assets/images/{{ $session->committee->committee_or_council == 1 ? 'folder' : 'business-meeting' }}.svg">
                                        {{ $session->committee->committee_or_council == 1 ? 'لجنة' : 'مجلس' }}
                                        {{ $session->committee->name }} 
                                        
                                        - الإجتماع  
                                        
                                        {{ $session->sessionTitle->name }}
                                    </h5>
                                 
                        </div><!-- add new sission -->

                       
                    </div>
                </div>
               

                <div class="row 33">
                    <div class="col SissionDetails">
                        <div class="row">
                            
                            <div class="col maindetails p-2 mt-3">
                               

                                
                                {{ Form::open(['route' => ['subjects.store', $session], 'method' => 'post']) }}
                                
                                <add-subject
                                title="{{ old('title') }}"
                                ></add-subject>
                                
                                <br />
                                {{ Form::close() }}
                                <br />
                                @foreach($sessionSubjects as $subject)
                                    
                                    <div class="d-flex flex-row mb-3 justify-content-between text-center topDetails" >
                                        <div class="p-1 m-2 sissionplace" style="text-align: right !important" >
                                            <span class="spTitle" ></span> {{ $subject->title }}
                                        </div>
                                      
                                         {{ Form::open(['route' => ['subjects.destroy', $subject], 'method' => 'delete', 'id' => $subject]) }}
                                            <button type="submit" class="p-1 m-2 sissionremove "
                                                    onclick="return confirm('متأكد من الحذف؟')">
                                                <i class="fa fa-trash ml-2" style="cursor: pointer;"></i>
                                            </button>
                                        {{ Form::close() }}
                                        <a href="{{ route('subjects.edit', $subject) }}" class="p-1 m-2 sissionedit">
                                            <img alt="" class="ml-1 w-50" src="/assets/images/editBtn.png">
                                        </a>
                                        
                                    </div>

                                    
                                    

                                @endforeach
                                
                                
                                
                                
        
                                    <a style="background:green!important"  href="{{route('sessions.files.index', [$session->committee, $session]) }}"
                                        class="btn nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button">
                                        <i class="fa fa-arrow-left ml-2"></i> التالي
                                    </a>
                             
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        
    </div>

@stop

