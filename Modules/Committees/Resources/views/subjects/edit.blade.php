@extends('layouts.main.index')

@section('page')

    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                    <div class="maintitle d-flex justify-content-between">
                        <h5><i class="fa fa-edit"></i> تعديل موضوع</h5>
                    </div>
                </div>
                <div class="col maindetails p-2 mt-3">
                    {{ Form::model($subject, ['route' => ['subjects.update', $subject], 'class' => 'form-horizontal', 'method' => 'patch']) }}

                    @include('committees::subjects.form')

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>

@stop
