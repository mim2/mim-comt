@extends('layouts.main.index')

@section('page')

     {{-- @include('committees::partial.category') --}} 
    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                        
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                            
                        <div class="maintitle d-flex justify-content-between">
                            <a class="temp p-2 d-table" href="{{ route('sessions.index', $committee) }}" target="" title=" إدارة الإجتماعات">
                                <i class="fa fa-gear"></i> إدارة الإجتماعات
                            </a>
                           

                            <h5>
                                <img alt="اللجان" class=" ml-1" src="/assets/images/addusers.png"> إضافة مدعويين
                            </h5>
                            <h5>
                                <img alt="اللجان" class=" ml-1" src="/assets/images/{{ $session->committee->committee_or_council == 1 ? 'folder' : 'business-meeting' }}.svg">
                                {{ $session->committee->committee_or_council == 1 ? 'لجنة' : 'مجلس' }}
                                {{ $session->committee->name }} 
                                
                                - الإجتماع  
                                
                                {{ $session->sessionTitle->name }}
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <div class="d-flex flex-row text-center topDetails">
                            <div class="col-sm-3">
                                <select id="userCategory">
                                    <option>أختر من القائمة</option>
                                    <option value="insystem">المدعو موجود بالنظام</option>
                                    <option value="outsystem">المدعو خارج النظام</option>
                                </select>
                            </div>
                            <div class="col-sm-9" style="color:red">
                                <!--  -->
                                        
                                {{ Form::open(['route' => ['guests.storeExistsUser', $session], 'name' => 'form2', 'class' => 'form-horizontal', 'method' => '', 'novalidate' => true]) }}
                                    <add-user-to-session
                                            search-url="{{ route('session_users_to_add', $session->id) }}"
                                            subjects="{{ $session->subjects->pluck('title', 'id') }}"
                                            class="users"
                                            id="insystem"
                                            style="display:none">
                                    </add-user-to-session>
                                {{Form::close() }}

                                <div class="users" id="outsystem" style="display:none">
                                        {{ Form::open(['route' => ['guests.store', $session], 'name' => 'form1', 'class' => 'form-horizontal', 'method' => 'post', 'novalidate' => true]) }}
                                    <div class="container mainadd outadd">
                                        <div class="row">
                                            <div class="col p-0">
                                        {{--  <input type="text" id="hiddenElement" name="user_id" value= -1>  --}}
                                        <input type="hidden" name="note" value="guest">
                                        {{--  <input type="hidden" name="user_id">  --}}

                                                <div class="form-group col rounded-0">
                                                    <label for="name">اسم المدعو ثلاثي</label>
                                                    <input id="name" name="guests[0][name]" placeholder="اسم المدعو"
                                                           type="text" required>
                                                </div>
                                            </div>
                                            <div class="col p-0">
                                                <div class="form-group col rounded-0">
                                                    <label for="national_id">رقم الهوية</label>
                                                    <input id="national_id" name="guests[0][national_id]"
                                                           placeholder="1012154141" type="tel" required>
                                                </div>
                                            </div>
                                            <div class="col p-0">
                                                <div class="form-group col rounded-0">
                                                    <label for="email">البريد الإلكتروني</label>
                                                    <input id="email" name="guests[0][email]"
                                                           placeholder="gov.sa" type="email" required>
                                                </div>
                                            </div>
                                            <div class="col-12 p-0">
                                                <div class="form-group col rounded-0">
                                                    <label for="exampleFormControlSelect1">الأجندة</label>
                                                    @foreach($session->subjects as $key => $subject)
                                                    @if ($key == 0)
                                                    <div class="form-check">
                                                        <input class="form-check-input" name="guests[0][subjects][]"
                                                               type="checkbox" value="{{ $subject->id }}" checked>
                                                        <label class="form-check-label mr-4">{{ $subject->title }}</label>
                                                    </div>
                                                    @else
                                                        <div class="form-check">
                                                            <input class="form-check-input" name="guests[0][subjects][]"
                                                                   type="checkbox" value="{{ $subject->id }}">
                                                            <label class="form-check-label mr-4">{{ $subject->title }}</label>
                                                        </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col">
                                                
                                                <button class="btn sendBtn border-0 rounded-0 ml-2 pl-4 pr-4 closediv"
                                                        type="submit" onclick="increment()">إضافة
                                                </button>
                                                <button class="btn nonBtn border-0 rounded-0 ml-2 pl-4 pr-4 closediv"
                                                        type="reset">إلغاء
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                                
                            </div>
                        </div>

                        <div class="d-flex flex-row text-center topDetails usersin mt-5 mb-5">
                            <div class="col">
                                <h4>مدعويين من داخل النظام</h4>
                                <table class="table">
                                    <thead class="thcus">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th>اسم المدعو</th>
                                        <th>رقم الهوية</th>
                                        <th>البريد الالكتروني</th>
                                        <th>الأجندة</th>
                                        <th>الاجراء</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @isset($guests)
                                        @foreach($guests as $guest)
                                            @if($guest->note == 'member')
                                                @if ($guest->user_id )

                                                    <tr>
                                                        <th scope="col">{{ @++$j }}</th>
                                                <th> {{ $guest->name }}</th>
                                                <th>{{ $guest->national_id }}</th>
                                                <th>{{ $guest->email }}</th>
                                                <th>
                                                    <ul>
                                                        @foreach($guest->subjects as $subject)
                                                            <li>{{ $subject->title }}</li>
                                                        @endforeach
                                                    </ul>
                                                </th>
                                                        <th>
                                                            {{ Form::open(['route' => ['guests.destroy', $guest], 'method' => 'delete']) }}
                                                            <button type="submit" class="p-1 m-2 sissionremove "
                                                                    onclick="return confirm('متأكد من الحذف؟')">
                                                                <i class="fa fa-trash ml-2" style="cursor: pointer;"></i>
                                                            </button>
                                                            {{ Form::close() }}
                                                        </th>
                                                    </tr>

                                                @endif
                                            @endif
                                        @endforeach
                                    @endisset

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr>

                        <div class="d-flex flex-row text-center topDetails usersfrom mt-5 mb-5">
                            <div class="col">
                                <h4>مدعويين من خارج النظام</h4>
                                <table class="table">
                                    <thead class="thcus">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th>اسم المدعو</th>
                                        <th>رقم الهوية</th>
                                        <th>البريد الالكتروني</th>
                                        <th>الأجندة</th>
                                        <th>الاجراء</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @isset($guests)
                                        @foreach($guests as $guest)

                                            @if($guest->note == 'guest')
                                                @if ($guest->user_id)
                                                
                                                    <tr>
                                                        <th scope="col">{{ @++$j }}</th>
                                                        <th>{{ $guest->name }} </th>
                                                        <th>{{ $guest->national_id }}</th>
                                                        <th>{{ $guest->email }}</th>
                                                        <th>
                                                            <ul>
                                                                @foreach($guest->subjects as $subject)
                                                                    <li>{{ $subject->title }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </th>
                                                        <th>
                                                            {{ Form::open(['route' => ['guests.destroy', $guest], 'method' => 'delete']) }}
                                                            <button type="submit" class="p-1 m-2 sissionremove "
                                                                    onclick="return confirm('متأكد من الحذف؟')">
                                                                <i class="fa fa-trash ml-2" style="cursor: pointer;"></i>
                                                            </button>
                                                            {{ Form::close() }}
                                                        </th>
                                                    </tr>
                                                
                                                @endif
                                            @endif  

                                        @endforeach
                                    @endisset

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                           
                        <a  href="{{ route('sessions.files.index', [$session->committee, $session]) }}"
                            class="btn btn-danger text-white nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button">
                             <i class="fa fa-arrow-right ml-2"></i> رجوع
                         </a>
                        

                            <a style="background:green!important" href="{{ route('sessions.attendees.index', [$session->committee, $session]) }}"
                                class="btn nonBtn btnall border-0 rounded-0 ml-2 pl-4 pr-4 text-center" role="button">
                                 <i class="fa fa-arrow-left ml-2"></i> التالي
                             </a>
                      

                    </div>
                </div>
            </div>
        </div>
        <br>
        
        
    </div>
    

@endsection

@section('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            url = $('#next_action').attr('href');
                url2 =  $('#next_action').attr('data-information');
            $('input[type="checkbox"]').click(function(){
                if($(this).prop("checked") == true){
                        $('#next_action').attr('href', url2);
                }else if($(this).prop("checked") === false){
                    $('#next_action').attr('href', url);
                }
            });
        });
        $(function () {
            $("#userCategory").change(function () {
                $(".users").hide();
                $("#" + $(this).val()).show();
            });
         })
         $("#hiddenElement").val($("#hiddenElement").val()+1);
         $("#hiddenElement").attr('type','hidden');
    </script>
@endsection