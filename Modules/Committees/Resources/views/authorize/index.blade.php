@extends('layouts.main.index')



@section('page')


    <div class="container mt-5 minhe">

        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-center">
                            <h5>المفوضيين</h5>
                        </div>
                    </div>
                    <table class="table text-center mt-2">
                        <tr>
                            <th>المفوض</th>
                            <th>اللجنة</th>
                            <th>درجة التفويض</th>
                            <th>الإجتماع</th>
                            <th></th>
                        </tr>
                        @if($authorizeList->count() == 0)
                            <tr>
                                <td colspan="4"><p>لا يوجد لديك مفوضين</p></td>
                            </tr>
                        @endif
                        @foreach ($authorizeList as $item)
                            <tr>
                                <th>{{ @$item->user->user_name }}</th>
                                <th>{{ @$item->committee->name }}</th>
                                <th>{{ $item->degree == 'membership' ? 'عضوية': 'إدارة' }}</th>
                                <th>
                                    @if($item->type == 1)
                                        جميع الإجتماعات
                                    @else
                                        {{  @$item->session->sessionTitle->name }}</th>
                                @endif
                                <th>
                                    {{ Form::open(['route' => ['authorize.destroy', $item->id], 'method' => 'delete', 'id' => $item->id]) }}

                                    <button type="submit" onclick="return confirm('متأكد من الحذف؟')"
                                            class="btn btn-danger btn-sm">
                                        إلغاء التفويض
                                    </button>
                                    {{ Form::close() }}
                                </th>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>


        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-center">
                            <h5>المفوض عنهم</h5>
                        </div>
                    </div>
                    <table class="table text-center mt-2">
                        <tr>
                            <th>المفوض</th>
                            <th>اللجنة</th>
                            <th>درجة التفويض</th>
                            <th>الإجتماع</th>
                        </tr>
                        @if($authorizedByList->count() == 0)
                            <tr>
                                <td colspan="4"><p>لست مفوضاً لأحد</p></td>
                            </tr>
                        @endif
                        @foreach ($authorizedByList as $item)
                            <tr>
                                <th>{{ @$item->auther->user_name }}</th>
                                <th>{{ @$item->committee->name }}</th>
                                <th>{{ $item->degree == 'membership' ? 'عضوية': 'إدارة' }}</th>
                                <th>
                                    @if($item->type == 1)
                                        جميع الإجتماعات
                                    @else
                                        {{  @$item->session->sessionTitle->name }}</th>
                                @endif
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>


        {{ Form::open(['route' => ['authorize.store'], 'name' => 'form2', 'class' => 'form-horizontal', 'method' => '', 'novalidate' => true]) }}

        <add-authorize
                v-bind:users="{{ $users }}"
                items-index-url="{{ route('authorize.get-data')  }}"
        ></add-authorize>

        {{Form::close() }}

    </div>



@endsection