<?php

namespace Modules\Committees\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Modules\Committees\Emails\NotifySessionSigned;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\Subject;

class GuestSessionsController extends Controller
{
    /**
     * Display a listing member sessions.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sessions = Session::where('status_id', 3)->whereHas('attendees', function ($query) {
                return $query->where('user_id', Auth::user()->user_id);
            })->get();
        return view('committees::sessions.guest-sessions', compact('sessions'));
    }

    /**
     * Show the specified resource.
     *
     * @param Session $session
     * @return \Illuminate\Http\Response
     */
    public function show(Committee $committee, Session $session)
    {
        $committees = Committee::with([
            'members' => function ($query) {
                $query->select(['id', 'committee_id', 'member_type_id', 'user_id']);
                $query->with([
                    'member' => function ($query) {
                        $query->select(['user_id', 'user_name', 'user_idno']);
                    },
                    'memberType' => function ($query) {
                        $query->select(['id', 'name']);
                    }
                ]);
            },
            'type' => function ($query) {
                $query->select(['id', 'name']);
            },
            'sessions' => function ($query) use ($session) {
                // dd($session->id);
                $query->select(['id', 'user_id', 'session_title_id', 'status_id', 'committee_id'])->where('id', $session->id);
                $query->with([
                    'status',
                    'sessionTitle' => function ($query) {
                        return $query->select(['id', 'name']);
                    },
                    'attendees' => function ($query) {
                        $query->select(['id', 'session_id', 'attendance_status_id', 'user_id']);
                        $query->with([
                            'attendanceStatus' => function ($query) {
                                return $query->select(['id', 'name']);
                            },
                        ]);
                    },
                    'history' => function ($query) {
                        $query->select(['id', 'session_id']);
                    },
                    'subjects' => function ($query) {
                        $query->select(['id', 'session_id', 'title', 'description', 'recommendation', 'law_document', 'relevant_jurisdiction']);
                    },
                    'opinions' => function ($query) {
                        $query->select(['id', 'session_id', 'opinion']);
                    },
                    'guests' => function ($query) {
                        $query->select(['id', 'session_id', 'name']);
                    },
                ]);
            }
        ])->where('id', $committee->id)->select(['id', 'name', 'type_id', 'user_id', 'decision_name', 'updated_at'])->first();
        // dd($committees);
        $authUser = Subject::whereHas('guests', function ($query) {
            return $query->where('user_id', Auth::user()->user_id);
        })->get();
        // dd($authUser);
        return view('committees::sessions.guest-show', compact('session', 'authUser', 'committees'));
    }

    /**
     * @param Session $session
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function opinion(Session $session, Request $request)
    {
        // if (!$session->guests->pluck('user_id')->contains(Auth::user()->user_id)) {
        //     return redirect('unauthorized');
        // }

        if ($opinion = $session->opinions->where('user_id', Auth::user()->user_id)->first()) {
            $opinion->delete();
        }
        $session->opinions()->create([
            'user_id' => Auth::user()->user_id,
            'opinion' => $request->opinion,
            'has_reservation' => $request->has_reservation,
            'reservation_reason' => $request->reservation_reason,
        ]);
        if ($session->fresh()->isSigned()) {
            foreach ($session->committee->members->whereIn('member_type_id', [1, 2]) as $member) {
                Mail::to($member->member)
                    ->queue(new NotifySessionSigned($member->member, Auth::user(), $session));
            }
        }
        return redirect(route('guest.sessions.show', [$committee, $session]));
    }
}
