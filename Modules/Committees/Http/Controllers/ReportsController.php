<?php

namespace Modules\Committees\Http\Controllers;

use App\Http\Controllers\UserBaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Auth\Entities\User;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\CommitteeType;
use Modules\Committees\Entities\Report;

use Modules\Committees\Entities\SessionTitle;
use Modules\Core\Entities\Department;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;



class ReportsController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('committees::reports.index');
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function committeesReports(Request $request)
    {
        $types = CommitteeType::all()->pluck('name', 'id')->toArray();
        $committees = QueryBuilder::for(Committee::class)
            ->allowedFilters('name', 'category', 'type_id', 'privacy', 'is_stopped',
                AllowedFilter::scope('type'),
                AllowedFilter::scope('decision')
            )
            ->get();
        return view('committees::reports.committees', compact('committees', 'types'));
    }

    public function membersReports(Request $request)
    {
        $users = QueryBuilder::for(User::class)
            ->allowedFilters('user_name', 'full_name', 'user_mobile', 'user_idno', 'user_mail', 'gender', 'user_dept')
            ->get()
            ;
        $departments = Department::all();
        return view('committees::reports.members', compact('users', 'departments'));
    }

    public function sessionsReports(Request $request)
    {
        // start_time, end_time, date, session_title_id, committee_id, status_id, academic_year_id, place, completed, created_at
        $committees = Committee::all();
        $sessions = QueryBuilder::for(Session::class)

            ->allowedAppends('title')
            ->allowedFilters(['committee_id','title'])
            ->get();
        return view('committees::reports.sessions', compact('sessions', 'committees'));
    }



    public function filters(Request $request)
    {
        $parms = $request->except(['_token', 'report_type']);

        $parmsList = '';
        foreach ($parms as $key => $pl) {
            foreach ((array) $pl as $k => $p) {
                if($p != '') $parmsList .= $key . '[' . $k . ']=' . $p . '&';
            }
        }
        $route = '';
        switch ($request->input('report_type')) {
            case 'committees':
                $route = 'reports.committees.reports';
                break;
            case 'members':
                $route = 'reports.members.reports';
                break;
            default :
                $route = 'reports.sessions.reports';
        }
        return redirect(route($route, $parmsList));
    }




}   // class
