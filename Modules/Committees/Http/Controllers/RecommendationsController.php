<?php

namespace Modules\Committees\Http\Controllers;

use App\Classes\Sms\Mobily;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Modules\Auth\Entities\User;
use Modules\Committees\Emails\NotifySessionReadyToSign;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\Committee;
use App\Http\Controllers\UserBaseController;
use Modules\Committees\Entities\SessionStatusHistory;
use Modules\Committees\Entities\Subject;
use Modules\Committees\Entities\TimeFrame;
use Modules\Committees\Entities\Recommendation;
use Modules\Committees\Entities\Discussion;
use Modules\Committees\Entities\Task;
use Validator;
use App\Traits\ArabicToEnglish;
use Modules\Committees\Emails\NotifyForFinalRecomndationsSession;
use Modules\Committees\Emails\NotifyMemberAddingNewSession;
use Modules\Committees\Entities\CommitteeMember;

class RecommendationsController extends UserBaseController
{
    use ArabicToEnglish;
    /**
     * Show the form for editing the specified resource.
     * @param Session $session
     * @return Response
     */
    public function edit(Committee $committee, Session $session, Request $request)
    {
      
     
        if (!$session->committee->managingMemberIdsForRecomendations()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }

        //dd($session->committee->id);
        $member = New CommitteeMember;
        
        $member = $member->where('committee_id', $session->committee->id)->get();

        

        
       // return response()->json($member, 200);
        $time = TimeFrame::get();
        return view('committees::sessions.recommendations', compact('session', 'committee', 'member', 'time'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request, Session $session, Committee $committee)
    {
        if (!$session->committee->managingMemberIdsForRecomendations()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
       // dd($request->all());
      
        $validatedData = $request->validate([
            'recommendation' => 'required',
           
        ]);

        $recommendation = New Recommendation;
        $task = New Task;
        $discussion = New Discussion;
        
     
        $action = \DB::transaction(function () use ($request, $session, $recommendation, $discussion, $task) {
           
            $recommendation->recommendation =  $request->recommendation;
            $recommendation->commettees_id =  $session->committee_id;
            $recommendation->session_id =  $session->id;
          
            $recommendationSaved =  Recommendation::create($recommendation->toArray());

            
            $discussionList = json_decode($request->selected_items);
          
            $taskList = json_decode($request->selected_items);
            
            foreach ($discussionList as $item) {
               
                // insert discussions

                $discussion->discussions =  $item->discussions;
                $discussion->commettees_id =  $session->committee_id;
                $discussion->session_id =  $session->id;
                $discussionsSaved =  Discussion::create($discussion->toArray());

                //insert tasks 

                $task->parent_id =  $discussionsSaved->id;
                $task->time_id =  $item->timeSelected->id;
                $task->commettees_id =  $session->committee_id;
                $task->session_id =  $session->id;
                $task->user_id =  $item->assignedTo->user_id;
                $taskSaved =  Task::create($task->toArray());

            

            }

             
            
            


          //  $this->update($session, $request);


        });
        

       

        if(!is_null($action))
            return alert()->success('يرجى مراجعة الأخطاء!', 'خطاء')->autoclose(4500);
        else
        alert()->success('تم حفظ التوصيات  بنجاح !', 'حفظ ناجح')->autoclose(4500);
      //  return redirect(route('recommendations.edit', $session->id));
        return redirect(route('sessions.index', $session->committee));
    }

    /**
     * Update the specified resource in storage.
     * @param Session $session
     * @param  Request $request
     * @return Response
     */
    public  function update(Session $session, Request $request)
    {
       
       
        
        

        SessionStatusHistory::create([
            'session_id' => $session->id,
            'status_id' => 3,
            'user_id' => Auth::user()->user_id
        ]);

        $session->update([
            'id' => $session->id,
            'status_id' => 3,
            'user_id' => Auth::user()->user_id
        ]);
        // foreach ($session->committee->members as $member) {
        //     if($request->has('send_mail')) {
        //         Mail::to($member->member->user_mail)
        //             ->queue(new NotifyForFinalRecomndationsSession($member->member, $session));
        //     }
        //     // if($request->has('send_sms')) {
        //         $message =   ' ' .  'تم إعداد المحضر للتوقيع  في لجنة ' . $session->committee->name . ' ' . 'الإجتماع ' . $session->sessionTitle->name  ;
        //         Mobily::send($message, [$member->member->user_mobile]);
        //     // }
        // }
      return  $session->update($request->all());
    }

    /**
     * Re-notify user of a session.
     *
     * @param Session $session
     * @param User $user
     */
    public function notify(Session $session, User $user)
    {
        Mail::to(@$user->user_mail)
            ->queue(new NotifySessionReadyToSign(@$user, Auth::user(), $session));
        return 'تم ارسال رسالة تذكير للعضو ' . $user->user_name;
    }

    public function change(Session $session,  $id)
    {
        
        $recommendation = recommendation::where('id', $id)->first();
    
        $discussionList =  Task::with('discussion','time', 'assignee')->where('session_id', $session->id)->get();
      

        $member = New CommitteeMember;
        
        $member = $member->where('committee_id', $session->committee->id)->get();

        $time = TimeFrame::get();

        return view('committees::sessions.recommendations-edit', compact('session', 'member', 'time', 'recommendation', 'discussionList'));
     
    }

    public function deleteTask(Request $request)
    {
      

      $task = Task::where('id', $request->task_id)->first();
      
      $discussion = Discussion::where('id', $request->discussion_id)->first() ;

      $action = \DB::transaction(function () use ($request, $discussion, $task) {


        $task->delete();

        $discussion->delete();

        });


        if(!is_null($action))
            return alert()->success('يرجى مراجعة الأخطاء!', 'خطاء')->autoclose(4500);
    }
}
