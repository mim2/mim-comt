<?php

namespace Modules\Committees\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\Subject;
use App\Http\Controllers\UserBaseController;
use Modules\Committees\Http\Requests\CreateSubjectsRequest;
use App\Traits\ArabicToEnglish;
use Illuminate\Support\Facades\Mail;
use Modules\Committees\Emails\NotifySessionAddingSubjects;
use App\Classes\Pdf\Tcp as TcpPdf;
use App\Classes\Pdf\subject\Generate;


class SubjectsController extends UserBaseController
{
    use ArabicToEnglish;
    /**
     * Display a listing of the resource.
     *
     * @param Session $session
     * @return mixed
     */
    public function index(Committee $committee, Session $session, Request $request)
    {
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }

            $sessionSubjects = $session->subjects;            // dd($subjects);
            $data = ['committee', 'session', 'sessionSubjects'];
            return view('committees::subjects.index', compact($data));

    }
    /**
     * Show the form for creating a new resource.
     *
     * @param Session $session
     * @return mixed
     */
    public function create(Session $session)
    {
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        return view('committees::subjects.create', compact('session'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param Session $session
     * @param  Request $request
     * @return mixed
     */

    public function store(Committee $committee, Session $session,  CreateSubjectsRequest $request)
    {

        
        $title = self::arabicNumToEnglish($request->title);
        $source = self::arabicNumToEnglish($request->source);
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        if ($session->status_id == 1) {
            $session->status_id = 2;
            $session->save();
        }
        $subject = new Subject();
        $subject->session_id = $session->id;
        $subject->title = $title;
       // $subject->source = $source;
        $subject->user_id = Auth::user()->user_id;
        $subject->save();
        // foreach ($session->committee->members->whereIn('member_type_id', [1, 2, 3, 4, 5, 6, 7]) as $member) {
        //     Mail::to($member->member->user_mail)
        //         ->queue(new NotifySessionAddingSubjects($member->member, Auth::user(), $session));
        // }
        alert()->success(' تم إنشاء الأجندة بنجاح  !', 'حفظ ناجح');
        return redirect((route('subjects.index', [$session->committee, $session])));    
        
    }
    public function sendMail()
    {

        alert()->success(' تم الحفظ بنجاح ', 'حفظ ناجح');
        if ($committee = Committee::find(request()->committee) && $session = Session::find(request()->session)) {
            foreach ($session->committee->members->whereIn('member_type_id', [1, 2]) as $member) {
                dd($member->member->user_mail);

                Mail::to($member->member->user_mail)
                    ->queue(new NotifySessionAddingSubjects($member->member, Auth::user(), $session));
            }
            alert()->success(' تم الحفظ بنجاح ', 'حفظ ناجح');
            return redirect((route('subjects.index', [$session->committee, $session])));
        }
        alert()->success(' !', 'حفظ ناجح');
        return redirect((route('subjects.index', [$session->committee, $session])));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param Subject $subject
     * @return mixed
     */
    public function edit(Subject $subject, Committee $committee)
    {
        $session = $subject->session;
        return view('committees::subjects.edit', compact('subject', 'session', 'committee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Subject $subject
     * @return mixed
     */
    public function update(Committee $committee, Session $session, Subject $subject,  Request $request)
    {
        $title = self::arabicNumToEnglish($request->title);
        $source = self::arabicNumToEnglish($request->source);
        $subject->update([
            'title' => $subject->title = $title,
            //'source' => $subject->source = $source
        ]);
        alert()->success(' تم تعديل الأجندة بنجاح ', 'حفظ ناجح');
        return redirect(route('subjects.index', [$subject->session->committee->id, $subject->session->id]));
           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Subject $subject
     * @return mixed
     * @throws \Exception
     */
    public function destroy(Committee $committee, Session $session, Subject $subject)
    {

        $session = $subject->session;
        $subject->delete();
        alert()->success('تم حذف الأجندة', 'حذف ناجح');
        return redirect(route('subjects.index', [$session->committee, $session]));
    }

    public function print(Committee $committee, Session $session)
    {


        $html =  view('committees::subjects.print', compact('session'));
        Generate::generate($html, true, base_path() .'/public/uploads/subjects/pdf/'. auth()->user()->id .'.pdf');
        
    }
}
