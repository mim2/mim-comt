<?php

namespace Modules\Committees\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\SessionTitle;
use Modules\Committees\Entities\SessionAttendance;
use App\Http\Controllers\UserBaseController;


class SessionsManagerController extends UserBaseController
{
    /**
     * Display a listing of signed resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Committee $committee)
    {
      
        $sessions = Session::where('status_id', 3)
            ->whereHas('committee', function ($query) {
                return $query->whereHas('members', function ($q) {
                    return $q->whereIn('member_type_id', [1, 2, 3, 4, 5, 6, 7])->where('user_id', Auth::user()->user_id);
                });
            })->whereHas('attendees', function ($query) {
                return $query->where('user_id', Auth::user()->user_id)->where('attendance_status_id', 1);
            })->whereDoesntHave('opinions', function ($query) {
                    return $query->where('user_id', Auth::user()->user_id);
            })->orderBy('session_title_id', 'desc')->get();
        $sessionTitleId = Session::where('committee_id', $committee->id)->max('session_title_id') + 1;
        $sessionTitle = SessionTitle::find($sessionTitleId);
        $titles = [$sessionTitle->id => $sessionTitle->name];
        return view('committees::sessions.manage.index', compact('sessions', 'committee', 'titles', 'sessionTitleId'));
    }

    /**
     * Display a listing of signed resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function signed(Committee $committee)
    {
        $sessions = Session::where('status_id', 4)
            ->whereHas('committee', function ($query) {
                return $query->whereHas('members', function ($q) {
                    return $q->whereIn('member_type_id', [1, 2, 3, 4, 5, 6, 7]);
                });
            })->orderBy('session_title_id', 'desc')->get();
        $signed = true;
        return view('committees::sessions.manage.signed', compact('sessions', 'signed', 'committee'));
    }

    /**
     * Show the specified resource.
     *
     * @param Session $session
     * @return \Illuminate\Http\Response
     */
    public function show(Session $session)
    {
        $userOpinion = [];
        foreach ($session->opinions as $opinion) 
        {
            $userOpinion = ($opinion->opinion);
        }
        // dd($userOpinion);
        $attendees = $session->attendees;
        return view('committees::sessions.manage.show', compact('session', 'attendees', 'userOpinion'));
    }

    public function store(Request $request)
    {
        $attendees = new SessionAttendance();
        $attendees->user_id = auth()->user()->user_id;
        $attendees->attendance_status_id = 3;
        $attendees->session_id = $request->session_id;
        $attendees->apology_reason = strip_tags($request->apology_reason);
        $attendees->save();
        return redirect()->back()
            ->with('success', __('messages.saved_successfully'));
    }
}
