<?php

namespace Modules\Committees\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Modules\Auth\Entities\User;
use Modules\Committees\Emails\NotifySessionSigned;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\SessionStatusHistory;
use App\Http\Controllers\UserBaseController;
use Modules\Committees\Emails\NotifySessionAddingSubjects;
use App\Classes\Pdf\Tcp as TcpPdf;
use App\Classes\Pdf\Statment\Generate as Statment;

use PDF;



class MemberSessionsController extends UserBaseController
{
    /**
     * Display a listing member sessions.
     *
     * @return mixed
     */
    public function index(Committee $committee, Session $session, Request $request)
    {

        $committees = Committee::with('members', function ($query) {
            return $query->where('user_id', Auth::user()->user_id);
        })->get();
        // dd($committees);

        $sessions = $committee->sessions->whereIn('status_id', [1, 2, 3, 6])->all();
        return view('committees::sessions.member-sessions', compact('sessions', 'committees'));
    }

    /**
     * Display a listing of pending sessions.
     *
     * @return mixed
     */
    public function pending()
    {
        $sessions = Session::where('status_id', 3)
            ->whereHas('attendees', function ($query) {
                return $query->where('user_id', Auth::user()->user_id)->where('attendance_status_id', 1);
            })->whereDoesntHave('opinions', function ($query) {
                return $query->where('user_id', Auth::user()->user_id);
            })->paginate(3);
        return view('committees::sessions.member-sessions', compact('sessions'));
    }

    /**
     * Display a listing of approved sessions.
     *
     * @return mixed
     */
    public function approved()
    {

        $sessions = Session::whereIn('status_id', [5, 7, 9, 10])
            ->whereHas('attendees', function ($query) {
                return $query->where('user_id', Auth::user()->user_id)->where('attendance_status_id', 1);
            })->paginate(3);
        return view('committees::sessions.member-sessions-approved', compact('sessions'));
    }

    /**
     * Show the specified resource.
     *
     * @param Session $session
     * @return mixed
     */
    public function show(Committee $committee, Session $session)
    {
        return view('committees::sessions.show', compact('session', 'committee'));
    }

    /**
     * @param Session $session
     * @param Request $request
     * @return mixed
     */
    public function opinion(Session $session, Request $request)
    {
        // if (!$session->committee->members->pluck('user_id')->contains(Auth::user()->user_id)) {
        //     return redirect('unauthorized');
        // }

        $user = User::find($request->user_id);

        if ($opinion = $session->opinions->where('user_id', $request->user_id)->first()) {
            $opinion->delete();
        }

        $session->opinions()->create([
            'user_id' => $request->user_id,
            'opinion' => $request->opinion,
            'has_reservation' => $request->has_reservation,
            'reservation_reason' => $request->reservation_reason,
        ]);

        if ($session->fresh()->isSigned()) {
            $session->status_id = 4;
            $session->save();
            foreach ($session->committee->members->whereIn('member_type_id', [1]) as $member) {
                Mail::to($member->member->user_mail)
                    ->queue(new NotifySessionAddingSubjects($member->member, $user, $session));
            }
            SessionStatusHistory::create([
                'session_id' => $session->id,
                'status_id' => $session->status_id,
                'user_id' => $session->user_id
            ]);
            foreach ($session->committee->members->whereIn('member_type_id', [1, 2]) as $member) {
                Mail::to($member->member->user_mail)
                    ->queue(new NotifySessionSigned($member->member, $user, $session));
            }
        }

        return redirect(route('sessions.show', $session));
    }


    /**
     * Show the specified resource.
     *
     * @param Session $session
     * @return mixed
     */
    public function print(Committee $committee, Session $session)
    {

    if ($session->status_id == 3 || $session->status_id == 5 || $session->status_id == 9 || $session->status_id == 10 || $session->status_id == 4 ) {

        $html =  view('committees::sessions.print', compact('session'));

        $recomandation = view('committees::partial.recommendations_for_admin-print', compact('session'));

        $tasks = view('committees::partial.tasks-print', compact('session'));

        Statment::generate($committee->managingMemberIds()->contains(Auth::user()->user_id), $html, $recomandation, $tasks, true, base_path() .'/public/uploads/pdf/'. auth()->user()->id .'.pdf');
        } else {
            alert()->info('لم يكتمل المحضر  ', 'تنبيه !');
            return back();
        }
    }

    /**
     * Show the specified resource.
     *
     * @param Session $session
     * @return mixed
     */
    public function draft(Committee $committee, Session $session)
    {
        $html =   view('committees::sessions.show-draft', compact('session'));

        $recomandation = view('committees::partial.recommendations_for_admin-print', compact('session'));

        $tasks = view('committees::partial.tasks-print', compact('session'));

        Statment::generate(true, $html, $recomandation, $tasks, true, base_path() .'/public/uploads/pdf/'. auth()->user()->id .'.pdf');
    }
}
