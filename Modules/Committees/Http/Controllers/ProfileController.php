<?php

namespace Modules\Committees\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return mixed
     */
    public function index()
    {

        return view('committees::profiles.index');
    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return mixed
     */
    public function show($id)
    {
        return view('committees::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return mixed
     */
    public function edit($id)
    {
        if(auth()->user()->user_id != $id) return redirect(route('committees.profile.edit', auth()->user()->user_id));
        return view('committees::profiles.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        if(auth()->user()->user_id != $id) return abort(403);

        if(!Hash::check($request->input('old_password'), auth()->user()->password)) {
            alert()->error('  كلمة المرور الحالية غير صحيحة !', 'خطاء')->autoclose(4500);
            return back();
        }

        $user = auth()->user();
        $user->password = Hash::make($request->input('password'));
        $user->save();
        alert()->success('تم تحديث كلمة المرور بنجاح', 'تحديث ناجح')->autoclose(1000);
        return redirect(route('committees.profile.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return mixed
     */
    public function destroy($id)
    {
        //
    }
}
