<?php

namespace Modules\Committees\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Date\CarbonHijri;
use App\Http\Controllers\Controller;
use App\Traits\StoreFiles;
use App\Traits\StoreImage;
use DB;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\SessionFile;
use Modules\Committees\Entities\CommitteeMember;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\CommitteeFile;
use Validator;
use App\Rules\SessionFilesRule;
use App\Http\Controllers\UserBaseController;
use Modules\Auth\Entities\User;

class MemberCommitteesController extends Controller
{
    use StoreImage;
    use StoreFiles;

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index(Committee $committee, Session $session)
    {
        $committees = Committee::with('members', 'type', 'sessions')->whereHas('members', function ($query) {
            return $query->where('user_id', Auth::user()->user_id);
        })->get();

        if (Auth::user()->is_super_admin == 1)
        {
            $committees = Committee::all();
        }

        return view('committees::member-committees.index', compact('committees'));
    }


    /**
     * Show the specified resource.
     *
     * @param Committee $committee
     * @return Response
     */
    public function show(Committee $committee, Session $session)
    {

        $cfiles = $committee->cfiles;

        /**
         * Get Session Files
         */
        $sessions = $committee->sessions;
        $sessionFiles = [];
        foreach ($sessions as $session) {

            $sessionFiles[] = $session->files;
        }
        $data = ['committee', 'cfiles', 'session',  'sessions', 'sessionFiles'];

        return view('committees::member-committees.show', compact($data));
    }

    public function store(Committee $committee, Request $request)
    {
        $validatedData = $request->validate([
            'file_name' => ['required', new SessionFilesRule()],
            'file_path' => 'required|file|mimes:jpeg,png,jpg,svg,pdf,docx,xlsx,xls,pptx|max:20048',
        ]);
        $file = new CommitteeFile;
        $file->file_name = $request->file_name;

        $file->committee_id = $committee->id;
        $file->file_path = $this->verifyAndStoreFiles($request, 'file_path');
        $file->save();
        alert()->success('تم رفع الملف بنجاح !', 'حفظ ناجح')->autoclose(4500);
        return redirect()->back();
    }

    public function delete(Committee $committee, CommitteeFile $cfile)
    {
        $cfile->delete();

        return redirect()->back()
            ->with('success', __('messages.deleted_successfully'));
    }

    public function print(Committee $committee, Session $session)
    {
        $cfiles = $committee->cfiles;

        /**
         * Get Session Files
         */
        $sessions = $committee->sessions;
        $sessionFiles = [];
        foreach ($sessions as $session) {

            $sessionFiles[] = $session->files;
        }

        $data = ['committee', 'cfiles', 'session',  'sessions', 'sessionFiles'];

        return view('committees::member-committees.print', compact($data));
    }

     public function delegate()
    {
       $user = User::Where('user_dept', Auth::user()->user_dept)->get();
       dd($user);

    }
}
