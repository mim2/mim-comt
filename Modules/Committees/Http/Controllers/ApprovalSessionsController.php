<?php

namespace Modules\Committees\Http\Controllers;

use App\Http\Controllers\UserBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\SessionStatusHistory;
use Modules\Auth\Entities\User;
use Modules\Committees\Emails\NotifyAfterApprovedSession;
use Modules\Committees\Entities\Committee;
use Modules\Core\Entities\Group;
use Modules\Core\Entities\UserGroup;

class ApprovalSessionsController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Committee $committee, Session $session)
    {
       
        
        return view('committees::sessions.approval.index', compact('committee'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function pending(Request $request)
    {
        $consulSessions = Session::where('status_id', 3)->get();
        $query = Session::where('user_id', Auth::user()->user_id)->where('status_id', '=', 1);
        if ($request->type) {
            $query = $query->whereHas('committee', function ($q) use ($request) {
                return $q->where('type_id', $request->type);
            });
        }
        $sessions = $query->get();
        $committees = [];
        foreach ($sessions as $session) {
            if (!in_array($session->committee, $committees)) {
                $committees[] = $session->committee;
            }
        }
        $committees = ['0' => 'الكل'] + (new Collection($committees))->pluck('name', 'id')->toArray();
        if ($request->committee) {
            $query = $query->where('committee_id', $request->committee);
        }
        $sessions = $query->get();
        return view('committees::sessions.approval.pending', compact('sessions', 'committees'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function approved(Request $request)
    {
        $query = Session::whereIn('status_id', [5, 7, 9, 10]);
        if ($request->type) {
            $query = $query->whereHas('committee', function ($q) use ($request) {
                return $q->where('type_id', $request->type);
            });
        }
        $sessions = $query->get();
        $committees = [];
        foreach ($sessions as $session) {
            if (!in_array($session->committee, $committees)) {
                $committees[] = $session->committee;
            }
        }
        $committees = ['0' => 'الكل'] + (new Collection($committees))->pluck('name', 'id')->toArray();
        if ($request->committee) {
            $query = $query->where('committee_id', $request->committee);
        }
        $sessions = $query->get();
        return view('committees::sessions.approval.approved', compact('sessions', 'committees'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function toComplete(Request $request)
    {
       
            $sessions = Session::where('status_id', 4)->get();
        
       
        return view('committees::sessions.approval.complete', compact('sessions'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function years(Request $request)
    {
        $query = Session::where('status_id', '>=', 5);
        if ($request->type) {
            $query = $query->whereHas('committee', function ($q) use ($request) {
                return $q->where('type_id', $request->type);
            });
        }
        if ($request->year) {
            $query = $query->where('academic_year_id', $request->year);
        }
        $sessions = $query->get();
        $committees = [];
        foreach ($sessions as $session) {
            if (!in_array($session->committee, $committees)) {
                $committees[] = $session->committee;
            }
        }
        $years =  Session::where('status_id', '>=', 5)->groupBy('academic_year_id')->pluck('academic_year_id')->toArray();

        $committees = ['0' => 'الكل'] + (new Collection($committees))->pluck('name', 'id')->toArray();
        if ($request->committee) {
            $query = $query->where('committee_id', $request->committee);
        }
        $sessions = $query->get();
        return view('committees::sessions.approval.years', compact('sessions', 'committees', 'years'));
    }

    /**
     * Show the specified resource.
     *
     * @param Session $session
     * @return \Illuminate\Http\Response
     */
    public function show(Committee $committee, Session $session)
    {
        $history = SessionStatusHistory::where('session_id', $session->id)->get();
        $committees = Committee::with([
            'members' => function ($query) {
                $query->select(['id', 'committee_id', 'member_type_id', 'user_id']);
                $query->with([
                    'member' => function ($query) {
                        $query->select(['user_id', 'user_name', 'user_idno']);
                    },
                    'memberType' => function ($query) {
                        $query->select(['id', 'name']);
                    }
                ]);
            },
            'type' => function ($query) {
                $query->select(['id', 'name']);
            },
            'sessions' => function ($query) use ($session) {
                // dd($session->id);
                $query->select(['id', 'user_id', 'session_title_id', 'status_id', 'committee_id'])->where('id', $session->id);
                $query->with([
                    'status',
                    'sessionTitle' => function ($query) {
                        return $query->select(['id', 'name']);
                    },
                    'attendees' => function ($query) {
                        $query->select(['id', 'session_id', 'attendance_status_id', 'user_id']);
                        $query->with([
                            'attendanceStatus' => function ($query) {
                                return $query->select(['id', 'name']);
                            },
                        ]);
                    },
                    'history' => function ($query) {
                        $query->select(['id', 'session_id']);
                    },
                    'subjects' => function ($query) {
                        $query->select(['id', 'session_id', 'title', 'description', 'recommendation', 'law_document', 'relevant_jurisdiction']);
                    },
                    'opinions' => function ($query) {
                        $query->select(['id', 'session_id', 'opinion']);
                    },
                    'guests' => function ($query) {
                        $query->select(['id', 'session_id', 'name']);
                    },
                ]);
            }
        ])->where('id', $committee->id)->select(['id', 'name', 'type_id', 'user_id'])->first();
        return view('committees::sessions.approval.show', compact('session', 'history', 'committees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Session $session
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Session $session)
    {
        // dd($request->all());
        $session->update($request->all());
        SessionStatusHistory::create([
            'session_id' => $session->id,
            'status_id' => $session->status_id,
            'user_id' => Auth::user()->user_id
        ]);
        // foreach ($session->committee->members->whereIn('member_type_id', [1, 2]) as $member) {
        //     Mail::to($member->member->user_mail)
        //         ->queue(new NotifyAfterApprovedSession($member->member, Auth::user(), $session));
        // }
        return redirect(route('approval.sessions.index'))
            ->with('success', __('messages.saved_successfully'));
    }

    /**
     * Show the specified resource.
     *
     * @param Session $session
     * @return \Illuminate\Http\Response
     */
    public function print(Session $session)
    {
        return view('committees::sessions.print', compact('session', 'history'));
    }
}
