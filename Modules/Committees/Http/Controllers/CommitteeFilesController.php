<?php

namespace Modules\Committees\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\CommitteeFile;
use Validator;
use App\Rules\SessionFilesRule;
use App\Http\Controllers\UserBaseController;
use App\Traits\StoreFile;

class CommitteeFilesController extends UserBaseController
{

    use StoreFile;

    public function index(Committee $committee)

    {
        $files = $committee->files;

        $data = ['committee', 'files'];

        return view('committees::committees.files.index', compact($data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreCommitteeAttendees $request
     * @return Illuminate\Http\Response
     */
    public function store(Committee $committee, Request $request)
    {

        $validatedData = $request->validate([
            'file_name' => ['required', new SessionFilesRule()],
            'file_path' => 'required|file|mimes:jpeg,png,jpg,svg,pdf,xlsx,xls,pptx|max:20048',
        ]);
        $file = new CommitteeFile;
        $file->file_name = $request->file_name;
        $file->committee_id = $committee->id;
        $file->file_path = $this->verifyAndStoreFile($request, 'file_path', 'session_files');
        $file->save();
        return redirect(route('committees.files.index', [$committee]))
            ->with('success', __('messages.saved_successfully'));
    }


    public function delete(Committee $committee, CommitteeFile $file)
    {
        $file->delete();

        return redirect(route('committees.files.index', [$committee]))
            ->with('success', __('messages.deleted_successfully'));
    }
}
