<?php

namespace Modules\Committees\Http\Controllers;

use App\EmailLogger;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $messages = EmailLogger::where('reciever_user_id', Auth::user()->user_id)->paginate();
        return view('committees::messages.index', compact('messages'));
    }

    /**
     * Show the specified resource.
     * @param $id
     * @return Response
     */
    public function show($id)
    {
        $message = EmailLogger::where('reciever_user_id', Auth::user()->user_id)
            ->where('id', $id)
            ->firstOrFail();
        $message->update(['read' => true]);
        return view('committees::messages.show', compact('message'));
    }
}
