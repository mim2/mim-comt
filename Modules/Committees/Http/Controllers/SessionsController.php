<?php

namespace Modules\Committees\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\UserBaseController;
use Illuminate\Support\Facades\Mail;
use Modules\Committees\Emails\NotifySessionAddingSubjects;
use Modules\Committees\Entities\AcademicYear;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\SessionStatusHistory;
use Modules\Committees\Entities\SessionTitle;
use Modules\Committees\Http\Requests\CreateSessionRequest;
use App\Classes\Date\CarbonHijri;
use App\Classes\Sms\Mobily;
use DB;
use Modules\Committees\Classes\CommitteesModule;
use Modules\Committees\Emails\NotifyMemberDeletingSession;

class SessionsController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Committee $committee
     * @return \Illuminate\Http\Response
     */
    public function index(Committee $committee, Session $session)
    {
    
        $data = [
            'status',
            'sessionTitle',
            'committee.members',
            'attendees',
            'guests',
            'opinions',
            'subjects',
            'files',
            'recommendation'
        ];
        $sessions = $committee->sessionSequence()
            ->with($data)
            ->whereIn('status_id', [1, 2, 3, 4, 6, 8, 9, 10])->paginate(3);
        return view('committees::sessions.index', compact('sessions', 'committee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Committee $committee
     * @return \Illuminate\Http\Response
     */
    public function create(Committee $committee)
    {
        if (!$committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        $sessionTitleId = Session::where('committee_id', $committee->id)->max('session_title_id') + 1;
        $sessionTitle = SessionTitle::find($sessionTitleId);
        $titles = [$sessionTitle->id => $sessionTitle->name];
        @$hours = DB::table('session_hours')->get();
        return view('committees::sessions.create', compact('committee', 'titles', 'sessionTitleId', 'hours'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Committee $committee
     * @param  CreateSessionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Committee $committee, CreateSessionRequest $request, Session $session)
    {
        if (!$committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        if (!$committee->canAddSessions()) {
            return redirect(route('sessions.index', $committee->id));
        }
        CommitteesModule::SessionStore($committee, $request, $session);

    

        alert()->success('تم اضافة الإجتماع بنجاح!', 'اضافة الإجتماع')->autoclose(4500);
        return redirect(route('sessions.index', $committee->id));
    }

    /**
     * Show the specified resource.
     *
     * @param Session $session
     * @return \Illuminate\Http\Response
     */
    public function show(Session $session)
    {
        // if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
        //     return redirect('unauthorized');
        // }
        
        $attendees = $session->attendees;
        return view('committees::sessions.show', compact('session', 'attendees'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Session $session
     * @return \Illuminate\Http\Response
     */
    public function edit(Session $session)
    {
        $committee = $session->committee;
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        $sessionTitleId = $session->session_title_id;
        $sessionTitle = SessionTitle::find($session->session_title_id);
        $titles = [$sessionTitle->id => $sessionTitle->name];
        @$hours = DB::table('session_hours')->get();
        return view('committees::sessions.edit', compact('session', 'committee', 'titles', 'sessionTitleId', 'hours'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Session $session
     * @return \Illuminate\Http\Response
     */
    public function update(CreateSessionRequest $request, Session $session)
    {
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }

        CommitteesModule::SessionUpdate($request, $session);
        alert()->success('تم تعديل الإجتماع بنجاح!', 'تعديل الإجتماع')->autoclose(4500);
        return redirect(route('sessions.index', $session->committee->id));
    }

    /**
     * @param Session $session
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Session $session)
    {
        $committee = $session->committee;
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        // foreach ($session->committee->members as $member) {
        //     Mail::to($member->member->user_mail)
        //         ->queue(new NotifyMemberDeletingSession($member->member, $session));
        // }
        $session->delete();
        // foreach ($committee->members as $member) {
        //     Mail::to($member->member->user_mail)
        //         ->queue(new NotifyMemberAddingNewSession($member->member, $session));
        //         $message = $committee->name . ' ' .  'تم اضافة  الإجتماع' . $session->sessionTitle->name  . ' في لجنة  ';
        //         Mobily::send($message, [$member->member->user_mobile]);
        // }
           
     
        alert()->success('تم حذف الإجتماع بنجاح', 'حذف الإجتماع!')->autoclose(4500);
        return redirect(route('sessions.index', $committee->id));
    }

    public function elevate(Session $session)
    {
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }

        $session->status_id = 3;
        $session->save();
        // foreach ($session->committee->members->whereIn('member_type_id', [1, 2]) as $member) {
        //     Mail::to($member->member->user_mail)
        //         ->queue(new NotifySessionSigned($member->member, Auth::user(), $session));
        // }
        SessionStatusHistory::create([
            'session_id' => $session->id,
            'status_id' => $session->status_id,
            'user_id' => Auth::user()->user_id
        ]);
        alert()->success('تم ارسال المحضر لتوقيع الأعضاء  بنجاح!', 'ارسال المحضر للتوقيع ')->autoclose(4500);

        return back();
    }


    /**
     * @param Session $session
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function send(Session $session)
    {
        // if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
        //     return redirect('unauthorized');
        // }

        if ($session->status_id == 2) {
            foreach ($session->committee->members as $member) {
                Mail::to($member->member)
                    ->queue(new NotifySessionAddingSubjects($member->member, Auth::user(), $session));
            }
        }
        alert()->success('تم الارسال  بنجاح!', 'ارسال المحضر للاعتماد')->autoclose(4500);

        return redirect(route('sessions.index', $session->committee));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Committee $committee
     * @param Session $session
     * @return \Illuminate\Http\Response
     */
    public function attendees(Committee $committee, Session $session)
    {
        // if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
        //     return redirect('unauthorized');
        // }

        $attendees = $session->attendees;
        $members = $committee->members()->with('member', 'memberType')->get();
        $data = ['committee', 'session', 'attendees', 'members'];

        return view('committees::sessions.attendees', compact($data));
    }
}
