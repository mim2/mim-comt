<?php

namespace Modules\Committees\Http\Controllers;

use App\Http\Controllers\UserBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Committees\Entities\Subject;

class CommentsController extends UserBaseController
{
    /**
     * Store a newly created resource in storage.
     * @param Subject $subject
     * @param  Request $request
     * @return string
     */
    public function store(Subject $subject, Request $request)
    {
        $validatedData = $request->validate([
            'comment' => 'required',
           
        ]);

        $subject->comments()->create([
            'comment' => $request->comment,
            'user_id' => Auth::user()->user_id,
        ]);
        alert()->success('تم حفظ المرئيات بنجاح !', 'حفظ ناجح')->autoclose(4500);
        return back();
    }
}
