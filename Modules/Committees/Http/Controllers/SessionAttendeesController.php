<?php

namespace Modules\Committees\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\AttendanceStatus;
use Modules\Committees\Entities\SessionAttendance;
use Modules\Committees\Http\Requests\StoreCommitteeAttendees;
use Modules\Committees\Entities\Subject;

class SessionAttendeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Committee $committee
     * @param Session $session
     * @return Illuminate\Http\Response
     */
    public function index(Committee $committee, Session $session, Request $request)
    {
        
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }

        $subjects = Subject::where('session_id', $session->id)->count();
        
        if ($subjects == 0)
        {

            alert()->error('يتوجب عليك إضافة موضوع على الأقل!', 'خطاء')->autoclose(4500);
            return back();

        }
        $members = $committee->members()->with('member', 'memberType')->get();
        $attendanceStatuses = AttendanceStatus::pluck('name', 'id')->toArray();
        $guests = $session->guests()->with('subjects', 'user', 'user', 'user')->get();
        return view('committees::sessions.attendees.index', compact('committee', 'session', 'members', 'attendanceStatuses', 'guests'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreCommitteeAttendees $request
     * @return Illuminate\Http\Response
     */
    public function store(Committee $committee, Session $session, StoreCommitteeAttendees $request)
    {
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        DB::transaction(function () use ($request, $committee, $session) {
            $attendees = $session->attendees;
            foreach ($request->attendees as $memberId => $attendeeInput) {
                $attendee = $attendees->filter(function ($attendee) use ($attendeeInput, $session) {
                    return $attendee->user_id == $attendeeInput['user_id'];
                })->first();
                // dd($session->status_id);
                if ($session->status_id == 2) {
                    $session->status_id = 6;
                    $session->save();
                }
                if ($attendee  == null) {
                    
                    $attendee = new SessionAttendance;
                    $attendee->user_id = $attendeeInput['user_id'];
                    $attendee->session_id = $session->id;
                }

                $attendee->apology_reason = $attendeeInput['apology_reason'];
                $attendee->attendance_status_id = $attendeeInput['attendance_status_id'];
                $attendee->save();
            }
        });
        alert()->success('تم حفظ تحضير الاعضاء بنجاح !', 'حفظ ناجح')->autoclose(4500);

        return redirect()->route('recommendations.edit',  $session);
    }
}
