<?php

namespace Modules\Committees\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\UserBaseController;
use Illuminate\Http\Response;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\Authorize;
use Modules\Auth\Entities\User;
use Modules\Core\Entities\Group;
use Modules\Core\Entities\UserGroup;


use Auth;


class AuthorizeController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $users = User::where('user_dept', Auth::user()->user_dept)->where('user_id', '!=', Auth::user()->user_id)->get();
        $authorizeList = Authorize::where('from_user_id', Auth::user()->user_id)->get();
        $authorizedByList = Authorize::where('user_id', Auth::user()->user_id)->get();
        return view('committees::authorize.index', compact('users', 'authorizeList', 'authorizedByList'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('committees::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return  mixed
     */
    public function store(Request $request)
    {


        $request->validate([
            'user_id' => 'required|numeric',
            'type' => 'required',
            'degree' => 'required',
            'committee_id' => 'required|numeric',
            'session_id' => 'required_if:type,==,2',
        ]);

        $request->request->add(['from_user_id' => Auth::user()->user_id]);


        $authorizeGroupId = Group::where('key', 'authorize')->first()->id;

        $checkIfAuthrised = Authorize::where('user_id', Auth::user()->user_id)
            ->where('committee_id', $request->committee_id)
            ->count();


        if ($checkIfAuthrised > 0) {
            alert()->error('حيث انك مفوض من قبل شخصاً آخر في اللجنة لا تستطيع تفويض آخرين !', 'خطاء')->autoclose(4500);
            return back();
        }

        $check = Authorize::where('from_user_id', Auth::user()->user_id)
            ->where('user_id', $request->user_id)
            ->where('committee_id', $request->committee_id)
            ->where('session_id', $request->session_id)
            ->count();


        if ($check > 0) {
            alert()->error('  يوجد تفويض سابق !', 'خطاء')->autoclose(4500);
            return back();
        }


        $action = \DB::transaction(function () use ($request, $authorizeGroupId) {

            $saveAuth = Authorize::create($request->all());

            $group = new UserGroup;
            $group->es_core_group_id = $authorizeGroupId;
            $group->user_id = $request->user_id;
            $group->committee_id = $request->committee_id;
            $group->save();


        });

        alert()->success('تم التفويض بنجاح !', 'حفظ ناجح')->autoclose(4500);
        return back();

    }


    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $delete = Authorize::where('id', $id)->where('from_user_id', Auth::user()->user_id)->first();
        $delete->delete();

        alert()->success('تم إلغاء التفويض بنجاح !', 'إلغاء ناجح')->autoclose(4500);
        return back();

    }


    public function getData()
    {

        $authCommittees = Committee::with(['sessions', 'members' => function ($query) {
            return $query->where('user_id', Auth::user()->user_id);
        }])->whereHas('members', function ($query) {
            return $query->where('user_id', Auth::user()->user_id);
        })->where('is_stopped', 0)
        ->get();


        $jsonItems = [];

        foreach ($authCommittees as $item) {
            $memberType = $item->members->where('user_id', auth()->user()->user_id)->first();
            $jsonItems[] = [
                    'id' => $item->id,
                    'name' => $item->name,
                    'member_type' => $memberType->member_type_id
            ];
        }


        return response()->json($jsonItems);

    }

    public function getSession(Request $request, $id)
    {

        $sessions = Session::where('committee_id', $id)->get();

        return response()->json($sessions, 200);

    }
}
