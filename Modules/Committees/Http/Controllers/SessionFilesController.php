<?php

namespace Modules\Committees\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\SessionFile;
use Validator;
use App\Rules\SessionFilesRule;
use App\Http\Controllers\UserBaseController;
use App\Traits\StoreFile;
use Illuminate\Support\Facades\Auth;
class SessionFilesController extends UserBaseController
{

    use StoreFile;
    /**
     * Display a listing of the resource.
     *
     * @param Session $session
     * @return Illuminate\Http\Response
     */

    public function index(Committee $committee, Session $session, Request $request)

    {
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        if ($request->file == 'no') {
            return redirect(route('sessions.files.index', [$committee, $session]))
                ->withErrors('يجب رفع ملف واحد علي الاقل');
        } else {
            $files = $session->files;

            $data = ['committee', 'session', 'files'];

            return view('committees::sessions.files.index', compact($data));
        }
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreCommitteeAttendees $request
     * @return Illuminate\Http\Response
     */
    public function store(Committee $committee, Session $session, Request $request)
    {
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        $validatedData = $request->validate([
            'file_name' => ['required', new SessionFilesRule()],
            'file_path' => 'required|file|mimes:jpeg,png,jpg,svg,pdf,xlsx,xls,pptx|max:20048',
        ]);
        $file = new SessionFile;
        $file->file_name = $request->file_name;
        $file->session_id = $session->id;
        $file->file_path = $this->verifyAndStoreFile($request, 'file_path', 'session_files');
        $file->save();
        alert()->success('تم رفع الملف بنجاح !', 'حفظ ناجح')->autoclose(4500);
        return redirect(route('sessions.files.index', [$committee, $session]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreCommitteeAttendees $request
     * @return Illuminate\Http\Response
     */
    public function delete(Committee $committee, Session $session, SessionFile $file)
    {
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        $file->delete();
        alert()->success('تم حذف الملف', 'حذف ناجح')->autoclose(4500);
        return redirect(route('sessions.files.index', [$committee, $session]));
    }
}
