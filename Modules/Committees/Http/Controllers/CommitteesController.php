<?php

namespace Modules\Committees\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\UserBaseController;
use Illuminate\Support\Facades\Auth;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\CommitteeType;
use Modules\Committees\Entities\CommitteeMember;
use Modules\Committees\Entities\MemberType;
use Modules\Committees\Http\Requests\PostCommittee;
use Modules\Committees\Http\Requests\UpdateCommittee;
use App\Traits\StoreImage;
use App\Traits\ArabicToEnglish;
use Modules\Auth\Entities\User;
use Modules\Committees\Classes\CommitteesModule;
use Modules\Committees\Emails\NotifyMemberAddingtoCommittee;

class CommitteesController extends UserBaseController
{
    use StoreImage;
    use ArabicToEnglish;


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->is_super_admin == 1)
        {

            $committees = Committee::get();

        } else {
        $committees = Committee::with('type')->whereHas('members', function ($query) {
            return $query->where('user_id', Auth::user()->user_id);
        })->orWhere('user_id', Auth::user()->user_id)->get();
        }

        return view('committees::committees.index', compact('committees'));
    }
    /**
     * Display a listing of the committees Datatable.
     *
     * @return Json
     */
    public function getCommittees()
    {
        return datatables(Committee::with('type')->whereHas('members', function ($query) {
            return $query->where('user_id', Auth::user()->user_id);
        })->orWhere('user_id', Auth::user()->user_id))->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request, Committee $commitee)
    {
        $committee = new Committee;
        $parents = Committee::mainCommittees();
        $sessionsParents = Committee::mainSession();
        $types = CommitteeType::all()->pluck('name', 'id')->toArray();
        $search = $request->search;
        if ($search == '') {
            $employees = User::orderby('user_name', 'asc')->select('user_id', 'user_name', 'user_idno')->get();
        } else {
            $employees = User::orderby('user_name', 'asc')->select('user_id', 'user_name', 'user_idno', 'dep_arname')->where('user_name', 'like', '%' . $search . '%')->limit(5)->get();
        }
        @$committee_id = $request->route('committee')->id;
        $commiteeMembers = CommitteeMember::with('member')->where('committee_id', $committee_id)->get();
        $commiteeIdForMember = CommitteeMember::with('member')->where('committee_id', $committee_id)->first();
        $commiteeMembersForManager = CommitteeMember::where('committee_id', $committee_id)->where('member_type_id', 1)->get();
        $commiteeMembersForVice = CommitteeMember::where('committee_id', $committee_id)->where('member_type_id', 2)->get();
        $commiteeMembersForMember = CommitteeMember::where('committee_id', $committee_id)->whereIn('member_type_id', [4, 3, 5, 6, 7])->get();
        $commiteeMemberType = MemberType::whereNotIn('id', [1, 2])->where('is_disable', 0)->get();

        return view('committees::committees.create', compact('types', 'commiteeMembersForManager', 'commiteeMembersForMember', 'commiteeMembersForVice', 'employees', 'parents', 'committee', 'sessionsParents', 'commiteeMemberType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PostCommittee $request
     * @return Response
     */
    public function store(PostCommittee $request, Committee $committee)
    {
        CommitteesModule::CommitteeStore($request);
        CommitteesModule::CommitteeMemberStore($request);

        alert()->success('تم حفظ اللجنة بنجاح !', 'حفظ ناجح')->autoclose(4500);
        return redirect(route('committees.index'));
    }

    /**
     * Show the specified resource.
     *
     * @param Committee $committee
     * @return Response
     */
    public function show(Committee $committee)
    {
        // $committee->start_date = CarbonHijri::toHijriFromMiladi($committee->start_date, 'Y-m-d');
        // $committee->end_date   = CarbonHijri::toHijriFromMiladi($committee->end_date, 'Y-m-d');

        return view('committees::committees.show', compact('committee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Committee $committee
     * @return Response
     */
    public function edit(Committee $committee, Request $request)
    {
        $parents = Committee::mainCommittees();
        $sessionsParents = Committee::mainSession();
        $types = CommitteeType::get()->pluck('name', 'id')->toArray();

        $search = $request->search;
        if ($request->input('manager') == '') {
            $employees = User::select('user_id', 'user_name', 'user_idno')->latest()->get(['user_id', 'user_name', 'user_idno']);
        } elseif ($request->input('manager')) {
            $employees = User::orderby('user_name', 'asc')->select('user_id', 'user_name', 'user_idno', 'dep_arname')->where('user_name', 'like', '%' . $search . '%')->get(['user_id', 'user_name', 'user_idno', 'dep_arname']);
        }
        $committee_id = $request->route('committee')->id;
        $commiteeMembersForManager = CommitteeMember::where('committee_id', $committee_id)->where('member_type_id', 1)->get();
        $commiteeMembersForVice = CommitteeMember::where('committee_id', $committee_id)->where('member_type_id', 2)->get();
        $commiteeMembersForMember = CommitteeMember::where('committee_id', $committee_id)->whereIn('member_type_id', [4, 3, 5, 6, 7])->get();
        $commiteeMemberType = MemberType::whereIn('id', [4, 3, 5, 6, 7])->get();

        return view('committees::committees.edit', compact('committee', 'commiteeMemberType', 'types', 'parents', 'sessionsParents', 'employees', 'commiteeMembersForManager', 'commiteeMembersForVice', 'commiteeMembersForMember'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCommittee $request
     * @param Committee $committee
     * @return Response
     */
    public function update(UpdateCommittee $request, Committee $committee)
    {


        CommitteesModule::CommitteeUpdate($request, $committee);
        CommitteesModule::CommitteeMemberUpdateForManeger($request, $committee);
        CommitteesModule::CommitteeMemberUpdateForVice($request, $committee);
        CommitteesModule::CommitteeMemberUpdateForMembers($request, $committee);


        return redirect(route('committees.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Committee $committee
     * @return Response
     * @throws \Exception
     */
    public function destroy(Committee $committee)
    {
        $committee->delete();

        alert()->success('تم حذف اللجنة', 'حذف ناجح')->autoclose(4500);

        return redirect(route('committees.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Committee $committee
     * @return Response
     * @throws \Exception
     */
    public function memberDestroy(CommitteeMember $committeeMember)
    {

        $committeeMember->delete();
//        $deleteMember = CommitteeMember::find($committeeMember);
//        $deleteMember->delete();
        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
    }

    /**
     * Set the specified resource to be stopped.
     *
     * @param Request $request
     * @return Response
     */
    public function stop(Request $request)
    {
        
        $committee = Committee::findOrFail($request->id);
        
       
        $committee->update(['is_stopped' => 1, 'stop_date' => Carbon::now(), 'stop_reason' => $request->reason]);
        alert()->info('تم ايقاف اللجنة', 'ايقاف اللجنة!');
        return redirect(route('committees.index'));
    }

    /**
     * Set the specified resource to be resumed.
     *
     * @param Committee $committee
     * @return Response
     */
    public function resume(Committee $committee)
    {
        $committee->update(['is_stopped' => 0, 'resume_date' => Carbon::now(), 'stop_reason' => null]);
        alert()->success('تم تفعيل اللجنة بنجاح !', 'اعادة تفعيل اللجنة ')->autoclose(4500);
        return redirect(route('committees.index'));
    }

    /**
     * Get list of available users to add
     *
     * @param Integer $committeeId
     * @return Response
     */
    public function usersToAdd(Request $request, $committeeId, User $user)
    {
        $notIncludeUsersIds  = [];
        $queryInput = $request->input('query');
        if ($committeeId != "new") {
            $notIncludeUsersIds = Committee::find($committeeId)->members()->pluck('user_id')->toArray();
        }
        $users = User::searches($queryInput);
        if (!empty($notIncludeUsersIds)) {
            $users = $users->whereNotIn('user_id', $notIncludeUsersIds);
        }
        $users = isset($queryInput) ? $users->get() : [];
        // dd($users);
        return $users;
    }

    /**
     * Get list of available users to add
     *
     * @return Response
     */
    public function memberTypes(Request $request)
    {
        return MemberType::all();
    }
    /**
     * Get list of available users to add
     *
     * @return Response
     */
    public function members(Request $request, $id)
    {
        $committee = Committee::find($id);
        $members = $committee->members()->with('memberType', 'member')->get();

        return $members;
    }



    /**
     * Get list of users.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEmployees(Request $request)
    {
        $employees = User::find($request->user_id);

        return response()->json([
            'employees' => $employees,
        ], 201);
    }

    public function dataAjax(Request $request)
    {
        $data = [];
        if ($request->has('q')) {
            $search = $request->q;
            $data = User::select("user_id", "user_name", "user_idno")
                ->where('user_name', 'LIKE', "%$search%")
                ->where('user_idno', 'LIKE', "%$search%")
                ->get(["user_id", "user_name", "user_idno"]);
        }

        return response()->json($data);
    }
}
