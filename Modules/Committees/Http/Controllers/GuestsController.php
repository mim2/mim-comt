<?php

namespace Modules\Committees\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Auth\Entities\User;
use Modules\Committees\Entities\Guest;
use Modules\Committees\Entities\GuestSubject;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\Committee;
use App\Http\Controllers\UserBaseController;
use Illuminate\Support\Facades\Auth;



class GuestsController extends UserBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Session $session
     * @return Response
     */
    public function index(Committee $committee, Session $session, Request $request, Guest $guest)
    {
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        if ($request->guest == 'no') {
            return redirect(route('guests.index', [$committee, $session]))
                ->withErrors('يجب اضافة شخص واحد علي الاقل');
        } else {

            $guests = $session->guests()->with('subjects', 'user', 'user', 'user')->get();
            return view('committees::guests.index', compact('session', 'guests', 'committee'));
        }
    }

    /**
     * Get session guests.
     *
     * @param Session $session
     * @return array
     */
    public function guests(Session $session)
    {
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        return [
            'guests' => $session->guests()->with('user', 'user.department', 'user.job')->get(),
            'subjects' => $session->subjects()->with('guests', 'guests.user')->get(),
        ];
    }

    /**
     * Add users as guest to session.
     *
     * @param Request $request
     * @param Session $session
     * @return Response
     */
    public function usersToAdd(Request $request, Session $session)
    {
         
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        $queryInput = $request->input('query');
        $notIncludeUsersIds = array_filter(array_merge(
            $session->committee->members->pluck('user_id')->toArray(),
            $session->guests->pluck('user_id')->toArray()
        ));

        $users = User::searches($queryInput);
        if (!empty($notIncludeUsersIds)) {
            $users = $users->whereNotIn('user_id', $notIncludeUsersIds);
        }
        return isset($queryInput) ? $users->get() : [];
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @param Session $session
     * @return Response
     */
    public function store(Request $request, Session $session)
    {
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        Guest::createGuestRequest($request, $session);
        return back();
    }

    public function storeExistsUser(Request $request, Session $session)
    {
        if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        Guest::createExistUser($request, $session);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Guest $guest
     *
     * @return Response
     * @throws \Exception
     */
    public function destroy(Guest $guest, Session $session)
    {
        // if (!$session->committee->managingMemberIdsForSessions()->contains(Auth::user()->user_id)) {
        //     return redirect('unauthorized');
        // }
        GuestSubject::where('guest_id', $guest->id)->delete();
        $guest->delete();
        alert()->success('تم حذف العضو بنجاح', 'حذف العضو!')->autoclose(2500);

        return back();
    }
}
