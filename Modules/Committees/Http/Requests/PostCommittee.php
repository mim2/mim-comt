<?php

namespace Modules\Committees\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostCommittee extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>  'required',
            'committee_or_council' => 'required|in:1,2,3',
            'type_id' => 'required_if:committee_or_council,1|exists:committee_types,id',
            'parent_id' => 'required_if:type_id,4|exists:committees_version,ref_id',
            // 'description' => 'regex:/^[\pL\s\,\.]+$/u',
            'description' => 'required',
            'decision_name' => 'required',
            'category' => 'required',
            'decision_image' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $memberFound = false;

        $validator->after(function ($validator) use (&$managerFound, &$memberFound) {
            $manager = $this->input('manager');
            $vice = $this->input('vice');

            $selectedMembers = empty($this->members) ? [] : $this->members;

            foreach ($selectedMembers as $memberId => $selected_member) {

                if ($selected_member['member_type_id'] == 3) {
                    $memberFound = true;
                }
            }
            // if (($manager == 0 || $vice == 0)) {
            //     $validator->errors()->add('general_error', 'لابد من إختيار رئيس امين على الأقل');
            // }

            if ($memberFound = false) {
                $validator->errors()->add('general_error', 'لابد من إختيار عضو واحد على الأقل');
            }
        });
    }
}
