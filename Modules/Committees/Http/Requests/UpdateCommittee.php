<?php

namespace Modules\Committees\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class UpdateCommittee extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name' =>  'required',
            'committee_or_council' => 'required|in:1,2,3',
            'type_id' => 'required_if:committee_or_council,1|exists:committee_types,id',
            'parent_id' => 'required_if:type_id,4|exists:committees_version,ref_id',
            'description' => 'required',
            'decision_name' => 'required',
            'category' => 'required',
            'decision_image' => 'mimes:jpeg,png,gif,pdf,doc,docx',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        
        
    }
}
