<?php

namespace Modules\Committees\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\SessionFilesRule;
class StoreSessionFileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules( )
    {
        
        
        return [
            'file_name' => ['required', new SessionFilesRule()],
            'file_path' => 'required|file|mimes:jpeg,png,jpg,svg,pdf,docx,xlsx,xls,pptx|max:5048',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

     /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
    }
}
