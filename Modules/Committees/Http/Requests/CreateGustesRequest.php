<?php

namespace Modules\Committees\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\GustesRule;

class CreateGustesRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'name' => 'required|regex:/^[\pL\s]+$/u',
            'national_id' => 'required|digits:10',
            'email' => 'required|email',
        ];
    }
    

    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
