<?php

namespace Modules\Committees\Http\Requests;

use App\Classes\Date\CarbonHijri;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\SessionsRule;
use App\Rules\ValidHijriDate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Committees\Entities\Session;

class CreateSessionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        // @$committee_id = $request->route('committee')->id;
        // $last_date =  @Session::where('committee_id', $committee_id)->orderBy('date', 'DESC')->first()->date;
        $dayAgo = 6; 
        $hejiriNow = Carbon::parse((CarbonHijri::toHijriFromMiladi(Carbon::now()->subDays($dayAgo))));

        return [
            'session_title_id' => 'required|integer|min:1',
            'start_time' => 'required',
            'end_time' => 'required|after:start_time',
          //  'date' => ['required', 'after:' . $hejiriNow, new ValidHijriDate],
            'date' => ['required',  new ValidHijriDate],
            'place' => 'required'

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
