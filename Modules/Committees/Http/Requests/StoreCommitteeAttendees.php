<?php

namespace Modules\Committees\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCommitteeAttendees extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'attendees.*.user_id' => 'required',
            'attendees.*.attendance_status_id' => 'required',
            'attendees.*.apology_reason' => 'required_if:attendees.*.attendance_status_id,in:3,3',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        // $validator->sometimes('attendees.*.apology_reason', 'required', function ($input) {
        //     // dd($input->attendance_status_id);
        //     return $input->attendance_status_id == 3;
        // });
    }
}
