<?php
//'middleware' => 'web', 'prefix' => 'committees',
Route::group(['middleware' => 'prevent-back-history'], function () {
    Route::group(['middleware' => 'web', 'prefix' => 'committees', 'namespace' => 'Modules\Committees\Http\Controllers'], function () {
        Route::get('committees/resume/{committee}', 'CommitteesController@resume')->name('committees.resume');
        Route::post('committees/stop', 'CommitteesController@stop')->name('committees.stop');
        Route::get('committees/users-to-add/{committeeId}', 'CommitteesController@usersToAdd')->name('committee_users_to_add');
        Route::get('committees/member-types', 'CommitteesController@memberTypes');
        Route::get('committees/departments', 'CommitteesController@departments');
        Route::get('committees/jobs', 'CommitteesController@jobs');
        Route::get('committees/{id}/members', 'CommitteesController@members')->name('committee_members');
        Route::get('committees/create', 'CommitteesController@create')->name('committee.create');
        Route::get('committees/{committee}', 'CommitteesController@show')->name('committees.show');
        Route::get('select2-autocomplete-ajax', 'CommitteesController@dataAjax')->name('select2.ajax');


        Route::get('/employees/getEmployees/', 'CommitteesController@getEmployees')->name('employees.getEmployees');
        Route::get('api/v1/cities/search', 'CommitteesController@search')
            ->name('emp.search');
        Route::delete('/members/remove/{committeeMember}', 'CommitteesController@memberDestroy')->name('member.delete');


        /* sessions section */
        Route::get('committees/{committee}/sessions', 'SessionsController@index')->name('sessions.index');
        Route::get('committees/{committee}/sessions/create', 'SessionsController@create')->name('sessions.create');
        Route::get('committees/sessions/{session}', 'SessionsController@show')->name('sessions.show');
        Route::get('committees/sessions/{session}/edit', 'SessionsController@edit')->name('sessions.edit');
        Route::patch('committees/sessions/{session}', 'SessionsController@update')->name('sessions.update');
        Route::delete('committees/sessions/{session}', 'SessionsController@destroy')->name('sessions.destroy');

        Route::get('committees/{committee}/sessions/{session}/attendees', 'SessionAttendeesController@index')->name('sessions.attendees.index');
        Route::post('committees/{committee}/sessions/{session}/attendees', 'SessionAttendeesController@store')->name('sessions.attendees.store');
        Route::post('committees/{committee}/sessions', 'SessionsController@store')->name('sessions.store');

        Route::get('committees/{committee}/sessions/{session}/files', 'SessionFilesController@index')->name('sessions.files.index');





        Route::post('committees/{committee}/sessions/{session}/files', 'SessionFilesController@store')->name('sessions.files.store');
        Route::delete('committees/{committee}/sessions/{session}/files/{file}', 'SessionFilesController@delete')->name('sessions.files.delete');

        Route::get('sessions/{session}/send', 'SessionsController@send')->name('sessions.send');
        Route::get('sessions/{session}/elevate', 'SessionsController@elevate')->name('sessions.elevate');
        /* end of sessions section */

        /* session manager section */
        Route::get('sessions/manage/committees', 'SessionsManagerController@index')->name('sessions.manage.index');
        Route::get('sessions/manage/show/{session}', 'SessionsManagerController@show')->name('sessions.manage.show');
        Route::get('sessions/manage/signed', 'SessionsManagerController@signed')->name('sessions.manage.signed');
        Route::post('sessions/manage/store', 'SessionsManagerController@store')->name('sessions.manage.store');
        /* end of sessions manager section */

        /* sessions approval section */
        Route::get('approval/sessions/', 'ApprovalSessionsController@index')->name('approval.sessions.index');
        Route::get('approval/sessions/pending', 'ApprovalSessionsController@pending')->name('approval.sessions.pending');
        Route::get('approval/sessions/approved', 'ApprovalSessionsController@approved')->name('approval.sessions.approved');
        Route::get('approval/sessions/to-complete', 'ApprovalSessionsController@toComplete')->name('approval.sessions.complete');
        Route::get('approval/sessions/years', 'ApprovalSessionsController@years')->name('approval.sessions.years');
        Route::get('approval/{committee}/session/{session}', 'ApprovalSessionsController@show')->name('approval.sessions.show');
        Route::get('approval/sessions/{session}/print', 'ApprovalSessionsController@print')->name('approval.sessions.print');
        Route::patch('approval/sessions/{session}', 'ApprovalSessionsController@update')->name('approval.sessions.update');
        /* end of sessions approval section */


        /* subjects section */
        Route::get('committees/{committee}/sessions/{session}/subjects', 'SubjectsController@index')->name('subjects.index');
        Route::get('sessions/{session}/subjects/create', 'SubjectsController@create')->name('subjects.create');
        Route::post('sessions/{session}/subjects', 'SubjectsController@store')->name('subjects.store');
        Route::get('sessions/subjects/{subject}', 'SubjectsController@edit')->name('subjects.edit');
        Route::patch('sessions/subjects/{subject}', 'SubjectsController@update')->name('subjects.update');
        Route::delete('sessions/subjects/{subject}', 'SubjectsController@destroy')->name('subjects.destroy');



        Route::get('committees/{committee}/sessions/{session}/guests', 'GuestsController@index')->name('guests.index');
        Route::post('sessions/{session}', 'GuestsController@store')->name('guests.store');
        Route::post('sessions/storeExistsUser/{session}', 'GuestsController@storeExistsUser')->name('guests.storeExistsUser');
        Route::delete('sessions/{guest}', 'GuestsController@destroy')->name('guests.destroy');
        Route::get('sessions/{session}/guestlist', 'GuestsController@guests')->name('session_guests');
        Route::get('sessions/users-to-add/{session}', 'GuestsController@usersToAdd')->name('session_users_to_add');
        /* end of subjects section */

        Route::post('subjects/{subject}/comments', 'CommentsController@store')->name('comments.store');
        
        Route::get('sessions/{session}/notify/{user}', 'RecommendationsController@notify')->name('sessions.notify');

        Route::get('subjects/{session}/print', 'SubjectsController@print')->name('subjects.print');

        /* recommendations section */
        Route::get('sessions/{session}/recommendations', 'RecommendationsController@edit')->name('recommendations.edit');
        Route::post('sessions/{session}/recommendations', 'RecommendationsController@store')->name('recommendations.store');
        Route::patch('sessions/{session}/recommendations', 'RecommendationsController@update')->name('recommendations.update');

        Route::get('sessions/{session}/recommendations-edit/{id}', 'RecommendationsController@change')->name('recommendations.change');
        Route::post('recommendations-destroy-task', 'RecommendationsController@deleteTask')->name('recommendations.destroy.task');

        /* end of recommendations section */

        /* messages section */
        Route::get('messages', 'MessagesController@index')->name('messages.index');
        Route::get('messages/{id}', 'MessagesController@show')->name('messages.show');
        /* end of messages section */

        /* reports section */
        Route::get('reports', 'ReportsController@index')->name('reports.index');
        Route::get('reports/committees-reports', 'ReportsController@committeesReports')->name('reports.committees.reports');
        Route::get('reports/sessions-reports', 'ReportsController@sessionsReports')->name('reports.sessions.reports');
        Route::get('reports/members-reports', 'ReportsController@membersReports')->name('reports.members.reports');

        Route::post('reports/filters', 'ReportsController@filters')->name('committees.reports.filters');
        /* end of reports section */

        /** calender resource */


        // authorize routes

        Route::resource('authorize', 'AuthorizeController', ['only' => ['index', 'create', 'store', 'destroy']]);
        Route::get('authorize/get-data', 'AuthorizeController@getData')->name('authorize.get-data');
        Route::get('authorize/get-session/{id}', 'AuthorizeController@getSession')->name('authorize.get-session');



        /** end of the calendar resource */

        Route::get('member/committees/', 'MemberCommitteesController@index')->name('member.committees.index');
        Route::get('member/committees/{committee}/session/', 'MemberCommitteesController@show')->name('member.committees.show');
        Route::get('member/committees/{committee}/delegate/', 'MemberCommitteesController@delegate')->name('member.committees.delegate');
        Route::get('member/committees/{committee}/session/print', 'MemberCommitteesController@print')->name('member.committees.print');
        Route::resource('committees', 'CommitteesController');

        Route::post('member/committees/{committee}', 'MemberCommitteesController@store')->name('member.committees.store');
        Route::delete('member/committees/{committee}/cfiles/{cfile}', 'MemberCommitteesController@delete')->name('member.committee.cfiles.delete');

        Route::get('member/sessions', 'MemberSessionsController@index')->name('member.sessions.index');
        Route::get('member/sessions/pending', 'MemberSessionsController@pending')->name('member.sessions.pending');
        Route::get('member/sessions/approved', 'MemberSessionsController@approved')->name('member.sessions.approved');
        Route::get('member/committees/{committee}/session/{session}', 'MemberSessionsController@show')->name('member.sessions.show');
        Route::get('member/committees/{committee}/session/{session}/print', 'MemberSessionsController@print')->name('member.sessions.print');
        Route::get('member/committees/{committee}/session/{session}/draft', 'MemberSessionsController@draft')->name('member.sessions.show-draft');
        Route::post('sessions/{session}/opinion', 'MemberSessionsController@opinion')->name('sessions.opinion');

        Route::get('guest/sessions', 'GuestSessionsController@index')->name('guest.sessions.index');
        Route::get('committees/{committee}/guest/sessions/{session}', 'GuestSessionsController@show')->name('guest.sessions.show');
        Route::post('guest/{session}/opinion', 'GuestSessionsController@opinion')->name('guest.opinion');

        Route::resource('profile', 'ProfileController', ['as' => 'committees']);



    });
    Route::post('comt/sendMail', 'Modules\Committees\Http\Controllers\SubjectsController@sendMail')->middleware('web');
});
