<?php

namespace Modules\Committees\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Committees\Entities\CommitteeMember;

use Modules\Auth\Entities\User;

class NotifyMemberAddingtoCommittee extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The committe member instance.
     *
     * @var CommitteeMember
     */
    public $commiteeMember;

    /**
     * The committe member instance.
     *
     * @var User
     */
    public $recieverUser;

     /**
     * The committe member instance.
     *
     * @var User
     */
    public $senderUser;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CommitteeMember $commiteeMember, User $senderUser)
    {
        $this->commiteeMember = $commiteeMember;
        $this->recieverUser = $this->commiteeMember->member;
        $this->senderUser = $senderUser;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('committees::mailers.notify_adding_member_to_commitee')
                    ->subject('إضافة الي لجنة ' . @$this->commiteeMember->committee->name);
    }
}
