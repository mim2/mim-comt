<?php

namespace Modules\Committees\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Committees\Entities\CommitteeMember;

use Modules\Auth\Entities\User;
use Modules\Committees\Entities\Session;

class NotifyMemberDeletingSession extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The committe member instance.
     *
     * @var CommitteeMember
     */
    public $committeeMember;

    /**
     * The committe member instance.
     *
     * @var User
     */
    public $recieverUser;

    /**
     * The committe member instance.
     *
     * @var User
     */
    public $session;

    /**
     * The committe member instance.
     *
     * @var Session
     */
    // public $session;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $committeeMember, Session $session)
    {
        $this->committeeMember = $committeeMember;
        $this->session = $session;

        // dd($committeeMember);
        $this->recieverUser = $this->committeeMember->member;
        // $this->senderUser = $senderUser;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('committees::mailers.notify_deleting_session')
            ->subject('حدف الإجتماع '. @$this->committeeMember->committee->name);
    }
}
