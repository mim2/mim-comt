<?php

namespace Modules\Committees\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Modules\Auth\Entities\User;
use Modules\Committees\Entities\Session;

class NotifySessionAddingSubjects extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The committee member instance.
     *
     * @var User
     */
    public $recieverUser;

    /**
     * The committee member instance.
     *
     * @var Session
     */
    public $session;

    /**
     * The committee member instance.
     *
     * @var User
     */
    public $senderUser;

    /**
     * Create a new message instance.
     *
     * @param User $committeeMember
     * @param User $senderUser
     * @param Session $session
     */
    public function __construct(User $committeeMember, User $senderUser, Session $session)
    {
        $this->session = $session;
        $this->recieverUser = $committeeMember;
        $this->senderUser = $senderUser;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('committees::mailers.notify_adding_subjects_to_session')
            ->subject('تم إعداد جدول الأعمال');
    }
}
