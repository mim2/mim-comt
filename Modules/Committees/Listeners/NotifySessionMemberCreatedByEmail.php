<?php

namespace Modules\Committees\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Modules\Committees\Emails\NotifyMemberAddingtoCommittee;

use Auth;

class NotifySessionMemberCreatedByEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {

        $committeeMember = $event->committeeMember;
        $senderUser = Auth::user();

        // Mail::to($committeeMember->member->user_mail)
        //     ->queue(new NotifyMemberAddingtoCommittee($committeeMember, $senderUser));

        // dd($committeeMember->member)->send(new NotifyMemberAddingtoCommittee($committeeMember, $senderUser));
        // Mail::to($committeeMember->member)->send(new NotifyMemberAddingtoCommittee($committeeMember, $senderUser));
    }
}
