<?php

namespace Modules\Committees\Listeners;

use App\Notifications\AddToCommittee;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Modules\Committees\Emails\NotifyMemberAddingtoCommittee;

use Auth;
use Illuminate\Notifications\Notifiable;

class NotifyCommitteeMemberCreatedByEmail
{
    use Notifiable;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {

        $committeeMember = $event->committeeMember;
        $senderUser = Auth::user();
       
        // Mail::to($committeeMember->member->user_mail)
        //     ->queue(new NotifyMemberAddingtoCommittee(@$committeeMember, $senderUser));

        
    }
}
