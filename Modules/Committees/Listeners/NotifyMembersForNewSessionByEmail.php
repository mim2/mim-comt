<?php

namespace Modules\Committees\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Modules\Committees\Emails\NotifyMemberAddingNewSession;

class NotifyMembersForNewSessionByEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // dd($event);

        // $sessions = $event->sessions;
        // $committeeMembers = $event->committeeMembers;
        // $senderUsers = Auth::user();
        // dd($committeeMembers);

        // $senderUser->notify(new AddToCommittee());

        Mail::to($event->committeeMembers->member->user_mail)
            ->send(new NotifyMemberAddingNewSession());
    }
}
