<?php

namespace Modules\Committees\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Modules\Committees\Emails\NotifySessionSigned;

use Auth;
class NotifySessionSignedByEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        
        $committeeMember = $event->session;
        $senderUser = Auth::user();

        Mail::to($session->committee->members->user_mail)
            ->queue(new NotifySessionSigned($session, $senderUser));
        
        // dd($committeeMember->member)->send(new NotifyMemberAddingtoCommittee($committeeMember, $senderUser));
        // Mail::to($committeeMember->member)->send(new NotifyMemberAddingtoCommittee($committeeMember, $senderUser));
    }
}
