<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
// use Modules\Committees\Entities\Committee;
// use Modules\Committees\Entities\CommitteeType;

use View;



use Route;

class UserBaseController extends Controller
{
    use DispatchesJobs, ValidatesRequests;

    protected function formatValidationErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    protected $committes;
    public $isApiCall = false;

    /**
     * will include array of controllers that generic middleware not run
     * @var array
     */
    private $exceptAuthorizeControllers = [
        'Modules\Auth\Http\Controllers\AuthController@showLoginForm',
        'Modules\Auth\Http\Controllers\AuthController@login',
        'App\Http\Controllers\Index\HomeController@index',
        'Modules\Committees\Http\Controllers\AuthorizeController@index',
        'Modules\Committees\Http\Controllers\AuthorizeController@store',
        'Modules\Committees\Http\Controllers\AuthorizeController@destroy',
        'Modules\Committees\Http\Controllers\AuthorizeController@getData',
        'Modules\Committees\Http\Controllers\AuthorizeController@getSession',
        'Modules\Committees\Http\Controllers\SubjectsController@print',
        'Modules\Committees\Http\Controllers\ApprovalSessionsController@index',
        'Modules\Committees\Http\Controllers\MemberSessionsController@opinion',
        'Modules\Committees\Http\Controllers\MemberSessionsController@draft',
        'Modules\Committees\Http\Controllers\RecommendationsController@deleteTask',
        'Modules\Committees\Http\Controllers\RecommendationsController@update',
        'Modules\Committees\Http\Controllers\RecommendationsController@change',
        'Modules\Committees\Http\Controllers\CommentsController@store',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */


    

    /**
     * Check if current request Controller exist in except array
     * @return true if exist
     * @return false if not exist
     */
    public function isInExceptAuthorizeControllers()
    {
        if (in_array(Route::currentRouteAction(), $this->exceptAuthorizeControllers)) {
            return true;
        }
        return true;
    }

    public function __construct()
    {
        

        $this->middleware('auth-user');

        if (!$this->isInExceptAuthorizeControllers()) {
            $this->middleware('authorize');
        }


    }
}
