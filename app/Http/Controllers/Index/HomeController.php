<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\UserBaseController;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Committees\Entities\Committee;
use Modules\Committees\Entities\Session;
use Modules\Core\Entities\Setting;
use App\Classes\Sms\Mobily;
use Calendar;

class HomeController extends UserBaseController
{

    /**
     * Return view index.
     *
     * @return Response
     */
    public function index(Committee $committee, Session $session)
    {
      
        $groupManagers = DB::table('es_core_users_groups')->where('user_id', Auth::user()->user_id)->select('user_id')->first();


        if (Auth::user()->is_super_admin == 1)
        {

            $committees = Committee::get();

        } else {

        $committees = Committee::with(['sessions', 'members' => function ($query) {
            return $query->where('user_id', Auth::user()->user_id);
        }])->whereHas('members', function ($query) {
            return $query->where('user_id', Auth::user()->user_id);
        })->orwhereHas('authorize', function ($query) {
            return $query->where('user_id', Auth::user()->user_id);
        })
        ->where('is_stopped', 0)->take(6)
        ->get();

    }
         
       
       // Calender
        $sessions = Session::with(['committee', 'sessionTitle', 'attendees'])->where('status_id', 3)
        ->whereHas('attendees', function ($query) {
            return $query->where('user_id', Auth::user()->user_id)->where('attendance_status_id', 1);
        })->whereDoesntHave('opinions', function ($query) {
                return $query->where('user_id', Auth::user()->user_id);
            })->take(5)->get();


        $events = [];
        $data = Session::with(['sessionTitle' => function ($query) {
            $query->select(['id', 'name']);
        }, 'committee' => function ($query) {
            $query->select(['id', 'name']);
        }])->where('user_id', Auth::user()->user_id)->latest()->get(['id', 'date', 'committee_id', 'session_title_id', 'start_time', 'end_time']);
        if ($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = [
                    'title' => [
                        'الإجتماع' . ' - '  .  $value->sessionTitle->name . ' '  ,
                         
                    ],
                    'title' => [
                        'الإجتماع' . ' - '  .  $value->sessionTitle->name . ' '  ,
                        $value->committee->name . ' - ' . 'اللجنة' ,
                    ],
                    'description' => $value->committee->name . ' - ' . 'اللجنة' ,
                    'date' => $value->date . ' ' . $value->start_time,
                    'color' => '#f05050',
                    'url' => route('member.sessions.show', [$value->committee, $value->id]),
                ];
            }
        }

        $setting = Setting::first();
       
        return view('index.index', compact('committees', 'sessions',  'groupManagers', 'committee', 'events', 'setting'));
    }
}
