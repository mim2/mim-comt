<?php

namespace App\Traits;

use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

trait StoreFile
{
    /**
     * Does very basic file validity checking and stores it. Redirects back if somethings wrong.
     * @Notice: This is not an alternative to the model validation for this field.
     *
     * @param Request $request
     * @param string $fieldName
     * @param string $directory
     * @return $this|false|string
     */
    public function verifyAndStoreFile(Request $request, $fieldName = 'file', $directory = '')
    {
        if ($request->hasFile($fieldName)) {
            $file = $request->file($fieldName);

            if (!$file->isValid()) {
                flash('Invalid File!')->error()->important();
                return redirect()->back()->withInput();
            }

            $path     = empty($directory) ? 'uploads' : 'uploads/' . $directory;
            $fileName = time() . Str::random(20) . '.' . $file->extension();

            $file->move(public_path($path), $fileName);

            $path = $path .'/'. $fileName;
            return $path;
        }

    }

    public function SessionsHome(Committee $committee)
    {
        // SessionsHome($committee);
        // dd($committee);
        if (!$committee->managingMemberIds()->contains(Auth::user()->user_id)) {
            return redirect('unauthorized');
        }
        $sessions = $committee->sessions->whereIn('status_id' , [1, 2, 3, 6])->all();
        $sessionTitleId = Session::where('committee_id', $committee->id)->max('session_title_id') + 1;
        $sessionTitle = SessionTitle::find($sessionTitleId);
        $titles = [$sessionTitle->id => $sessionTitle->name];
        
        return view('committees::sessions.index', compact('sessions', 'committee', 'titles', 'sessionTitleId'));
    }
}
