<?php

namespace App\Traits;

use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

trait StoreFiles
{
    /**
     * Does very basic image validity checking and stores it. Redirects back if somethings wrong.
     * @Notice: This is not an alternative to the model validation for this field.
     *
     * @param Request $request
     * @param string $fieldName
     * @param string $directory
     * @return $this|false|string
     */
    public function verifyAndStoreFiles(Request $request, $fieldName = 'files', $directory = '')
    {
        if ($request->hasFile($fieldName)) {
            $file = $request->file($fieldName);
            if (!$file->isValid()) {
                flash('Invalid File!')->error()->important();
                return redirect()->back()->withInput();
            }
            if (!is_dir(public_path('uploads'))) {
                mkdir(public_path('uploads'), 0777);
            }
            $path     = empty($directory) ? 'uploads' : 'uploads/' . $directory;
            if (!is_dir(public_path($path))) {
                mkdir(public_path($path), 0777);
            }
            $fileName = Str::random(20) . '.' . $file->extension();
            $file->move(public_path($path), $fileName);

            $path = $path .'/'. $fileName;
            return $path;
        }
    }
}
