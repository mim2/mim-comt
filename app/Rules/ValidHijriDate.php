<?php

namespace App\Rules;

use App\Classes\Date\CarbonHijri;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;
use Modules\Committees\Entities\Session;

class ValidHijriDate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
       
        //$dayAgo = 5; 
       // $hejiriNow = Carbon::parse((CarbonHijri::toHijriFromMiladi(Carbon::now()->subDays($dayAgo))));


        $datereq = str_replace('/', '-', $value);
        $datereq = Carbon::parse($datereq); 

        // if ($datereq >= $hejiriNow) {
        //     return true;
        // }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ' هذا التاريخ غير صالح';
    }
}
