<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SubjectsRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // $value = $_POST["title"];
        // $value = $_POST["source"];
        $value = $_POST["description"];
        return preg_match('regex:/^\[p{Arabic}\s\p{N}][\u0660-\u0669]+$/u', $value);
        return true;
       
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
       return "تاكد من ادخال القيم بشكل صحيح"; 
    }
}
