<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Committees\Entities\Opinion;
use Modules\Committees\Entities\Session;
use Carbon\Carbon;
use DB;

class ThursdaySigniture extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ThuSign';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto sign for session when today is thursday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dayAgo = 3; 
        $dayToCheck = Carbon::now()->subDays($dayAgo);
        
        Opinion::signitureSend($dayToCheck);
    }
}
