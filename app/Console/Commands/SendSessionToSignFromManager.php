<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Committees\Entities\Session;
use Modules\Committees\Entities\SessionStatusHistory;
use Illuminate\Support\Facades\Auth;

use DB;
class SendSessionToSignFromManager extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:SendSessionToSignFromManager';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sessions = Session::where('status_id', 3)
            ->whereHas('committee', function ($query) {
                return $query->whereHas('members', function ($q) {
                    return $q->whereIn('member_type_id', [1, 2]);
                });
            })->get();

       foreach($sessions as $session){
        if ($session->isSigned() && isset($signed) and $signed == 1){
          $session->status_id = 9;
          $session->save();
          SessionStatusHistory::create([
              'session_id' => $session->id,
              'status_id' => $session->status_id,
              'user_id' => $session->user_id
          ]);
        }
       }
        // dd($sessions);
    }
}
