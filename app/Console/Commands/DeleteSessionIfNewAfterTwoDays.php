<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Committees\Entities\Session;

class DeleteSessionIfNewAfterTwoDays extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:DeleteSession';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete Session If it`s new after 28 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         Session::deleteSessionIfNew();
    }
}
