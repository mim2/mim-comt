<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Modules\Committees\Entities\Opinion;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->call('\Modules\Committees\Http\Controllers\SessionsController@elevate')->everyMinute()->timezone('Asia/Riyadh');
        $schedule->command('command:ThuSign')
                 ->everyFifteenMinutes()->timezone('Asia/Riyadh');

        $schedule->command('command:ThuSign')
                 ->everyFifteenMinutes()->timezone('Asia/Riyadh');

        $schedule->command('command:DeleteSession')
                 ->everyFifteenMinutes()->timezone('Asia/Riyadh');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
