<?php


namespace App\Classes;


class Utils
{

    public static function privacyList() {
        return [
            'highSecret' => 'سري للغاية',
            'internalSecret' => 'سري داخلي',
            'externalSecret' => 'سري خارجي',
            'normal' => 'عادي',
        ];
    }
}