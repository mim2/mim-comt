<?php
namespace App\Classes\Pdf\Statment;

use \TCPDF;
use Intervention\Image\Facades\Image;
use Johntaa\Arabic\I18N_Arabic;

class BackgroundWithWaterMark extends TCPDF {

    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image

        $img = Image::make(public_path('assets/images/statment.jpg'));


        $arabic = new I18N_Arabic('Glyphs');
        $text = $arabic->utf8Glyphs(auth()->user()->user_name);

        $x = 1000;
        for($i = 0; $i < 3; $i ++) {
         
            $img->text($text, 825, $x, function($font) {
              $font->file(public_path('assets/fonts/GEDinarTwo-Medium.ttf'));
                $font->size(40);
                $font->color('#b9b9b9');
                $font->align('center');
                $font->valign('bottom');
                $font->angle(30);
            }); 

            $x += 400;
        }
       


        $path = public_path('uploads/watermark/');


        $img->save($path . auth()->user()->user_id .'.png');


        $img->dirname.'/'.$img->basename;

        $img_file = public_path() .'/uploads/watermark/' . auth()->user()->user_id.'.png';
         
        $this->Image($img_file, 0, 0, 210, 270, '', '', '', false, 300, '', false, false, 0);


    

        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }

    function RotatedText($x, $y, $txt, $angle)
    {
        //Text rotated around its origin
        $this->Rotate($angle,$x,$y);
        $this->Text($x,$y,$txt);
        $this->Rotate(0);
    }



}
