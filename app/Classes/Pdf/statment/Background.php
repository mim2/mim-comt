<?php
namespace App\Classes\Pdf\Statment;

use \TCPDF;
use Intervention\Image\Facades\Image;
use Johntaa\Arabic\I18N_Arabic;

class Background extends TCPDF {
    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);

        $img_file = public_path() .'/assets/images/statment.jpg';
         
        $this->Image($img_file, 0, 0, 210, 270, '', '', '', false, 300, '', false, false, 0);



        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }

    function RotatedText($x, $y, $txt, $angle)
    {
        //Text rotated around its origin
        $this->Rotate($angle,$x,$y);
        $this->Text($x,$y,$txt);
        $this->Rotate(0);
    }



}
