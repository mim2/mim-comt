<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailLogger extends Model
{
    protected $fillable = [
        'date', 'from', 'to', 'cc', 'bcc', 'subject', 'body', 'headers', 'attachments',
        'sender_user_id', 'reciever_user_id', 'read'];
}
