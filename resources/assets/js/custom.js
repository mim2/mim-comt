$(document).ready(function() {

  /* Delete object Modal */
  $('.delete-object-modal-table').on('click', '.delete-object-modal-button', function(e) {
    e.preventDefault();

    var r = confirm(lodashLib.get(window.i18n, 'messages.are_you_sure'));

    if (r == true) {
      var form = $(this).parents('form:first');
      form.submit();
    }
  });

  $('.yes-no-submit-button-form').on('click', function(e) {
    e.preventDefault();

    var r = confirm(lodashLib.get(window.i18n, 'messages.are_you_sure'));

    if (r == true) {
      var form = $(this).parents('form:first');
      form.submit();
    }
  });

  /* Multi input */
  $(".add-more").click(function() {
    var html = $(".copy").html();
    $(".after-add-more").after(html);
  });

  $("body").on("click", ".remove", function() {
    $(this).parents(".control-group").remove();
  });


  /* Hijri Date Picker */
  var hijriCalendar = $.calendars.instance('ummalqura', 'ar');
  $('.hijri-datepicker-input').calendarsPicker({
    calendar: hijriCalendar,
    dateFormat: 'yyyy-mm-dd'
  });

  /* Miladi Date Picker */
  var calendar = $.calendars.instance();
  $('.en-datepicker-input').calendarsPicker({
    calendar: calendar,
    dateFormat: 'yyyy-mm-dd'
  });

$(".select2-user-select").select2({
  minimumInputLength: 3,
  ajax: {
    url: "/core/users-search",
    data: function (params) {
      var query = {
        query: params.term
      }
      // Query parameters will be ?search=[term]&type=public
      return query;
    },
    processResults: function (data) {
      var users = [];
      users.push({ id: "null" , text: lodashLib.get(window.i18n, 'messages.not_exist') });
      for (let i = 0; i < data.length; i++) {
        users.push({ id: data[i].user_id , text: data[i].user_name });
      }
      //console.log(users);
      // Tranforms the top-level key of the response object from 'items' to 'results'
      return {
        results: users
      };
    }
  }
});

$("#np-extensions-export-to-excel").click(function() {
  $('<input />').attr('type', 'hidden')
        .attr('name', "export")
        .attr('value', "1")
        .attr('id', "export-to-excel-input-flag")
        .appendTo('#np-manage-extensions-search');
});

$("#np-extensions-search-button").click(function() {
  $('#export-to-excel-input-flag').remove();
});

// Get and set vacation days

var hijriCalendar = $.calendars.instance('ummalqura', 'ar');
$('#vacation-start-hijri-date').calendarsPicker({
  calendar: hijriCalendar,
  dateFormat: 'yyyy-mm-dd',
  onSelect: function(dates) {
    setVacationDates();
  }
});

$('#attendance-vacation-number-of-days').on('change', function() {
  setVacationDates();
})

$('#select-vacation-code').on('change', function() {

  var url = $('#select-vacation-code').attr('get-balance-url') +  "?vacation_code=" + $(this).val();

  axios.get(url).then(response => {
    $('#attendance-vacation-balance').val(response.data.balance);
    $('.loading-icon').css('display', 'none');
  }).catch(errors => {
    $('.loading-icon').css('display', 'none');
  });
})


function setVacationDates() {
  var url = $('#vacation-start-hijri-date').attr('get-vacation-dates-url');
  var hijriStartDate = $('#vacation-start-hijri-date').val();
  var numberOfDays = $("#attendance-vacation-number-of-days").val();

  url += "?start_hijri_date=" + hijriStartDate +
         "&number_of_days=" + numberOfDays;
         
  if (hijriStartDate != null && numberOfDays != null) {
    $('.loading-icon').css('display', 'inline');
    axios.get(url).then(response => {
      var days = response.data;
      $("#vacation-end-hijri-date").val(days.end_hijri_date);
      $("#vacation-start-miladi-date").val(days.start_miladi_date);
      $("#vacation-end-miladi-date").val(days.end_miladi_date);
      $('.loading-icon').css('display', 'none');
    }).catch(errors => {
      $('.loading-icon').css('display', 'none');
    });
  }
}

});

$(document).ready(function() {
  $("#checkAll").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
  });
});


$(document).ready(function() {
  $( ".session-attendance-status" ).change(function() {
    var itemId = '#session-attendance-apology-reason-' + $(this).attr('member-id');

    if ($(this).val() == 3) {
      $(itemId).show();
    } else {
      $(itemId).hide();
    }
  });

  $(".session-attendance-status").each(function(i) {
    var itemId = '#session-attendance-apology-reason-' + $(this).attr('member-id');
    
    if ($(this).val() != 3) {
      $(itemId).hide();
    }
  });

});

$(document).ready(function() {

  $('.timepicker').timepicker({
    showSeconds: true
  });
  $('#timepicker1').timepicker();

  // $('.timepicker').datetimepicker({
  //   pickDate: false
  // });
});


