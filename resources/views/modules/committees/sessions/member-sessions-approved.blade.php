@extends('layouts.main.index')

@section('page')

            {{-- @include('committees::partial.category') --}}



    <div class="container mt-5 minhe">
        <div class="row mt-2">
            <div class="col SissionDetails">
                <div class="row">
                    <div class="col Sissiontitle p-3 d-flex flex-column justify-content-between">
                        <div class="maintitle d-flex justify-content-between">
                            <span class="sissionPage p-2 acolor">
                                <img src="/assets/images/medal.svg"> آخر المحاضر المعتمدة
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col maindetails p-2 mt-3">
                        <div class="col ownertabelecon">
                            <table class="table table-bordered tb-b">
                                <thead class="ownertabele">
                                <tr>
                                    <th>اللجنة أو المجلس</th>
                                    <th>الإجتماع</th>
                                    <th>تاريخ الانعقاد</th>
                                    <th>حالة الإجتماع</th>
                                    <th></th>
                                </tr>
                                </thead>

                                @unless($sessions->count())

                                    <tr>
                                        <td colspan="5" class="ttd">لا توجد محاضر بانتظار الاعتماد.</td>
                                    </tr>

                                @endunless

                                @foreach($sessions as $session)

                                    <tr>
                                        <th class="ttd">{{ $session->committee->name }}</th>
                                        <th class="ttd">{{ $session->sessionTitle->name }}</th>
                                        <th class="ttd">{{ $session->date }}</th>
                                        <th class="ttd inused">{{ $session->status->name }}</th>
                                        <th class="ttd">
                                            <a href="{{ route('member.sessions.show', [$session->committee, $session]) }}">محضر الإجتماع</a> -
                                            <a href="{{ route('member.sessions.print', [$session->committee, $session]) }}">طباعة المحضر</a>
                                        </th>
                                    </tr>

                                @endforeach

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
