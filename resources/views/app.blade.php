<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>اللجان- @yield('page')</title>
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link href="/assets/css/icheck-bootstrap.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <link href="images/favicon.ico" rel="shortcut icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="text-right">
<div class="br-mainpanel" id="vue-app">

    @include('layouts.main.header')

    @if ($errors->count() > 0)
                                            <div id="ERROR_COPY" style="display: none;" class="alert alert-warning">
                                                @foreach ($errors->all() as $error)
                                                {{ $error }}<br/>
                                                @endforeach
                                            </div>
                                        @endif
    @if ($errors->count() > 0)
                                            <div id="ERROR_COPY" style="display: none;" class="alert alert-warning">
                                                @foreach ($errors->all() as $error)
                                                {{ $error }}<br/>
                                                @endforeach
                                            </div>
                                        @endif

    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif

    @yield('content')

    <br/>
</div>

@include('layouts.main.js')
@yield('scripts')
@yield('footer')
</body>
</html>
