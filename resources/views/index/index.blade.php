@extends('layouts.main.index')

@section('stylesheets')
    <link href="/assets/css/fullcalendar.min.css" rel="stylesheet"/>
    <style>
        #menu {
            float: right;
            text-align: right;
            margin: 0;
            padding: 0;
            width: auto;
        }

        #menu ul {
            margin: 0;
            padding: 0;
            width: 100%;
        }

        #menu li {
            list-style-type: none;
            padding: 2px 4px;
        }

        #menu .menu-header {
            cursor: pointer;
            background: #0c6eb9;
            color: #fff;
        }

        #menu .menu-header ul {
            display: none;
            background: #0c6eb9;
            position: absolute;
            z-index: 999999
        }

        #menu .menu-header li {
            cursor: pointer;
            text-align: right;
            font-weight: normal;
        }

        #menu .menu-header li:hover {
            background-color: #0c6eb9;
            color: #62555c;
        }

        #menu .menu-header:hover ul {
            display: block;
            z-index: 500;
            /* position: fixed; */
            max-width: 380px;
        }

        .nav-link {
            color: #6d9529;
        }

        #menu .menu-header li {
            background: #3196e3;
            border-right: 5px solid #0c6eb9;
            margin: 5px 0;
        }

        #menu .menu-header li a {
            color: #fff;
        }

        img {
            width: 30px;
        }

        .ico-container .ico-h-2 {
            border: 2px #e2c889 solid;
        }

        .ico-container .ico-h-2:hover, .ico-container .ico-h-3:hover, .ico-container .ico-h-4:hover, .ico-container .ico-h-5:hover, .ico-container .ico-h-6:hover, .ico-container .ico-h-7:hover, .ico-container .ico-h-8:hover, .ico-container .ico-h-9:hover {
            background: #c39e45;
        }

        .mainMenu li:hover {
            background: #173d84;
        }

        .mainMenu li {
            border-left: 1px solid #183d83;
            transition: all 0.5s ease;
        }
    </style>
@endsection


@section('page')

    @include('committees::partial.category')



    {{-- @if(request()->user()->hasPermission('Modules\Committees\Http\Controllers\CommitteesController@index'))

        <div class="container-lg mt-2">
            <div class="row">

                <div class="btn text-center p-0 mt-4">

                    <ul id="menu">
                        <li class="menu-header">
                            <i class="fa fa-bars ml-2"></i>الاطلاع على كافة اللجان
                            <ul class=" p-1 pl-2">

                                @unless($committees->count())

                                    <h5>لا توجد لجان حالياً</h5>

                                @else
                                    @foreach($committees as $committee)

                                        <li>
                                            <a href="{{ route('committees.show', $committee) }}">
                                                <img alt="meeting" class="p-1 ml-1" src="/assets/images/pen.svg">
                                                {{ @$committee->name }}
                                            </a>
                                        </li>

                                    @endforeach

                                    <li class="morecomm text-center">
                                        <a href="{{ route('committees.index') }}">
                                            <span> المزيذ من اللجان</span>
                                            <img alt="meeting" class="p-1 mr-1" src="/assets/images/add.svg">
                                        </a>
                                    </li>


                            </ul>
                            @endunless

                        <li>
                    </ul>
                </div>
            </div>
        </div>

    @endif --}}

    <div class="container mt-5">
        <div class="row text-right">
            <!-- start commit -->
            <div class="col committee">
                <div class="row mb-5">
                    <div class="col-sm-3 title text-center p-1">
                        <div class="title_001">

                            <img alt="meeting" class="" src="/assets/images/meeting-white.svg">
                            <h6>اللجان والمجالس</h6>
                            <div class="btn_001">
                                <a class=" p-1 mt-3" href={{ route('member.committees.index') }}>المزيد</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9 details pt-2">
                        <a class="carousel-control-prev mt-1 ml-1" data-slide="prev" href="#carouselExampleControls"
                           role="button">
                            <i class="fa fa-chevron-left rounded-circle"></i>
                        </a>
                        <a class="carousel-control-next mt-1 ml-1" data-slide="next" href="#carouselExampleControls"
                           role="button">
                            <i class="fa fa-chevron-right rounded-circle"></i>
                        </a>
                        <div class="carousel slide mt-4" data-ride="carousel" id="carouselExampleControls">
                            <div class="carousel-inner">

                                @unless($committees->count())

                                    <h3 class="mt-4">لا توجد لجان حالياً</h3>

                                @else
                                    @foreach($committees as $committee)

                                        <div class="carousel-item {{$loop->iteration == 1 ? 'active' : ''}}">
                                            {{ @$committee->name }}
                                            <ul>
                                                {{-- @if ($committee->managingMemberIds()->contains(auth()->user()->user_id))
                                                <li class="p-1 mt-1 mb-1">
                                                    <i class="fa fa-gear"></i><a class=" p-1 mt-3"
                                                                                 href="{{ route('sessions.index', $committee) }}">إدارة
                                                                                 الإجتماعات</a>
                                                </li>
                                                @endif --}}
                                                <li class="p-1 mt-1 mb-1">
                                                    <i class="fa fa-info"></i>
                                                    <a class=" p-1 mt-3"
                                                       href="{{ route('member.committees.show', [$committee]) }}">
                                                        <span>{{ $committee->committee_or_council == 1 ? 'معلومات اللجنة' : 'معلومات المجلس' }}</span>
                                                    </a>
                                                </li>
                                                {{-- @if ($committee->managingMemberIds()->contains(auth()->user()->user_id)) --}}
                                                <li class="p-1 mt-1 mb-1">
                                                    <i class="fa fa-sitemap"></i>
                                                    <a href="{{ route('sessions.index', $committee) }}">الإجتماعات
                                                        ({{ $committee->sessions->count() }})</a>
                                                </li>
                                                {{-- @endif --}}
                                            </ul>
                                        </div>

                                    @endforeach
                                @endunless

                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end commit -->
            <div class="col-md-1"></div><!-- start Signature -->
            <div class="col committee">
                <div class="row">
                    <div class="col-sm-3 title text-center p-1">
                        <div class="title_002">

                            <img alt="meeting" class="" src="/assets/images/pen.svg">
                            <h6>محاضر تنتظر توقيعك</h6>
                            <div class="btn_002">
                                <a class=" p-1 mt-3" href="{{ route('member.sessions.pending') }}">المزيد </i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9 details">
                        <div class="overflow-auto">
                            <ul>
                                @if(@!$committee->sessions))
                                <h6 class="mt-2">لا توجد محاضر تنتظر التوقيع</h6>
                                @else
                                    @foreach($sessions as $session)
                                        @if ($session->status_id == 3)
                                            <li class="p-1 mt-1 mb-1">
                                                <img alt="meeting" class="p-1 ml-1" src="/assets/images/pen.svg">
                                                <a class=" p-1 mt-3"
                                                   href="{{ route('member.sessions.show', [$session->committee, $session]) }}">
                                                    <span>{{ @$session->committee->name }}</span> -
                                                    <span>الإجتماع {{ @$session->sessionTitle->name }}</span>
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- end Signature -->
        </div>
    </div><!-- starticons -->



    <section class="mt-2" id="icons-sect">


        <div class="container text-center ico-container">
            @if (auth()->user()->isMemberOfGroup('supervisor') || Auth::user()->is_super_admin == 1)
                <div class="col-md-1 col-sm-12 title"
                     style="min-width:180px; padding:10px; color:#fff; top:30px; background: rgb(195 158 71); font-size:16px; margin-bottom: 30px;box-shadow: 10px 0px 11px -15px rgba(120,120,120,0.8);">
                    <img alt="" class="p-1 ml-1 mb-0" src="/assets/images/clipboard.svg"> الوصول السريع
                </div>

                <div class="row mt-2 mb-5 pt-2 pb-3 commit-tasks">
                    @if(auth()->user()->isMemberOfGroup('supervisor') || Auth::user()->is_super_admin == 1)
                        <div class="col ico text-center">
                            <a class="ico-h-2 p-3 mt-3 mb-1 rounded-circle mx-auto"
                               href="{{ route('approval.sessions.index') }}">
                                <img alt="" class="p-1 ml-1 mb-0" src="/assets/images/icons/sent_members.svg">
                            </a>
                            المحاضر
                        </div>

                    @endif

                    @if(auth()->user()->isMemberOfGroup('reports') || Auth::user()->is_super_admin == 1)
                        <div class="col ico text-center">
                            <a class="ico-h-2 p-3 mt-3 mb-1 rounded-circle mx-auto" href="{{ route('reports.index') }}">
                                <img alt="" class="p-1 ml-1 mb-0" src="/assets/images/icons/file.svg">
                            </a>
                            التقارير
                        </div>
                    @endif

                    {{--
                      <div class="col ico text-center">
                        <a class="ico-h-2 p-3 mt-3 mb-1 rounded-circle mx-auto" href="{{ route('guest.sessions.index') }}">
                            <img alt="" class="p-1 ml-1 mb-0" src="/assets/images/icons/meetin2.svg">
                        </a>
                        إجتماعات انت مدعو لها
                      </div> --}}

                </div>
            @endif
            @include('index.calender')
        </div>

    </section>





@endsection
@section('scripts')
    @include('index.scripts')
@endsection