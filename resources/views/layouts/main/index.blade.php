<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf8_unicode_ci"/>
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="ie=edge" http-equiv="X-UA-Compatible"/>
    <title>{{ @Modules\Core\Entities\Setting::first()->app_name }}</title>
    <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
    <link href="/assets/css/icheck-bootstrap.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.css" rel="stylesheet"/>
    <link href="/assets/css/bootstrapp.css" rel="stylesheet"/>
    <link href="/assets/css/icheck-bootstrapp.css" rel="stylesheet">
    <link href="/assets/css/font-awesomep.css" rel="stylesheet"/>
    <link href="/assets/css/mainp.css?v={{ rand(1000, 10000) }}" rel="stylesheet"/>
    <link href="/assets/fullCalendar/main.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" rel="stylesheet"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />


    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
    <link rel="stylesheet" type="text/css" href="{{asset('select2/dist/css/select2.min.css')}}">

    <!-- Script -->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    @yield('stylesheets')

</head>
<body>
    
    {{--<div class="notification text-right p-2">--}}
        {{--<svg viewbox="0 0 166 197">--}}
            {{--<path class="notification--bellClapper" d="M82.8652955,196.898522 C97.8853137,196.898522 110.154225,184.733014 110.154225,169.792619 L55.4909279,169.792619 C55.4909279,184.733014 67.8452774,196.898522 82.8652955,196.898522 L82.8652955,196.898522 Z"></path>--}}
            {{--<path class="notification--bell" d="M146.189736,135.093562 L146.189736,82.040478 C146.189736,52.1121695 125.723173,27.9861651 97.4598237,21.2550099 L97.4598237,14.4635396 C97.4598237,6.74321823 90.6498186,0 82.8530327,0 C75.0440643,0 68.2462416,6.74321823 68.2462416,14.4635396 L68.2462416,21.2550099 C39.9707102,27.9861651 19.5163297,52.1121695 19.5163297,82.040478 L19.5163297,135.093562 L0,154.418491 L0,164.080956 L165.706065,164.080956 L165.706065,154.418491 L146.189736,135.093562 Z"></path></svg> <!-- <span class="notification==num">5</span> -->--}}
        {{--<p>لديك جلسه جديدة</p>--}}
        {{--<div class="close">--}}
            {{--<i class="fa fa-times-circle"></i>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div id="page-container">
        <div class="br-mainpanel" id="vue-app" >
        

            

                @include('layouts.main.header')
                
                
                @if ($errors->count() > 0)
                    <div id="ERROR_COPY" style="display: none;" class="alert alert-warning">
                        @foreach ($errors->all() as $error)
                        {{ $error }}<br/>
                        @endforeach
                    </div>
                @endif
                @if ($errors->count() > 0)
                    <div id="ERROR_COPY" style="display: none;" class="alert alert-warning">
                        @foreach ($errors->all() as $error)
                        {{ $error }}<br/>
                        @endforeach
                    </div>
                @endif

                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif

                @yield('page')
                

                 <div class="footer lfooter   p-1 container-fluid">
                    {{-- <div class="row">
                        <div class="col-sm-11">
                            <h6 class="text-center mt-3 lfooter">جميع الحقوق محفوظة © 2020</h6>
                            <div class="foote_span text-center">
                                للدعم الفني  بريد الكتروني: <a href="mailto:support@mim.gov.sa" title="راسلنا">support@mim.gov.sa</a>
                            </div>
                        </div>
                        <div class="col-sm-1 footer-border"></div>
                    </div>
                </div> 
                 <div class="site-feedback d-md-block d-none" data-target="#site-feedback" data-toggle="modal"
                    style="display: block;" title="شاركنا برأيك">
                    <i aria-hidden="true" class="fa fa-commenting"></i>
                </div> 
                 <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade border-0" id="site-feedback"
                    role="dialog" tabindex="-1">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content rounded-0 border-0">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">ضع اقتراحك</h5>
                                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span
                                            aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body text-right">
                                <form>
                                    <div class="form-group">
                                        <label class="col-form-label" for="recipient-name">الاسم:</label> <input
                                                class="form-control rounded-0" id="recipient-name" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label rounded-0" for="message-text">الرسالة:</label>
                                        <textarea class="form-control rounded-0" id="message-text"></textarea>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">

                                <button class="btn btn-block rounded-0" type="button">إرسال</button>
                            </div>
                        </div>
                    </div>
            </div> --}}
            </div>
        </div>
        {{-- <div class="seetings">
            <div id="scroll-top">
                <i class="fa fa-angle-up fa-2x"></i> للأعلي
            </div><a href="/"><button class="  border-0"><i class="fa fa-reply"></i>  رجوع الرئيسية</button></a>
        </div> --}}

        <br>
        
    </div>

@include('layouts.main.js')
@yield('scripts')
<script>
        window.onpageshow = function(evt) {
        if (evt.persisted) {
            document.body.style.display = "none";
            location.reload();
        }
    };
</script>

</body>
</html>
