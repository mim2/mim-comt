<script src="/vendor-assets/lib/jquery/jquery-3.5.1.min.js"></script>
<script src="/vendor-assets/lib/jquery-ui/jquery-ui.js"></script>

<script src="/vendor-assets/lib/bootstrap/js/bootstrap.4.5.min.js"></script>
<script src="/vendor-assets/lib/moment/moment.js"></script>
<script src="/vendor-assets/lib/select2/js/select2.full.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js' defer></script>

<script src="{{asset('select2/dist/js/select2.min.js')}}" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="/js/html5shiv.js"></script>
<script src="/js/respond.min.js"></script>
<![endif]-->

<script src="/vendor-assets/js/bootstrap-timepicker.min.js"></script>
<script src="/vendor-assets/js/main.js"></script>
<script src="/assets/js/bootstrap-datetimepicker.js"></script>
<script src="/assets/fullcalendar/main.js" defer></script>
<script src="/assets/fullcalendar/main.min.js" defer></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
   
<script>

    var has_errors = {{  $errors->count() > 0 ? 'true' : 'false' }};

    if ( has_errors) {
        Swal.fire({
            title: 'ERRORS',
            type: 'error',
            html: jQuery("#ERROR_COPY").html(),
 
        })
    }

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

@include('sweet::alert')

{{ Html::script('assets/js/app.js') }}
{{ Html::script('js/lang.js') }}

{{-- <script src="/vendor-assets/js/js/app-metronic.js"></script> --}}
<script src="/vendor-assets/js/js/datatables.all.min.js" defer></script>
<script src="/vendor-assets/js/js/datatables.bootstrap.js" defer></script>
<script src="/vendor-assets/js/js/components-multi-select.min.js" defer></script>
<script src="/vendor-assets/js/js/components-datatable_ar.js" defer></script>


{{-- {{ Html::script(mix('assets/js/js/app-metronic.js'))}} --}}
{{-- {{ Html::script(mix('assets/js/js/datatables.all.min.js'))}} --}}
{{-- {{ Html::script(mix('assets/js/js/datatables.bootstrap.js'))}} --}}
{{-- {{ Html::script(mix('vendor-assets/js/js/components-datatable_ar.js'))}} --}}
{{-- {{ Html::script(mix('assets/js/js/components-multi-select.min.js'))}} --}}
