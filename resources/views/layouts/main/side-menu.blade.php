<div class="br-sideleft overflow-y-auto">
    <label class="sidebar-label">القائمة الرئيسية</label>
    <ul class="br-sideleft-menu">
        <li class="br-menu-item">
            <a class="br-menu-link with-sub" href="#">
                <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
                <span class="menu-item-label">اللجان</span>
            </a>
            <ul class="br-menu-sub">
                @foreach(\Modules\Committees\Entities\Committee::mainCommittees() as $id => $committee)
                <li class="sub-item">
                    <a class="sub-link" href="{{ route('committees.show', $id) }}">{{ $committee }}</a>
                    <ul>
                        <li><a href="{{ route('sessions.index', $id) }}">إدارة الإجتماعات</a></li>
                    </ul>
                </li>
                @endforeach
            </ul>
        </li>
        <li class="br-menu-item">
            <a class="br-menu-link with-sub" href="#">
                <i class="menu-item-icon icon ion-ios-filing-outline tx-24"></i>
                <span class="menu-item-label">اللجان الفرعية</span>
            </a>
            <ul class="br-menu-sub">
                <li class="sub-item">
                    <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                        <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                    <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                    <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                        <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                        <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                        <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
           <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>

            </ul>
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a class="br-menu-link with-sub" href="#"><i class="menu-item-icon ion-ios-redo-outline tx-24"></i> <span class="menu-item-label">المجالس</span></a> <!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
            </ul>
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a class="br-menu-link with-sub" href="#"><i class="menu-item-icon ion-ios-pie-outline tx-20"></i> <span class="menu-item-label">اقسام ولجان المجالس</span></a> <!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                                <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                    <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>

            </ul>
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a class="br-menu-link with-sub" href="#"><i class="menu-item-icon icon ion-ios-gear-outline tx-24"></i> <span class="menu-item-label">إجتماع انت مدعو له</span></a> <!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                        <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>

            </ul>
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a class="br-menu-link with-sub" href="#"><i class="menu-item-icon icon ion-ios-bookmarks-outline tx-20"></i> <span class="menu-item-label">اخر المحاضر المعتمدة</span></a> <!-- br-menu-link -->
            <ul class="br-menu-sub nav flex-column">
                <li class="sub-item">
                        <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
            </ul>
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a class="br-menu-link with-sub" href="#"><i class="menu-item-icon icon ion-ios-navigate-outline tx-24"></i> <span class="menu-item-label">الاشعارات</span></a> <!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item">
                                <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
            </ul>
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a class="br-menu-link with-sub" href="#"><i class="menu-item-icon icon ion-ios-color-filter-outline tx-24"></i> <span class="menu-item-label">محاضر تنتظر توقيعك</span></a> <!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>>
                </li>
                <li class="sub-item">
                        <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
            </ul>
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a class="br-menu-link with-sub" href="#"><i class="menu-item-icon icon ion-ios-briefcase-outline tx-22"></i> <span class="menu-item-label">بانتظار الارسال للاعتماد</span></a> <!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>
                <li class="sub-item">
                            <a class="sub-link" href="card-dashboard.html">لجنه فرعية</a>
                </li>

            </ul>
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a class="br-menu-link with-sub" href="#"><i class="menu-item-icon icon ion-ios-briefcase-outline tx-22"></i>
                <span class="menu-item-label">التحكم</span>
            </a> <!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item">
                    <a class="sub-link" href="{{ ('/core/departments') }}">الإدارات</a>
                </li>
                <li class="sub-item">
                    <a class="sub-link" href="{{ ('/core/jobs') }}">الوظائف</a>
                </li>
                <li class="sub-item">
                    <a class="sub-link" href="{{ ('/core/users') }}">المستخدمين</a>
                </li>
                <li class="sub-item">
                    <a class="sub-link" href="{{ ('/committees/committees') }}">اللجان والمجالس</a>
                </li>
            </ul>
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a class="br-menu-link" href="pages.html"><i class="menu-item-icon icon ion-ios-paper-outline tx-22"></i> <span class="menu-item-label">محاضر تنتظر الارسال<br>
            لتوقيع الاعضاء</span></a> <!-- br-menu-link -->
        </li><!-- br-menu-item -->
    </ul><!-- br-sideleft-menu -->
    <br>
</div><!-- br-sideleft -->
