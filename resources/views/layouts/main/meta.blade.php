<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"><!-- Twitter -->
<meta content="@themepixels" name="twitter:site">
<meta content="@themepixels" name="twitter:creator">
<meta content="summary_large_image" name="twitter:card">
<meta content="Bracket Plus" name="twitter:title">
<meta content="Premium Quality and Responsive UI for Dashboard." name="twitter:description">
<meta content="http://themepixels.me/bracketplus/img/bracketplus-social.png" name="twitter:image"><!-- Facebook -->
<meta content="http://themepixels.me/bracketplus" property="og:url">
<meta content="Bracket Plus" property="og:title">
<meta content="Premium Quality and Responsive UI for Dashboard." property="og:description">
<meta content="http://themepixels.me/bracketplus/img/bracketplus-social.png" property="og:image">
<meta content="http://themepixels.me/bracketplus/img/bracketplus-social.png" property="og:image:secure_url">
<meta content="image/png" property="og:image:type">
<meta content="1200" property="og:image:width">
<meta content="600" property="og:image:height"><!-- Meta -->
<meta content="Premium Quality and Responsive UI for Dashboard." name="description">
<meta content="ThemePixels" name="author">