<link href="/vendor-assets/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/vendor-assets/lib/Ionicons/css/ionicons.css" rel="stylesheet">
<link href="/vendor-assets/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
<link href="/vendor-assets/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
<link href="/vendor-assets/lib/rickshaw/rickshaw.min.css" rel="stylesheet">
<link href="/vendor-assets/lib/select2/css/select2.min.css" rel="stylesheet">
<link href="/vendor-assets/lib/select2/css/select2_ar.min.css" rel="stylesheet">
<link href="/vendor-assets/lib/select2/css/select2_en.min.css" rel="stylesheet">
<link href="/vendor-assets/lib/select2/css/select2-bootstrap_ar.min.css" rel="stylesheet">
<link href="/vendor-assets/lib/select2/css/select2-bootstrap_en.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Cairo:400,600,700,900&amp;subset=arabic,latin-ext" rel="stylesheet"><!-- Bracket CSS -->
<link href="/vendor-assets/css/bracket.css" rel="stylesheet">
<link href="/vendor-assets/css/bracket.dark.css" rel="stylesheet">
<link href="/vendor-assets/css/custome.css" rel="stylesheet">
<link href="/vendor-assets/css/custom-app.css" rel="stylesheet">
<link href="/vendor-assets/css/datatables.min.css" rel="stylesheet">
<link href="/vendor-assets/css/datatables.bootstrap.css" rel="stylesheet">
<link href="/vendor-assets/css/datatables.bootstrap-rtl.css" rel="stylesheet">
 