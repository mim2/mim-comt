<?php

return [
    'highSecret' => 'High Secret',
    'internalSecret' => 'Internal Secret',
    'externalSecret' => 'External Secret',
    'normal' => 'Normal',
];