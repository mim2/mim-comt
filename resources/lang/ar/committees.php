<?php

return [
    'highSecret' => 'سري للغاية',
    'internalSecret' => 'سري داخلي',
    'externalSecret' => 'سري خارجي',
    'normal' => 'عادي',
];