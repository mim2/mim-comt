<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages.
    |
    */

    'accepted'             => 'يجب قبول :attribute',
    'active_url'           => ':attribute لا يُمثّل رابطًا صحيحًا',
    'after'                => 'يجب على :attribute أن يكون تاريخًا لاحقًا للتاريخ :date.',
    'after_or_equal'       => ':attribute يجب أن يكون تاريخاً لاحقاً أو مطابقاً للتاريخ :date.',
    'alpha'                => 'يجب أن لا يحتوي :attribute سوى على حروف',
    'alpha_dash'           => 'يجب أن لا يحتوي :attribute على حروف، أرقام ومطّات.',
    'alpha_num'            => 'يجب أن يحتوي :attribute على حروفٍ وأرقامٍ فقط',
    'array'                => 'يجب أن يكون :attribute ًمصفوفة',
    'before'               => 'يجب على :attribute أن يكون تاريخًا سابقًا للتاريخ :date.',
    'before_or_equal'      => ':attribute يجب أن يكون تاريخا سابقا أو مطابقا للتاريخ :date',
    'between'              => [
        'numeric' => 'يجب أن تكون قيمة :attribute بين :min و :max.',
        'file'    => 'يجب أن يكون حجم الملف :attribute بين :min و :max كيلوبايت.',
        'string'  => 'يجب أن يكون عدد حروف النّص :attribute بين :min و :max',
        'array'   => 'يجب أن يحتوي :attribute على عدد من العناصر بين :min و :max',
    ],
    'boolean'              => 'يجب أن تكون قيمة :attribute إما true أو false ',
    'confirmed'            => 'حقل التأكيد غير مُطابق للحقل :attribute',
    'date'                 => ':attribute ليس تاريخًا صحيحًا',
    'date_format'          => 'لا يتوافق :attribute مع الشكل :format.',
    'different'            => 'يجب أن يكون الحقلان :attribute و :other مُختلفان',
    'digits'               => 'يجب أن يحتوي :attribute على :digits رقمًا/أرقام',
    'digits_between'       => 'يجب أن يحتوي :attribute بين :min و :max رقمًا/أرقام ',
    'dimensions'           => 'الـ :attribute يحتوي على أبعاد صورة غير صالحة.',
    'distinct'             => 'للحقل :attribute قيمة مُكرّرة.',
    'email'                => 'يجب أن يكون :attribute عنوان بريد إلكتروني صحيح البُنية',
    'exists'               => ':attribute لاغٍ',
    'file'                 => 'الـ :attribute يجب أن يكون ملفا.',
    'filled'               => ':attribute إجباري',
    'image'                => 'يجب أن يكون :attribute صورةً',
    'in'                   => ':attribute لاغٍ',
    'in_array'             => ':attribute غير موجود في :other.',
    'integer'              => 'يجب أن يكون :attribute عددًا صحيحًا',
    'ip'                   => 'يجب أن يكون :attribute عنوان IP صحيحًا',
    'ipv4'                 => 'يجب أن يكون :attribute عنوان IPv4 صحيحًا.',
    'ipv6'                 => 'يجب أن يكون :attribute عنوان IPv6 صحيحًا.',
    'json'                 => 'يجب أن يكون :attribute نصآ من نوع JSON.',
    'max'                  => [
        'numeric' => 'يجب أن تكون قيمة :attribute مساوية أو أصغر لـ :max.',
        'file'    => 'يجب أن لا يتجاوز حجم الملف :attribute :max كيلوبايت',
        'string'  => 'يجب أن لا يتجاوز طول النّص :attribute :max حروفٍ/حرفًا',
        'array'   => 'يجب أن لا يحتوي :attribute على أكثر من :max عناصر/عنصر.',
    ],
    'mimes'                => 'يجب أن يكون ملفًا من نوع : :values.',
    'mimetypes'            => 'يجب أن يكون ملفًا من نوع : :values.',
    'min'                  => [
        'numeric' => 'يجب أن تكون قيمة :attribute مساوية أو أكبر لـ :min.',
        'file'    => 'يجب أن يكون حجم الملف :attribute على الأقل :min كيلوبايت',
        'string'  => 'يجب أن يكون طول النص :attribute على الأقل :min حروفٍ/حرفًا',
        'array'   => 'يجب أن يحتوي :attribute على الأقل على :min عُنصرًا/عناصر',
    ],
    'not_in'               => ':attribute لاغٍ',
    'numeric'              => 'يجب على :attribute أن يكون رقمًا',
    'present'              => 'يجب تقديم :attribute',
    'regex'                => 'صيغة :attribute .غير صحيحة',
    'required'             => ':attribute مطلوب.',
    'required_if'          => ':attribute مطلوب في حال ما إذا كان :other يساوي :value.',
    'required_unless'      => ':attribute مطلوب في حال ما لم يكن :other يساوي :values.',
    'required_with'        => ':attribute مطلوب إذا توفّر :values.',
    'required_with_all'    => ':attribute مطلوب إذا توفّر :values.',
    'required_without'     => ':attribute مطلوب إذا لم يتوفّر :values.',
    'required_without_all' => ':attribute مطلوب إذا لم يتوفّر :values.',
    'same'                 => 'يجب أن يتطابق :attribute مع :other',
    'size'                 => [
        'numeric' => 'يجب أن تكون قيمة :attribute مساوية لـ :size',
        'file'    => 'يجب أن يكون حجم الملف :attribute :size كيلوبايت',
        'string'  => 'يجب أن يحتوي النص :attribute على :size حروفٍ/حرفًا بالظبط',
        'array'   => 'يجب أن يحتوي :attribute على :size عنصرٍ/عناصر بالظبط',
    ],
    'string'               => 'يجب أن يكون :attribute نصآ.',
    'timezone'             => 'يجب أن يكون :attribute نطاقًا زمنيًا صحيحًا',
    'unique'               => 'قيمة :attribute مُستخدمة من قبل',
    'uploaded'             => 'فشل في تحميل الـ :attribute',
    'url'                  => 'صيغة الرابط :attribute غير صحيحة',
    'user_mobile'         => 'يجب أن يكون  :attribute بهذه الصيغة 05XXXXXXXX',
    'national_id'          => 'الرجاء إدخال رقم هوية صحيح ',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom'               => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'attendees.*.attendance_status_id' => [
            'required' => 'هذا الحقل مطلوب',
        ],
        'attendees.*.apology_reason' => [
            'required_if' => 'هذا الحقل مطلوب',
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes'           => [
        'name'                  => 'الاسم',
        'username'              => 'اسم المُستخدم',
        'email'                 => 'البريد الالكتروني',
        'first_name'            => 'الاسم الأول',
        'last_name'             => 'اسم العائلة',
        'password'              => 'كلمة السر',
        'password_confirmation' => 'تأكيد كلمة السر',
        'city'                  => 'المدينة',
        'country'               => 'الدولة',
        'address'               => 'عنوان السكن',
        'phone'                 => 'الهاتف',
        'mobile'                => 'الجوال',
        'age'                   => 'العمر',
        'sex'                   => 'الجنس',
        'gender'                => 'النوع',
        'day'                   => 'اليوم',
        'month'                 => 'الشهر',
        'year'                  => 'السنة',
        'hour'                  => 'ساعة',
        'minute'                => 'دقيقة',
        'second'                => 'ثانية',
        'title'                 => 'العنوان',
        'content'               => 'المُحتوى',
        'description'           => 'الوصف',
        'decision_image'        => 'صورة القرار',
        'committee_or_council'  => 'نوع اللجنة',
        'category'       => 'التصنيف',
        'excerpt'               => 'المُلخص',
        'date'                  => 'التاريخ',
        'time'                  => 'الوقت',
        'available'             => 'مُتاح',
        'size'                  => 'الحجم',
        'type'                  => 'النوع',
        'decision_name'         => 'رقم القرار ',
        'decision_url'         => 'صورة القرار ',
        'name' => 'الاسم',
        'name_en' => 'الاسم باللغة الانجليزية',
        'resource_name' => 'مسار تحكم الصلاحية',
        'icon' => 'الأيقونة',
        'sort' => 'الترتيب',
        'frontend_path' => 'المسار في الموقع الخارجي',
        'li_color' => ' لون النصنيف ',
        'displayed_in_menu' => 'هل يظهر بالقائمة ؟',
        'menu_type' => 'نوع القائمة',
        'access_level' => 'مستوي الصلاحية',
        'key' => 'المفتاح التعريفي',
        'user_id' => 'المستخدم',
        'rel_mob' => 'رقم الجوال ',
        'report_date' => 'تاريخ التقرير',
        'report_body' => 'نص التقرير',
        'report_degree' => 'درجة البلاغ',
        'report_resource' => 'مصادر بلاغ التوجيه',
        'action_from_transport_office' => 'الاجراء المتخذ من مكتب النقل',
        'status' => 'الحالة',
        'display' => 'عرض',
        'new' => 'جديد',
        'repeated' => 'متكرر',
        'guidance_done' => 'تم التوجيه',
        'add_report' => 'اضافة تقرير',
        'body' => 'نص التقرير',
        'guidance' => 'التوجيه',
        'person.rel_mob' => 'رقم الجوال ' ,
        'person.job_contract' =>'اسم عقد العمل ',
        'person.authority' =>'الجهة المشرفة على العقد ',
        'person.domicile' =>'مقر السكن ',
        'person.district' =>'الحي ',
        'person.street' =>'الشارع ',
        'person.relative' =>'شخص يمكن الإتصال به',
        'person.rel_realation' => 'صلة القرابة ',
        'car.plate' => 'لوحة المركبة ',
        'car.type' => 'نوع المركبة ',
        'car.model' => 'موديل المركبة',
        'car.color' => 'لون المركبة ',
        'car.provenance_other' => 'البلد',
        'file_car' => 'صورة من إستمارة المركبة ',
        'file_license' => 'صورة من رخصة القيادة ',
        'permit.getfrom'=>'مكان الإستلام ',
        'start' => 'البداية',
        'end' => 'النهاية',
        'phone_model_id' => 'موديل الهاتف',
        'permit.getfrom'=>'lمكان الإستلام ',

        'phenomenon_status'=>'حالة الظاهرة' ,
        'phenomenon_type'=>'نوع الظاهرة',
        'the_name_of_informer'=>'اسم المبلغ',
        'place'=>'مكان الإجتماع',
        'actualtime'=>'وقت الواقعه',
        'dayreality'=>'يوم الواقعه',
        'track'=>'الخط المتأثر',
        'attachment'=>'المرفقات',
        'coordinator'=>'اسم المنسقة مع الطالبات',
        'coordinator_mobile'=>'رقم جوال المنسقة',
        'service_organization'=>'جهة الخدمة المطلوبه',
        'destination'=>'اسم الوجهة المراد النقل اليها',
        'location'=>'موقع الوجهة',
        'transport_date'=>'تاريخ النقل',
        'transport_going'=>'وقت الذهاب',
        'transport_back'=>'وقت العودة',
        'students_count'=>'عدد الطالبات المراد توفير وسيلة النقل لهن',
        'coordinators_count'=>'عدد المرافقات من الكادر الاداري',


        // News Module
        'news_title'       => 'عنوان الخبر',
        'news_body'        => 'نص الخبر',
        'news_rej_comment' => 'سبب الرفض',


        // MyServices/SalaryDefinitionController
        'definition_language'  => 'اللغة',
        'organization_id'      => 'الجهة الموجة لها',
        'from'=> 'من',
        'to'=>' الي',
        'vacation_code' => 'نوع الأجازة',
        'start_hijri_date' => 'تاريخ البداية هجري',
        // MyServices/PermissionsController
        'permission_type_id'      => 'يوم الإستئذان',
        'permission_reason'       => 'السبب',
        'alternative_employee_id' => 'الموظف البديل',
        'balance' => 'الرصيد',
        'period' => 'الفترة',
        'file_name' => 'إسم الملف',
        'file_path' => 'الملف',
        'start_time' => 'وقت البداية',
        'end_time' => 'وقت النهاية',
        'recommendation' => 'المناقشة',
        'discussions' => 'التوصية / المهمة'
    ],
];
